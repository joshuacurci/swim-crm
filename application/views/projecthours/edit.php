<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Edit hours</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projecthours/save/">
      <input type="hidden" name="staffID" value="<? echo $staffID; ?>" >
      <input type="hidden" name="projectID" value="<? echo $hoursinfo[0]['projectID']; ?>" >
      <input type="hidden" name="hourID" value="<? echo $hoursinfo[0]['hourID']; ?>" >


        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Hour Title:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Hour Title" name="hour_title" value="<? echo $hoursinfo[0]['hour_title']; ?>">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Hour Amount:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Hour Amount in hour value (eg 1 for an hour, 0.5 for 30 mins, 0.25 for 15 mins)" name="hour_amount" value="<? echo $hoursinfo[0]['hour_amount']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert any comments here..." name="hour_content">
               <? echo $hoursinfo[0]['hour_content']; ?>
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Date Hours Completed:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="hour_datedone" value="<? echo date('d-m-Y',$hoursinfo[0]['hour_datedone']); ?>" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>