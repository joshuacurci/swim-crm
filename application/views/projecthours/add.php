<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Log new hours</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projecthours/newhour/">
      <input type="hidden" name="staffID" value="<? echo $staffID; ?>" >
      <input type="hidden" name="projectID" value="<? echo $projectID; ?>" >


        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Hour Title:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Hour Title" name="hour_title" <? if (isset($taskinfo)){ echo'value="'.$taskinfo[0]['task_name'].'"'; } ?> >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Hour Amount:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Hour Amount in hour value (eg 1 for an hour, 0.5 for 30 mins, 0.25 for 15 mins)" name="hour_amount" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert any comments here..." name="hour_content">
              <? if (isset($taskinfo)){ echo $taskinfo[0]['task_content']; } ?>
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Date Hours Completed:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="hour_datedone" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <input type="submit" class="btn btn-primary" name="action" value="Submit" />
            <input type="submit" class="btn btn-primary" name="action" value="Submit and Add another" />
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>