<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1 <? if($client[0]['client_archive'] == 'Y') { echo 'class="archived-client"'; } ?>>
    <? echo $client[0]['client_name']; ?>
  </h1>
  <? if($client[0]['client_archive'] == 'Y') { echo '<h3 class="archived-client"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Archived</h3>'; } ?>
<div style="clear:both"></div>
    <?
    if ($client[0]['client_resoled'] != '' || $client[0]['client_resoled'] != NULL) {
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$client[0]['client_resoled']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <h3>Resold by - <? echo $row['client_name']; ?></h3>
                <? }
            } else { ?>
                <h3>No Client</h3>
  <? } } ?>
  

  <a href="<? echo base_url(); ?>index.php/client/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Back to Clients</a>
  <a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
  <a href="<? echo base_url(); ?>index.php/client/edit/<? echo $client[0]['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
  <br/><br/>
  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Address: </b><br/><br/><br/><br/></td>
        <td>
          <? echo $client[0]['client_add_num']; ?><br/><? echo $client[0]['client_add_street']; ?><br/>
          <? echo $client[0]['client_add_city']; ?> <? echo $client[0]['client_add_state']; ?><br/>
          <? echo $client[0]['client_add_country']; ?> <? echo $client[0]['client_add_postcode']; ?><br/>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Phone: </b>  </td>
        <td>
          <? echo $client[0]['client_phone']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Fax: </b>  </td>
        <td>
          <? echo $client[0]['client_fax']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>ACN: </b>  </td>
        <td>
          <? echo $client[0]['client_acn']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Types of work completed: </b>  </td>
        <td>
          <? if ($client[0]['client_work']) { ?>
          <? $indvalues = explode(";",$client[0]['client_work']); ?>
          <? foreach ($indvalues as $clientworkvalue) { ?>
          <?
            if ($clientworkvalue != "") {

            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client_work WHERE workID = ".$clientworkvalue."";
            $result = $conn->query($sql);
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
              <? echo $row['work_value']; ?><br/>
              <? }
            } else { ?>
              
            <? }} ?>
          <? } ?>
          <? } ?>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Trading Terms: </b>  </td>
        <td>
          <? echo $client[0]['client_traiding_terms']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Client Type: </b>  </td>
        <td>
          <? echo $client[0]['client_type']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>ABN: </b>  </td>
        <td>
          <? echo $client[0]['client_abn']; ?>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-12">
    <h3>Contacts</h3>
    <a href="<? echo base_url(); ?>index.php/clientcontact/add/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Contact</a>
    <br/><br/>
    <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>Contact Name</th>
        <th>Phone</th>
        <th>Mobile</th>
        <th>Email</th>
        <th>Hot Drink</th>
        <th></th>
        <th></th>
      </tr>
    <? if (isset($client_contacts)) { ?>
      <? foreach ($client_contacts as $contactdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/clientcontact/view/<? echo $contactdata['contactID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $contactdata['contact_name']; ?> <? echo $contactdata['contact_lastname']; ?></td>
          <td><? echo $contactdata['contact_phone']; ?></td>
          <td><? echo $contactdata['contact_mobile']; ?></td>
          <td><a href="mailto:<? echo $contactdata['contact_email']; ?>"><? echo $contactdata['contact_email']; ?></a></td>
          <td><? echo $contactdata['contact_hotdrink']; ?></td>
          <td><a href="<? echo base_url(); ?>index.php/clientcontact/edit/<? echo $contactdata['contactID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/clientcontact/delete/<? echo $contactdata['contactID']; ?>/<? echo $contactdata['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
    <? } ?>
    </table>
  </div>

  <div class="col-sm-4">
    <h3>Notes</h3>
    <a href="<? echo base_url(); ?>index.php/clientnotes/add/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    <a href="<? echo base_url(); ?>index.php/clientnotes/seeall/<? echo $client[0]['clientID']; ?>" class="btn btn-default">See all</a>
    <br/><br/>
    <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>Title</th>
        <th>Date</th>
      </tr>
    <? if (isset($client_notes)) { ?>
      <? foreach ($client_notes as $notedata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/clientnotes/view/<? echo $notedata['noteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $notedata['note_title']; ?></td>
          <td><? echo date('d/m/Y', $notedata['note_date']); ?></td>
        </tr>
      <? } ?>
    <? } ?>
    </table>
  </div>

  <div class="col-sm-4">
    <h3>Domains</h3>
    <a href="<? echo base_url(); ?>index.php/domains/clientadd/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    <a href="<? echo base_url(); ?>index.php/domains/clientview/<? echo $client[0]['clientID']; ?>" class="btn btn-default">See all</a>
    <br/><br/>
    <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>Domain Name</th>
      </tr>
    <? if (isset($client_domain)) { ?>
      <? foreach ($client_domain as $domaindata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/domains/view/<? echo $domaindata['domainID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $domaindata['domain_name']; ?></td>
        </tr>
      <? } ?>
    <? } ?>
    </table>
  </div>

  <div class="col-sm-4">
    <h3>Projects</h3>
    <a href="<? echo base_url(); ?>index.php/projects/clientadd/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    <a href="<? echo base_url(); ?>index.php/projects/clientview/<? echo $client[0]['clientID']; ?>" class="btn btn-default">See all</a>
    <br/><br/>
    <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>Title</th>
        <th>Job Number</th>
        <th>Project Status</th>
      </tr>
    <? if (isset($client_projects)) { ?>
      <? foreach ($client_projects as $projectdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $projectdata['project_name']; ?></td>
          <td><? echo $projectdata['job_bag']; ?></td>
          <td>
            <? if($projectdata['project_status'] == 'Prospect' || $projectdata['project_status'] == 'Quote') { ?> <span class="label label-warning"> <? }
               else if($projectdata['project_status'] == 'Job in progress') { ?> <span class="label label-success"> <? } 
               else if($projectdata['project_status'] == 'Invoiced') { ?> <span class="label label-primary"> <? } 
               else if($projectdata['project_status'] == 'Completed' || $projectdata['project_status'] == 'Paid' || $projectdata['project_status'] == 'Declined' || $projectdata['project_status'] == 'Archived' || $projectdata['project_status'] == 'Canceled') { ?> <span class="label label-default"> <? } else{} ?>
            <? echo $projectdata['project_status']; ?>
            </span>
          </td>
        </tr>
      <? } ?>
    <? } ?>
    </table>
  </div>
<div style="clear:both"></div>
  <div class="col-sm-4">
    <h3>Proposals</h3>
    <a href="<? echo base_url(); ?>index.php/salesproposals/clientadd/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    <a href="<? echo base_url(); ?>index.php/salesproposals/clientview/<? echo $client[0]['clientID']; ?>" class="btn btn-default">See all</a>
    <br/><br/>
    <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>Title</th>
      </tr>
    <? if (isset($client_proposals)) { ?>
      <? foreach ($client_proposals as $proposaldata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $proposaldata['salesID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $proposaldata['sales_title']; ?></td>
        </tr>
      <? } ?>
    <? } ?>
    </table>
  </div>

  <div class="col-sm-4">
    <h3>Recurring</h3>
    <a href="<? echo base_url(); ?>index.php/recuring/clientadd/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
    <a href="<? echo base_url(); ?>index.php/recuring/clientview/<? echo $client[0]['clientID']; ?>" class="btn btn-default">See all</a>
    <br/><br/>
    <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>Name</th>
        <th>Next Renewal Date</th>
      </tr>
    <? if (isset($client_recurring)) { ?>
      <? foreach ($client_recurring as $recurringdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/recuring/view/<? echo $recurringdata['recID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $recurringdata['rec_name']; ?></td>
          <td><? echo date('d/m/Y', $recurringdata['rec_nextrenew']); ?></td>
        </tr>
      <? } ?>
    <? } ?>
    </table>
  </div>
  	

</div>

</div>