<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Full Client List</h1>

  	<div class="client-search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
        <h3>Client Search</h3>
        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Client Name" name="client_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>

    <div class="client-search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/contactsearch/">
        <h3>Contact Search</h3>
        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Name" name="contact_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['contact_name'].'"';} ?> >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
    <div style="clear:both"></div>

  	<a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
    <a href="<? echo base_url(); ?>index.php/clientcontact/adddash/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client Contact</a>
    <a href="<? echo base_url(); ?>index.php/client/archived/" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Archived Clients</a>
    <br/><br/>
    <div class="col-md-12 text-center">
    <?php 
      $letters = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
      if(!isset($filter)){$filter = "";}
    ?>
      <ul class="pagination pagination-lg">
        <? foreach ($letters as $letter){ ?>
          <li <? if ($letter == $filter){ ?> class="active" <? } ?>><a href="<? echo base_url(); ?>index.php/client/filter/<? echo $letter; ?>/"><? echo $letter; ?></a></li>
        <? } ?>
      </ul>
    </div>
    <br/><br/>
  	<table class="table table-striped table-responsive" id="client-data">
  		<thead>
      <tr>
  			<th></th>
  			<th>Client Name</th>
  			<th></th>
        <th></th>
  			<th></th>
        <? if (isset($searchresults)) { echo '<th></th>'; } ?>
  		</tr>
      </thead>
          <tbody id="myTable">
  		<? foreach ($client as $clientids) { ?>
      <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$clientids['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($clientdata = $result->fetch_assoc()) { ?>
  			   <tr <? if (isset($searchresults) && $clientdata['client_archive'] == "Y") { echo 'class="info"'; } ?>>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/client/view/<? echo $clientdata['clientID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientdata['clientID']; ?>/" ><? echo $clientdata['client_name']; ?></a></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/clientadd/<? echo $client[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add Project</a></td>
  				<td><a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientdata['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/client/delete/<? echo $clientdata['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
          <? if (isset($searchresults) && $clientdata['client_archive'] == "Y") { echo '<td>Archived</td>'; } else {echo '<td></td>';} ?>
  			</tr>
        <? }
            } else { ?>
            <? } ?>
  		<? } ?>
    </tbody>
  	</table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>