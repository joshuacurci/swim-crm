<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Edit note</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/clientnotes/save/">
      <input type="hidden" name="staffID" value="<? echo $staffID; ?>" >
      <input type="hidden" name="clientID" value="<? echo $clientnote[0]['clientID']; ?>" >
      <input type="hidden" name="noteID" value="<? echo $clientnote[0]['noteID']; ?>" >
      <input type="hidden" name="note_date" value="<? echo $clientnote[0]['note_date']; ?>" >


        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note Title:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Note Title" name="note_title" value="<? echo $clientnote[0]['note_title']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert note here..." name="note_content">
            <? echo $clientnote[0]['note_content']; ?>
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note Type:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Note Type" name="note_type" value="<? echo $clientnote[0]['note_type']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>