<? $state = "" ?>

<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1><? echo $clientnote[0]['note_title']; ?></h1>
  <a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientnote[0]['clientID']; ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/clientnotes/edit/<? echo $clientnote[0]['noteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
  <br/><br/>
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Date Created:</label>
          <div class="col-sm-9">
            <? echo date('d/m/Y H:i:s', $clientnote[0]['note_date']); ?>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Created by:</label>
          <div class="col-sm-9">
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_users WHERE userID = ".$clientnote[0]['staffID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><? echo $row['name']; ?></td>
                <? }
            } else { ?>
                <td>No Staff</td>
            <? } ?>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note Type:</label>
          <div class="col-sm-9">
            <? echo $clientnote[0]['note_type']; ?>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Note:</label>
          <div class="col-sm-9">
            <? echo $clientnote[0]['note_content']; ?>
          </div>
                <div style="clear:both"></div>
        </div>

  	

</div>

</div>