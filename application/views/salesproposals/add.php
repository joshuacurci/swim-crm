<? $state = "VIC" ?>

<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Add new Sales in progress information</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/salesproposals/newprop/">
        <input type="hidden" name="sales_createdby" value="<? echo $staffID; ?>" />

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Proposal Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Proposal Name" name="sales_title">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">New Customer?:</label>
          <div class="col-sm-9">
            <label class="checkbox-inline">
              <input type="radio" id="inlineCheckbox1" name="existingcustomer" value="Y" onclick="dynInput(this);"> Yes
            </label>
            <label class="checkbox-inline">
              <input type="radio" id="inlineCheckbox2" name="existingcustomer" value="N" checked onclick="dynInput(this);"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

  <div id="customerexists" class="form-group">
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="clientID" onChange="getCity('<? echo base_url(); ?>clientcontact.php?clientID='+this.value)">
              <option value="">Please Select</option>
              <? foreach ($clientlist as $clientdata){ ?>
                <option value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Main Contact:</label>
          <div class="col-sm-9">
            <div id="clientcontact"><select class="form-control disabled" id="inputorg1" name="contactID" disabled>
            <option value="">Select Client</option>
            </select></div>
          </div>
                <div style="clear:both"></div>
        </div>
  </div>
  

  <div id="newcustomer" class="form-group" style="display:none;">

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Company Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Company Name" name="sales_companyname">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact First Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact First Name" name="sales_name">
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Last Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Last Name" name="sales_lastname">
          </div>
                <div style="clear:both"></div>
        </div>


        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Address:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum2" placeholder="Address Line 1" name="sales_add_1">
            <div class="clearboth"></div>
            <input type="text" class="form-control" id="inputrecNum3" placeholder="Address Line 2" name="sales_add_2">
            <div class="clearboth"></div>
            <input type="text" class="form-control" id="inputrecNum4" placeholder="City/Suburb" name="sales_add_city">
            <div class="clearboth"></div>
            <select class="form-control" id="inputorg1" name="sales_add_state">
              <option <? if($state == "NSW") { echo "selected"; } ?> value="NSW">NSW</option>
              <option <? if($state == "VIC") { echo "selected"; } ?> value="VIC">VIC</option>
              <option <? if($state == "QLD") { echo "selected"; } ?> value="QLD">QLD</option>
              <option <? if($state == "SA") { echo "selected"; } ?> value="SA">SA</option>
              <option <? if($state == "WA") { echo "selected"; } ?> value="WA">WA</option>
              <option <? if($state == "TAS") { echo "selected"; } ?> value="TAS">TAS</option>
              <option <? if($state == "NT") { echo "selected"; } ?> value="NT">NT</option>
              <option <? if($state == "ACT") { echo "selected"; } ?> value="ACT">ACT</option>
              <option <? if($state == "Outside AU") { echo "selected"; } ?> value="Outside AU">Outside AU</option>
            </select>
            <input type="text" class="form-control" id="inputrecNum5" placeholder="Postcode" name="sales_add_postcode">
            <div class="clearboth"></div>
            <select class="form-control" id="inputorg2" name="sales_add_country">
              <option value="Select Country">Select Country</option>
              <option selected="selected" value="Australia">Australia</option>
              <option value="New Zealand">New Zealand</option>
              <option value="United States">United States</option>
              <option value="Afghanistan">Afghanistan</option>
              <option value="Albania">Albania</option>
              <option value="Algeria">Algeria</option>
              <option value="American Samoa">American Samoa</option>
              <option value="Andorra">Andorra</option>
              <option value="Angola">Angola</option>
              <option value="Anguilla">Anguilla</option>
              <option value="Antarctica">Antarctica</option>
              <option value="Antigua And Barbuda">Antigua And Barbuda</option>
              <option value="Argentina">Argentina</option>
              <option value="Armenia">Armenia</option>
              <option value="Aruba">Aruba</option>
              <option value="Austria">Austria</option>
              <option value="Azerbaijan">Azerbaijan</option>
              <option value="Bahamas">Bahamas</option>
              <option value="Bahrain">Bahrain</option>
              <option value="Bangladesh">Bangladesh</option>
              <option value="Barbados">Barbados</option>
              <option value="Belarus">Belarus</option>
              <option value="Belgium">Belgium</option>
              <option value="Belize">Belize</option>
              <option value="Benin">Benin</option>
              <option value="Bermuda">Bermuda</option>
              <option value="Bhutan">Bhutan</option>
              <option value="Bolivia">Bolivia</option>
              <option value="Bosnia And Herzegowina">Bosnia And Herzegowina</option>
              <option value="Botswana">Botswana</option>
              <option value="Bouvet Island">Bouvet Island</option>
              <option value="Brazil">Brazil</option>
              <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
              <option value="Brunei Darussalam">Brunei Darussalam</option>
              <option value="Bulgaria">Bulgaria</option>
              <option value="Burkina Faso">Burkina Faso</option>
              <option value="Burundi">Burundi</option>
              <option value="Cambodia">Cambodia</option>
              <option value="Cameroon">Cameroon</option>
              <option value="Canada">Canada</option>
              <option value="Cape Verde">Cape Verde</option>
              <option value="Cayman Islands">Cayman Islands</option>
              <option value="Central African Republic">Central African Republic</option>
              <option value="Chad">Chad</option>
              <option value="Chile">Chile</option>
              <option value="China">China</option>
              <option value="Christmas Island">Christmas Island</option>
              <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
              <option value="Colombia">Colombia</option>
              <option value="Comoros">Comoros</option>
              <option value="Congo">Congo</option>
              <option value="Cook Islands">Cook Islands</option>
              <option value="Costa Rica">Costa Rica</option>
              <option value="Cote D'Ivoire">Cote D'Ivoire</option>
              <option value="Croatia (Local Name: Hrvatska)">Croatia (Local Name: Hrvatska)</option>
              <option value="Cuba">Cuba</option>
              <option value="Cyprus">Cyprus</option>
              <option value="Czech Republic">Czech Republic</option>
              <option value="Denmark">Denmark</option>
              <option value="Djibouti">Djibouti</option>
              <option value="Dominica">Dominica</option>
              <option value="Dominican Republic">Dominican Republic</option>
              <option value="East Timor">East Timor</option>
              <option value="Ecuador">Ecuador</option>
              <option value="Egypt">Egypt</option>
              <option value="El Salvador">El Salvador</option>
              <option value="Equatorial Guinea">Equatorial Guinea</option>
              <option value="Eritrea">Eritrea</option>
              <option value="Estonia">Estonia</option>
              <option value="Ethiopia">Ethiopia</option>
              <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
              <option value="Faroe Islands">Faroe Islands</option>
              <option value="Fiji">Fiji</option>
              <option value="Finland">Finland</option>
              <option value="France">France</option>
              <option value="French Guiana">French Guiana</option>
              <option value="French Polynesia">French Polynesia</option>
              <option value="French Southern Territories">French Southern Territories</option>
              <option value="Gabon">Gabon</option>
              <option value="Gambia">Gambia</option>
              <option value="Georgia">Georgia</option>
              <option value="Germany">Germany</option>
              <option value="Ghana">Ghana</option>
              <option value="Gibraltar">Gibraltar</option>
              <option value="Greece">Greece</option>
              <option value="Greenland">Greenland</option>
              <option value="Grenada">Grenada</option>
              <option value="Guadeloupe">Guadeloupe</option>
              <option value="Guam">Guam</option>
              <option value="Guatemala">Guatemala</option>
              <option value="Guinea">Guinea</option>
              <option value="Guinea-Bissau">Guinea-Bissau</option>
              <option value="Guyana">Guyana</option>
              <option value="Haiti">Haiti</option>
              <option value="Honduras">Honduras</option>
              <option value="Hong Kong">Hong Kong</option>
              <option value="Hungary">Hungary</option>
              <option value="Icel And">Icel And</option>
              <option value="India">India</option>
              <option value="Indonesia">Indonesia</option>
              <option value="Iran (Islamic Republic Of)">Iran (Islamic Republic Of)</option>
              <option value="Iraq">Iraq</option>
              <option value="Ireland">Ireland</option>
              <option value="Israel">Israel</option>
              <option value="Italy">Italy</option>
              <option value="Jamaica">Jamaica</option>
              <option value="Japan">Japan</option>
              <option value="Jordan">Jordan</option>
              <option value="Kazakhstan">Kazakhstan</option>
              <option value="Kenya">Kenya</option>
              <option value="Kiribati">Kiribati</option>
              <option value="Korea">Korea</option>
              <option value="Kuwait">Kuwait</option>
              <option value="Kyrgyzstan">Kyrgyzstan</option>
              <option value="Lao People'S Dem Republic">Lao People'S Dem Republic</option>
              <option value="Latvia">Latvia</option>
              <option value="Lebanon">Lebanon</option>
              <option value="Lesotho">Lesotho</option>
              <option value="Liberia">Liberia</option>
              <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
              <option value="Liechtenstein">Liechtenstein</option>
              <option value="Lithuania">Lithuania</option>
              <option value="Luxembourg">Luxembourg</option>
              <option value="Macau">Macau</option>
              <option value="Macedonia">Macedonia</option>
              <option value="Madagascar">Madagascar</option>
              <option value="Malawi">Malawi</option>
              <option value="Malaysia">Malaysia</option>
              <option value="Maldives">Maldives</option>
              <option value="Mali">Mali</option>
              <option value="Malta">Malta</option>
              <option value="Marshall Islands">Marshall Islands</option>
              <option value="Martinique">Martinique</option>
              <option value="Mauritania">Mauritania</option>
              <option value="Mauritius">Mauritius</option>
              <option value="Mayotte">Mayotte</option>
              <option value="Mexico">Mexico</option>
              <option value="Micronesia, Federated States">Micronesia, Federated States</option>
              <option value="Moldova, Republic Of">Moldova, Republic Of</option>
              <option value="Monaco">Monaco</option>
              <option value="Mongolia">Mongolia</option>
              <option value="Montserrat">Montserrat</option>
              <option value="Morocco">Morocco</option>
              <option value="Mozambique">Mozambique</option>
              <option value="Myanmar">Myanmar</option>
              <option value="Namibia">Namibia</option>
              <option value="Nauru">Nauru</option>
              <option value="Nepal">Nepal</option>
              <option value="Netherlands">Netherlands</option>
              <option value="Netherlands Ant Illes">Netherlands Ant Illes</option>
              <option value="New Caledonia">New Caledonia</option>
              <option value="Nicaragua">Nicaragua</option>
              <option value="Niger">Niger</option>
              <option value="Nigeria">Nigeria</option>
              <option value="Niue">Niue</option>
              <option value="Norfolk Island">Norfolk Island</option>
              <option value="Northern Mariana Islands">Northern Mariana Islands</option>
              <option value="Norway">Norway</option>
              <option value="Oman">Oman</option>
              <option value="Pakistan">Pakistan</option>
              <option value="Palau">Palau</option>
              <option value="Panama">Panama</option>
              <option value="Papua New Guinea">Papua New Guinea</option>
              <option value="Paraguay">Paraguay</option>
              <option value="Peru">Peru</option>
              <option value="Philippines">Philippines</option>
              <option value="Pitcairn">Pitcairn</option>
              <option value="Poland">Poland</option>
              <option value="Portugal">Portugal</option>
              <option value="Puerto Rico">Puerto Rico</option>
              <option value="Qatar">Qatar</option>
              <option value="Reunion">Reunion</option>
              <option value="Romania">Romania</option>
              <option value="Russian Federation">Russian Federation</option>
              <option value="Rwanda">Rwanda</option>
              <option value="Saint K Itts And Nevis">Saint K Itts And Nevis</option>
              <option value="Saint Lucia">Saint Lucia</option>
              <option value="Saint Vincent, The Grenadines">Saint Vincent, The Grenadines</option>
              <option value="Samoa">Samoa</option>
              <option value="San Marino">San Marino</option>
              <option value="Sao Tome And Principe">Sao Tome And Principe</option>
              <option value="Saudi Arabia">Saudi Arabia</option>
              <option value="Senegal">Senegal</option>
              <option value="Seychelles">Seychelles</option>
              <option value="Sierra Leone">Sierra Leone</option>
              <option value="Singapore">Singapore</option>
              <option value="Slovakia (Slovak Republic)">Slovakia (Slovak Republic)</option>
              <option value="Slovenia">Slovenia</option>
              <option value="Solomon Islands">Solomon Islands</option>
              <option value="Somalia">Somalia</option>
              <option value="South Africa">South Africa</option>
              <option value="South Georgia , S Sandwich Is.">South Georgia , S Sandwich Is.</option>
              <option value="Spain">Spain</option>
              <option value="Sri Lanka">Sri Lanka</option>
              <option value="St. Helena">St. Helena</option>
              <option value="St. Pierre And Miquelon">St. Pierre And Miquelon</option>
              <option value="Sudan">Sudan</option>
              <option value="Suriname">Suriname</option>
              <option value="Svalbard, Jan Mayen Islands">Svalbard, Jan Mayen Islands</option>
              <option value="Sw Aziland">Sw Aziland</option>
              <option value="Sweden">Sweden</option>
              <option value="Switzerland">Switzerland</option>
              <option value="Syrian Arab Republic">Syrian Arab Republic</option>
              <option value="Taiwan">Taiwan</option>
              <option value="Tajikistan">Tajikistan</option>
              <option value="Tanzania, United Republic Of">Tanzania, United Republic Of</option>
              <option value="Thailand">Thailand</option>
              <option value="Togo">Togo</option>
              <option value="Tokelau">Tokelau</option>
              <option value="Tonga">Tonga</option>
              <option value="Trinidad And Tobago">Trinidad And Tobago</option>
              <option value="Tunisia">Tunisia</option>
              <option value="Turkey">Turkey</option>
              <option value="Turkmenistan">Turkmenistan</option>
              <option value="Turks And Caicos Islands">Turks And Caicos Islands</option>
              <option value="Tuvalu">Tuvalu</option>
              <option value="Uganda">Uganda</option>
              <option value="Ukraine">Ukraine</option>
              <option value="United Arab Emirates">United Arab Emirates</option>
              <option value="United Kingdom">United Kingdom</option>
              <option value="United States">United States</option>
              <option value="United States Minor Is.">United States Minor Is.</option>
              <option value="Uruguay">Uruguay</option>
              <option value="Uzbekistan">Uzbekistan</option>
              <option value="Vanuatu">Vanuatu</option>
              <option value="Venezuela">Venezuela</option>
              <option value="Viet Nam">Viet Nam</option>
              <option value="Virgin Islands (British)">Virgin Islands (British)</option>
              <option value="Wallis And Futuna Islands">Wallis And Futuna Islands</option>
              <option value="Western Sahara">Western Sahara</option>
              <option value="Yemen">Yemen</option>
              <option value="Yugoslavia">Yugoslavia</option>
              <option value="Zaire">Zaire</option>
              <option value="Zambia">Zambia</option>
              <option value="Zimbabwe">Zimbabwe</option>
          </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Phone:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Phone Number" name="sales_phone" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Fax:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Fax Number" name="sales_fax" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Mobile:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Mobile" name="sales_mobile" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Contact Email:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Email" name="sales_email" >
          </div>
                <div style="clear:both"></div>
        </div> 
      </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Proposal Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="sales_status">
              <option value="Prospect">Prospect</option>
              <option value="Quote">Quote</option>
              <option value="Declined">Declined</option>
              <option value="Archived">Archived</option>
              <option value="Canceled">Canceled</option>
              <option value="Approved">Approved</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Potental Project Budget:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Potental Project Budget" name="sales_budget" >
          </div>
                <div style="clear:both"></div>
        </div>  

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Percentage of likelyhood of project going ahead:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Percentage of likelyhood of project going ahead (eg. 80)" name="sales_goingahead" >
          </div>
                <div style="clear:both"></div>
        </div>  

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Percentage of likelyhood of project going ahead with SWiM:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Percentage of likelyhood of project going ahead with SWiM (eg. 60)" name="sales_aheadwith" >
          </div>
                <div style="clear:both"></div>
        </div>  

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Likely Start Date of Project:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="sales_startdate" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Likely Due Date of Project:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="sales_duedate" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Follow up date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="sales_followup" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div> 

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Details:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert any details here..." name="sales_details">
             
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div> 

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
    

</div>

</div>