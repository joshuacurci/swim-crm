<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Full Sales in progress List</h1>

  	<div class="search-box">
      <h3>Search</h3>
      Coming soon
    </div>

  	<a href="<? echo base_url(); ?>index.php/salesproposals/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Sale in progress</a>
    <br/><br/>
  	<table class="table table-striped table-responsive" id="client-data">
  		<thead>
      <tr>
  			<th></th>
  			<th>Proposal Name</th>
  			<th>Company Name</th>
        <th></th>
  			<th></th>
  		</tr>
      </thead>
          <tbody id="myTable">
  		<? foreach ($salesprop as $clientdata) { ?>
  			<tr>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $clientdata['salesID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><a href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $clientdata['salesID']; ?>/" ><? echo $clientdata['sales_title']; ?></a></td>
          <? if ($clientdata['sales_new'] == 'Y') { ?>
          <td><? echo $clientdata['sales_companyname']; ?></td>
          <? } else { ?>
              <?  $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
                    $sql = "SELECT * FROM tbl_client WHERE clientID = ".$clientdata['sales_clientID']."";
                $result = $conn->query($sql);
                $num_rec = $result->num_rows;
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { ?>
                        <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/" ><? echo $row['client_name']; ?></a></td>
                    <? } } else { ?>
                        <td>No Client</td>
              <? } ?>
          <? } ?>

  				<td><a href="<? echo base_url(); ?>index.php/salesproposals/edit/<? echo $clientdata['salesID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/salesproposals/delete/<? echo $clientdata['salesID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
  			</tr>
  		<? } ?>
    </tbody>
  	</table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>