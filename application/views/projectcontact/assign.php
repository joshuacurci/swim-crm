<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Add new project contact</h1>
  <a href="<? echo base_url(); ?>index.php/clientcontact/add/<? echo $clientID; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Contact</a>
  <br/><br/>
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projectcontact/savecontact/">
    <input type="hidden" name="projectID" value="<? echo $projectID; ?>" >

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Assign Contact:</label>
          <div class="col-sm-9">
              <? foreach ($client_contacts as $contactdata){ ?>
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="contactID[]" value="<? echo $contactdata['contactID'] ?>">
                    <? echo $contactdata['contact_name'] ?> <? echo $contactdata['contact_lastname'] ?> - <? echo $contactdata['contact_email'] ?>
                </label>
              </div>
              <? } ?>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <input type="submit" class="btn btn-primary" name="action" value="Submit" />
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>