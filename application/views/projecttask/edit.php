<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Edit task</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projecttask/save/">
      <input type="hidden" name="projectID" value="<? echo $project_tasks[0]['projectID']; ?>" >
      <input type="hidden" name="projecttaskID" value="<? echo $project_tasks[0]['projecttaskID']; ?>" >

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Title:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Task Title" name="task_name" value="<? echo $project_tasks[0]['task_name']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Assigned to:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="assuserID">
              <? foreach ($stafflist as $staffdata){ ?>
                <option <? if($project_tasks[0]['assuserID'] == $staffdata['userID']) {echo "selected=selected";} ?> value="<? echo $staffdata['userID'] ?>"><? echo $staffdata['name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Content:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert task content here..." name="task_content">
            	<? echo $project_tasks[0]['task_content']; ?>
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Priority:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="task_priority">
              <option <? if($project_tasks[0]['task_priority'] == "High") {echo "selected=selected";} ?> value="High">High</option>
              <option <? if($project_tasks[0]['task_priority'] == "Medium") {echo "selected=selected";} ?> value="Medium">Medium</option>
              <option <? if($project_tasks[0]['task_priority'] == "Low") {echo "selected=selected";} ?> value="Low">Low</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Due Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="task_duedate" value="<? echo date('d-m-Y',$project_tasks[0]['task_duedate']); ?>" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="task_status">
                <option value="A">Active</option>
                <option value="C">Complete</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>