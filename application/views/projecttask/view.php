<div role="main" class="container-fluid main-wrapper theme-showcase">

<div clsas="col-sm-12">
	<h1><? echo $project_tasks[0]['task_name']; ?></h1>
    
  <a href="<? echo base_url(); ?>index.php/projects/view/<? echo $project_tasks[0]['projectID']; ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $project_tasks[0]['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
    <br/><br/>
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Task Priority:</label>
        <div class="col-sm-9"><? if ($project_tasks[0]['task_priority'] == 'High'){echo '<span class="label label-danger">'.$project_tasks[0]['task_priority'].'</span>';}
            elseif ($project_tasks[0]['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$project_tasks[0]['task_priority'].'</span>';}
            elseif ($project_tasks[0]['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$project_tasks[0]['task_priority'].'</span>';} ?></div>
    <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Task Due Date:</label>
        <div class="col-sm-9"><? echo date('d/m/Y',$project_tasks[0]['task_duedate']); ?></div>
    <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Task Assigned:</label>
        <div class="col-sm-9">
        	<?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_users WHERE userID = ".$project_tasks[0]['assuserID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <? echo $row['name']; ?>
                <? }
            } else { ?>
                No Client
  <? } ?>
        </div>
    <div style="clear:both"></div>
    </div>

    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Created by:</label>
        <div class="col-sm-9">
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_users WHERE userID = ".$project_tasks[0]['task_createdbyID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <? echo $row['name']; ?>
                <? }
            } else { ?>
                No Client
  <? } ?>
        </div>
    <div style="clear:both"></div>
    </div>

    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Created on:</label>
        <div class="col-sm-9"><? echo date('d/m/Y',$project_tasks[0]['task_createdate']); ?></div>
    <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Task Content:</label>
        <div class="col-sm-9"><? echo $project_tasks[0]['task_content']; ?></div>
    <div style="clear:both"></div>
    </div>
    <br/>
    <br/>
    <div class="task-reply">
        <h2>Replies</h2>
        <h3>Add new reply</h3>
        <div class="reply-form">
            <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/reply/newreply/tasks/<? echo $project_tasks[0]['projecttaskID']; ?>/">
                        <input type="hidden" name="reply_user" value="<? echo $_SESSION['usersname'] ?>" />
                        <textarea name="reply_content" style="width:100%">
                        </textarea>
                        <br>
                        <input type="submit" name="Submit" value="Submit Reply" class="btn btn-primary" />
                    </form>
        </div>

        <div class="reply-list">
                    <?
                        $servername = $this->db->hostname;
                        $username = $this->db->username;
                        $password = $this->db->password;
                        $dbname = $this->db->database;

                        // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM tbl_projects_tasks_reply WHERE conectorID = ".$project_tasks[0]['projecttaskID']."";
                        $result = $conn->query($sql);
                        
                        $num_rec = $result->num_rows;

                        if ($result->num_rows > 0) {
                            // output data of each row
                            echo "<h2>Replies (".$num_rec.")</h2>";
                            while($row = $result->fetch_assoc()) {
                            
                            echo "<div class='posted-reply-wrapper'>";
                            echo "<div class='posted-user'><b>Posted by:</b> ".$row['reply_user']."</div>";
                            echo "<div class='posted-date'><b>Date Posted:</b> ".date('d/m/Y H:i:s',$row['reply_date'])."</div>";
                            echo "<div class='posted-reply'>".$row['reply_content']."</div>";
                            echo "</div>";
                            }
                        } else {
                            echo "<h3>0 replies</h3>";
                        }
                                            
                    ?>
                    
            
        </div>
    </div>

</div>
</div>