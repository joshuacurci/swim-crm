<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Add new task</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projecttask/newtask/">
      <input type="hidden" name="projectID" value="<? echo $projectID; ?>" >
      <input type="hidden" name="staffID" value="<? echo $staffID; ?>" >

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Title:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Task Title" name="task_name" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Assigned to:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="assuserID">
              <? foreach ($stafflist as $staffdata){ ?>
                <option value="<? echo $staffdata['userID'] ?>"><? echo $staffdata['name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Content:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert task content here..." name="task_content">
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Priority:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="task_priority">
              <option value="High">High</option>
              <option value="Medium" selected>Medium</option>
              <option value="Low">Low</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Task Due Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="task_duedate" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <input type="submit" class="btn btn-primary" name="action" value="Submit" />
            <input type="submit" class="btn btn-primary" name="action" value="Submit and Add another" />
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>