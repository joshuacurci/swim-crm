<div role="main" class="container-fluid main-wrapper theme-showcase">

<div clsas="col-sm-12">
  <h1><? echo $project[0]['project_name']; ?> - Completed Tasks</h1>

   <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$project[0]['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <h3><? echo $row['client_name']; ?></h3>
                <? }
            } else { ?>
                <h3>No Client</h3>
  <? } ?>
  
  <a href="<? echo base_url(); ?>index.php/projects/view/<? echo $project[0]['projectID']; ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <br/><br/>
  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Job Bag #: </b></td>
        <td>
          <? echo $project[0]['job_bag']; ?><br/>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Project Type: </b></td>
        <td>
          <? echo $project[0]['project_type']; ?><br/>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Project Status: </b></td>
        <td>
          <? echo $project[0]['project_status']; ?><br/>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-12 project-info">

        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Assigned to</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($project_tasks)) { ?>
          <? foreach ($project_tasks as $taskdata) { ?>
            <tr <? if ($taskdata['task_status'] == 'C'){echo 'class="success"';}?> >
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_users WHERE userID = ".$taskdata['assuserID']."";
              $result = $conn->query($sql);
              
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['name']; ?></td>
                  <? }
              } else { ?>
                  <td>Not Assigned</td>
              <? } ?>

              <td><? echo date('d/m/Y',$taskdata['task_duedate']); ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecthours/addtask/<? echo $project[0]['projectID']; ?>/<? echo $taskdata['projecttaskID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Hours</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>

  </div>
  	

</div>

</div>