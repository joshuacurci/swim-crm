<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Results For <? echo $searchresults['startdate']; ?> - <? echo $searchresults['lastdate']; ?></h1>

  	
  	<a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
    <a href="<? echo base_url(); ?>index.php/clientcontact/adddash/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client Contact</a>
    <a href="<? echo base_url(); ?>index.php/client/archived/" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Archived Clients</a>
  	<table class="table table-striped table-responsive" id="client-data">
  		<thead>
      <tr>
  			<th>Client Name</th>
  			<th></th>
        <th></th>
  			<th></th>
        <? if (isset($searchresults)) { echo '<th></th>'; } ?>
  		</tr>
      </thead>
          <tbody id="myTable">
  		<? foreach ($clientData as $clientData) { ?>
  			<tr <? if (isset($searchresults) && $clientData['client_archive'] == "Y") { echo 'class="info"'; } ?>>
  				<td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientData['clientID']; ?>/" ><? echo $clientData['client_name']; ?></a></td>
          <td><? echo $clientData['client_add_num']; ?>, <? echo $clientData['client_add_street']; ?>, <? echo $clientData['client_add_city']; ?>, <? echo $clientData['client_add_state']; ?>, <? echo $clientData['client_add_postcode']; ?></td>
  				<td><? echo $clientData['client_phone']; ?></td>
          <td><? echo $clientData['note_content']; ?></td>
          <? if (isset($searchresults) && $clientData['client_archive'] == "Y") { echo '<td>Archived</td>'; } else {echo '<td></td>';} ?>
  			</tr>
  		<? } ?>
    </tbody>
  	</table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-md" id="myPager"></ul>
      </div>

</div>

</div>