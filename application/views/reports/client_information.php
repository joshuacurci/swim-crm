<style>
  .add_border_bottom {
    border-bottom: 1px solid #000;
    padding: 0px !important;
  }

</style>
<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1 <? if($clientData[0]['client_archive'] == 'Y') { echo 'class="archived-client"'; } ?>>
      <? echo $clientData[0]['client_name']; ?>
    </h1>
    <? if($clientData[0]['client_archive'] == 'Y') { echo '<h3 class="archived-client"><span class="glyphicon glyphicon-inbox" aria-hidden="true"></span> Archived</h3>'; } ?>
    <div style="clear:both"></div>
    <?
    if ($clientData[0]['client_resoled'] != '' || $clientData[0]['client_resoled'] != NULL) {
      $servername = $this->db->hostname;
      $username = $this->db->username;
      $password = $this->db->password;
      $dbname = $this->db->database;

            // Create connection
      $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
      if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
      }

      $sql = "SELECT * FROM tbl_client WHERE clientID = ".$clientData[0]['client_resoled']."";
      $result = $conn->query($sql);

      $num_rec = $result->num_rows;

      if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) { ?>
          <h3>Resold by - <? echo $row['client_name']; ?></h3>
          <? }
        } else { ?>
          <h3>No Client</h3>
          <? } } ?>
          <table>
            <tr>
              <td >
                <table>
                  <tr>
                    <td style="padding-right:5px"><b>Address: </b><br/><br/><br/><br/></td>
                    <td>
                      <? echo $clientData[0]['client_add_num']; ?><br/><? echo $clientData[0]['client_add_street']; ?><br/>
                      <? echo $clientData[0]['client_add_city']; ?> <? echo $clientData[0]['client_add_state']; ?><br/>
                      <? echo $clientData[0]['client_add_country']; ?> <? echo $clientData[0]['client_add_postcode']; ?><br/>
                    </td>
                  </tr>
                </table>
              </td>
              <td>
                <table>
                  <tr>
                    <td style="padding-right:5px"><b>Phone: </b>  </td>
                    <td>
                      <? echo $clientData[0]['client_phone']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-right:5px"><b>Fax: </b>  </td>
                    <td>
                      <? echo $clientData[0]['client_fax']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-right:5px"><b>ACN: </b>  </td>
                    <td>
                      <? echo $clientData[0]['client_acn']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td style="padding-right:5px"><b>Types of work completed: </b>  </td>
                    <td>
                      <? if ($clientData[0]['client_work']) { ?>
                        <? $indvalues = explode(";",$clientData[0]['client_work']); ?>
                        <? foreach ($indvalues as $clientworkvalue) { ?>
                          <?
                          if ($clientworkvalue != "") {

                            $servername = $this->db->hostname;
                            $username = $this->db->username;
                            $password = $this->db->password;
                            $dbname = $this->db->database;

            // Create connection
                            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
                            if ($conn->connect_error) {
                              die("Connection failed: " . $conn->connect_error);
                            }

                            $sql = "SELECT * FROM tbl_client_work WHERE workID = ".$clientworkvalue."";
                            $result = $conn->query($sql);
                            $num_rec = $result->num_rows;

                            if ($result->num_rows > 0) {
                              while($row = $result->fetch_assoc()) { ?>
                                <? echo $row['work_value']; ?><br/>
                                <? }
                              } else { ?>

                                <? }} ?>
                                <? } ?>
                                <? } ?>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>



                    <div class="add_border_bottom">
                      <table>
                        <tr>
                          <td style="padding-right:5px"><b>Trading Terms: </b>  </td>
                          <td>
                            <? echo $clientData[0]['client_traiding_terms']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-right:5px"><b>Client Type: </b>  </td>
                          <td>
                            <? echo $clientData[0]['client_type']; ?>
                          </td>
                        </tr>
                        <tr>
                          <td style="padding-right:5px"><b>ABN: </b>  </td>
                          <td>
                            <? echo $clientData[0]['client_abn']; ?>
                          </td>
                        </tr>
                      </table>
                    </div>

                    <div class="add_border_bottom">
                      <h3>Contacts</h3>
                      <table>
                        <tr style="padding-bottom: 20px;">
                          <th><strong>Contact Name</strong></th>
                          <th width="80px"><strong>Phone</strong></th>
                          <th width="80px"><strong>Mobile</strong></th>
                          <th width="160px"><strong>Email</strong></th>
                          <th><strong>Hot Drink</strong></th>
                        </tr>
                        <? if (isset($client_contacts)) { ?>
                          <? foreach ($client_contacts as $contactdata) { ?>
                            <tr>
                              <td><? echo $contactdata['contact_name']; ?> <? echo $contactdata['contact_lastname']; ?></td>
                              <td><? echo $contactdata['contact_phone']; ?></td>
                              <td><? echo $contactdata['contact_mobile']; ?></td>
                              <td><? echo $contactdata['contact_email']; ?></td>
                              <td><? echo $contactdata['contact_hotdrink']; ?></td>
                            </tr>
                            <? } ?>
                            <? } ?>
                          </table>
                        </div>

                        <div class="add_border_bottom">
                        <table>
                          <tr>
                            <td width="400px">

                              <h3>Projects</h3>
                              <table class="table table-striped table-responsive">
                                <tr>
                                  <th><strong>Title</strong></th>
                                  <th><strong>Job Number</strong></th>
                                  <th><strong>Project Status</strong></th>
                                </tr>
                                <? if (isset($client_projects)) { ?>
                                  <? foreach ($client_projects as $projectdata) { ?>
                                    <tr>
                                      <td><? echo $projectdata['project_name']; ?></td>
                                      <td><? echo $projectdata['job_bag']; ?></td>
                                      <td>
                                        <? if($projectdata['project_status'] == 'Prospect' || $projectdata['project_status'] == 'Quote') { ?> <span class="label label-warning"> <? }
                                        else if($projectdata['project_status'] == 'Job in progress') { ?> <span class="label label-success"> <? } 
                                         else if($projectdata['project_status'] == 'Invoiced') { ?> <span class="label label-primary"> <? } 
                                           else if($projectdata['project_status'] == 'Completed' || $projectdata['project_status'] == 'Paid' || $projectdata['project_status'] == 'Declined' || $projectdata['project_status'] == 'Archived' || $projectdata['project_status'] == 'Canceled') { ?> <span class="label label-default"> <? } else{} ?>
                                            <? echo $projectdata['project_status']; ?>
                                        </span>
                                      </td>
                                    </tr>
                                    <? } ?>
                                    <? } ?>
                                  </table>
                                </td>
                                <td width="200px">
                                  <h3>Proposals</h3>
                                  <table class="table table-striped table-responsive">
                                    <tr>
                                      <th><strong>Title</strong></th>
                                    </tr>
                                    <? if (isset($client_proposals)) { ?>
                                      <? foreach ($client_proposals as $proposaldata) { ?>
                                        <tr>
                                          
                                          <td><? echo $proposaldata['sales_title']; ?></td>
                                        </tr>
                                        <? } ?>
                                        <? } ?>
                                      </table>

                                    </td>
                                  </tr>
                                </table>
                                </div>
                                <div class="add_border_bottom">
                                <table>
                                  <tr>
                                    <td width="400px">
                                      <h3>Recurring</h3>
                                      <table class="table table-striped table-responsive">
                                        <tr>
                                          <th><strong>Name</strong></th>
                                          <th><strong>Next Renewal Date</strong></th>
                                        </tr>
                                        <? if (isset($client_recurring)) { ?>
                                          <? foreach ($client_recurring as $recurringdata) { ?>
                                            <tr>
                                              <td><? echo $recurringdata['rec_name']; ?></td>
                                              <td><? echo date('d/m/Y', $recurringdata['rec_nextrenew']); ?></td>
                                            </tr>
                                            <? } ?>
                                            <? } ?>
                                          </table>
                                        </td>
                                        <td width="200px">
                                          <h3>Domains</h3>
                                          <table>
                                            <tr>
                                              <th><strong>Domain Name</strong></th>
                                            </tr>
                                            <? if (isset($client_domain)) { ?>
                                              <? foreach ($client_domain as $domaindata) { ?>
                                                <tr>
                                                  
                                                  <td><? echo $domaindata['domain_name']; ?></td>
                                                </tr>
                                                <? } ?>
                                                <? } ?>
                                              </table>
                                            </td>
                                          </tr>
                                        </table>
                                        </div>


                                        <div>
                                        <h3>Notes</h3>
                                          <table class="table table-striped table-responsive">
                                            <tr>
                                              <th><strong>Title</strong></th>
                                              <th><strong>Date</strong></th>
                                            </tr>
                                            <? if (isset($client_notes)) { ?>
                                              <? foreach ($client_notes as $notedata) { ?>
                                                <tr>
                                                  <td><? echo $notedata['note_title']; ?></td>
                                                  <td><? echo date('d/m/Y', $notedata['note_date']); ?></td>
                                                </tr>
                                                <? } ?>
                                                <? } ?>
                                              </table>
                                          
                                            </div>


                                          </div>

                                        </div>