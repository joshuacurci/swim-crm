<div role="main" class="container-fluid main-wrapper theme-showcase">

	<div class="col-sm-12">
		<h1>Reports</h1>

		<div class="col-xs-12 main-data-content">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#clients">Clients</a></li>
				<li><a data-toggle="tab" href="#projects">Projects</a></li>
				<li><a data-toggle="tab" href="#domains">Domains</a></li>
			</ul>
			<div class="">
				<div class="tab-content">
					<div id="clients" class="tab-pane fade in active">
						<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/reports/search_client/">
							<h3>Generate Client Information Report</h3>
							<div class="form-group">
								<div class="col-sm-4">
									<select class="form-control"x name="clientID">
										<option value="">Please Select a Client</option>
										<? foreach ($clientlist as $clientdata){ ?>
											<option value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
											<? } ?>
										</select>
									</div>
									<div style="clear:both"></div>
								</div>

								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary">Export Client Info</button>
									</div>
									<div style="clear:both"></div>
								</div>
							</form>

							<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/reports/search_dates/">
								<h3>Generate Client List</h3>
								<div class="form-group">
									<div class="col-sm-4">
										<div class="controls">
											<div class="input-group">
											<input id="date-picker-2" type="text" class="date-picker form-control" name="startdate" placeholder="Select a date when project is created" />
												<label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

												</label>
											</div>
										</div>
									</div>
									<div class="col-sm-4">
										<div class="controls">
											<div class="input-group">
												<input id="date-picker-2" type="text" placeholder="Select a date when project is created" class="date-picker form-control" name="lastdate" />
												<label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

												</label>
											</div>
										</div>
									</div>
									<div style="clear:both"></div>
								</div>

								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary">Export Client List</button>
									</div>
									<div style="clear:both"></div>
								</div>
							</form>
						</div>

						<div id="projects" class="tab-pane fade in">
							<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
								<h3>Generate Project Report</h3>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" class="form-control" id="inputrecNum1" placeholder="Client Name" name="client_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
									</div>
									<div style="clear:both"></div>
								</div>

								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary">Search</button>
									</div>
									<div style="clear:both"></div>
								</div>
							</form>
						</div>

						<div id="domains" class="tab-pane fade in">
							<form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
								<h3>Generate Domain Report</h3>
								<div class="form-group">
									<div class="col-sm-12">
										<input type="text" class="form-control" id="inputrecNum1" placeholder="Client Name" name="client_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
									</div>
									<div style="clear:both"></div>
								</div>

								<div class="form-group">
									<div class="col-sm-12">
										<button type="submit" class="btn btn-primary">Search</button>
									</div>
									<div style="clear:both"></div>
								</div>
							</form>
						</div>
					</div>
				</div>