<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Edit task</h1>
    
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/domainsrenewal/save/">
        <input type="hidden" name="domainID" value="<? echo $domainsrenewal[0]['domainID']; ?>" >
        <input type="hidden" name="domainrenewID" value="<? echo $domainsrenewal[0]['domainrenewID']; ?>" >

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Item Name:</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" id="inputrecNum1" placeholder="Task Title" name="renew_name" value="<? echo $domainsrenewal[0]['renew_name']; ?>" >
            </div>
                  <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Start Date:</label>
            <div class="col-sm-9">
              <div class="controls">
                <div class="input-group">
                    <input id="date-picker-2" type="text" class="date-picker form-control" name="renew_startdate" value="<? echo date('d-m-Y',$domainsrenewal[0]['renew_startdate']); ?>" />
                    <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                    </label>
                </div>
              </div>
              </div>
              <div style="clear:both"></div>
          </div>

           <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Due Date:</label>
            <div class="col-sm-9">
              <div class="controls">
                <div class="input-group">
                    <input id="date-picker-2" type="text" class="date-picker form-control" name="renew_lastdate" value="<? echo date('d-m-Y',$domainsrenewal[0]['renew_lastdate']); ?>" />
                    <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                    </label>
                </div>
              </div>
              </div>
              <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Renewal Rate:</label>
            <div class="col-sm-9">
              <input type="text" id="inputDetails1" value="<? echo $domainsrenewal[0]['renew_rate']; ?>" name="renew_rate" placeholder="Enter the price">
                
              </input>
            </div>
                  <div style="clear:both"></div>
          </div>

           <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Frequency:</label>
            <div class="col-sm-9">
              <select class="form-control" id="inputorg1" name="renew_frequency">
                <option <? if($domainsrenewal[0]['renew_frequency'] == "Monthly") {echo "selected=selected";} ?> value="Monthly">Monthly</option>
                <option <? if($domainsrenewal[0]['renew_frequency'] == "Quarterly") {echo "selected=selected";} ?> value="Quarterly">Quarterly</option>
                <option <? if($domainsrenewal[0]['renew_frequency'] == "Annually") {echo "selected=selected";} ?> value="Annually">Annually</option>
              </select>
            </div>
                  <div style="clear:both"></div>
          </div>

          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <button type="submit" class="btn btn-primary">Submit</button>
              <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
            </div>
          </div>

    </form>
  </div>
</div>