<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Add new renewal item</h1>

    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>index.php/domainsrenewal/newdomainsrenewal/">
      <input type="hidden" name="domainID" value="<? echo $domainID; ?>" >

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Renewal Item Title:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Renewal Item Title" name="renew_name" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Renewal Start date:</label>
        <div class="col-sm-9">
          <div class="controls">
            <div class="input-group">
            <input id="date-picker-3" type="text" class="date-picker form-control" name="renew_startdate" />
              <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

              </label>
            </div>
          </div>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Renewal Due Date:</label>
        <div class="col-sm-9">
          <div class="controls">
            <div class="input-group">
              <input id="date-picker-3" type="text" class="date-picker form-control" name="renew_lastdate" />
              <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

              </label>
            </div>
          </div>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Renew Rate:</label>
        <div class="col-sm-9">
         <input type="text" class="form-control" id="inputrecNum1" placeholder="Renewal Rate" name="renew_rate" >
       </div>
       <div style="clear:both"></div>
     </div>

     <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Renewal Frequency:</label>
      <div class="col-sm-9">
        <select class="form-control" id="inputorg1" name="renew_frequency">
          <option value="Monthly">Monthly</option>
          <option value="Quarterly">Quarterly</option>
          <option value="Annually">Annually</option>
        </select>
      </div>
      <div style="clear:both"></div>
    </div>



    <div class="form-group">
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-primary">Submit</button>
        <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
      </div>
    </div>

  </form>


</div>

</div>