<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Full Client List</h1>

  	<div class="client-search-box col-sm-6">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/client/search/">
        <h3>Client Search</h3>
        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Client Name" name="client_name" <? if(isset($searchresults['client_name'])) { echo 'value = "'.$searchresults['client_name'].'"';} ?> >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>

    <div class="client-search-box col-sm-6">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/clientcontact/search/">
        <h3>Client Contact Search</h3>
        <div class="form-group">
          <div class="col-sm-12">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Contact Name" name="clientcontact_name" <? if(isset($searchresults['clientcontact_name'])) { echo 'value = "'.$searchresults['clientcontact_name'].'"';} ?> >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>
    <div style="clear:both;"></div>
    <br/><br/>
  	<a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
    <br/><br/>
    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg">
        <?php
        $letter = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
          foreach ($letter as $char) { ?>
            <li <? if (isset($selectedletter)){ if ($selectedletter == $char){ echo 'class="active"'; } } ?> ><a href="<? echo base_url(); ?>index.php/client/filter/<? echo $char; ?>/"><? echo $char; ?></a></li>
         <? } ?>
      </ul>
    </div>
  	<table class="table table-striped table-responsive" id="client-data">
  		<thead>
      <tr>
  			<th></th>
  			<th>Contact Name</th>
        <th>Client Name</th>
  			<th></th>
        <th></th>
  			<th></th>
  		</tr>
      </thead>
          <tbody id="myTable">
  		<? foreach ($clientcontact as $clientcontactdata) { ?>
  			<tr>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/client/view/<? echo $clientcontactdata['clientID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><a href="<? echo base_url(); ?>index.php/clientcontact/view/<? echo $clientcontactdata['clientID']; ?>/" ><? echo $clientcontactdata['contact_name']; ?></a></td>
          <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientcontactdata['clientID']; ?>/" >
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$clientcontactdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <? echo $row['client_name']; ?>
                <? }
            } else { ?>
                No Client
  <? } ?>
          </a></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/clientadd/<? echo $clientcontactdata['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Add Project</a></td>
  				<td><a href="<? echo base_url(); ?>index.php/client/edit/<? echo $clientcontactdata['clientID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/client/delete/<? echo $clientcontactdata['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
  			</tr>
  		<? } ?>
    </tbody>
  	</table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>