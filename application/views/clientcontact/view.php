<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1><? echo $clientcontact[0]['contact_name']; ?> <? echo $clientcontact[0]['contact_lastname']; ?></h1>
  <a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientcontact[0]['clientID']; ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/clientcontact/edit/<? echo $clientcontact[0]['contactID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
  <br/><br/>
  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Address: </b><br/><br/><br/></td>
        <td>
          <? echo $clientcontact[0]['contact_add_num']; ?> <br/> <? echo $clientcontact[0]['contact_add_street']; ?><br/>
          <? echo $clientcontact[0]['contact_add_city']; ?> <? echo $clientcontact[0]['contact_add_state']; ?><br/>
          <? echo $clientcontact[0]['contact_add_country']; ?> <? echo $clientcontact[0]['contact_add_postcode']; ?><br/>
        </td>
      </tr>
    </table>

    <table>
      <tr>
        <td style="padding-right:5px"><b>Job Title: </b>  </td>
        <td>
          <? echo $clientcontact[0]['contact_jobtitle']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Phone: </b>  </td>
        <td>
          <? echo $clientcontact[0]['contact_phone']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Mobile: </b>  </td>
        <td>
          <? echo $clientcontact[0]['contact_mobile']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Fax: </b>  </td>
        <td>
          <? echo $clientcontact[0]['contact_fax']; ?>
        </td>
      </tr>
    </table>

    <table>
      <tr>
        <td style="padding-right:5px"><b>Email: </b>  </td>
        <td>
          <a href="mailto:<? echo $clientcontact[0]['contact_email']; ?>"><? echo $clientcontact[0]['contact_email']; ?></a>
        </td>
      </tr>

      <tr>
        <td style="padding-right:5px"><b>Hot Drink Preference: </b>  </td>
        <td>
          <? echo $clientcontact[0]['contact_hotdrink']; ?>
        </td>
      </tr>
    </table>
  </div>  	

</div>

</div>