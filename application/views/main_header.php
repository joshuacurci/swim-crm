<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><? echo $pagetitle; ?></title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-datepicker.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>tinymce.init({selector:'textarea'});</script>
<script>tinymce.init({selector:'.mynotes', inline: true});</script>
	
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button aria-controls="navbar" aria-expanded="false" data-target="<? echo base_url(); ?>index.php/login/logout" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            Logout
          </button>
          <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a href="<? echo base_url(); ?>index.php/client/index" class="navbar-brand"><img src="<?php echo base_url(); ?>images/main-logo.png" height="50px" /></a>
        </div>
        <div class="navbar-collapse collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li <? if($menuitem=="1"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/home/index">Dashboard</a></li>
            <li <? if($menuitem=="2"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/client/index">Clients</a></li>
            <li <? if($menuitem=="3"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/salesproposals/index">Sales in progress</a></li>
            <li <? if($menuitem=="4"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/projects/index">Projects</a></li>
            <li <? if($menuitem=="5"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/tickets/index">Support Tickets</a></li>
            <li <? if($menuitem=="6"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/domains/index">Domains</a></li>
            <li <? if($menuitem=="7"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/recuring/index">Recurring</a></li>
            <li <? if($menuitem=="8"){ echo 'class="active"';} ?>><a href="<? echo base_url(); ?>index.php/staff/index">Staff</a></li>
          </ul>
          <ul class="nav navbar-nav navbar-right mobilehide">
              <li><a href="<? echo base_url(); ?>index.php/debug/form" style="padding:0px; margin-right:10px;" target="_blank"><img src="<? echo base_url(); ?>images/debug-icon.png" height="40px"></a></li>
              <li class="login-style"><a href="<? echo base_url(); ?>index.php/login/logout">Logout</a></li>
              </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>