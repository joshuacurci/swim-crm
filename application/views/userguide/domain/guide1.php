<div role="main" class="container-fluid main-wrapper theme-showcase">
	<div class="col-sm-12">
		<h2>Type of domain data</h2>
		<p>Most domains are in the CRM but the data is not complete yet. If you make any changes for a client on a domain eg. Email change, DNS change, Go Live etc. Please go in and update the domains details. There are a few areas where we need to put different data and I have outlined them bellow:</p>
<ul>
  <li><strong>Renewal Items</strong> – This is for anything that renews in a selected time frame or has an ongoing cost. Examples of this are SSLs, monthly subscriptions that includes security and maintenance and plugin subscriptions.</li>
  <li><strong>Attributes</strong> – This is for any points for information about the domain at are special to it. Some examples of this are the CMS, custom plugins, custom code, custom installations, EDI and API connections, Bank Connections etc.</li>
  <li><strong>Notes</strong> – This section is for changes to the domains that have been requested. Things like sub domains, A record changes, email set ups, go live dates, roll back requests etc</li>
</ul>
<p>With inputting all of this information we should have a very good database of all the domains and what is currently running on them.</p>

	</div>
</div>
