<div role="main" class="container-fluid main-wrapper theme-showcase">
	<div class="col-sm-12">
		<h2>Adding a new job bag/project</h2>
		The job bag system that’s in place will keep the numbers in order and prevent double ups. To ensure the right order number please make sure you are adding Job Bags in the following order.
		<ol>
			<li>Create Job bag in CRM (make sure to include as many details as possible)</li>
			<li>Find the Job Bag in the pile on the shelf</li>
			<li>Fill in the Job bag with the same details as the CRM</li>
			<li>Complete the creation of job bags by putting them into the the physical greenbook</li>
		</ol>
		If you go to grab a job bag from the pile and end up skipping a few then that’s ok. The CRM is meant to automatically create a new job bag for Renewals so the one skipped could simply be a renewal. There is no more need to put the H or the A at the end of the job bag number. If it’s hosting or an annual job bag it should be entered as a recurring item and not with the letters any more.


	</div>
</div>
