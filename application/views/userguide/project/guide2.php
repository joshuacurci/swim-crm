<div role="main" class="container-fluid main-wrapper theme-showcase">
	<div class="col-sm-12">
		<h2>Changing the status of the job bag</h2>
		<p>Here are a list of the different statuses and when they should be changed:</p>
		<ul>
		  <li><strong>Prospect</strong> – If we need a bag to track hours on a job that is not yet &lsquo;live&rsquo;</li>
		  <li><strong>Quote</strong> – If we have a job that a quote has been sent out for but need to track the project management hours for</li>
		  <li><strong>Job in progress</strong> – As soon as the job is live this should be set</li>
		  <li><strong>JB with Joe</strong> – When the studio has completed the job and has handed it into Joe for processing</li>
		  <li><strong>Completed</strong> – To be changed only by Joe once the job bag has been given to accounts ready to be invoiced</li>
		  <li><strong>Invoiced</strong> – Once the invoice has been sent out</li>
		  <li><strong>Paid</strong> – Once the client has paid the invoice</li>
		  <li><strong>Declined</strong> – If the client declines the job and no more work is to be done and no invoices are to be sent out</li>
		  <li><strong>Archived</strong> – To store old job bags that no longer need to appear in the search</li>
		  <li><strong>Canceled</strong> – If a job ever gets canceled</li>
		</ul>

	</div>
</div>
