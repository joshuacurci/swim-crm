<div role="main" class="container-fluid main-wrapper theme-showcase">

  <script>
  function updateFields(selectObject,projectID){
    window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchange/"+projectID+"/"+selectObject.value+"/index";
  };
  </script>

<div class="col-sm-12">
  <h1>Full Project List</h1>

  	<div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projects/search/">
        <h3>Search</h3>
        <div class="col-sm-6 search-box-item">
          <div class="search-title">Project Name:</div>
          <div class="search-field">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Project Name" name="project_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['project_name'].'"';} ?> >
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Job Bag #:</div>
          <div class="search-field">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Job Bag #" name="job_bag" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['job_bag'].'"';} ?> >
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Client:</div>
          <div class="search-field">
            <select class="form-control" id="" name="clientID">
              <option value="">All</option>
              <? foreach ($clientlist as $clientdata){ ?>
                <option <? if(isset($searchresults)) { if($searchresults['clientID'] == $clientdata['clientID']) {echo 'selected';} } ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Status:</div>
          <div class="search-field">
           <select class="form-control" id="" name="project_status">
              <option value="">All</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Prospect") {echo 'selected';} } ?> value="Prospect">Prospect</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Quote") {echo 'selected';} } ?> value="Quote">Quote</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Job in progress") {echo 'selected';} } ?> value="Job in progress">Job in progress</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Completed") {echo 'selected';} } ?> value="Completed">Completed</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Invoiced") {echo 'selected';} } ?> value="Invoiced">Invoiced</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Paid") {echo 'selected';} } ?> value="Paid">Paid</option>
              <option <? if(isset($searchresults)) { if($searchresults['project_status'] == "Declined") {echo 'selected';} } ?> value="Declined">Declined</option>
            </select>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>

  	<a href="<? echo base_url(); ?>index.php/projects/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Project</a>
    <a href="<? echo base_url(); ?>index.php/projects/wip/" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View WIP Report</a>
    <br/><br/>
  	<table class="table table-striped table-responsive" id="project-data">
      <thead>
  		<tr>
  			<th></th>
  			<th>Job Bag #</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
  			<th></th>
  			<th></th>
  		</tr>
      </thead>
      <tbody id="myTable">
  		<? foreach ($project as $projectdata) { ?>
  			<tr>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><span class="label 
              <? if ($row['client_light'] == 'G') { echo "label-success"; } 
              elseif ($row['client_light'] == 'Y') { echo "label-warning"; }
              elseif ($row['client_light'] == 'R') { echo "label-danger"; }
              ?>
             traffic-light"> </span>&nbsp;&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a>
            </td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
          <td>
            <select name="status" class="form-control" id="inlineStatusChange" onchange="updateFields(this,<? echo $project[0]['projectID'] ?>)" >
              <option <? if($projectdata['project_status'] == "Prospect") { echo "selected"; } ?> value="Prospect">Prospect</option>
              <option <? if($projectdata['project_status'] == "Quote") { echo "selected"; } ?> value="Quote">Quote</option>
              <option <? if($projectdata['project_status'] == "Job in progress") { echo "selected"; } ?> value="Job in progress">Job in progress</option>
              <option <? if($projectdata['project_status'] == "JB with Joe") { echo "selected"; } ?> value="JB with Joe">JB with Joe</option>
              <option <? if($projectdata['project_status'] == "Completed") { echo "selected"; } ?> value="Completed">Completed</option>
              <option <? if($projectdata['project_status'] == "Invoiced") { echo "selected"; } ?> value="Invoiced">Invoiced</option>
              <option <? if($projectdata['project_status'] == "Paid") { echo "selected"; } ?> value="Paid">Paid</option>
              <option <? if($projectdata['project_status'] == "Declined") { echo "selected"; } ?> value="Declined">Declined</option>
              <option <? if($projectdata['project_status'] == "Archived") { echo "selected"; } ?> value="Archived">Archived</option>
              <option <? if($projectdata['project_status'] == "Canceled") { echo "selected"; } ?> value="Canceled">Canceled</option>
          </select>
          </td>
  				<td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
  			</tr>
  		<? } ?>
  	</tbody>
    </table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>

