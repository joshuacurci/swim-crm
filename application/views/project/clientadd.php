<?
  if (($currentJB[0]['global_value'] > 7500 && $currentJB[0]['global_value'] < 8000)){ 
    $newJB = '8000';
  } else {
    $newJB = $currentJB[0]['global_value']+1;
  }
?>

<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Add new project</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projects/newproject/">
  <input type="hidden" name="staffID" value="<? echo $staffID; ?>" >
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Job Bag #:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Job Bag #" name="job_bag" value="<? echo $newJB ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Is this recurring?:</label>
          <div class="col-sm-9">
            <label class="checkbox-inline">
              <input type="radio" id="inlineCheckbox1" name="recuring" value="Y" onclick="dynInput(this);"> Yes
            </label>
            <label class="checkbox-inline">
              <input type="radio" id="inlineCheckbox2" name="recuring" value="N" checked onclick="dynInput(this);"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div id="recuringinfo" class="form-group" style="display:none;">
          <div class="col-sm-9 col-sm-offset-3">
            <table>
              <tr>
                <td class="bold">
                  Renewal Frequency
                </td>
                <td>
                  <select class="form-control" id="inputorg1" name="rec_frequency">
                    <option value="M">Monthly</option>
                    <option value="B">Bi-Monthly</option>
                    <option value="Q">Quarterly</option>
                    <option value="A">Annually</option>
                  </select>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Days before expiry to be notified
                </td>
                <td>
                  <input type="text" class="form-control" id="inputrecNum1" placeholder="Days before expiry to be notified eg. 30, 60, 90" name="rec_notify" >
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Rate per frequency period
                </td>
                <td>
                  <input type="text" class="form-control" id="inputrecNum1" placeholder="Rate per frequency period" name="rec_rate" >
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Domain (if any)
                </td>
                <td>
                  <select class="form-control" id="inputorg1" name="domainID">
                    <option value="0">No Domain</option>
                    <? foreach ($domainlist as $domaindata){ ?>
                      <option value="<? echo $domaindata['domainID'] ?>"><? echo $domaindata['domain_name'] ?></option>
                    <? } ?>
                  </select>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Start Date
                </td>
                <td>
                  <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="rec_startdate" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Last Renewal Date (can be the same as/or before start date)
                </td>
                <td>
                  <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="rec_lastdate" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Next Renewal Date (leave blank for it to be automaticly calculated)
                </td>
                <td>
                  <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="rec_nextrenew" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>
                  </div>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Details
                </td>
                <td>
                  <textarea id="inputDetails1" placeholder="Insert note here..." name="rec_details">
                  </textarea>
                </td>
              </tr>
            </table>
          </div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="clientID">
              <? foreach ($clientlist as $clientdata){ ?>
                <option <? if($clientID == $clientdata['clientID']){ echo "selected";} ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Sales rep:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="salesuserID">
              <? foreach ($stafflist as $staffdata){ ?>
                <option <? if($staffID == $staffdata['userID']){ echo "selected";} ?> value="<? echo $staffdata['userID'] ?>"><? echo $staffdata['name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Project Name" name="project_name" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Type:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_type">
              <option value="Website Build - Wordpress">Website Build - Wordpress</option>
              <option value="Website Build - CS-Cart">Website Build - CS-Cart</option>
              <option value="Website Build - Other">Website Build - Other</option>
              <option value="Website Alt">Website Alt</option>
              <option value="EDM Design">EDM Design</option>
              <option value="EDM Update">EDM Update</option>
              <option value="Domain Registration">Domain Registration</option>
              <option value="Domain Renewal">Domain Renewal</option>
              <option value="Hosting">Hosting</option>
              <option value="Security and Maintenance">Security and Maintenance</option>
              <option value="Social Media Works">Social Media Works</option>
              <option value="SSL">SSL</option>
              <option value="Support">Support</option>
              <option value="Design">Design</option>
              <option value="Video">Video</option>
              <option value="Other">Other</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_status">
              <option value="Prospect">Prospect</option>
              <option value="Quote">Quote</option>
              <option value="Job in progress">Job in progress</option>
              <option value="JB with Joe">JB with Joe</option>
              <option value="Completed">Completed</option>
              <option value="Invoiced">Invoiced</option>
              <option value="Paid">Paid</option>
              <option value="Declined">Declined</option>
              <option value="Archived">Archived</option>
              <option value="Canceled">Canceled</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Budget:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Project Budget (eg. 30000)" name="project_budget" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Priority:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_priority">
              <option value="High">High</option>
              <option value="Medium" selected>Medium</option>
              <option value="Low">Low</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Is this a work bag?:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_workbag">
              <option value="No" selected>No</option>
              <option value="Yes - Preinvoiced">Yes - Preinvoiced</option>
              <option value="Yes">Yes</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Staff Assigned to:</label>
          <div class="col-sm-9">
            <? foreach ($stafflist as $staffdata){ ?>
                <label class="checkbox-inline">
                  <input type="checkbox" id="inlineCheckbox1" value="<? echo $staffdata['userID'] ?>" name="assigned_staff[]"> <? echo $staffdata['name'] ?>
                </label>
              <? } ?>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Due Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="project_duedate" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Follow up Date:</label>
          <div class="col-sm-9">
            <div class="controls">
            <div class="input-group">
                <input id="date-picker-3" type="text" class="date-picker form-control" name="project_followupdate" />
                <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
             </div>
             </div>
            <div style="clear:both"></div>
        </div>

                

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientid; ?>/" class="btn btn-default">Cancel</a>
          </div>
        </div>

  </form>
    

</div>

</div>