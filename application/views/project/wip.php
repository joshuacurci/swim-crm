<div role="main" class="container-fluid main-wrapper theme-showcase">

<script>
  function updateFields(selectObject,projectID){
    window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchange/"+projectID+"/"+selectObject.value+"/wip";
  };
  </script>

<div class="col-sm-12">
  <h1>WIP Report</h1>

  	<a href="<? echo base_url(); ?>index.php/projects/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
    <br/><br/>
  	<table class="table table-hover table-responsive" id="">
      <thead>
  		<tr>
  			<th></th>
  			<th>Job Bag #</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
        <th>Created Date</th>
  		</tr>
      </thead>
      <tbody>
  		<? foreach ($project as $projectdata) { ?>
  			<tr id="linked-<? echo $projectdata['projectID']; ?>">
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><span class="label 
              <? if ($row['client_light'] == 'G') { echo "label-success"; } 
              elseif ($row['client_light'] == 'Y') { echo "label-warning"; }
              elseif ($row['client_light'] == 'R') { echo "label-danger"; }
              ?>
             traffic-light"> </span>&nbsp;&nbsp;&nbsp;<a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a>
            </td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a>
            <?php if ($projectdata['project_type'] == 'Security and Maintenance') { ?>
      
          <?  $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
          $conn = new mysqli($servername, $username, $password, $dbname);
          if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
            $sql = "SELECT * FROM tbl_projects_security WHERE projectID = ".$projectdata['projectID']."";
            $result = $conn->query($sql);
            $num_rec = $result->num_rows;
            if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>  
              <br/>            
              <? if(date('m') >= 1 && $row['security_march'] == 'N'){ echo '<span style="color:red;">'; }
              elseif ($row['security_march'] == 'Y') { echo '<span style="color:green;">'; }
              else { echo '<span style="color:grey;">'; } ?>
                <span class="glyphicon <? if($row['security_march'] == 'Y'){ echo 'glyphicon-ok'; } else { echo 'glyphicon-remove'; } ?>" aria-hidden="true"></span> January &nbsp;&nbsp;
              </span>

              <? if(date('m') >= 4 && $row['security_june'] == 'N'){ echo '<span style="color:red;">'; }
              elseif ($row['security_june'] == 'Y') { echo '<span style="color:green;">'; }
              else { echo '<span style="color:grey;">'; } ?>
              <span class="glyphicon <? if($row['security_june'] == 'Y'){ echo 'glyphicon-ok'; } else { echo 'glyphicon-remove'; } ?>" aria-hidden="true"></span> April &nbsp;&nbsp;
              </span>
              
              <? if(date('m') >= 7 && $row['security_sep'] == 'N'){ echo '<span style="color:red;">'; }
              elseif ($row['security_sep'] == 'Y') { echo '<span style="color:green;">'; }
              else { echo '<span style="color:grey;">'; } ?>
              <span class="glyphicon <? if($row['security_sep'] == 'Y'){ echo 'glyphicon-ok'; } else { echo 'glyphicon-remove'; } ?>" aria-hidden="true"></span> July &nbsp;&nbsp;
              </span>
              
              <? if(date('m') >= 10 && $row['security_dec'] == 'N'){ echo '<span style="color:red;">'; }
              elseif ($row['security_dec'] == 'Y') { echo '<span style="color:green;">'; }
              else { echo '<span style="color:grey;">'; } ?>
              <span class="glyphicon <? if($row['security_dec'] == 'Y'){ echo 'glyphicon-ok'; } else { echo 'glyphicon-remove'; } ?>" aria-hidden="true"></span> October &nbsp;&nbsp;
              </span>
              <? } 
            } else { ?>
                  
            <? } ?>
            <? } ?>

          </td>

          <td><select name="status" class="form-control" id="inlineStatusChange" onchange="updateFields(this,<? echo $project[0]['projectID'] ?>)" >
              <option <? if($projectdata['project_status'] == "Prospect") { echo "selected"; } ?> value="Prospect">Prospect</option>
              <option <? if($projectdata['project_status'] == "Quote") { echo "selected"; } ?> value="Quote">Quote</option>
              <option <? if($projectdata['project_status'] == "Job in progress") { echo "selected"; } ?> value="Job in progress">Job in progress</option>
              <option <? if($projectdata['project_status'] == "JB with Joe") { echo "selected"; } ?> value="JB with Joe">JB with Joe</option>
              <option <? if($projectdata['project_status'] == "Completed") { echo "selected"; } ?> value="Completed">Completed</option>
              <option <? if($projectdata['project_status'] == "Invoiced") { echo "selected"; } ?> value="Invoiced">Invoiced</option>
              <option <? if($projectdata['project_status'] == "Paid") { echo "selected"; } ?> value="Paid">Paid</option>
              <option <? if($projectdata['project_status'] == "Declined") { echo "selected"; } ?> value="Declined">Declined</option>
              <option <? if($projectdata['project_status'] == "Archived") { echo "selected"; } ?> value="Archived">Archived</option>
              <option <? if($projectdata['project_status'] == "Canceled") { echo "selected"; } ?> value="Canceled">Canceled</option>
          </select></td>
          <td><? echo date('d/m/Y',$projectdata['project_createdate']); ?></td>           

  			</tr>
  		<? } ?>
  	</tbody>
    </table>

</div>

</div>