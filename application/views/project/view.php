<div role="main" class="container-fluid main-wrapper theme-showcase">

<div clsas="col-sm-12">
  <h1><? echo $project[0]['project_name']; ?></h1>

   <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$project[0]['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <h3><? echo $row['client_name']; ?></h3>
                <? }
            } else { ?>
                <h3>No Client</h3>
  <? } ?>
  
  <a href="<? echo base_url(); ?>index.php/projects/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/projects/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Project</a>
  <a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $project[0]['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
  <br/><br/>
  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Job Bag #: </b></td>
        <td>
          <? echo $project[0]['job_bag']; ?><br/>
        </td>
      </tr>

<? if(count($salesconected) > 0 && $salesconected[0]['salesID'] != '0') { ?>
  <tr>
        <td style="padding-right:5px"><b>Conected to proposal: </b></td> <?
    foreach($salesconected as $conected) { ?>
        <?  $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
          $conn = new mysqli($servername, $username, $password, $dbname);
          if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
              $sql = "SELECT * FROM tbl_salesproposals WHERE salesID = ".$conected['salesID']."";
          $result = $conn->query($sql);
          $num_rec = $result->num_rows;
          if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $row['salesID']; ?>/"><? echo $row['sales_title']; ?></a> </td>
              <? } } else { ?>
                  
        <? } ?>
    <? } ?>
  </tr>

  <? } ?>

      <tr>
        <td style="padding-right:5px"><b>Project Type: </b></td>
        <td>
          <? echo $project[0]['project_type']; ?><br/>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Project Status: </b></td>
        <td>
          <select name="status" class="form-control" id="inlineStatusChange" >
              <option <? if($project[0]['project_status'] == "Prospect") { echo "selected"; } ?> value="Prospect">Prospect</option>
              <option <? if($project[0]['project_status'] == "Quote") { echo "selected"; } ?> value="Quote">Quote</option>
              <option <? if($project[0]['project_status'] == "Job in progress") { echo "selected"; } ?> value="Job in progress">Job in progress</option>
              <option <? if($project[0]['project_status'] == "JB with Joe") { echo "selected"; } ?> value="JB with Joe">JB with Joe</option>
              <option <? if($project[0]['project_status'] == "Completed") { echo "selected"; } ?> value="Completed">Completed</option>
              <option <? if($project[0]['project_status'] == "Invoiced") { echo "selected"; } ?> value="Invoiced">Invoiced</option>
              <option <? if($project[0]['project_status'] == "Paid") { echo "selected"; } ?> value="Paid">Paid</option>
              <option <? if($project[0]['project_status'] == "Declined") { echo "selected"; } ?> value="Declined">Declined</option>
              <option <? if($project[0]['project_status'] == "Archived") { echo "selected"; } ?> value="Archived">Archived</option>
              <option <? if($project[0]['project_status'] == "Canceled") { echo "selected"; } ?> value="Canceled">Canceled</option>
          </select>
            
          <script>
            $("#inlineStatusChange").change(function(){
              window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchange/<? echo $project[0]['projectID'] ?>/"+$(this).val();
            });
          </script>
        </td>
      </tr>
      <?php if ($project[0]['project_type'] == 'Security and Maintenance') { ?>
      <tr>
        <td style="padding-right:5px"><b>Update Status: </b></td>
        <td>
          <?  $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
          $conn = new mysqli($servername, $username, $password, $dbname);
          if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
              $sql = "SELECT * FROM tbl_projects_security WHERE projectID = ".$project[0]['projectID']."";
          $result = $conn->query($sql);
          $num_rec = $result->num_rows;
          if ($result->num_rows > 0) {
              while($row = $result->fetch_assoc()) { ?>              
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckboxM" value="M" <? if($row['security_march'] == 'Y'){ echo 'checked'; } ?>> January
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckboxJ" value="J" <? if($row['security_june'] == 'Y'){ echo 'checked'; } ?>> April
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckboxS" value="S" <? if($row['security_sep'] == 'Y'){ echo 'checked'; } ?>> July
          </label>
          <label class="checkbox-inline">
            <input type="checkbox" id="inlineCheckboxD" value="D" <? if($row['security_dec'] == 'Y'){ echo 'checked'; } ?>> October
          </label>
          <? } } else { ?>
                  
        <? } ?>

            
          <script>
            $('#inlineCheckboxM').change(function(){
                var c = this.checked ? '<?php echo $_SESSION['userID']; ?>' : 'NA';
                window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchangesecurity/<? echo $project[0]['projectID'] ?>/"+c+"/M";
            });

            $('#inlineCheckboxJ').change(function(){
                var c = this.checked ? '<?php echo $_SESSION['userID']; ?>' : 'NA';
                window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchangesecurity/<? echo $project[0]['projectID'] ?>/"+c+"/J";
            });

            $('#inlineCheckboxS').change(function(){
                var c = this.checked ? '<?php echo $_SESSION['userID']; ?>' : 'NA';
                window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchangesecurity/<? echo $project[0]['projectID'] ?>/"+c+"/S";
            });

            $('#inlineCheckboxD').change(function(){
                var c = this.checked ? '<?php echo $_SESSION['userID']; ?>' : 'NA';
                window.location.href = "<?php echo base_url(); ?>index.php/projects/quickchangesecurity/<? echo $project[0]['projectID'] ?>/"+c+"/D";
            });

          </script>
        </td>
      </tr>
      <? } ?>
    </table>
  </div>

  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Project Budget: </b>  </td>
        <td>
          <? if($project[0]['project_budget'] == '') { ?>
            No set budget
          <? } else { ?>
            <? echo "$".$project[0]['project_budget']." +GST"; ?>
          <? } ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Project Priority: </b>  </td>
        <td>
          <? if ($project[0]['project_priority'] == 'High'){echo '<span class="label label-danger">'.$project[0]['project_priority'].'</span>';}
            elseif ($project[0]['project_priority'] == 'Medium'){echo '<span class="label label-warning">'.$project[0]['project_priority'].'</span>';}
            elseif ($project[0]['project_priority'] == 'Low'){echo '<span class="label label-primary">'.$project[0]['project_priority'].'</span>';}
           ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right: 5px; vertical-align: top;"><b>Staff Assigned to: </b>  </td>
        <td>
          <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_projects_assign WHERE projectID = ".$project[0]['projectID']."";
              $result = $conn->query($sql);
              
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { 
                    $sql2 = "SELECT * FROM tbl_users WHERE userID = ".$row['userID']."";
                    $result2 = $conn->query($sql2);
                      $num_rec2 = $result2->num_rows;
                      if ($result2->num_rows > 0) {
                          while($row2 = $result2->fetch_assoc()) { ?>
                            <? echo $row2['name']."<br/>"; ?>
                          <? }
                      } else { ?>
                          Not Assigned
                    <? } 
            } } ?>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Due Date: </b>  </td>
        <td>
          <? echo date('d/m/Y',$project[0]['project_duedate']); ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Follow up date: </b>  </td>
        <td>
          <? echo date('d/m/Y',$project[0]['project_followupdate']); ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Created Date: </b>  </td>
        <td>
          <? echo date('d/m/Y H:i:s',$project[0]['project_createdate']); ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Is this a work bag?: </b>  </td>
        <td>
          <? echo $project[0]['project_workbag']; ?>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-12">
  <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_recuring_item WHERE projectID = ".$project[0]['projectID']."";
              $result = $conn->query($sql);
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { 
                    $sql2 = "SELECT * FROM tbl_recuring WHERE recID = ".$row['recID']."";
                    $result2 = $conn->query($sql2);
                    $num_rec2 = $result2->num_rows;
                      if ($result2->num_rows > 0) {
                          while($row2 = $result2->fetch_assoc()) { ?>
                           <b>Conected to recurring item:</b> <a href="<? echo base_url(); ?>recuring/view/<? echo $row2['recID'];?>/" target="_blank">View item</a>
                          <? }
                      }
                  } 
              } ?>
            </div>


  <div class="col-sm-12 project-info">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tasks">Tasks</a></li>
      <li><a data-toggle="tab" href="#hours">Hours</a></li>
      <li><a data-toggle="tab" href="#expences">Expences</a></li>
      <li><a data-toggle="tab" href="#contact">Project Contacts</a></li>
      <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
      <div id="tasks" class="tab-pane fade in active">
        <a href="<? echo base_url(); ?>index.php/projecttask/add/<? echo $project[0]['projectID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Task</a>
        <a href="<? echo base_url(); ?>index.php/projecttask/complete/<? echo $project[0]['projectID']; ?>" class="btn btn-primary"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View Completed Tasks</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Assigned to</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($project_tasks)) { ?>
          <? foreach ($project_tasks as $taskdata) { ?>
            <tr <? if ($taskdata['task_status'] == 'C'){echo 'class="success"';}?> >
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_users WHERE userID = ".$taskdata['assuserID']."";
              $result = $conn->query($sql);
              
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['name']; ?></td>
                  <? }
              } else { ?>
                  <td>Not Assigned</td>
              <? } ?>

              <td><? echo date('d/m/Y',$taskdata['task_duedate']); ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecthours/addtask/<? echo $project[0]['projectID']; ?>/<? echo $taskdata['projecttaskID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Hours</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/markcomplete/<? echo $project[0]['projectID']; ?>/<? echo $taskdata['projecttaskID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-ok" aria-hidden="true"></span> Mark Complete</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>

      <div id="hours" class="tab-pane fade">
        <a href="<? echo base_url(); ?>index.php/projecthours/add/<? echo $project[0]['projectID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Log Hours</a>

        <div class="total-hours">Total Hours:
          <? $totalhours = 0 ?>
          <? if (isset($project_hours)) { ?>
          <? foreach ($project_hours as $hourdata) { 
              $totalhours = $totalhours + $hourdata['hour_amount'];
           } ?>
        <? } ?>
        <? echo $totalhours.'h'; ?>
        </div>

        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Date Done</th>
            <th>Title</th>
            <th>Hour Amount</th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($project_hours)) { ?>
          <? foreach ($project_hours as $hourdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecthours/view/<? echo $hourdata['hourID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo date('d/m/Y', $hourdata['hour_datedone']); ?></td>
              <td><? echo $hourdata['hour_title']; ?></td>
              <td><? echo $hourdata['hour_amount']."h"; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecthours/edit/<? echo $hourdata['hourID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecthours/delete/<? echo $hourdata['hourID']; ?>/<? echo $hourdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>

      <div id="expences" class="tab-pane fade">
        <a href="<? echo base_url(); ?>index.php/projectexpences/add/<? echo $project[0]['projectID']; ?>/<? echo $project[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Expence</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Expence Name</th>
            <th>Submitted by</th>
            <th>Authorised?</th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($project_expences)) { ?>
          <? foreach ($project_expences as $expencesdata) { ?>
            <tr >
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projectexpences/view/<? echo $expencesdata['expenceID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $expencesdata['expence_name']; ?></td>

              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_users WHERE userID = ".$expencesdata['userID']."";
              $result = $conn->query($sql);
              
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><? echo $row['name']; ?></td>
                  <? }
              } else { ?>
                  <td>Master</td>
              <? } ?>

              <td><? if ($expencesdata['expence_auth'] == 'N' || $expencesdata['expence_auth'] == ''){echo '<span class="label label-danger">No</span>';}
            elseif ($expencesdata['expence_auth'] == 'Y'){echo '<span class="label label-primary">Yes</span>';} ?></td>

              <td><a href="<? echo base_url(); ?>index.php/projectexpences/edit/<? echo $expencesdata['expenceID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projectexpences/delete/<? echo $expencesdata['expenceID']; ?>/<? echo $expencesdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>

      <div id="contact" class="tab-pane fade">
        <a href="<? echo base_url(); ?>index.php/projectcontact/assign/<? echo $project[0]['projectID']; ?>/<? echo $project[0]['clientID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Assign/Remove Contact</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Contact Name</th>
            <th>Phone</th>
            <th>Mobile</th>
            <th>Email</th>
          </tr>
        <? if (isset($project_contacts)) { ?>
          <? foreach ($project_contacts as $contactid) { ?>
            <tr>
              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_client_contact WHERE contactID = ".$contactid['contactID']."";
              $result = $conn->query($sql);
              
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/clientcontact/view/<? echo $row['contactID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
                    <td><? echo $row['contact_name']; ?> <? echo $row['contact_lastname']; ?></td>
                    <td><? echo $row['contact_phone']; ?></td>
                    <td><? echo $row['contact_mobile']; ?></td>
                    <td><? echo $row['contact_email']; ?></td>  
                  <? }
              } else { ?>
              <? } ?>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>

      <div id="notes" class="tab-pane fade">
        <a href="<? echo base_url(); ?>index.php/projectnotes/add/<? echo $project[0]['projectID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Note</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Title</th>
            <th>Date</th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($project_notes)) { ?>
          <? foreach ($project_notes as $notedata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projectnotes/view/<? echo $notedata['noteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $notedata['note_title']; ?></td>
              <td><? echo date('d/m/Y', $notedata['note_date']); ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projectnotes/edit/<? echo $notedata['noteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projectnotes/delete/<? echo $notedata['noteID']; ?>/<? echo $notedata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>
    </div>
  </div>
  	

</div>

</div>