<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Edit project</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? if (isset($error)){ ?><? echo base_url(); ?>index.php/projects/newproject/<? } else { ?><? echo base_url(); ?>index.php/projects/save/<? } ?>">
  <? if (isset($project[0]['projectID'])){ ?><input type="hidden" name="projectID" value="<? echo $project[0]['projectID']; ?>" > <? } ?>
        <? if (isset($error)){ ?><div class="alert alert-danger" role="alert"><strong>Job Bag already exsists!</strong> Please confirm you have the right job bag number and try again.</div> <? } ?>
<input type="hidden" name="staffID" value="<? echo $staffID; ?>" >

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Job Bag #:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Job Bag #" name="job_bag" value="<? echo $project[0]['job_bag']; ?>" disabled>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group hidden">
          <label for="inputrecNum1" class="col-sm-3 control-label">Is this recurring?:</label>
          <div class="col-sm-9">
            <label class="checkbox-inline">
              <input type="radio" id="inlineCheckbox1" name="recuring" value="Y" onclick="dynInput(this);"> Yes
            </label>
            <label class="checkbox-inline">
              <input type="radio" id="inlineCheckbox2" name="recuring" value="N" checked onclick="dynInput(this);"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div id="recuringinfo" class="form-group" style="display:none;">
          <div class="col-sm-9 col-sm-offset-3">
            <table>
              <tr>
                <td class="bold">
                  Frequency
                </td>
                <td>
                  <select class="form-control" id="inputorg1" name="rec_frequency">
                    <option value="M">Monthly</option>
                    <option value="B">Bi-Monthly</option>
                    <option value="Q">Quarterly</option>
                    <option value="A">Annually</option>
                  </select>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Days before expiry to be notified
                </td>
                <td>
                  <input type="text" class="form-control" id="inputrecNum1" placeholder="Days before expiry to be notified eg. 30, 60, 90" name="rec_notify" >
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Rate per frequency period
                </td>
                <td>
                  <input type="text" class="form-control" id="inputrecNum1" placeholder="Rate per frequency period" name="rec_rate" >
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Domain (if any)
                </td>
                <td>
                  <select class="form-control" id="inputorg1" name="domainID">
                    <option value="0">No Domain</option>
                    <? foreach ($domainlist as $domaindata){ ?>
                      <option value="<? echo $domaindata['domainID'] ?>"><? echo $domaindata['domain_name'] ?></option>
                    <? } ?>
                  </select>
                </td>
              </tr>

              <tr>
                <td class="bold">
                  Details
                </td>
                <td>
                  <textarea id="inputDetails1" placeholder="Insert note here..." name="rec_details">
                  </textarea>
                </td>
              </tr>
            </table>
          </div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="clientID">
              <? foreach ($clientlist as $clientdata){ ?>
                <option <? if($project[0]['clientID'] == $clientdata['clientID']) {echo "selected=selected";} ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Sales rep:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="salesuserID">
              <? foreach ($stafflist as $staffdata){ ?>
                <option <? if($project[0]['salesuserID'] == $staffdata['userID']) {echo "selected=selected";} ?> value="<? echo $staffdata['userID'] ?>"><? echo $staffdata['name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Project Name" name="project_name" value="<? echo $project[0]['project_name']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Type:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_type">
              <option <? if($project[0]['project_type'] == 'Website Build - Wordpress') {echo "selected=selected";} ?> value="Website Build - Wordpress">Website Build - Wordpress</option>
              <option <? if($project[0]['project_type'] == 'Website Build - CS-Cart') {echo "selected=selected";} ?> value="Website Build - CS-Cart">Website Build - CS-Cart</option>
              <option <? if($project[0]['project_type'] == 'Website Build - Other') {echo "selected=selected";} ?> value="Website Build - Other">Website Build - Other</option>
              <option <? if($project[0]['project_type'] == 'Website Alt') {echo "selected=selected";} ?> value="Website Alt">Website Alt</option>
              <option <? if($project[0]['project_type'] == 'EDM Design') {echo "selected=selected";} ?> value="EDM Design">EDM Design</option>
              <option <? if($project[0]['project_type'] == 'EDM Update') {echo "selected=selected";} ?> value="EDM Update">EDM Update</option>
              <option <? if($project[0]['project_type'] == 'Domain Registration') {echo "selected=selected";} ?> value="Domain Registration">Domain Registration</option>
              <option <? if($project[0]['project_type'] == 'Domain Renewal') {echo "selected=selected";} ?> value="Domain Renewal">Domain Renewal</option>
              <option <? if($project[0]['project_type'] == 'Hosting') {echo "selected=selected";} ?> value="Hosting">Hosting</option>
              <option <? if($project[0]['project_type'] == 'Security and Maintenance') {echo "selected=selected";} ?> value="Security and Maintenance">Security and Maintenance</option>
              <option <? if($project[0]['project_type'] == 'Social Media Works') {echo "selected=selected";} ?> value="Social Media Works">Social Media Works</option>
              <option <? if($project[0]['project_type'] == 'SSL') {echo "selected=selected";} ?> value="SSL">SSL</option>
              <option <? if($project[0]['project_type'] == 'Support') {echo "selected=selected";} ?> value="Support">Support</option>
              <option <? if($project[0]['project_type'] == 'Design') {echo "selected=selected";} ?> value="Design">Design</option>
              <option <? if($project[0]['project_type'] == 'Video') {echo "selected=selected";} ?> value="Video">Video</option>
              <option <? if($project[0]['project_type'] == 'Other') {echo "selected=selected";} ?> value="Other">Other</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_status">
              <option <? if($project[0]['project_status'] == "Prospect") { echo "selected"; } ?> value="Prospect">Prospect</option>
              <option <? if($project[0]['project_status'] == "Quote") { echo "selected"; } ?> value="Quote">Quote</option>
              <option <? if($project[0]['project_status'] == "Job in progress") { echo "selected"; } ?> value="Job in progress">Job in progress</option>
              <option <? if($project[0]['project_status'] == "JB with Joe") { echo "selected"; } ?> value="JB with Joe">JB with Joe</option>
              <option <? if($project[0]['project_status'] == "Completed") { echo "selected"; } ?> value="Completed">Completed</option>
              <option <? if($project[0]['project_status'] == "Invoiced") { echo "selected"; } ?> value="Invoiced">Invoiced</option>
              <option <? if($project[0]['project_status'] == "Paid") { echo "selected"; } ?> value="Paid">Paid</option>
              <option <? if($project[0]['project_status'] == "Declined") { echo "selected"; } ?> value="Declined">Declined</option>
              <option <? if($project[0]['project_status'] == "Archived") { echo "selected"; } ?> value="Archived">Archived</option>
              <option <? if($project[0]['project_status'] == "Canceled") { echo "selected"; } ?> value="Canceled">Canceled</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Budget:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Project Budget (eg. 30000)" name="project_budget" value="<? echo $project[0]['project_budget']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Priority:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_priority">
              <option <? if($project[0]['project_priority'] == "High") { echo "selected"; } ?> value="High">High</option>
              <option <? if($project[0]['project_priority'] == "Medium") { echo "selected"; } ?> value="Medium">Medium</option>
              <option <? if($project[0]['project_priority'] == "Low") { echo "selected"; } ?> value="Low">Low</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Is this a work bag?:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="project_workbag">
              <option <? if($project[0]['project_workbag'] == "No") { echo "selected"; } ?> value="No">No</option>
              <option <? if($project[0]['project_workbag'] == "Yes - Preinvoiced") { echo "selected"; } ?> value="Yes - Preinvoiced">Yes - Preinvoiced</option>
              <option <? if($project[0]['project_workbag'] == "Yes") { echo "selected"; } ?> value="Yes" selected>Yes</option>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Staff Assigned to:</label>
          <div class="col-sm-9">
            <? foreach ($stafflist as $staffdata){ ?>
                <label class="checkbox-inline">
                  <input type="checkbox" id="inlineCheckbox1" 
                  <? if (isset($error)){ } else {?>
                  <? foreach($assignedstaff as $staffassigned) {if($staffassigned['userID'] == $staffdata['userID']) {echo "checked";}} ?> 
                  <? } ?>

                  value="<? echo $staffdata['userID'] ?>" name="assigned_staff[]"> <? echo $staffdata['name'] ?>
                </label>
              <? } ?>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Project Due Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="project_duedate"
                    <? if (isset($error)){ ?>
                      value="<? echo $project[0]['project_duedate']; ?>" 
                    <? } else { ?>
                      <? if ($project[0]['project_duedate'] != "") { ?>
                      value="<? echo date('d-m-Y',$project[0]['project_duedate']); ?>" 
                      <? } ?> 
                    <? } ?> 
                   />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Follow up Date:</label>
          <div class="col-sm-9">
            <div class="controls">
            <div class="input-group">
                <input id="date-picker-3" type="text" class="date-picker form-control" name="project_followupdate" 
                  <? if (isset($error)){ ?>
                    value="<? echo $project[0]['project_followupdate']; ?>" 
                  <? } else { ?>
                    <? if ($project[0]['project_followupdate'] != "") { ?>
                    value="<? echo date('d-m-Y',$project[0]['project_followupdate']); ?>" 
                    <? } ?>
                  <? } ?>
                />
                <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
            </div>
             </div>
             </div>
            <div style="clear:both"></div>
        </div>

                

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<? echo base_url(); ?>index.php/projects/view/<? echo $project[0]['projectID']; ?>/" class="btn btn-default">Cancel</a>
          </div>
        </div>

  </form>
  	

</div>

</div>