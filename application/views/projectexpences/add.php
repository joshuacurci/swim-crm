<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>New Expense</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projectexpences/newexpence/">
      <input type="hidden" name="userID" value="<? echo $staffID; ?>" >
      <input type="hidden" name="projectID" value="<? echo $projectID; ?>" >
      <input type="hidden" name="clientID" value="<? echo $clientID; ?>" >


        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Nature of Expense:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Nature of Expense" name="expence_name" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Expense incurred:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Expense incurred" name="expence_value" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Expense markup:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Expense markup" name="expence_markup" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Has the client been charged:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="expence_clientcharged" id="inlineRadio1" value="Y"> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="expence_clientcharged" id="inlineRadio2" value="N"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Date supplier invoice due:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="expence_dateinvoicedue" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Date supplier paid:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-3" type="text" class="date-picker form-control" name="expence_supppaid" />
                  <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Were the goods recived in good order/condition?:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="expence_condition" id="inlineRadio1" value="Y"> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="expence_condition" id="inlineRadio2" value="N"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Has the detail been entered on the job bag?:</label>
          <div class="col-sm-9">
            <label class="radio-inline">
              <input type="radio" name="expence_jobbag" id="inlineRadio1" value="Y"> Yes
            </label>
            <label class="radio-inline">
              <input type="radio" name="expence_jobbag" id="inlineRadio2" value="N"> No
            </label>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Additional Notes:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert any comments/notes here..." name="expence_notes">
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>