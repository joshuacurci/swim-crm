<? $state = "" ?>

<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1><? echo $clientexpence[0]['expence_name']; ?></h1>
    <a href="<? echo base_url(); ?>index.php/projects/view/<? echo $clientexpence[0]['projectID']; ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
    <a href="<? echo base_url(); ?>index.php/projectexpences/edit/<? echo $clientexpence[0]['expenceID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
    <br/><br/>
    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Date supplier invoice due:</label>
      <div class="col-sm-9">
        <? echo date('d/m/Y H:i:s', $clientexpence[0]['expence_dateinvoicedue']); ?>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Date supplier paid:</label>
      <div class="col-sm-9">
        <? echo date('d/m/Y H:i:s', $clientexpence[0]['expence_supppaid']); ?>
      </div>
      <div style="clear:both"></div>
    </div>

    <div class="form-group">
      <label for="inputrecNum1" class="col-sm-3 control-label">Created by:</label>
      <div class="col-sm-9">
        <?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

            // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }

        $sql = "SELECT * FROM tbl_users WHERE userID = ".$clientexpence[0]['userID']."";
        $result = $conn->query($sql);

        $num_rec = $result->num_rows;

        if ($result->num_rows > 0) {
          while($row = $result->fetch_assoc()) { ?>
            <td><? echo $row['name']; ?></td>
            <? }
          } else { ?>
            <td>No Staff</td>
            <? } ?>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Value:</label>
          <div class="col-sm-9">
            <? echo $clientexpence[0]['expence_value']; ?>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Expence Markup:</label>
          <div class="col-sm-9">
            <? echo $clientexpence[0]['expence_markup']; ?>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Client Charged:</label>
          <div class="col-sm-9">
            <? if( $clientexpence[0]['expence_clientcharged'] == 'Y' ) {
              echo 'Client is charged';
            } else if ( $clientexpence[0]['expence_clientcharged'] == 'N' ) {
              echo 'Client is not charged';
            } else {
              echo 'Not set';
            }
            ?>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Were the goods recived in good order/condition?:</label>
          <div class="col-sm-9">
            <? if( $clientexpence[0]['expence_condition'] == 'Y' ) {
              echo 'In good order/condition';
            } else if ( $clientexpence[0]['expence_condition'] == 'N' ) {
              echo 'Not in good order/condition';
            } else {
              echo 'Not set';
            }
            ?>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Has the detail been entered on the job bag?:</label>
          <div class="col-sm-9">
            <? if( $clientexpence[0]['expence_jobbag'] == 'Y' ) {
              echo 'Entered in the Job Bag';
            } else if ( $clientexpence[0]['expence_jobbag'] == 'N' ) {
              echo 'Not entered in the Job Bag';
            } else {
              echo 'Not set';
            }
            ?>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Additional Notes:</label>
          <div class="col-sm-9">
            <? echo $clientexpence[0]['expence_notes']; ?>
          </div>
          <div style="clear:both"></div>
        </div>



      </div>

    </div>