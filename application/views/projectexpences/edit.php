<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Edit Expense</h1>

    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/projectexpences/save/">
      <input type="hidden" name="userID" value="<? echo $staffID; ?>" >
      <input type="hidden" name="expenceID" value="<? echo $clientexpence[0]['expenceID']; ?>" >
      <input type="hidden" name="projectID" value="<? echo $clientexpence[0]['projectID']; ?>" >
      <input type="hidden" name="clientID" value="<? echo $clientexpence[0]['clientID']; ?>" >

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Nature of Expense:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Nature of Expense" value="<?php echo $clientexpence[0]['expence_name']?>" name="expence_name" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Expense incurred:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Expense incurred" value="<?php echo $clientexpence[0]['expence_value']; ?>" name="expence_value" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Expense markup:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Expense markup" value="<?php echo $clientexpence[0]['expence_markup']; ?>" name="expence_markup" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Has the client been charged:</label>
        <div class="col-sm-9">
          <label class="radio-inline">
            <input checked="<?php if ($clientexpence[0]['expence_clientcharged']=="Y") echo "checked";?>" type="radio" name="expence_clientcharged" id="inlineRadio1" value="Y"> Yes
          </label>
          <label class="radio-inline">
            <input type="radio" checked="<?php if ($clientexpence[0]['expence_clientcharged']=="N") echo "checked";?>" name="expence_clientcharged" id="inlineRadio2" value="N"> No
          </label>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Date supplier invoice due:</label>
        <div class="col-sm-9">
          <div class="controls">
            <div class="input-group">
              <input id="date-picker-2" type="text" class="date-picker form-control" value="<?php echo date('d/m/Y', $clientexpence[0]['expence_dateinvoicedue']); ?>" name="expence_dateinvoicedue" />
              <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

              </label>
            </div>
          </div>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Date supplier paid:</label>
        <div class="col-sm-9">
          <div class="controls">
            <div class="input-group">
              <input id="date-picker-3" type="text" class="date-picker form-control" value="<?php echo date('d/m/Y', $clientexpence[0]['expence_supppaid']); ?>"  name="expence_supppaid" />
              <label for="date-picker-3" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

              </label>
            </div>
          </div>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Were the goods recived in good order/condition?:</label>
        <div class="col-sm-9">
          <label class="radio-inline">
            <input type="radio" checked="<?php if ($clientexpence[0]['expence_condition']== "Y") { echo "checked"; }?>" name="expence_condition" id="inlineRadio1" value="Y"> Yes
          </label>
          <label class="radio-inline">
            <input type="radio" checked="<?php if ($clientexpence[0]['expence_condition']== "N") { echo "checked"; }?>" name="expence_condition" id="inlineRadio2" value="N"> No
          </label>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Has the detail been entered on the job bag?:</label>
        <div class="col-sm-9">
          <label class="radio-inline">
            <input checked="<?php if ($clientexpence[0]['expence_jobbag']=="Y") echo "checked";?>" type="radio" name="expence_jobbag" id="inlineRadio1" value="Y"> Yes
          </label>
          <label class="radio-inline">
            <input checked="<?php if ($clientexpence[0]['expence_jobbag']=="N") echo "checked";?>" type="radio" name="expence_jobbag" id="inlineRadio2" value="N"> No
          </label>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Additional Notes:</label>
        <div class="col-sm-9">
          <textarea id="inputDetails1" placeholder="Insert any comments/notes here..." name="expence_notes">
            <?php echo $clientexpence[0]['expence_notes']; ?>
          </textarea>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
        </div>
      </div>

    </form>


  </div>

</div>