<div role="main" class="container-fluid main-wrapper theme-showcase">
	<div class="col-sm-6">
		<h2>Server WHM links</h2>
		<a href="https://swim.wpcpanel.com:2087/cpsess9604244425/" target="_blank">Web Prophets SWiM1</a><br/>
		<a href="https://swim2.wpcpanel.com:2087/" target="_blank">Web Prophets SWiM2</a><br/>
		<a href="http://swim.com.au:2086/cpsess4646608460/?post_login=97601081183843" target="_blank">Comcen/Spintel</a><br/>
		<a href="https://toolkit.ilisys.com.au/" target="_blank">ilisys</a><br/>
		<br/>
		<h2>Backups</h2>
		<a href="http://122.129.218.62:5000/webman/index.cgi" target="_blank">WP Synology Box</a><br/>
		<br/>
		<h2>VPS</h2>
		<a href="https://msports.wpcpanel.com:2087/cpsess8884012729/scripts3/feature_showcase/" target="_blank">Market Sports</a><br/>
		<a href="https://dailyreviews.wpcpanel.com:2087/" target="_blank">Daily Review</a><br/>
	</div>

	<div class="col-sm-6">
		<h2>Domain Resources</h2>
		<a href="https://www.instra.com/en/login" target="_blank">Instra Login Page</a><br/>
		<a href="http://whois.ausregistry.net.au/whois/whois_local.jsp?" target="_blank">Domain Registration Lookup (.Au whois)</a><br/>
		<a href="http://whois.ausregistry.com.au/password_recovery/recover_password.jsp?tab=0" target="_blank">Domain Password Recovery (.Au whois)</a><br/>
		<a href="https://www.sslshopper.com/ssl-checker.html" target="_blank">SSL Checker</a><br/>
		<a href="http://network-tools.com/nslook/" target="_blank">NS Lookup</a><br/>
		<a href="http://mxtoolbox.com/blacklists.aspx" target="_blank">Email blacklist checker</a><br/>
	</div>

	<div class="col-sm-6">
		<h2>CMS Resources</h2>
		<a href="https://wordpress.org/" target="_blank">Wordpress</a><br/>
		<a href="https://codex.wordpress.org/Main_Page" target="_blank">Wordpress Documentation</a><br/>
		<a href="https://roots.io/sage/" target="_blank">Wordpress Sage Framework</a><br/>
		<a href="https://www.cs-cart.com/" target="_blank">CS-Cart</a><br/>
		<a href="http://docs.cs-cart.com/4.3.x/" target="_blank">CS-Cart Documentation</a><br/>
		<a href="http://api.docs.cs-cart.com/latest" target="_blank">CS-Cart API</a><br/>
		<a href="http://forum.cs-cart.com/" target="_blank">CS-Cart Forum</a><br/>
	</div>

	<div class="col-sm-6">
		<h2>SWiM Forms/Documents</h2>
		<a href="http://www.swim.com.au/welcome/" target="_blank">Client onboard</a><br/>
		<a href="http://www.swim.com.au/support/go-live/?website=www.example.com.au" target="_blank">Go live authorisation</a><br/>
		<a href="https://www.swim.com.au/support/data-restoration-authorisation/" target="_blank">Data restoration authorisation</a><br/>
		<a href="http://www.swim.com.au/supportticket/" target="_blank">Support Ticket Form</a><br/>
	</div>

	<div class="col-sm-6">
		<h2>Other Resources</h2>
		<a href="http://www.downforeveryoneorjustme.com" target="_blank">Site active check</a><br/>
		<a href="http://au.urlm.com" target="_blank">Domain stats</a><br/>
		<a href="http://archive.org/web/web.php" target="_blank">Internet Wayback machine</a><br/>
		<a href="https://www.whatismyip.com" target="_blank">What’s my IP Lookup</a><br/>
		<a href="http://supportdetails.com" target="_blank">Browser support details</a><br/>
	</div>


</div>
