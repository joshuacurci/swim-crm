<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>All notes</h1>

  	<a href="<? echo base_url(); ?>index.php/client/view/<? echo $clientID; ?>" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
    <a href="<? echo base_url(); ?>index.php/clientnotes/add/<? echo $clientID; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Note</a>
    <br/><br/>
  	<table class="table table-striped table-responsive">
  		<tr>
  			<th></th>
  			<th>Note Name</th>
        <th>Note Date</th>
        <th>Created by</th>
        <th>Note Type</th>
  			<th></th>
  			<th></th>
  		</tr>
  		<? foreach ($clientnotes as $notedata) { ?>
  			<tr>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/clientnotes/view/<? echo $notedata['noteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><? echo $notedata['note_title']; ?></td>
          <td><? echo date('d/m/Y H:i:s', $notedata['note_date']); ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_users WHERE userID = ".$notedata['staffID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><? echo $row['name']; ?></td>
                <? }
            } else { ?>
                <td>No Staff</td>
            <? } ?>

          <td><? echo $notedata['note_type']; ?></td>
  				<td><a href="<? echo base_url(); ?>index.php/clientnotes/edit/<? echo $notedata['noteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/clientnotes/delete/<? echo $notedata['noteID']; ?>/<? echo $notedata['clientID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
  			</tr>
  		<? } ?>
  	</table>

</div>

</div>