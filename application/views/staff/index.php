<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Full Staff Account List</h1>

  	<? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'){ ?>
      <a href="<? echo base_url(); ?>index.php/staff/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Staff Member</a>
    <? } ?>
    <br/><br/>
  	<table class="table table-striped table-responsive" id="project-data">
      <thead>
  		<tr>
  			<th></th>
  			<th>Name</th>
        <th>Email</th>
        <th>Mobile</th>
        <th>Last Login</th>
  			<th></th>
  			<th></th>
  		</tr>
      </thead>
      <tbody id="myTable">
  		<? foreach ($staff as $staffdata) { ?>
  			<tr>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/staff/view/<? echo $staffdata['userID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><a href="<? echo base_url(); ?>index.php/staff/view/<? echo $staffdata['userID']; ?>/"><? echo $staffdata['name']; ?></a></td>
          <td><? echo $staffdata['email']; ?></td>
          <td><? echo $staffdata['mobile']; ?></td>
          <td><? echo $staffdata['lastlogin']; ?></td>
  				<td>
            <? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['userID'] == $staffdata['userID']){ ?>
              <a href="<? echo base_url(); ?>index.php/staff/edit/<? echo $staffdata['userID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
            <? } ?>
          </td>
          <td>
            <? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'){ ?>
              <a href="<? echo base_url(); ?>index.php/staff/delete/<? echo $staffdata['userID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a>
            <? } ?>
          </td>
  			</tr>
  		<? } ?>
  	</tbody>
    </table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>