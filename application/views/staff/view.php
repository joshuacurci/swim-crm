<div role="main" class="container-fluid main-wrapper theme-showcase">

<div clsas="col-sm-12">
  <h1><? echo $staffdetails[0]['name']; ?></h1>
  
  <a href="<? echo base_url(); ?>index.php/staff/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B'){ ?>
      <a href="<? echo base_url(); ?>index.php/staff/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Staff Member</a>
  <? } ?>
  <? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['userID'] == $staffdetails[0]['userID']){ ?>
      <a href="<? echo base_url(); ?>index.php/staff/edit/<? echo $staffdetails[0]['userID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
  <? } ?>
  <br/><br/>
  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Email: </b></td>
        <td>
          <? echo $staffdetails[0]['email']; ?><br/>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Mobile: </b></td>
        <td>
          <? echo $staffdetails[0]['mobile']; ?><br/>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Last Login: </b></td>
        <td>
          <? echo $staffdetails[0]['lastlogin']; ?><br/>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-12 project-info">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#projects">Projects</a></li>
      <li><a data-toggle="tab" href="#tasks">Tasks</a></li>
      <li><a data-toggle="tab" href="#tickets">Support Tickets</a></li>
    </ul>

    <div class="tab-content">
      <div id="projects" class="tab-pane fade in active">
        <a href="<? echo base_url(); ?>index.php/projects/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Project</a>
        <br/><br/>
        <table class="table table-striped table-responsive" id="project-data">
          <tr>
            <th></th>
            <th>Job Bag #</th>
            <th>Project Client</th>
            <th>Project Name</th>
            <th>Project Status</th>
            <th></th>
            <th></th>
          </tr>
            <? if (isset($projects)) { ?>
          <? foreach ($projects as $projectdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $projectdata['job_bag']; ?></td>

              <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

                // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                // Check connection
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
                $result = $conn->query($sql);
                
                $num_rec = $result->num_rows;

                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { ?>
                      <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                    <? }
                } else { ?>
                    <td>No Client</td>
                <? } ?>

              <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
              <td><? echo $projectdata['project_status']; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                    <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
          <? } ?>
        </table>
      </div>

      <div id="tasks" class="tab-pane fade in">
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($tasks)) { ?>
          <? foreach ($tasks as $taskdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <td><? echo date('d/m/Y',$taskdata['task_duedate']); ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
      </div>

      <div id="tickets" class="tab-pane fade in">
        <a href="<? echo base_url(); ?>index.php/tickets/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New ticket</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
             <th></th>
             <th>Ticket Name</th>
             <th>Ticket Domain</th>
             <th>Ticket Urgency</th>
             <th>Created Date:</th>
             <th></th>
             <th></th>
          </tr>
  <? if (isset($supporttickets)) { ?>
          <? foreach ($supporttickets as $ticketdata) { ?>
            <tr>
            <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/"><? echo $ticketdata['ticket_title']; ?></a></td>
            <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

                        // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }
              $sql_domain = "SELECT * FROM tbl_domains WHERE domainID = ".$ticketdata['ticket_domain']."";

              $result_domain = $conn -> query($sql_domain);

              $num_rec_domain = $result_domain;

              if ($result_domain > '0') {
                while($row = $result_domain->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a></td>
                  <? }
                } else { ?>
                  <td>No assigned domain</td>
            <? } ?>
            <td><? echo $ticketdata['ticket_urgency']; ?></td>
            <td><? echo date('d/m/Y H:i:s',$ticketdata['ticket_createdate']); ?></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/edit/<? echo $ticketdata['ticketID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/delete/<? echo $ticketdata['ticketID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table>
      </div>

    </div>
  </div>
  </div>
  
</div>