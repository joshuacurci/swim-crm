<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Edit staff member</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/staff/save/">
    <input type="hidden" name="userID" value="<? echo $staffinfo[0]['userID']; ?>" >
        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Full Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Full Name" name="name" value="<? echo $staffinfo[0]['name']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Email:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Email" name="email" value="<? echo $staffinfo[0]['email']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Mobile:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Mobile" name="mobile" value="<? echo $staffinfo[0]['mobile']; ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Change Password:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Change Password" name="password" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group <? if($_SESSION['usertype'] == 'A' || $_SESSION['usertype'] == 'B' || $_SESSION['userID'] == $staffdata['userID']){ ?> hidden <? } ?>">
          <label for="inputrecNum1" class="col-sm-3 control-label">User Type:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="type_letter">
              <? foreach ($usertypes as $typedata){ ?>
                <option <? if($staffinfo[0]['type_letter'] == $typedata['type_letter']) {echo "selected=selected";} ?> value="<? echo $typedata['type_letter'] ?>"><? echo $typedata['type_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

  </form>
  	

</div>

</div>