<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to The SWIM CRM</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/main.css">
	
</head>
<body>


<div class="login-wrapper">
	<div class="login-image">
		<img src="<?php echo base_url(); ?>images/logo-login.png" />
	</div>
	<div class="login-fields">
		<?php 
          $attributes = array("id" => "loginform", "name" => "loginform");
          echo form_open("login/login", $attributes);?>
		  <div class="form-group">
		    <label for="exampleInputEmail1">Email address</label>
		    <input type="email" class="form-control" id="exampleInputEmail1" name="txt_username" placeholder="Email">
		  </div>
		  <div class="form-group">
		    <label for="exampleInputPassword1">Password</label>
		    <input type="password" class="form-control" id="exampleInputPassword1" name="txt_password" placeholder="Password">
		  </div>
		  <input id="btn_login" name="btn_login" type="submit" class="btn btn-default" value="Login" />
          <?php echo form_close(); ?>
          <?php echo $this->session->flashdata('msg'); ?>
	</div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
</body>
</html>