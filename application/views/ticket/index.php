<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Full ticket List</h1>

    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/tickets/search/">
        <h3>Search</h3>
        <div class="col-sm-4 search-box-item">
          <div class="search-title">Ticket Name:</div>
          <div class="search-field">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Ticket Name" name="ticket_title" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['ticket_title'].'"';} ?> >
          </div>
        </div>

        <div class="col-sm-4 search-box-item">
          <div class="search-title">Assigned To:</div>
          <div class="search-field">
            <select class="form-control" id="inputorg1" name="assignID" style="width:100%">
              <option value="">All</option>
              <? foreach ($stafflist as $staffdata){ ?>
                <option <? if(isset($searchresults)) { if($searchresults['assignID'] == $staffdata['userID']) {echo 'selected';} } ?> value="<? echo $staffdata['userID'] ?>"><? echo $staffdata['name'] ?></option>
                <? } ?>
              </select>
            </div>
          </div>

          <div class="col-sm-4 search-box-item">
            <div class="search-title">Status:</div>
            <div class="search-field">
             <select class="form-control" id="" name="ticket_status">
              <option value="">All</option>
              <option <? if(isset($searchresults)) { if($searchresults['ticket_status'] == "Open") {echo 'selected';} } ?> value="Open">Open</option>
              <option <? if(isset($searchresults)) { if($searchresults['ticket_status'] == "Closed") {echo 'selected';} } ?> value="Closed">Closed</option>
              <option <? if(isset($searchresults)) { if($searchresults['ticket_status'] == "Lapsed") {echo 'selected';} } ?> value="Lapsed">Lapsed</option>
            </select>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary" style="margin-top:10px;">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>



    <a href="<? echo base_url(); ?>index.php/tickets/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New ticket</a>
    <br/><br/>
    <table class="table table-striped table-responsive" id="ticket-data">
      <thead>
        <tr>
         <th></th>
         <th>Ticket Name</th>
         <th>Ticket Domain</th>
         <th>Ticket Urgency</th>
         <th>Created By:</th>
         <th>Created Date:</th>
         <th>Assign To:</th>
         <th></th>
         <th></th>

       </tr>
     </thead>
     <tbody id="myTable">
      <? foreach ($ticket as $ticketdata) { ?>
       <tr>
        <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
        <td><a href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/"><? echo $ticketdata['ticket_title']; ?></a></td>
        <?
        $servername = $this->db->hostname;
        $username = $this->db->username;
        $password = $this->db->password;
        $dbname = $this->db->database;

                  // Create connection
        $conn = new mysqli($servername, $username, $password, $dbname);
                  // Check connection
        if ($conn->connect_error) {
          die("Connection failed: " . $conn->connect_error);
        }
        $sql_domain = "SELECT * FROM tbl_domains WHERE domainID = ".$ticketdata['ticket_domain']."";

        $result_domain = $conn -> query($sql_domain);

        $num_rec_domain = $result_domain;

        if ($result_domain > '0') {
          while($row = $result_domain->fetch_assoc()) { ?>
            <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a></td>
            <? }
          } else { ?>
            <td>No assigned domain</td>
            <? } ?>

            <td><? echo $ticketdata['ticket_urgency']; ?></td>

            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

                  // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
                  // Check connection
            if ($conn->connect_error) {
              die("Connection failed: " . $conn->connect_error);
            }

            $sql_user = "SELECT * FROM tbl_users WHERE userID = ".$ticketdata['ticket_createdbyID']."";
            $result_user = $conn -> query($sql_user);
            $num_rec_user = $result_user;

            if ($result_user > '0') {
              while($row = $result_user->fetch_assoc()) { ?>
                <td><? echo $row['name']; ?></td>
                <? }
              } else { ?>
                <td>No user</td>
                <? } ?>

                <td><? echo date('d/m/Y H:i:s',$ticketdata['ticket_createdate']); ?></td>

                <?
                $servername = $this->db->hostname;
                $username = $this->db->username;
                $password = $this->db->password;
                $dbname = $this->db->database;

                  // Create connection
                $conn = new mysqli($servername, $username, $password, $dbname);
                  // Check connection
                if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
                }

                $sql_user = "SELECT * FROM tbl_users WHERE userID = ".$ticketdata['assignID']."";
                $result_user = $conn -> query($sql_user);
                $num_rec_user = $result_user;

                if ($result_user > '0') {
                  while($row = $result_user->fetch_assoc()) { ?>
                    <td><? echo $row['name']; ?></td>
                    <? }
                  } else { ?>
                    <td>No user</td>
                    <? } ?>

                    <td><a href="<? echo base_url(); ?>index.php/tickets/edit/<? echo $ticketdata['ticketID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                    <td><a href="<? echo base_url(); ?>index.php/tickets/delete/<? echo $ticketdata['ticketID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
                  </tr>
                  <? } ?>
                </tbody>
              </table>

              <div class="col-md-12 text-center">
                <ul class="pagination pagination-lg" id="myPager"></ul>
              </div>
            </div>
          </div>