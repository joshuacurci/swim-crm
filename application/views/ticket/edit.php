<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Edit ticket</h1>

    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/tickets/save/">
      <input type="hidden" name="ticketID" value="<? echo $ticket[0]['ticketID']; ?>" >


      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Name:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Ticket Name" name="ticket_title" value="<? echo $ticket[0]['ticket_title']; ?>" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
            <label for="inputrecNum1" class="col-sm-3 control-label">Domain:</label>
            <div class="col-sm-9">
                <select class="form-control" id="inputorg1" name="ticket_domain">
                <? foreach ($domainlist as $domaindata){ ?>
                  <option <? if($ticket[0]['ticket_domain'] == $domaindata['domainID']) {echo "selected=selected";} ?> value="<? echo $domaindata['domainID'] ?>"><? echo $domaindata['domain_name'] ?></option>
                  <? } ?>
                </select>
              </div>
              <div style="clear:both"></div>
        </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Type:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Ticket Type" name="ticket_type" value="<? echo $ticket[0]['ticket_type']; ?>" >
        </div>
        <div style="clear:both"></div>
      </div>   

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Urgency:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="ticket_urgency">
              <option <? if($ticket[0]['ticket_urgency'] == "High") {echo "selected=selected";} ?> value="High">High</option>
              <option <? if($ticket[0]['ticket_urgency'] == "Medium") {echo "selected=selected";} ?> value="Medium">Medium</option>
              <option <? if($ticket[0]['ticket_urgency'] == "Low") {echo "selected=selected";} ?> value="Low">Low</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Staff Assigned To::</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="assignID">
              <? foreach ($stafflist as $staffdata){ ?>
                <option <? if($ticket[0]['assignID'] == $staffdata['userID']) {echo "selected=selected";} ?> value="<? echo $staffdata['userID'] ?>"><? echo $staffdata['name'] ?></option>
                <? } ?>
              </select>
            </div>
            <div style="clear:both"></div>
          </div>

            <div class="form-group">
              <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Information:</label>
              <div class="col-sm-9">
                <textarea id="inputDetails1" placeholder="Ticket Information here..." name="ticket_info">
                  <? echo $ticket[0]['ticket_info']; ?>
                </textarea>
              </div>
              <div style="clear:both"></div>
            </div>

            <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="ticket_status">
              <option <? if($ticket[0]['ticket_status'] == "Open") {echo "selected=selected";} ?> value="Open">Open</option>
              <option <? if($ticket[0]['ticket_status'] == "Resolved") {echo "selected=selected";} ?> value="Resolved">Resolved</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>
            
            

            <div class="form-group">
              <div class="col-sm-offset-3 col-sm-9">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>

          </form>


        </div>

      </div>