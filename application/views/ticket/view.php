<div role="main" class="container-fluid main-wrapper theme-showcase">

<div clsas="col-sm-12">
  <h1><? echo $ticket[0]['ticket_title']; ?></h1>

  <?
    if($ticket[0]['ticket_domain'] == "") { ?> 
        <h3>No assigned domain</h3> 
    <? } else {
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_domains WHERE domainID = ".$ticket[0]['ticket_domain']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <h3><? echo $row['domain_name']; ?></h3>
                <? }
            } else { ?>
                <h3>No assigned domain</h3>
            
  <? } } ?>
    
  <a href="<? echo base_url(); ?>index.php/tickets/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/tickets/edit/<? echo $ticket[0]['ticketID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a>
    <br/><br/>
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Status:</label>
        <div class="col-sm-9"><? if ($ticket[0]['ticket_status'] == 'Open'){echo '<span class="label label-danger">'.$ticket[0]['ticket_status'].'</span>';}
            elseif ($ticket[0]['ticket_status'] == 'Resolved'){echo '<span class="label label-primary">'.$ticket[0]['ticket_status'].'</span>';} ?></div>
    <div style="clear:both"></div>
    </div>

    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Urgency:</label>
        <div class="col-sm-9"><? if ($ticket[0]['ticket_urgency'] == 'High'){echo '<span class="label label-danger">'.$ticket[0]['ticket_urgency'].'</span>';}
            elseif ($ticket[0]['ticket_urgency'] == 'Medium'){echo '<span class="label label-warning">'.$ticket[0]['ticket_urgency'].'</span>';}
            elseif ($ticket[0]['ticket_urgency'] == 'Low'){echo '<span class="label label-primary">'.$ticket[0]['ticket_urgency'].'</span>';} ?></div>
    <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Assigned:</label>
        <div class="col-sm-9">
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_users WHERE userID = ".$ticket[0]['assignID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <? echo $row['name']; ?>
                <? }
            } else { ?>
                No Client
  <? } ?>
        </div>
    <div style="clear:both"></div>
    </div>

    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Created by:</label>
        <div class="col-sm-9">
            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_users WHERE userID = ".$ticket[0]['ticket_createdbyID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <? echo $row['name']; ?>
                <? }
            } else { ?>
                No Client
  <? } ?>
        </div>
    <div style="clear:both"></div>
    </div>

    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Created on:</label>
        <div class="col-sm-9"><? echo date('d/m/Y',$ticket[0]['ticket_createdate']); ?></div>
    <div style="clear:both"></div>
    </div>
    
    <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Ticket Information:</label>
        <div class="col-sm-9"><? echo $ticket[0]['ticket_info']; ?></div>
    <div style="clear:both"></div>
    </div>
    <br/>
    <br/>
    <div class="ticket-reply">
        <h2>Replies</h2>
        <h3>Add new reply</h3>
        <div class="reply-form">
            <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/reply/newreply/ticket/<? echo $ticket[0]['ticketID']; ?>/">
                        <input type="hidden" name="reply_user" value="<? echo $_SESSION['usersname'] ?>" />
                        <textarea name="reply_content" style="width:100%">
                        </textarea>
                        <br>
                        <input type="submit" name="Submit" value="Submit Reply" class="btn btn-primary" />
                    </form>
        </div>

        <div class="reply-list">
                    <?
                        $servername = $this->db->hostname;
                        $username = $this->db->username;
                        $password = $this->db->password;
                        $dbname = $this->db->database;

                        // Create connection
                        $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
                        if ($conn->connect_error) {
                            die("Connection failed: " . $conn->connect_error);
                        }

                        $sql = "SELECT * FROM tbl_tickets_reply WHERE conectorID = ".$ticket[0]['ticketID']."";
                        $result = $conn->query($sql);
                        
                        $num_rec = $result->num_rows;

                        if ($result->num_rows > 0) {
                            // output data of each row
                            echo "<h2>Replies (".$num_rec.")</h2>";
                            while($row = $result->fetch_assoc()) {
                            
                            echo "<div class='posted-reply-wrapper'>";
                            echo "<div class='posted-user'><b>Posted by:</b> ".$row['reply_user']."</div>";
                            echo "<div class='posted-date'><b>Date Posted:</b> ".date('d/m/Y H:i:s',$row['reply_date'])."</div>";
                            echo "<div class='posted-reply'>".$row['reply_content']."</div>";
                            echo "</div>";
                            }
                        } else {
                            echo "<h3>0 replies</h3>";
                        }
                                            
                    ?>
                    
            
        </div>
    </div>

</div>
</div>