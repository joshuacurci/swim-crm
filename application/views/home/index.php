<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <a href="<? echo base_url(); ?>index.php/client/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client</a>
  <a href="<? echo base_url(); ?>index.php/clientcontact/adddash/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Client Contact</a>
  <a href="<? echo base_url(); ?>index.php/projects/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Project</a>
  <a href="<? echo base_url(); ?>index.php/salesproposals/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Sale in progress</a>
</div>

<!-- Super Admin, Admin, Studio -->
<? if($_SESSION['usertype'] == "A" || $_SESSION['usertype'] == "B"){ ?>
<div class="col-sm-6">
  <h1>Outstanding Tasks</h1>
  <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($projecttask)) { ?>
          <? foreach ($projecttask as $taskdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <td><? if($taskdata['task_duedate']) {echo date('d/m/Y',$taskdata['task_duedate']); }; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/projects/index">Open Projects</a></h1>
  <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>#</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
        <th></th>
        <th></th>
      </tr>
        <? if (isset($projects)) { ?>
      <? foreach ($projects as $projectdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
          <td><? echo $projectdata['project_status']; ?></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
      <? } ?>
  </table>
</div>
<div style="clear:both;"></div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/tickets/index">Outstanding Support Tickets</a></h1>
  <table class="table table-striped table-responsive">
          <tr>
             <th></th>
             <th>Ticket Name</th>
             <th>Ticket Domain</th>
             <th>Ticket Urgency</th>
             <th>Created Date:</th>
             <th></th>
             <th></th>
          </tr>
  <? if (isset($tickets)) { ?>
          <? foreach ($tickets as $ticketdata) { ?>
            <tr>
            <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/"><? echo $ticketdata['ticket_title']; ?></a></td>
            <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

                        // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }
              $sql_domain = "SELECT * FROM tbl_domains WHERE domainID = ".$ticketdata['ticket_domain']."";

              $result_domain = $conn -> query($sql_domain);

              $num_rec_domain = $result_domain;

              if ($result_domain > '0') {
                while($row = $result_domain->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a></td>
                  <? }
                } else { ?>
                  <td>No assigned domain</td>
            <? } ?>
            <td><? echo $ticketdata['ticket_urgency']; ?></td>
            <td><? echo date('d/m/Y H:i:s',$ticketdata['ticket_createdate']); ?></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/edit/<? echo $ticketdata['ticketID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/delete/<? echo $ticketdata['ticketID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/salesproposals/index">Sales & Proposals</a></h1>
  <table class="table table-striped table-responsive">
<thead>
      <tr>
        <th></th>
        <th>Proposal Name</th>
        <th>Company Name</th>
        <th></th>
        <th></th>
      </tr>
      </thead>
          <tbody id="myTable">
            <? if (isset($sales)) { ?>
      <? foreach ($sales as $clientdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $clientdata['salesID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><a href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $clientdata['salesID']; ?>/" ><? echo $clientdata['sales_title']; ?></a></td>
          <? if ($clientdata['sales_new'] == 'Y') { ?>
          <td><? echo $clientdata['sales_companyname']; ?></td>
          <? } else { ?>
              <?  $servername = $this->db->hostname; $username = $this->db->username; $password = $this->db->password; $dbname = $this->db->database;
                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) { die("Connection failed: " . $conn->connect_error); }
                    $sql = "SELECT * FROM tbl_client WHERE clientID = ".$clientdata['sales_clientID']."";
                $result = $conn->query($sql);
                $num_rec = $result->num_rows;
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) { ?>
                        <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/" ><? echo $row['client_name']; ?></a></td>
                    <? } } else { ?>
                        <td>No Client</td>
              <? } ?>
          <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/salesproposals/edit/<? echo $clientdata['salesID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/salesproposals/delete/<? echo $clientdata['salesID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
      <? } ?>
    </tbody>
</table> 
</div>

<!-- Studio -->
<? } elseif ($_SESSION['usertype'] == "S") { ?>
<div class="col-sm-6">
  <h1>Outstanding Tasks</h1>
  <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($projecttask)) { ?>
          <? foreach ($projecttask as $taskdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <td><? if($taskdata['task_duedate']) { echo date('d/m/Y',$taskdata['task_duedate']); }; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/projects/index">Open Projects</a></h1>
  <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>#</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
        <th></th>
        <th></th>
      </tr>
        <? if (isset($projects)) { ?>
      <? foreach ($projects as $projectdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
          <td><? echo $projectdata['project_status']; ?></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
      <? } ?>
  </table>
</div>
<div style="clear:both;"></div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/tickets/index">Outstanding Support Tickets</a></h1>
  <table class="table table-striped table-responsive">
          <tr>
             <th></th>
             <th>Ticket Name</th>
             <th>Ticket Domain</th>
             <th>Ticket Urgency</th>
             <th>Created Date:</th>
             <th></th>
             <th></th>
          </tr>
  <? if (isset($tickets)) { ?>
          <? foreach ($tickets as $ticketdata) { ?>
            <tr>
            <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/"><? echo $ticketdata['ticket_title']; ?></a></td>
            <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

                        // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }
              $sql_domain = "SELECT * FROM tbl_domains WHERE domainID = ".$ticketdata['ticket_domain']."";

              $result_domain = $conn -> query($sql_domain);

              $num_rec_domain = $result_domain;

              if ($result_domain > '0') {
                while($row = $result_domain->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a></td>
                  <? }
                } else { ?>
                  <td>No assigned domain</td>
            <? } ?>
            <td><? echo $ticketdata['ticket_urgency']; ?></td>
            <td><? echo date('d/m/Y H:i:s',$ticketdata['ticket_createdate']); ?></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/edit/<? echo $ticketdata['ticketID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/delete/<? echo $ticketdata['ticketID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>


<!-- Accounts -->
<? } elseif ($_SESSION['usertype'] == "C") { ?>
<div class="col-sm-6">
  <h1>Outstanding Tasks</h1>
  <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($projecttask)) { ?>
          <? foreach ($projecttask as $taskdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <td><? if($taskdata['task_duedate']) { echo date('d/m/Y',$taskdata['task_duedate']); }; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/projects/index">Open Projects</a></h1>
  <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>#</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
        <th></th>
        <th></th>
      </tr>
        <? if (isset($projects)) { ?>
      <? foreach ($projects as $projectdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
          <td><? echo $projectdata['project_status']; ?></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
      <? } ?>
  </table>
</div>
<div style="clear:both;"></div>


<!-- Account Manager -->
<? } elseif ($_SESSION['usertype'] == "M") { ?>
<div class="col-sm-6">
  <h1>Outstanding Tasks</h1>
  <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($projecttask)) { ?>
          <? foreach ($projecttask as $taskdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <td><? if($taskdata['task_duedate']) { echo date('d/m/Y',$taskdata['task_duedate']); }; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/projects/index">Open Projects</a></h1>
  <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>#</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
        <th></th>
        <th></th>
      </tr>
        <? if (isset($projects)) { ?>
      <? foreach ($projects as $projectdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
          <td><? echo $projectdata['project_status']; ?></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
      <? } ?>
  </table>
</div>
<div style="clear:both;"></div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/salesproposals/index">Sales & Proposals</a></h1>
  <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Company Name</th>
            <th>Prospect Name</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($sales)) { ?>
          <? foreach ($sales as $salesdata) { ?>
          <tr>
            <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $salesdata['salesID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td><a href="<? echo base_url(); ?>index.php/salesproposals/view/<? echo $salesdata['salesID']; ?>/" ><? echo $salesdata['sales_companyname']; ?></a></td>
            <td><? echo $salesdata['sales_name']; ?></td>
            <td><a href="<? echo base_url(); ?>index.php/salesproposals/edit/<? echo $salesdata['salesID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
            <td><a href="<? echo base_url(); ?>index.php/salesproposals/delete/<? echo $salesdata['salesID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
          </tr>
          <? } ?>
<? } ?>
</table> 
</div>
<!-- Contractor, Private -->
<? } else { ?>
<div class="col-sm-6">
  <h1>Outstanding Tasks</h1>
  <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Task Name</th>
            <th>Priority</th>
            <th>Due Date</th>
            <th></th>
            <th></th>
          </tr>
  <? if (isset($projecttask)) { ?>
          <? foreach ($projecttask as $taskdata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projecttask/view/<? echo $taskdata['projecttaskID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $taskdata['task_name']; ?></td>

              <td><? if ($taskdata['task_priority'] == 'High'){echo '<span class="label label-danger">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Medium'){echo '<span class="label label-warning">'.$taskdata['task_priority'].'</span>';}
            elseif ($taskdata['task_priority'] == 'Low'){echo '<span class="label label-primary">'.$taskdata['task_priority'].'</span>';} ?></td>

              <td><? if($taskdata['task_duedate']){ echo date('d/m/Y',$taskdata['task_duedate']);}; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/edit/<? echo $taskdata['projecttaskID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/projecttask/delete/<? echo $taskdata['projecttaskID']; ?>/<? echo $taskdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/projects/index">Open Projects</a></h1>
  <table class="table table-striped table-responsive">
      <tr>
        <th></th>
        <th>#</th>
        <th>Project Client</th>
        <th>Project Name</th>
        <th>Project Status</th>
        <th></th>
        <th></th>
      </tr>
        <? if (isset($projects)) { ?>
      <? foreach ($projects as $projectdata) { ?>
        <tr>
          <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <td><? echo $projectdata['job_bag']; ?></td>

          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$projectdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

          <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $projectdata['projectID']; ?>/"><? echo $projectdata['project_name']; ?></a></td>
          <td><? echo $projectdata['project_status']; ?></td>
          <td><a href="<? echo base_url(); ?>index.php/projects/edit/<? echo $projectdata['projectID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
                <td><a href="<? echo base_url(); ?>index.php/projects/delete/<? echo $projectdata['projectID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
      <? } ?>
      <? } ?>
  </table>
</div>
<div style="clear:both;"></div>

<div class="col-sm-6">
  <h1><a href="<? echo base_url(); ?>index.php/tickets/index">Outstanding Support Tickets</a></h1>
  <table class="table table-striped table-responsive">
          <tr>
             <th></th>
             <th>Ticket Name</th>
             <th>Ticket Domain</th>
             <th>Ticket Urgency</th>
             <th>Created Date:</th>
             <th></th>
             <th></th>
          </tr>
  <? if (isset($tickets)) { ?>
          <? foreach ($tickets as $ticketdata) { ?>
            <tr>
            <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/view/<? echo $ticketdata['ticketID']; ?>/"><? echo $ticketdata['ticket_title']; ?></a></td>
            <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

                        // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
                        // Check connection
              if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
              }
              $sql_domain = "SELECT * FROM tbl_domains WHERE domainID = ".$ticketdata['ticket_domain']."";

              $result_domain = $conn -> query($sql_domain);

              $num_rec_domain = $result_domain;

              if ($result_domain > '0') {
                while($row = $result_domain->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a></td>
                  <? }
                } else { ?>
                  <td>No assigned domain</td>
            <? } ?>
            <td><? echo $ticketdata['ticket_urgency']; ?></td>
            <td><? echo date('d/m/Y H:i:s',$ticketdata['ticket_createdate']); ?></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/edit/<? echo $ticketdata['ticketID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
            <td><a href="<? echo base_url(); ?>index.php/tickets/delete/<? echo $ticketdata['ticketID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
<? } ?>
</table> 
</div>

<? } ?>

</div>
