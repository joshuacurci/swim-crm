<footer class="footer-section">
<a href="http://wiki.swim.com.au">SWiM Wiki</a> |
<a href="<? echo base_url(); ?>index.php/pages/resources">Resources</a> |
<a href="<? echo base_url(); ?>index.php/userguide/index">User guide</a> |  
<a href="<?php echo base_url(); ?>change-log.php" target="_blank">Change Log</a>
</br>
<i>&copy; <? echo date('Y'); ?> SWiM Communications</i>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js"></script>
    <script type="text/javascript">
            jQuery(".date-picker").datepicker({
                  format: "dd-mm-yyyy",
                  todayBtn: "linked",
                  autoclose: true,
                  todayHighlight: true
              });

           $("#capital-text").keyup(function(evt){
              // to capitalize all words  
              var cp_value= ucwords($(this).val(),true) ;
              $(this).val(cp_value );
            });

			
     </script>
     <script src="<?php echo base_url(); ?>js/pagenation-script.js"></script>
     <script src="<?php echo base_url(); ?>js/capital.js"></script>
     <script src="<?php echo base_url(); ?>js/formscripts.js"></script>
      
</body>
</html>