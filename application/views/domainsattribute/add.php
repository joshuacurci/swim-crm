<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Add new domain attribute</h1>

    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<?php echo base_url(); ?>index.php/domainsattribute/newdomainsattribute/">
      <input type="hidden" name="domainID" value="<? echo $domainID; ?>" >

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Title:</label>
        <div class="col-sm-9">
        <input type="text" class="form-control" id="inputrecNum1" placeholder="Title" name="att_name" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Version:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Version" name="att_version" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Type:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" id="inputrecNum1" placeholder="Type" name="att_type" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Details:</label>
        <div class="col-sm-9">
          <textarea id="inputDetails1" placeholder="Insert any comments here..." name="att_details">

          </textarea>
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <div class="col-sm-offset-3 col-sm-9">
          <button type="submit" class="btn btn-primary">Submit</button>
          <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
        </div>
      </div>

    </form>
  </div>

</div>