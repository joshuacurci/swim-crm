<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Edit domain</h1>
    
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/domains/save/">
      <input type="hidden" name="domainID" value="<? echo $domains[0]['domainID']; ?>" >

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Domain Name:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" placeholder="Domain Name" name="domain_name" value="<? echo $domains[0]['domain_name']; ?>" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
        <div class="col-sm-9">
          <select class="form-control" id="inputorg1" name="clientID">
            <? foreach ($clientlist as $clientdata){ ?>
              <option <? if($domains[0]['clientID'] == $clientdata['clientID']) {echo "selected=selected";} ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Registrant:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Domain Registrant" name="domain_registrant" value="<? echo $domains[0]['domain_registrant']; ?>" >
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Registrar:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Domain Registrar" name="domain_registrar" value="<? echo $domains[0]['domain_registrar']; ?>" >
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domain_status">
              <option <? if($domains[0]['domain_status'] == 'Active'){ echo "selected"; } ?> value="Active">Active</option>
              <option <? if($domains[0]['domain_status'] == 'Expired'){ echo "selected"; } ?> value="Expired">Expired</option>
              <option <? if($domains[0]['domain_status'] == 'Parked'){ echo "selected"; } ?> value="Parked">Parked</option>
              <option <? if($domains[0]['domain_status'] == 'Allow to lapse'){ echo "selected"; } ?> value="Allow to lapse">Allow to lapse</option>
              <option <? if($domains[0]['domain_status'] == 'Lapsed'){ echo "selected"; } ?> value="Lapsed">Lapsed</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Webhost:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domain_webhost">
              <option <? if($domains[0]['domain_webhost'] == 'Spintel'){ echo "selected"; } ?> value="Spintel">Spintel</option>
              <option <? if($domains[0]['domain_webhost'] == 'Web Prophets' || $domains[0]['domain_webhost'] == 'Web Prophets Server 1'){ echo "selected"; } ?> value="Web Prophets Server 1">Web Prophets Server 1</option>
              <option <? if($domains[0]['domain_webhost'] == 'Web Prophets Server 2'){ echo "selected"; } ?> value="Web Prophets Server 2">Web Prophets Server 2</option>
              <option <? if($domains[0]['domain_webhost'] == 'Others'){ echo "selected"; } ?> value="Others">Others</option>            
            </select>

          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Hosting Rate:</label>

          <div class="col-sm-9">
            <input type="text" class="form-control" placeholder="Hosting rate in $" name="domain_webhost_rate" style="width:60%; display:inline-block" value="<? echo $domains[0]['domain_webhost_rate']; ?>" >

            <select class="form-control" name="domain_webhost_rate_frequency" style="width:38%; display:inline-block">
              <option <? if($domains[0]['domain_webhost_rate_frequency'] == 'F'){ echo "selected"; } ?> value="F">per Fortnight</option>
              <option <? if($domains[0]['domain_webhost_rate_frequency'] == 'M'){ echo "selected"; } ?> value="M">per Month</option>
              <option <? if($domains[0]['domain_webhost_rate_frequency'] == 'Y'){ echo "selected"; } ?> value="Y">per Year</option>
            </select>

          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Managed:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domain_managed">
              <option <? if($domains[0]['domain_managed'] == 'SWIM'){ echo "selected"; } ?> value="Y">SWIM</option>
              <option <? if($domains[0]['domain_managed'] == 'Client'){ echo "selected"; } ?> value="N">Client</option>
              <option <? if($domains[0]['domain_managed'] == 'Others'){ echo "selected"; } ?> value="O">Others</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>



        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Renewal Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                <input id="date-picker-2" type="text" class="date-picker form-control" name="domain_renewaldate" value="<? echo date('d-m-Y',$domains[0]['domain_renewaldate']); ?>" />
                <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
              </div>
            </div>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

      </form>
      

    </div>

  </div>