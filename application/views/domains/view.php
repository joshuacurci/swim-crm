<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
	<h1><? echo $domains[0]['domain_name']; ?></h1>

	<a href="<? echo base_url(); ?>index.php/domains/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
	<a href="<? echo base_url(); ?>index.php/domains/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Domain</a>
	<a href="<? echo base_url(); ?>index.php/domains/edit/<? echo $domains[0]['domainID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Domain</a>
	<br/><br/>

	<div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Client: </b>  </td>
        <td>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$domains[0]['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a>
                <? }
            } else { ?>
                No Client
            <? } ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Domain Registrar: </b>  </td>
        <td>
          <? echo $domains[0]['domain_registrar']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Domain Registrant: </b>  </td>
        <td>
          <? echo $domains[0]['domain_registrant']; ?>
        </td>
      </tr>
      
    </table>
  </div>

  <div class="col-sm-4">
    <table>
    <tr>
        <td style="padding-right:5px"><b>Managed By: </b>  </td>
        <td>
          <? if($domains[0]['domain_managed'] == 'Y'){
            echo "SWIM";
          } else if($domains[0]['domain_managed'] == 'N'){
            echo "Client";
          } else if($domains[0]['domain_managed'] == 'O'){
            echo "Others";
          }
          else {
            echo "not set";
          }
          ; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Domain Status: </b>  </td>
        <td>
          <? if ($domains[0]['domain_status'] == 'Expired'){echo '<span class="label label-danger">'.$domains[0]['domain_status'].'</span>';}
             elseif ($domains[0]['domain_status'] == 'Lapsed'){echo '<span class="label label-warning">'.$domains[0]['domain_status'].'</span>';}
          	 elseif ($domains[0]['domain_status'] == 'Active'){echo '<span class="label label-primary">'.$domains[0]['domain_status'].'</span>';}
          ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Domain Renewal Date: </b>  </td>
        <td>
          <? if ($domains[0]['domain_renewaldate'] != NULL) {  echo date('d/m/Y',$domains[0]['domain_renewaldate']); } ?>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-4">
    <table>
    <tr>
        <td style="padding-right:5px"><b>Domain Webhost: </b>  </td>
        <td>
        <? echo $domains[0]['domain_webhost']; ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Domain Webhost Rate: </b>  </td>
        <td>
          $<? echo $domains[0]['domain_webhost_rate']; ?><? if ($domains[0]['domain_webhost_rate_frequency'] == 'F'){echo '/per Fortnight';}
             elseif ($domains[0]['domain_webhost_rate_frequency'] == 'M'){echo '/per Month';}
             elseif ($domains[0]['domain_webhost_rate_frequency'] == 'Y'){echo '/per Year';}
          ?>
        </td>
      </tr>
      
    </table>
  </div>

  <div style="clear:both;"></div>
<br/><br/>

  <div class="col-sm-12 domain-info">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#renew">Renewal Items</a></li>
      <li><a data-toggle="tab" href="#attributes">Attributes</a></li>
      <li><a data-toggle="tab" href="#notes">Notes</a></li>
    </ul>

    <div class="tab-content">
      <div id="renew" class="tab-pane fade in active">
        <a href="<? echo base_url(); ?>index.php/recuring/adddomain/<? echo $domains[0]['domainID']; ?>/<? echo $domains[0]['clientID'] ?>/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Renewal Item</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Item Name</th>
            <th>Start Date</th>
            <th>Due Date</th>
            <th>Renew Rate</th>
            <th>Frequency</th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($domainsrenewal)) { ?>
          <? foreach ($domainsrenewal as $domainsrenewaldata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/recuring/view/<? echo $domainsrenewaldata['recID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $domainsrenewaldata['rec_name']; ?></td>
              <td><? echo date('d/m/Y',$domainsrenewaldata['rec_startdate']); ?></td>
              <td><? echo date('d/m/Y',$domainsrenewaldata['rec_nextrenew']); ?></td>
              <td>$<? echo $domainsrenewaldata['rec_rate']; ?></td>
              <td><?
            if($domainsrenewaldata['rec_frequency'] == 'M'){ echo "Monthly"; }
            elseif($domainsrenewaldata['rec_frequency'] == 'Q'){ echo "Quarterly"; }
            elseif($domainsrenewaldata['rec_frequency'] == 'A'){ echo "Annually"; }
          ?></td>
              <td><a href="<? echo base_url(); ?>index.php/recuring/edit/<? echo $domainsrenewaldata['recID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/recuring/deletedomain/<? echo $domainsrenewaldata['recID']; ?>/<? echo $domainsrenewaldata['domainID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>

      <div id="attributes" class="tab-pane fade">
        <a href="<? echo base_url(); ?>index.php/domainsattribute/add/<? echo $domains[0]['domainID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Domain Attribute</a>

        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Name</th>
            <th>Version</th>
            <th>Type</th>
            <th>Details</th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($domainsattribute)) { ?>
          <? foreach ($domainsattribute as $domainsattributedata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/domainsattribute/view/<? echo $domainsattribute[0]['domainattID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $domainsattributedata['att_name']; ?></td>
              <td><? echo $domainsattributedata['att_version']; ?></td>
              <td><? echo $domainsattributedata['att_type']; ?></td>
              <td><? echo $domainsattributedata['att_details']; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/domainsattribute/edit/<? echo $domainsattributedata['domainattID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/domainsattribute/delete/<? echo $domainsattributedata['domainattID']; ?>/<? echo $domainsattributedata['domainID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>

      <div id="notes" class="tab-pane fade">
        <a href="<? echo base_url(); ?>index.php/domainnotes/add/<? echo $domains[0]['domainID']; ?>" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add Note</a>
        <br/><br/>
        <table class="table table-striped table-responsive">
          <tr>
            <th></th>
            <th>Title</th>
            <th>Date</th>
            <th>Note Type</th>
            <th></th>
            <th></th>
          </tr>
        <? if (isset($domainnotes)) { ?>
          <? foreach ($domainnotes as $notedata) { ?>
            <tr>
              <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/domainnotes/view/<? echo $notedata['noteID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
              <td><? echo $notedata['note_title']; ?></td>
              <td><? echo date('d/m/Y', $notedata['note_date']); ?></td>
              <td><? echo $notedata['note_type']; ?></td>
              <td><a href="<? echo base_url(); ?>index.php/domainnotes/edit/<? echo $notedata['noteID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
              <td><a href="<? echo base_url(); ?>index.php/domainnotes/delete/<? echo $notedata['noteID']; ?>/<? echo $notedata['domainID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
            </tr>
          <? } ?>
        <? } ?>
        </table>
      </div>
    </div>
  </div>

</div>

</div>