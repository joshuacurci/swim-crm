<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Full Domain List</h1>

    <div class="search-box">
      <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/domains/search/">
        <h3>Search</h3>
        <div class="col-sm-6 search-box-item">
          <div class="search-title">Domain Name:</div>
          <div class="search-field">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Domain Name" name="domain_name" <? if(isset($searchresults)) { echo 'value = "'.$searchresults['domain_name'].'"';} ?> >
          </div>
        </div>

        <div class="col-sm-6 search-box-item">
          <div class="search-title">Client:</div>
          <div class="search-field">
            <select class="form-control" id="" name="clientID">
              <option value="">All</option>
              <? foreach ($clientlist as $clientdata){ ?>
                <option <? if(isset($searchresults)) { if($searchresults['clientID'] == $clientdata['clientID']) {echo 'selected';} } ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
                <? } ?>
              </select>
            </div>
          </div>

          <div class="col-sm-6 search-box-item">
            <div class="search-title">Status:</div>
            <div class="search-field">
             <select class="form-control" id="" name="domain_status">
              <option value="">All</option>
              <option <? if(isset($searchresults)) { if($searchresults['domain_status'] == "Active") {echo 'selected';} } ?> value="Active">Active</option>
              <option <? if(isset($searchresults)) { if($searchresults['domain_status'] == "Expired") {echo 'selected';} } ?> value="Expired">Expired</option>
              <option <? if(isset($searchresults)) { if($searchresults['domain_status'] == "Lapsed") {echo 'selected';} } ?> value="Lapsed">Lapsed</option>
            </select>
          </div>
        </div>

        <div class="form-group search-box-button">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-primary">Search</button>
          </div>
          <div style="clear:both"></div>
        </div>
      </form>
    </div>

    <a href="<? echo base_url(); ?>index.php/domains/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New domains</a>
    <!-- <a href="<? echo base_url(); ?>index.php/domains/reportwizard/" class="btn btn-primary"><span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Generate contact report</a> -->
    <br/><br/>
    <table class="table table-striped table-responsive" id="domains-data">
      <thead>
        <tr>
         <th></th>
         <th>Domain Name</th>
         <th>Client</th>
         <th>Domain Status</th>
         <th>Domain Registrar</th>
         <th>Managed By</th>
         <th>Domain Renewal Date</th>        
         <th></th>
         <th></th>
       </tr>
     </thead>
     <tbody id="myTable">
      <? foreach ($domains as $domainsdata) { ?>
       <tr>
        <td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/domains/view/<? echo $domainsdata['domainID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
        <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $domainsdata['domainID']; ?>/" ><? echo $domainsdata['domain_name']; ?></a></td>
        <td>
        <?
        if ($domainsdata['clientID'] != NULL){
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$domainsdata['clientID']."";
            $result = $conn->query($sql);
            
            //$num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a>
                <? }
            } else { ?>
                No Assigned Client
            <? } } ?>
          </td>
        <td><? if ($domainsdata['domain_status'] == 'Expired'){echo '<span class="label label-danger">'.$domainsdata['domain_status'].'</span>';}
          elseif ($domainsdata['domain_status'] == 'Lapsed'){echo '<span class="label label-warning">'.$domainsdata['domain_status'].'</span>';}
          elseif ($domainsdata['domain_status'] == 'Active'){echo '<span class="label label-primary">'.$domainsdata['domain_status'].'</span>';}
          else {echo 'Unknown';}
          ?></td>
          <td><? echo $domainsdata['domain_registrar']; ?></td>
          <td> <? if($domainsdata['domain_managed'] == 'Y'){
            echo "SWIM";
          } else if($domainsdata['domain_managed'] == 'N'){
            echo "Client";
          } else if($domainsdata['domain_managed'] == 'O'){
            echo "Others";
          }
          else {
            echo "not set";
          }
          ?></td>
          <td><? if ($domainsdata['domain_renewaldate'] != NULL) { echo date('d/m/Y',$domainsdata['domain_renewaldate']); } ?></td>
          <td><a href="<? echo base_url(); ?>index.php/domains/edit/<? echo $domainsdata['domainID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/domains/delete/<? echo $domainsdata['domainID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
        </tr>
        <? } ?>
      </tbody>
    </table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
    </div>

  </div>

</div>