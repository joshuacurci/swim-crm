<div role="main" class="container-fluid main-wrapper theme-showcase">

  <div class="col-sm-12">
    <h1>Add new domain</h1>
    
    <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/domains/newdomains/">

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Domain Name:</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" placeholder="Domain Name" name="domain_name" >
        </div>
        <div style="clear:both"></div>
      </div>

      <div class="form-group">
        <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
        <div class="col-sm-9">
          <select class="form-control" id="inputorg1" name="clientID">
            <? foreach ($clientlist as $clientdata){ ?>
              <option <? if (isset($clientID)) { if ($clientID == $clientdata['clientID']) { echo "selected"; } } ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Registrant:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Domain Registrant" name="domain_registrant" >
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Registrar:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="capital-text" placeholder="Domain Registrar" name="domain_registrar" >
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Status:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domain_status">
              <option value="Active">Active</option>
              <option value="Expired">Expired</option>
              <option value="Parked">Parked</option>
              <option value="Allow to lapse">Allow to lapse</option>
              <option value="Lapsed">Lapsed</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Webhost:</label>

          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domain_webhost">
              <option value="Spintel">Spintel</option>
              <option value="Web Prophets Server 1">Web Prophets Server 1</option>
              <option value="Web Prophets Server 2">Web Prophets Server 2</option>
              <option value="Others">Others</option>            
            </select>

          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Hosting Rate:</label>

          <div class="col-sm-9">
            <input type="text" class="form-control" placeholder="Hosting rate in $" name="domain_webhost_rate" style="width:60%; display:inline-block" >

            <select class="form-control" name="domain_webhost_rate_frequency" style="width:38%; display:inline-block">
              <option value="F">per Fortnight</option>
              <option value="M" selected>per Month</option>
              <option value="Y">per Year</option>
            </select>

          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Managed:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domain_managed">
              <option value="Y">SWIM</option>
              <option value="N">Client</option>
              <option value="O">Others</option>
            </select>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain Renewal Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                <input id="date-picker-2" type="text" class="date-picker form-control" name="domain_renewaldate" />
                <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                </label>
              </div>
            </div>
          </div>
          <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a onclick="goBack()" class="btn btn-default">Cancel</a> <script> function goBack() { window.history.back(); } </script>
          </div>
        </div>

      </form>
      

    </div>

  </div>