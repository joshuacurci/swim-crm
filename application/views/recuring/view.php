<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1><? echo $recuringinfo[0]['rec_name']; ?></h1>

  <a href="<? echo base_url(); ?>index.php/recuring/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
  <a href="<? echo base_url(); ?>index.php/recuring/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Recurring Item</a>
  <a href="<? echo base_url(); ?>index.php/recuring/edit/<? echo $recuringinfo[0]['recID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit Reccuring Item</a>
  <br/><br/>

  <div class="col-sm-4">
    <table>
      <tr>
        <td style="padding-right:5px"><b>Client: </b>  </td>
        <td>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$recuringinfo[0]['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a>
                <? }
            } else { ?>
                No Client
            <? } ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Domain: </b>  </td>
        <td>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_domains WHERE domainID = ".$recuringinfo[0]['domainID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a>
                <? }
            } else { ?>
                No Assigned Domain
            <? } ?>
        </td>
      </tr>
      
    </table>
  </div>

  <div class="col-sm-4">
    <table>
    <tr>
        <td style="padding-right:5px"><b>Start Date: </b>  </td>
        <td>
          <? echo date('d/m/Y',$recuringinfo[0]['rec_startdate']); ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Last Renewal Date: </b>  </td>
        <td>
          <? echo date('d/m/Y',$recuringinfo[0]['rec_lastdate']); ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Next Renewal Date: </b>  </td>
        <td>
          <? echo date('d/m/Y',$recuringinfo[0]['rec_nextrenew']); ?>
        </td>
      </tr>
    </table>
  </div>

  <div class="col-sm-4">
    <table>
    <tr>
        <td style="padding-right:5px"><b>Rate: </b>  </td>
        <td>
          $<? echo $recuringinfo[0]['rec_rate']; ?> +gst
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Frequency: </b>  </td>
        <td>
          <?
            if($recuringinfo[0]['rec_frequency'] == 'M'){ echo "Monthly"; }
            elseif($recuringinfo[0]['rec_frequency'] == 'Q'){ echo "Quarterly"; }
            elseif($recuringinfo[0]['rec_frequency'] == 'A'){ echo "Annually"; }
            elseif($recuringinfo[0]['rec_frequency'] == 'T'){ echo "Every 2 years"; }
          ?>
        </td>
      </tr>
      <tr>
        <td style="padding-right:5px"><b>Notification: </b>  </td>
        <td>
          <? echo $recuringinfo[0]['rec_notify']; ?> days
        </td>
      </tr>
    </table>
  </div>

  <div style="clear:both;"></div>


  <div class="col-sm-12">
    <h3>Details</h3>
    <? echo $recuringinfo[0]['rec_details']; ?>
  </div>

   <div class="col-sm-12">
    <h3>Recurring History</h3>
    <table class="table table-striped table-responsive">
          <tr>
            <th>Renewal Created</th>
            <th>Date Actioned</th>
            <th>Due Date</th>
            <th>Job Bag Assigned</th>
            <th>Assigned JB</th>
          </tr>
  <? if (isset($recuringItems)) { ?>
          <? foreach ($recuringItems as $taskdata) { ?>
            <tr>
              <td><? if($taskdata['recitem_datecreated']) {echo date('d/m/Y',$taskdata['recitem_datecreated']); }; ?></td>
              <td><? if($taskdata['recitem_dateactioned']) {echo date('d/m/Y',$taskdata['recitem_dateactioned']); }; ?></td>
              <td><? if($taskdata['recitem_datedue']) {echo date('d/m/Y',$taskdata['recitem_datedue']); }; ?></td>
              <td><? if($taskdata['recitem_completed'] == 'Y') {echo 'Yes'; } else {echo 'No'; }; ?></td>
              <?
              $servername = $this->db->hostname;
              $username = $this->db->username;
              $password = $this->db->password;
              $dbname = $this->db->database;

              // Create connection
              $conn = new mysqli($servername, $username, $password, $dbname);
              // Check connection
              if ($conn->connect_error) {
                  die("Connection failed: " . $conn->connect_error);
              }

              $sql = "SELECT * FROM tbl_projects WHERE projectID = ".$taskdata['projectID']."";
              $result = $conn->query($sql);
              
              $num_rec = $result->num_rows;

              if ($result->num_rows > 0) {
                  while($row = $result->fetch_assoc()) { ?>
                    <td><a href="<? echo base_url(); ?>index.php/projects/view/<? echo $row['projectID']; ?>/" target="_blank">#<? echo $row['job_bag']; ?></a></td>
                  <? }
              } else { ?>
                  <td>No Assigned Project</td>
              <? } ?>
            </tr>
          <? } ?>
<? } ?>
</table> 
  </div>


</div>

</div>