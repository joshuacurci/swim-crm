<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Renewal Results</h1>

  <table class="table table-striped table-responsive">
          <tr>
            <th>Assigned Job Bag</th>
            <th>Title</th>
            <th>Client</th>
          </tr>
  <? if (isset($jobbags)) { ?>
          <? foreach ($jobbags as $taskdata) { ?>
            <tr>
              <td><? echo $taskdata['assignedJB']; ?></td>
              <td><? echo $taskdata['Name']; ?></td>
              <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$taskdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>
            </tr>
          <? } ?>
<? } ?>
</table> 

</div>

</div>