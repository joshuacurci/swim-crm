<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Items up for renewal</h1>

    <a href="<? echo base_url(); ?>index.php/recuring/index/" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span> Go Back</a>
    <a href="<? echo base_url(); ?>index.php/recuring/process/" class="btn btn-primary"><span class="glyphicon glyphicon glyphicon-inbox" aria-hidden="true"></span> Process renewals</a>
    <br/><br/>
  	<table class="table table-striped table-responsive" id="client-data">
  		<thead>
      <tr>
  			<th></th>
  			<th>Item Name</th>
        <th>Due Date</th>
        <th></th>
  			<th></th>
  		</tr>
      </thead>
          <tbody id="myTable">
  		<? foreach ($ind_recuring_items as $recurringdata) { ?>
  			<tr>
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/recuring/view/<? echo $recurringdata['recID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_recuring WHERE recID = ".$recurringdata['recID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/recuring/view/<? echo $recurringdata['recID']; ?>/"><? echo $row['rec_name']; ?></a></td>
                <? }
            } else { ?>
            <? } ?>

          <td><? echo date('d/m/Y',$recurringdata['recitem_datedue']); ?></td>

  				<td><a href="<? echo base_url(); ?>index.php/recuring/edit/<? echo $recurringdata['recID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/recuring/delete/<? echo $recurringdata['recID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
  			</tr>
  		<? } ?>
    </tbody>
  	</table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>