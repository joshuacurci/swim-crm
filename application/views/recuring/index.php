<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Full Recurring List</h1>

  	<div class="search-box">
      <h3>Search</h3>
      Coming soon
    </div>

  	<a href="<? echo base_url(); ?>index.php/recuring/add/" class="btn btn-success"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Recurring Item</a>
    <a href="<? echo base_url(); ?>index.php/recuring/viewitems/" class="btn btn-default"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span> View items up for renewal</a>
    <br/><br/>
  	<table class="table table-striped table-responsive" id="client-data">
  		<thead>
      <tr>
  			<th></th>
  			<th>Item Name</th>
        <th>Client</th>
  			<th>Domain (if any)</th>
        <th>Next Renewal Date</th>
        <th></th>
  			<th></th>
  		</tr>
      </thead>
          <tbody id="myTable">
  		<? foreach ($recuring_items as $recurringdata) { ?>
  			<tr
        <?
          $today = time();
          if ($recurringdata['rec_nextrenew'] <= $today ) { echo 'class="urgent"';}
          
        ?> >
  				<td><a class="btn btn-default" href="<? echo base_url(); ?>index.php/recuring/view/<? echo $recurringdata['recID']; ?>/" role="button"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a></td>
  				<td><a href="<? echo base_url(); ?>index.php/recuring/view/<? echo $recurringdata['recID']; ?>/" ><? echo $recurringdata['rec_name']; ?></a></td>
          <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_client WHERE clientID = ".$recurringdata['clientID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/client/view/<? echo $row['clientID']; ?>/"><? echo $row['client_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Client</td>
            <? } ?>

            <?
            $servername = $this->db->hostname;
            $username = $this->db->username;
            $password = $this->db->password;
            $dbname = $this->db->database;

            // Create connection
            $conn = new mysqli($servername, $username, $password, $dbname);
            // Check connection
            if ($conn->connect_error) {
                die("Connection failed: " . $conn->connect_error);
            }

            $sql = "SELECT * FROM tbl_domains WHERE domainID = ".$recurringdata['domainID']."";
            $result = $conn->query($sql);
            
            $num_rec = $result->num_rows;

            if ($result->num_rows > 0) {
                while($row = $result->fetch_assoc()) { ?>
                  <td><a href="<? echo base_url(); ?>index.php/domains/view/<? echo $row['domainID']; ?>/"><? echo $row['domain_name']; ?></a></td>
                <? }
            } else { ?>
                <td>No Associated Domain</td>
            <? } ?>

          <td><? echo date('d/m/Y',$recurringdata['rec_nextrenew']); ?></td>

  				<td><a href="<? echo base_url(); ?>index.php/recuring/edit/<? echo $recurringdata['recID']; ?>/" class="btn btn-primary"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit</a></td>
          <td><a href="<? echo base_url(); ?>index.php/recuring/delete/<? echo $recurringdata['recID']; ?>/" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Delete</a></td>
  			</tr>
  		<? } ?>
    </tbody>
  	</table>

    <div class="col-md-12 text-center">
      <ul class="pagination pagination-lg" id="myPager"></ul>
      </div>

</div>

</div>