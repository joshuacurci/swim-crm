<div role="main" class="container-fluid main-wrapper theme-showcase">

<div class="col-sm-12">
  <h1>Edit recurring item</h1>
  
  <form enctype="multipart/form-data" accept-charset="utf-8" method="post" action="<? echo base_url(); ?>index.php/recuring/save/">
    <input type="hidden" name="recID" value="<? echo $recuringinfo[0]['recID']; ?>" >

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Client:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="clientID">
              <? foreach ($clientlist as $clientdata){ ?>
                <option <? if($recuringinfo[0]['clientID'] == $clientdata['clientID']) {echo "selected=selected";} ?> value="<? echo $clientdata['clientID'] ?>"><? echo $clientdata['client_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Domain (if any):</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="domainID">
              <option value="0">None</option>
              <? foreach ($domainlist as $domaindata){ ?>
                <option <? if($recuringinfo[0]['domainID'] == $domaindata['domainID']) {echo "selected=selected";} ?> value="<? echo $domaindata['domainID'] ?>"><? echo $domaindata['domain_name'] ?></option>
              <? } ?>
            </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Item Name:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Item Name" name="rec_name" value="<? echo $recuringinfo[0]['rec_name'] ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Start Date:</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="rec_startdate" value="<? echo date('d-m-Y',$recuringinfo[0]['rec_startdate']); ?>" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Last Renewal Date (can be the same as/or before start date):</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="rec_lastdate" value="<? echo date('d-m-Y',$recuringinfo[0]['rec_lastdate']); ?>"  />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Next Renewal Date (leave blank for it to be automaticly calculated):</label>
          <div class="col-sm-9">
            <div class="controls">
              <div class="input-group">
                  <input id="date-picker-2" type="text" class="date-picker form-control" name="rec_nextrenew" value="<? echo date('d-m-Y',$recuringinfo[0]['rec_nextrenew']); ?>" />
                  <label for="date-picker-2" class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span>

                  </label>
              </div>
            </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Rate:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Rate" name="rec_rate" value="<? echo $recuringinfo[0]['rec_rate'] ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Frequency:</label>
          <div class="col-sm-9">
            <select class="form-control" id="inputorg1" name="rec_frequency">
                    <option <? if($recuringinfo[0]['rec_frequency'] == 'M') {echo "selected=selected";} ?> value="M">Monthly</option>
                    <option <? if($recuringinfo[0]['rec_frequency'] == 'Q') {echo "selected=selected";} ?> value="Q">Quarterly</option>
                    <option <? if($recuringinfo[0]['rec_frequency'] == 'A') {echo "selected=selected";} ?> value="A">Annually</option>
                    <option <? if($recuringinfo[0]['rec_frequency'] == 'T') {echo "selected=selected";} ?> value="T">Every 2 years</option>
                  </select>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Days before expiry to be notified:</label>
          <div class="col-sm-9">
            <input type="text" class="form-control" id="inputrecNum1" placeholder="Days before expiry to be notified eg. 30, 60, 90" name="rec_notify" value="<? echo $recuringinfo[0]['rec_notify'] ?>" >
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <label for="inputrecNum1" class="col-sm-3 control-label">Details:</label>
          <div class="col-sm-9">
            <textarea id="inputDetails1" placeholder="Insert details here..." name="rec_details">
              <? echo $recuringinfo[0]['rec_details'] ?>
            </textarea>
          </div>
                <div style="clear:both"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-3 col-sm-9">
            <button type="submit" class="btn btn-primary">Submit</button>
            <a href="<? echo base_url(); ?>index.php/client/index/" class="btn btn-default">Cancel</a>
          </div>
        </div>

  </form>
  	

</div>

</div>