<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Joshua's Week Blocked out</title>

<style>
  body, html, div, table {
    height:100%;  
  }
  
  table {
    width:100%; 
  }
  
  .setheight {
    height:5%;  
  }
  
  tr {
    height: 10.5%;
  }
  
  td {
    padding-left:5px;
    padding-top:5px;
    vertical-align:text-top;  
  }
  
   table, tr, td {
     border-collapse:collapse;
     border:2px solid #000; 
   }
   
   .time {
    width:5%; 
   }
   
   .timeframe {
     width:19%;
   }
   
   .topdays {
     text-align:center;
     font-weight:bold; 
   }
   
   .time, .topdays {
    background: #D5D5D5; 
   }
   
   .blue { background:#2121E3; color:#fff;}
   .yellow { background:#F0FF25;}
   .black { background:#000000;}
   .green { background:#0FB318;}
   .red { background:#EA2F32; color:#fff;}
   
   .block3 { background:#0FB318;}
   .block2 { background:#EA2F32; color:#fff;}
   .block1 { background:#FFBD11;}
   .block5 { background:#6E4B0F; color:#fff;}
   .block4 { background:#9B0FFD; color:#fff;}
   .block6 { background:#09f1ff;}
   
   .locked {
    background-image:url('http://www.solcius.com/wp-content/uploads/2014/04/small-lock.png');
    background-repeat: no-repeat;
      background-position: bottom right;  
   }
</style>

</head>

<body>
<div class="wrapper">
  <table>
      <tr class="setheight">
          <td style="background: #D5D5D5;"></td>
           <td class="topdays">Monday</td>
           <td class="topdays">Tuesday</td>
           <td class="topdays">Wednesday</td>
           <td class="topdays">Thursday</td>
           <td class="topdays">Friday</td>
       </tr>
       <tr>
          <td class="time">9.00am</td>
           <td class="timeframe blue locked">Check/Answer emails and Set Tasks</td>
           <td class="timeframe blue locked">Check/Answer emails and Set Tasks</td>
           <td class="timeframe blue locked">Check/Answer emails and Set Tasks</td>
           <td class="timeframe blue locked">Check/Answer emails and Set Tasks</td>
           <td class="timeframe blue locked">Check/Answer emails and Set Tasks</td>
       </tr>
       <tr>
          <td class="time">10.00am</td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block1">Plan out Adore Plugin</td>
           <td class="timeframe block4">Code Adore Plugin</td>
           <td class="timeframe block"></td>
       </tr>
       <tr>
          <td class="time">11.00am</td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block5">RiskTech Development</td>
           <td class="timeframe block4"></td>
           <td class="timeframe block"></td>
       </tr>
       <tr>
          <td class="time">12.00pm</td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block5"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
       </tr>
       <tr>
          <td class="time">1.00pm</td>
           <td class="timeframe yellow">Lunch and Check/Answer emails</td>
           <td class="timeframe yellow">Lunch and Check/Answer emails</td>
           <td class="timeframe yellow">Lunch and Check/Answer emails</td>
           <td class="timeframe yellow">Lunch and Check/Answer emails</td>
           <td class="timeframe yellow">Lunch and Check/Answer emails</td>
       </tr>
       <tr>
          <td class="time">2.00pm</td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block2">Code Adore Plugin</td>
           <td class="timeframe blue locked">SWiM CRM</td>
           <td class="timeframe block"></td>
       </tr>
       <tr>
          <td class="time">3.00pm</td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block2"></td>
           <td class="timeframe blue locked"></td>
           <td class="timeframe block"></td>
       </tr>
       <tr>
          <td class="time">4.00pm</td>
           <td class="timeframe block"></td>
           <td class="timeframe block"></td>
           <td class="timeframe block3">Write up WIP Docs</td>
           <td class="timeframe blue locked"></td>
           <td class="timeframe red locked">SWiM CRM email update</td>
       </tr>
       <tr>
          <td class="time">5.00pm</td>
           <td class="timeframe blue">SWiM CRM</td>
           <td class="timeframe blue">SWiM CRM</td>
           <td class="timeframe green locked">Studio WIP</td>
           <td class="timeframe blue locked"></td>
           <td class="timeframe black"></td>
       </tr>
    </table>
</div>
</body>
</html>