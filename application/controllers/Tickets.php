<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tickets extends CI_Controller {

	   public function __construct()
     {
      parent::__construct();

      $this->load->library('session');
      $this->load->helper('form');
      $this->load->helper('url');
      $this->load->helper('html');
      $this->load->database();
      $this->load->library('form_validation');
              //load the login model
              //$this->load->model('login_model');
    		  //$this->load->model('news_model');
    		  //$this->load->model('admin_model');
      $this->load->model('tickets_model');
      $this->load->model('client_model');
      $this->load->model('staff_model');
      $this->load->model('domains_model');

      if($this->config->item('maintenance_mode') == TRUE) {
        $this->load->view('under_construction');
        $content = $this->load->view('under_construction', '', TRUE); 
        echo $content;
        die();
      }

      if ( ! $this->session->userdata('loginuser'))
      { 
        redirect('login/index');
      }
    }

    public function index()
    {
      $header['menuitem'] = '5';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Support Tickets';

      $data['ticket'] = $this->tickets_model->get_tickets();
      $data['stafflist'] = $this->staff_model->get_staff();
      $data['clientlist'] = $this->client_model->get_clients();

      $this->load->view('main_header', $header);
      $this->load->view('ticket/index', $data);
      $this->load->view('main_footer');
    }
    public function search()
    {
      $header['menuitem'] = '5';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Tickets';

      $postdata = $this->input->post();
      $data['ticket'] = $this->tickets_model->search_tickets($postdata);
      $data['searchresults'] = $postdata;
      $data['clientlist'] = $this->client_model->get_clients();
      $data['stafflist'] = $this->staff_model->get_staff();
      $data['domainlist'] = $this->domains_model->get_domains();

      $this->load->view('main_header', $header);
      $this->load->view('ticket/index', $data);
      $this->load->view('main_footer');
    }

    public function view($ticketID)
    {
      $header['menuitem'] = '5';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Tickets';

      $data['ticket'] = $this->tickets_model->get_ticket_info($ticketID);
              //$data['ticket_tickets'] = $this->tickets_model->get_ticket_notes($ticketID);

      $this->load->view('main_header', $header);
      $this->load->view('ticket/view', $data);
      $this->load->view('main_footer');
    }

    public function edit($ticketID)
    {
      $header['menuitem'] = '5';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Tickets';

      $data['ticket'] = $this->tickets_model->get_ticket_info($ticketID);
      $data['clientlist'] = $this->client_model->get_clients();
      $data['stafflist'] = $this->staff_model->get_staff();
      $data['domainlist'] = $this->domains_model->get_domains();

      $this->load->view('main_header', $header);
      $this->load->view('ticket/edit', $data);
      $this->load->view('main_footer');
    }

    public function add()
    {
      $header['menuitem'] = '5';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Tickets';

      $data['clientlist'] = $this->client_model->get_clients();
      $data['stafflist'] = $this->staff_model->get_staff();
      $data['staffID'] = $_SESSION['userID'];
      $data['domainlist'] = $this->domains_model->get_domains();

      $this->load->view('main_header', $header);
      $this->load->view('ticket/add', $data);
      $this->load->view('main_footer');
    }

    public function save()
    {
      $postdata = $this->input->post();
      $ticketID = $this->input->post('ticketID');

      $this->tickets_model->update_ticket_info($postdata);

      redirect('/tickets/view/'.$ticketID);
    }

    public function newticket()
    {
      $postdata = $this->input->post();

      $ticketID = $this->tickets_model->new_ticket_info($postdata);

      redirect('/tickets/index/');
    }

    public function delete($ticketID)
    {
      $this->tickets_model->delete_ticket_info($ticketID);

      redirect('/tickets/index/');
    }



}
