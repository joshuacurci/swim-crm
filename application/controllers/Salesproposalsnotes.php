<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesproposalsnotes extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          //$this->load->model('client_model');
          $this->load->model('salesproposalsnotes_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($noteID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposal Notes';

          $data['clientnote'] = $this->salesproposalsnotes_model->get_salesnotes_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('salesproposalsnotes/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($noteID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposal Notes';

          $data['staffID'] = $_SESSION['userID'];
          $data['clientnote'] = $this->salesproposalsnotes_model->get_salesnotes_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('salesproposalsnotes/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($clientID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposal Notes';

          $data['salesID'] = $clientID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('salesproposalsnotes/add', $data);
          $this->load->view('main_footer');
  }

  public function seeall($clientID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposal Notes';

          $data['clientnotes'] = $this->salesproposalsnotes_model->get_salesnotes_info($clientID);
          $data['salesID'] = $clientID;

          $this->load->view('main_header', $header);
          $this->load->view('salesproposalsnotes/seeall', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('salesID');

    $this->salesproposalsnotes_model->update_salesnotes_info($postdata);
         
    redirect('/salesproposals/view/'.$clientid);
  }

  public function newnote()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('salesID');

    $this->salesproposalsnotes_model->new_salesnotes_info($postdata);
         
    redirect('/salesproposals/view/'.$clientid);
  }

  public function delete($noteID,$clientid)
  {
    $this->salesproposalsnotes_model->delete_salesnotes_info($noteID);
         
    redirect('/salesproposals/view/'.$clientid);
  }

}
