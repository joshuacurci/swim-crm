<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Debug extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('debug_model');
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function form()
	{
          $header['menuitem'] = '0';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Debug';

          $this->load->view('main_header', $header);
          $this->load->view('debug/form');
          $this->load->view('main_footer');
	}

     public function sendemail()
     {
          $postdata = $this->input->post();
          $to = 'josh@swim.com.au';

          $subject = 'New Feature/Bug Request';

          $headers = "From: " . $_SESSION['username'] . " <" . $_SESSION['useremail'] . ">\r\n";
          $headers .= "Reply-To: " . $_SESSION['username'] . " <". $_SESSION['useremail'] . ">\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

          $message = '<html><body>';
          $message .= '<h1>New Feature/Bug Request</h1>';
          $message .= '<div>';
          $message .= '<b>Bug Location:</b> '.$postdata['site_location'].'<br/>';
          $message .= '<b>Bug Content:</b> '.$postdata['bug_guide'].'<br/>';
          $message .= '<b>Bug Urgency:</b> '.$postdata['bug_severity'].'<br/>';
          $message .= '</div>';
          $message .= '</body></html>';

          mail($to, $subject, $message, $headers);

          $header['menuitem'] = '0';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Debug';

          $notification['sentemail'] = "Y";

          $this->load->view('main_header', $header);
          $this->load->view('debug/form',$notification);
          $this->load->view('main_footer');

     }

}
