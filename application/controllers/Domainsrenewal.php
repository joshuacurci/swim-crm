<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class domainsrenewal extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('domainsrenewal_model');
          $this->load->model('domains_model');
          $this->load->model('staff_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function complete($domainID)
	{
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Renewal';

          $data['domains'] = $this->domains_model->get_domains_info($domainID);
          $data['domainsrenewal'] = $this->domainsrenewal_model->get_domains_renewal_info($domainID);

		      $this->load->view('main_header', $header);
          $this->load->view('domainsrenewal/index', $data);
          $this->load->view('main_footer');
	}

  public function view($domainrenewalID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Renewal';

          $data['domainsrenewal'] = $this->domainsrenewal_model->get_domains_renewal_info($domainrenewalID);
          //$data['domains_notes'] = $this->domainss_model->get_domains_notes($domainID);
          //$data['domains_domain'] = $this->domainss_model->get_domains_notes($domainID);
          //$data['domains_domainss'] = $this->domainss_model->get_domains_notes($domainID);

          $this->load->view('main_header', $header);
          $this->load->view('domainsrenewal/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($domainrenewalID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domains Renewal';

          $data['domainsrenewal'] = $this->domainsrenewal_model->get_domains_renewal_info($domainrenewalID);
		  $data['stafflist'] = $this->staff_model->get_staff();

          $this->load->view('main_header', $header);
          $this->load->view('domainsrenewal/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($domainID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domains Renewal';

          $data['stafflist'] = $this->staff_model->get_staff();
          $data['domainID'] = $domainID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('domainsrenewal/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
	
    $this->domainsrenewal_model->update_domainsrenewal_info($postdata);
    $domainID = $postdata['domainID'];
         
    redirect('/domains/view/'.$domainID);
  }

  public function newdomainsrenewal()
  {
    $postdata = $this->input->post();

    $this->domainsrenewal_model->new_domainsrenewal_info($postdata);
    $domainID = $postdata['domainID'];
	
    redirect('/domains/view/'.$domainID);
  }

  public function delete($domainrenewalID,$domainID)
  {
    $this->domainsrenewal_model->delete_domainsrenewal_info($domainrenewalID);
         
    redirect('/domains/view/'.$domainID);
  }

}
