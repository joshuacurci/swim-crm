<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Domainnotes extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('domains_model');
          $this->load->model('domainnotes_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($noteID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Notes';

          $data['domainnotes'] = $this->domainnotes_model->get_domain_notes($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('domainnotes/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($noteID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Notes';

          $data['staffID'] = $_SESSION['userID'];
          $data['domainnotes'] = $this->domainnotes_model->get_domain_notes($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('domainnotes/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($domainID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Notes';

          $data['domainID'] = $domainID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('domainnotes/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $domainID = $this->input->post('domainID');

    $this->domainnotes_model->update_domainnotes_info($postdata);
         
    redirect('/domains/view/'.$domainID);
  }

  public function newnote()
  {
    $postdata = $this->input->post();
    $domainID = $this->input->post('domainID');

    $this->domainnotes_model->new_domainnotes_info($postdata);
         
    redirect('/domains/view/'.$domainID);
  }

  public function delete($noteID,$domainID)
  {
    $this->domainnotes_model->delete_domainnotes_info($noteID);
         
    redirect('/domains/view/'.$domainID);
  }

}
