<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reply extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('reply_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function newreply($replytype,$conectorid)
	{
        if ($replytype == 'tasks'){
          $redirecturl = 'projecttask/view/'.$conectorid.'/';
          $table = 'tbl_projects_tasks_reply';
        } elseif ($replytype == 'ticket'){
          $redirecturl = 'tickets/view/'.$conectorid.'/';
          $table = 'tbl_tickets_reply';
        }

		      $postdata = $this->input->post();

          $this->reply_model->new_reply($postdata,$table,$conectorid);
         
    redirect($redirecturl);
	}

  public function view($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_project_info($projectID);
          $data['project_tasks'] = $this->projects_model->get_project_tasks($projectID);
          //$data['project_notes'] = $this->projects_model->get_project_notes($projectID);
          //$data['project_domain'] = $this->projects_model->get_project_notes($projectID);
          //$data['project_projects'] = $this->projects_model->get_project_notes($projectID);

          $this->load->view('main_header', $header);
          $this->load->view('project/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_project_info($projectID);
          $data['assignedstaff'] = $this->projects_model->get_project_asigned_staff($projectID);
          $data['clientlist'] = $this->client_model->get_clients();
          $data['stafflist'] = $this->staff_model->get_staff();

          $this->load->view('main_header', $header);
          $this->load->view('project/edit', $data);
          $this->load->view('main_footer');
  }

  public function add()
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['stafflist'] = $this->staff_model->get_staff();
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('project/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projects_model->update_project_info($postdata);
    $this->projects_model->update_project_staffassigned($postdata,$projectID);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newproject()
  {
    $postdata = $this->input->post();

    $projectID = $this->projects_model->new_project_info($postdata);
    $this->projects_model->new_project_staffassigned($postdata,$projectID);
         
    redirect('/projects/index/');
  }

  public function delete($projectID)
  {
    $this->projects_model->delete_project_info($projectID);
         
    redirect('/projects/index/');
  }

}
