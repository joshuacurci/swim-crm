<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recuring extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('client_model');
          $this->load->model('staff_model');
          $this->load->model('domains_model');

          $this->load->model('recuring_model');
          $this->load->model('recuring_process_model');
          

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function index()
	{
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['recuring_items'] = $this->recuring_model->get_recuring();

		      $this->load->view('main_header', $header);
          $this->load->view('recuring/index', $data);
          $this->load->view('main_footer');
	}

  public function clientview($clientID)
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['recuring_items'] = $this->recuring_model->get_recuring_client($clientID);

          $this->load->view('main_header', $header);
          $this->load->view('recuring/index', $data);
          $this->load->view('main_footer');
  }

  public function search()
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $postdata = $this->input->post();
          $data['project'] = $this->projects_model->search_projects($postdata);
          $data['searchresults'] = $postdata;
          $data['clientlist'] = $this->client_model->get_clients();

          $this->load->view('main_header', $header);
          $this->load->view('recuring/index', $data);
          $this->load->view('main_footer');
  }

  public function view($recID)
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['recuringinfo'] = $this->recuring_model->get_recuring_info($recID);
          $data['recuringItems'] = $this->recuring_model->get_recuring_ind_items($recID);

          $this->load->view('main_header', $header);
          $this->load->view('recuring/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($recID)
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['recuringinfo'] = $this->recuring_model->get_recuring_info($recID);
          $data['clientlist'] = $this->client_model->get_clients();
          $data['domainlist'] = $this->domains_model->get_domains();

          $this->load->view('main_header', $header);
          $this->load->view('recuring/edit', $data);
          $this->load->view('main_footer');
  }

  public function add()
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['domainlist'] = $this->domains_model->get_domains();

          $this->load->view('main_header', $header);
          $this->load->view('recuring/add', $data);
          $this->load->view('main_footer');
  }

  public function clientadd($clientID)
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['domainlist'] = $this->domains_model->get_domains();
          $data['clientID'] = $clientID;

          $this->load->view('main_header', $header);
          $this->load->view('recuring/clientadd', $data);
          $this->load->view('main_footer');
  }

  public function adddomain($domainID,$clientID)
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['domainlist'] = $this->domains_model->get_domains();
          $data['domainID'] = $domainID;
          $data['clientID'] = $clientID;

          $this->load->view('main_header', $header);
          $this->load->view('recuring/adddomain', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $recID = $this->input->post('recID');

    $this->recuring_model->update_recuring_info($postdata);
         
    redirect('/recuring/view/'.$recID);
  }

  public function newrecurring()
  {
    $postdata = $this->input->post();

    $projectID = $this->recuring_model->new_recuring_info($postdata);
    
    redirect('/recuring/index/');

    //print_r($projectID);
  }

  public function delete($recID)
  {
    $this->projects_model->delete_recuring_info($recID);
         
    redirect('/recuring/index/');
  }

  public function deletedomain($recID, $domainID)
  {
    $this->projects_model->delete_recuring_info($recID);
         
    redirect('/domains/view/'.$domainID);
  }

  public function viewitems()
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['ind_recuring_items'] = $this->recuring_model->get_recuring_items();

          $this->load->view('main_header', $header);
          $this->load->view('recuring/viewitems', $data);
          $this->load->view('main_footer');
  }

  public function process()
  {
          $header['menuitem'] = '7';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Recurring';

          $data['jobbags'] = $this->recuring_process_model->process_renewals();

          $this->load->view('main_header', $header);
          $this->load->view('recuring/processresults', $data);
          $this->load->view('main_footer');
  }

}
