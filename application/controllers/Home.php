<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		      $this->load->model('home_model');

      if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function index()
	{
          $header['menuitem'] = '1';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Dashboard';

          $userID = $_SESSION['userID'];

          $data['projects'] = $this->home_model->get_projects($userID);
          $data['projecttask'] = $this->home_model->get_projects_tasks($userID);          
          $data['tickets'] = $this->home_model->get_support_tickets($userID);
          $data['sales'] = $this->home_model->get_salesproposals($userID);

		      $this->load->view('main_header', $header);
          $this->load->view('home/index', $data);
          $this->load->view('main_footer');
	}

}
