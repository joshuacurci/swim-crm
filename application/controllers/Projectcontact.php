<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projectcontact extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('projectcontact_model');
          $this->load->model('projects_model');
          $this->load->model('staff_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function assign($projectID, $clientID)
	{
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Contact';

          $data['client_contacts'] = $this->projectcontact_model->get_client_contacts($clientID);
          $data['projectID'] = $projectID;
          $data['clientID'] = $clientID;

		      $this->load->view('main_header', $header);
          $this->load->view('projectcontact/assign', $data);
          $this->load->view('main_footer');
	}

  public function savecontact()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projectcontact_model->delete_current_assign($projectID);
    $this->projectcontact_model->save_new_contacts($postdata, $projectID);
         
    redirect('/projects/view/'.$projectID);
  }

  

}
