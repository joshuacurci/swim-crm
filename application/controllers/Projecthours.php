<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projecthours extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('projects_model');
          $this->load->model('projecthours_model');
          $this->load->model('projecttask_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($noteID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Hours';

          $data['clientnote'] = $this->projecthours_model->get_hour_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('projecthours/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($hourID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Hours';

          $data['staffID'] = $_SESSION['userID'];
          $data['hoursinfo'] = $this->projecthours_model->get_hour_info($hourID);

          $this->load->view('main_header', $header);
          $this->load->view('projecthours/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Hours';

          $data['projectID'] = $projectID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('projecthours/add', $data);
          $this->load->view('main_footer');
  }

  public function addtask($projectID,$taskID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Hours';

          $data['projectID'] = $projectID;
          $data['staffID'] = $_SESSION['userID'];
          $data['taskinfo'] = $this->projecttask_model->get_project_tasks_info($taskID);

          $this->load->view('main_header', $header);
          $this->load->view('projecthours/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projecthours_model->update_project_hours_info($postdata);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newhour()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projecthours_model->new_project_hours_info($postdata);
    
    if ($postdata['action'] == "Submit and Add another"){
      redirect('/projecthours/add/'.$projectID);
    } else {
      redirect('/projects/view/'.$projectID);
    }

  }

  public function delete($noteID,$projectID)
  {
    $this->projecthours_model->delete_project_hours_info($noteID);
         
    redirect('/projects/view/'.$projectID);
  }

}
