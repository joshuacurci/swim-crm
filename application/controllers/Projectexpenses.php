<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projectexpences extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('projects_model');
          $this->load->model('projectexpences_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($expenceID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Expenses';

          $data['clientexpence'] = $this->projectexpences_model->get_expence_info($expenceID);

          $this->load->view('main_header', $header);
          $this->load->view('projectexpences/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($expenceID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project expenses';

          $data['staffID'] = $_SESSION['userID'];
          $data['hoursinfo'] = $this->projectexpences_model->get_expence_info($hourID);

          $this->load->view('main_header', $header);
          $this->load->view('projecthours/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($projectID,$clientID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project expenses';

          $data['projectID'] = $projectID;
          $data['clientID'] = $clientID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('projectexpences/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projectexpences_model->update_project_hours_info($postdata);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newexpence()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projectexpences_model->new_project_expence_info($postdata);
    //$this->projectexpences_model->send_notification($postdata);
         
    redirect('/projects/view/'.$projectID);
  }

  public function delete($expenceID,$projectID)
  {
    $this->projecthours_model->delete_project_hours_info($expenceID);
         
    redirect('/projects/view/'.$projectID);
  }

}
