<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientnotes extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('client_model');
          $this->load->model('clientnotes_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($noteID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Notes';

          $data['clientnote'] = $this->clientnotes_model->get_client_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('clientnotes/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($noteID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Notes';

          $data['staffID'] = $_SESSION['userID'];
          $data['clientnote'] = $this->clientnotes_model->get_client_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('clientnotes/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($clientID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Notes';

          $data['clientID'] = $clientID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('clientnotes/add', $data);
          $this->load->view('main_footer');
  }

  public function seeall($clientID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Notes';

          $data['clientnotes'] = $this->clientnotes_model->get_clientnote_info($clientID);
          $data['clientID'] = $clientID;

          $this->load->view('main_header', $header);
          $this->load->view('clientnotes/seeall', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('clientID');

    $this->clientnotes_model->update_note_info($postdata);
         
    redirect('/client/view/'.$clientid);
  }

  public function newnote()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('clientID');

    $this->clientnotes_model->new_clientnote_info($postdata);
         
    redirect('/client/view/'.$clientid);
  }

  public function delete($noteID,$clientid)
  {
    $this->clientnotes_model->delete_clientnote_info($noteID);
         
    redirect('/client/view/'.$clientid);
  }

}
