<?php
class Domains extends CI_Controller {

	public function __construct()
   {
      parent::__construct();
      
      $this->load->library('session');
      $this->load->helper('form');
      $this->load->helper('url');
      $this->load->helper('html');
      $this->load->database();
      $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
      $this->load->model('domains_model');
      $this->load->model('client_model');
      $this->load->model('tickets_model');
      $this->load->model('recuring_model');

      if($this->config->item('maintenance_mode') == TRUE) {
          $this->load->view('under_construction');
          $content = $this->load->view('under_construction', '', TRUE); 
          echo $content;
          die();
      }
      
      if ( ! $this->session->userdata('loginuser'))
      { 
          redirect('login/index');
      }
  }
  
  public function index()
  {

      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $data['domains'] = $this->domains_model->get_domains();
      $data['clientlist'] = $this->client_model->get_clients();


      $this->load->view('main_header', $header);
      $this->load->view('domains/index', $data);
      $this->load->view('main_footer');
  }

  public function search()
  {
      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $postdata = $this->input->post();
      $data['domains'] = $this->domains_model->search_domains($postdata);
      $data['searchresults'] = $postdata;
      $data['clientlist'] = $this->client_model->get_clients();

      $this->load->view('main_header', $header);
      $this->load->view('domains/index', $data);
      $this->load->view('main_footer');
  }

  public function clientview($clientID)
  {
      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $data['domains'] = $this->domains_model->get_client_domains($clientID);

      $this->load->view('main_header', $header);
      $this->load->view('domains/index', $data);
      $this->load->view('main_footer');
  }

  public function view($domainID)
  {
      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $data['domains'] = $this->domains_model->get_domains_info($domainID);
      $data['domain_name'] = $this->client_model->get_client_contact($domainID);
      $data['domainsrenewal'] = $this->recuring_model->get_domains_renewal($domainID);
      $data['domainsattribute'] = $this->domains_model->get_domains_attribute($domainID);
      $data['domainnotes'] = $this->domains_model->get_domain_notes($domainID);
      $data['client_registrar'] = $this->client_model->get_client_notes($domainID);
      /*$data['client_domain'] = $this->client_model->get_client_notes($clientID);/**/
      $data['clientID'] = $this->client_model->get_client_projects($domainID);

      

      $this->load->view('main_header', $header);
      $this->load->view('domains/view', $data);
      $this->load->view('main_footer');
  }

  public function edit($domainsID)
  {
      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $data['domains'] = $this->domains_model->get_domains_info($domainsID);
      $data['clientlist'] = $this->client_model->get_clients();

      $this->load->view('main_header', $header);
      $this->load->view('domains/edit', $data);
      $this->load->view('main_footer');
  }

  public function add()
  {
      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $data['clientlist'] = $this->client_model->get_clients();

      $this->load->view('main_header', $header);
      $this->load->view('domains/add', $data);
      $this->load->view('main_footer');
  }

  public function clientadd($clientID)
  {
      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $data['clientlist'] = $this->client_model->get_clients();
      $data['clientID'] = $clientID;

      $this->load->view('main_header', $header);
      $this->load->view('domains/add', $data);
      $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $domainsid = $this->input->post('domainID');

    $this->domains_model->update_domain_info($postdata);
    
    redirect('/domains/view/'.$domainsid);
}

public function newdomains()
{
    $postdata = $this->input->post();

    $this->domains_model->new_domain_info($postdata);
    
    redirect('/domains/index/');
}

public function delete($domainID)
{
    $this->domains_model->delete_domain_info($domainID);
    
    redirect('/domains/index/');
}

public function reportwizard()
  {

      $header['menuitem'] = '6';
      $header['usergroup'] = '';
      $header['pagetitle'] = 'Domains';

      $this->load->view('main_header', $header);
      $this->load->view('domains/wizard', $data);
      $this->load->view('main_footer');
  }

}
