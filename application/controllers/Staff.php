<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          
          $this->load->model('staff_model');
          $this->load->model('projects_model');
          $this->load->model('projecttask_model');
          $this->load->model('tickets_model');
          

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		      if ( ! $this->session->userdata('loginuser')){ 
        		  redirect('login/index');
		      }
     }
	
  public function index()
  {
          $header['menuitem'] = '8';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Staff';

          $data['staff'] = $this->staff_model->get_staff();

          $this->load->view('main_header', $header);
          $this->load->view('staff/index', $data);
          $this->load->view('main_footer');
  }

  public function view($staffID)
  {
          $header['menuitem'] = '8';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Staff';

          $data['staffdetails'] = $this->staff_model->get_staff_details($staffID);
          $data['projects'] = $this->projects_model->get_user_projects($staffID);
          $data['tasks'] = $this->projecttask_model->get_user_project_tasks($staffID);
          $data['supporttickets'] = $this->tickets_model->get_user_support_tickets($staffID);

          $this->load->view('main_header', $header);
          $this->load->view('staff/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($userID)
  {
          $header['menuitem'] = '8';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Staff';

          $data['usertypes'] = $this->staff_model->get_usertypes();
          $data['staffinfo'] = $this->staff_model->get_staff_details($userID);

          $this->load->view('main_header', $header);
          $this->load->view('staff/edit', $data);
          $this->load->view('main_footer');
  }

  public function add()
  {
          $header['menuitem'] = '8';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Staff';

          $data['usertypes'] = $this->staff_model->get_usertypes();

          $this->load->view('main_header', $header);
          $this->load->view('staff/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $userid = $this->input->post('userID');

    $this->staff_model->update_staff_info($postdata);
         
    redirect('/staff/view/'.$userid);
  }

  public function newnote()
  {
    $postdata = $this->input->post();

    $this->staff_model->new_staff_info($postdata);
         
    redirect('/staff/index/');
  }

  public function delete($noteID,$clientid)
  {
    $this->salesproposalsnotes_model->delete_salesnotes_info($noteID);
         
    redirect('/staff/view/'.$clientid);
  }

}
