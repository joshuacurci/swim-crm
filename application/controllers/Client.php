<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Client extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('client_model');
		  
      if($this->config->item('maintenance_mode') == TRUE) {
        $this->load->view('under_construction');
        $content = $this->load->view('under_construction', '', TRUE); 
        echo $content;
        die();
    }

		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function index()
	{
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->get_clients();

		      $this->load->view('main_header', $header);
          $this->load->view('client/index', $data);
          $this->load->view('main_footer');
	}

  public function search()
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $postdata = $this->input->post();
          $data['client'] = $this->client_model->search_clients($postdata);
          $data['searchresults'] = $postdata;
          $data['searchresults']['contact_name'] = "";

          $this->load->view('main_header', $header);
          $this->load->view('client/index', $data);
          $this->load->view('main_footer');
  }

  public function contactsearch()
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $postdata = $this->input->post();
          $data['client'] = $this->client_model->search_contacts_clients($postdata);
          $data['searchresults'] = $postdata;
          $data['searchresults']['client_name'] = "";

          $this->load->view('main_header', $header);
          $this->load->view('client/contactsearch', $data);
          $this->load->view('main_footer');
  }

  public function filter($filterletter)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->filter_letter_clients($filterletter);
          $data['filter'] = $filterletter;

          $this->load->view('main_header', $header);
          $this->load->view('client/index', $data);
          $this->load->view('main_footer');
  }

  public function archived()
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->get_archived_clients();

          $this->load->view('main_header', $header);
          $this->load->view('client/archive', $data);
          $this->load->view('main_footer');
  }

  public function archivedfilter($filterletter)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->filter_letter_archived_clients($filterletter);
          $data['filter'] = $filterletter;

          $this->load->view('main_header', $header);
          $this->load->view('client/archive', $data);
          $this->load->view('main_footer');
  }

  public function view($clientID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->get_client_info($clientID);
          $data['client_contacts'] = $this->client_model->get_client_contact($clientID);
          $data['client_notes'] = $this->client_model->get_client_notes($clientID);
          $data['client_domain'] = $this->client_model->get_client_domains($clientID);/**/
          $data['client_projects'] = $this->client_model->get_client_projects($clientID);

          $data['client_recurring'] = $this->client_model->get_client_recurring($clientID);
          $data['client_proposals'] = $this->client_model->get_client_proposals($clientID);

          $this->load->view('main_header', $header);
          $this->load->view('client/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($clientID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['client'] = $this->client_model->get_client_info($clientID);
          $data['clientlist'] = $this->client_model->get_clients();
          $data['clientwork'] = $this->client_model->get_client_work();

          $this->load->view('main_header', $header);
          $this->load->view('client/edit', $data);
          $this->load->view('main_footer');
  }

  public function add()
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Clients';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['clientwork'] = $this->client_model->get_client_work();

          $this->load->view('main_header', $header);
          $this->load->view('client/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('clientid');

    $this->client_model->update_client_info($postdata);
         
    redirect('/client/view/'.$clientid);
  }

  public function newclient()
  {
    $postdata = $this->input->post();

    $this->client_model->new_client_info($postdata);
         
    redirect('/client/index/');
  }

  public function delete($clientID)
  {
    $this->client_model->delete_client_info($clientID);
         
    redirect('/client/index/');
  }

}
