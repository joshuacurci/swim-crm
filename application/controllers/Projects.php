<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('projects_model');
          $this->load->model('client_model');
          $this->load->model('staff_model');
          $this->load->model('domains_model');
          $this->load->model('salesproposals_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function index()
	{
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_projects();
          $data['clientlist'] = $this->client_model->get_clients();

		      $this->load->view('main_header', $header);
          $this->load->view('project/index', $data);
          $this->load->view('main_footer');
	}

  public function search()
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $postdata = $this->input->post();
          $data['project'] = $this->projects_model->search_projects($postdata);
          $data['searchresults'] = $postdata;
          $data['clientlist'] = $this->client_model->get_clients();

          $this->load->view('main_header', $header);
          $this->load->view('project/index', $data);
          $this->load->view('main_footer');
  }

  public function wip()
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_wip_projects();
          $data['clientlist'] = $this->client_model->get_clients();

          $this->load->view('main_header', $header);
          $this->load->view('project/wip', $data);
          $this->load->view('main_footer');
  }

  public function clientview($clientID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_client_projects($clientID);

          $this->load->view('main_header', $header);
          $this->load->view('project/index', $data);
          $this->load->view('main_footer');
  }

  public function view($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_project_info($projectID);
          $data['project_tasks'] = $this->projects_model->get_project_tasks($projectID);
          $data['project_notes'] = $this->projects_model->get_project_notes($projectID);
          $data['project_hours'] = $this->projects_model->get_project_hours($projectID);
          $data['project_expences'] = $this->projects_model->get_project_expences($projectID);
          $data['project_contacts'] = $this->projects_model->get_project_contacts($projectID);
          $data['salesconected'] = $this->projects_model->get_salesproposals_project_conected($projectID);

          $this->load->view('main_header', $header);
          $this->load->view('project/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['project'] = $this->projects_model->get_project_info($projectID);
          $data['assignedstaff'] = $this->projects_model->get_project_asigned_staff($projectID);
          $data['clientlist'] = $this->client_model->get_clients();
          $data['stafflist'] = $this->staff_model->get_staff();
          $data['domainlist'] = $this->domains_model->get_domains();
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('project/edit', $data);
          $this->load->view('main_footer');
  }

  public function add()
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['stafflist'] = $this->staff_model->get_staff();
          $data['staffID'] = $_SESSION['userID'];
          $data['currentJB'] = $this->projects_model->get_job_id();
          $data['domainlist'] = $this->domains_model->get_domains();

          $this->load->view('main_header', $header);
          $this->load->view('project/add', $data);
          $this->load->view('main_footer');
  }

  public function propadd($salesID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['stafflist'] = $this->staff_model->get_staff();
          $data['staffID'] = $_SESSION['userID'];
          $data['currentJB'] = $this->projects_model->get_job_id();
          $data['domainlist'] = $this->domains_model->get_domains();
          $data['propData'] = $this->salesproposals_model->get_salesproposals_info($salesID);

          $this->load->view('main_header', $header);
          $this->load->view('project/propadd', $data);
          $this->load->view('main_footer');
  }

   public function clientadd($clientID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

          $data['clientlist'] = $this->client_model->get_clients();
          $data['stafflist'] = $this->staff_model->get_staff();
          $data['staffID'] = $_SESSION['userID'];
          $data['clientID'] = $clientID;
          $data['currentJB'] = $this->projects_model->get_job_id();
          $data['domainlist'] = $this->domains_model->get_domains();

          $this->load->view('main_header', $header);
          $this->load->view('project/clientadd', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projects_model->update_project_info($postdata);
    $this->projects_model->update_project_staffassigned($postdata,$projectID);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newproject()
  {
    $postdata = $this->input->post();

    $projectID = $this->projects_model->new_project_info($postdata);
    
    if ($projectID == 'JBE') {
      $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Projects';

      $data['project'][0] = $postdata;
      $data['clientlist'] = $this->client_model->get_clients();
      $data['stafflist'] = $this->staff_model->get_staff();
      $data['staffID'] = $_SESSION['userID'];
      $data['assignedstaff'] = $postdata['assigned_staff'];
      $data['currentJB'] = $this->projects_model->get_job_id();
      $data['error'] = $projectID;

      $this->load->view('main_header', $header);
      $this->load->view('project/edit', $data);
      $this->load->view('main_footer');
    } else {
      if ($postdata['propConected'] != 'N') {
        $this->projects_model->match_sales_project($postdata,$projectID);
      }
      $this->projects_model->new_project_staffassigned($postdata,$projectID);    
      redirect('/projects/index/');
    }

    //print_r($projectID);
  }

  public function delete($projectID)
  {
    $this->projects_model->delete_project_info($projectID);
         
    redirect('/projects/index/');
  }

  public function quickchange($projectID, $status, $redirect)
  {
    $this->projects_model->quickupdate_project_status($projectID, $status);
    
    if ($redirect == 'index'){
      redirect('/projects/index');
    } elseif ($redirect == 'wip'){
      redirect('/projects/wip');
    } else {
      redirect('/projects/view/'.$projectID);
    }
  }

  public function quickchangesecurity($projectID, $userID, $month)
  {
    $returned = $this->projects_model->quickupdate_project_list($projectID, $userID, $month);

    /*echo "Project ID: ".$projectID."<br/>";
    echo "User ID: ".$userID."<br/>";
    echo "Month ID: ".$month."<br/>";
    echo "SQL: ".$returned."<br/>";/**/
    
    redirect('/projects/view/'.$projectID);
  }

}
