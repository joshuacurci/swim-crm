<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Salesproposals extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('salesproposals_model');
          $this->load->model('staff_model');
          $this->load->model('client_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function index()
	{
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposals';

          $data['salesprop'] = $this->salesproposals_model->get_salesproposals();

		      $this->load->view('main_header', $header);
          $this->load->view('salesproposals/index', $data);
          $this->load->view('main_footer');
	}

  public function clientview($clientID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposals';

          $data['salesprop'] = $this->salesproposals_model->get_salesproposals_client($clientID);

          $this->load->view('main_header', $header);
          $this->load->view('salesproposals/index', $data);
          $this->load->view('main_footer');
  }

  public function view($salesID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposals';

          $data['sales'] = $this->salesproposals_model->get_salesproposals_info($salesID);
          $data['salesnotes'] = $this->salesproposals_model->get_salesproposals_notes($salesID);
          $data['salesconected'] = $this->salesproposals_model->get_salesproposals_conected($salesID);

          $this->load->view('main_header', $header);
          $this->load->view('salesproposals/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($salesID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposals';

          $data['sales'] = $this->salesproposals_model->get_salesproposals_info($salesID);

          $this->load->view('main_header', $header);
          $this->load->view('salesproposals/edit', $data);
          $this->load->view('main_footer');
  }

  public function add()
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposals';

          $data['staffID'] = $_SESSION['userID'];
          $data['clientlist'] = $this->client_model->get_clients();

          $this->load->view('main_header', $header);
          $this->load->view('salesproposals/add', $data);
          $this->load->view('main_footer');
  }

  public function clientadd($clientID)
  {
          $header['menuitem'] = '3';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Sales and Proposals';

          $data['staffID'] = $_SESSION['userID'];
          $data['clientlist'] = $this->client_model->get_clients();
          $data['clientID'] = $clientID;

          $this->load->view('main_header', $header);
          $this->load->view('salesproposals/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
	
    $this->salesproposals_model->update_salesproposals($postdata);
    $salesID = $postdata['salesID'];
         
    redirect('/salesproposals/view/'.$salesID);
  }

  public function newprop()
  {
    $postdata = $this->input->post();

    $this->salesproposals_model->new_salesproposals($postdata);
	   
      redirect('/salesproposals/index/');
  }

  public function delete($projectID)
  {
    $this->salesproposals_model->delete_salesproposals($projectID);
         
    redirect('/salesproposals/index/');
  }

}
