<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	public function __construct()
 {
  parent::__construct();

  $this->load->library('session');
  $this->load->helper('form');
  $this->load->helper('url');
  $this->load->helper('html');
  $this->load->database();
  $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
  $this->load->model('reports_model');
  $this->load->model('projects_model');
  $this->load->model('client_model');
  $this->load->model('staff_model');
  $this->load->model('domains_model');
  $this->load->model('salesproposals_model');
  $this->load->library('Pdf');

  if($this->config->item('maintenance_mode') == TRUE) {
    $this->load->view('under_construction');
    $content = $this->load->view('under_construction', '', TRUE); 
    echo $content;
    die();
}

if ( ! $this->session->userdata('loginuser'))
{ 
    redirect('login/index');
}
}

public function index()
{
  $header['menuitem'] = '9';
  $header['usergroup'] = '';
  $header['pagetitle'] = 'Projects';

  $data['project'] = $this->projects_model->get_projects();
  $data['clientlist'] = $this->client_model->get_clients();

  $this->load->view('main_header', $header);
  $this->load->view('reports/index', $data);
  $this->load->view('main_footer');
}

public function search_client()
{
  $header['menuitem'] = '9';
  $header['usergroup'] = '';
  $header['pagetitle'] = 'Projects';

  $postdata = $this->input->post();
  $clientID = $postdata['clientID'];

  $data['clientData'] = $this->reports_model->search_client($postdata);
  $data['client_contacts'] = $this->client_model->get_client_contact($clientID);
  $data['client_notes'] = $this->client_model->get_client_notes($clientID);
  $data['client_domain'] = $this->client_model->get_client_domains($clientID);/**/
  $data['client_projects'] = $this->client_model->get_client_projects($clientID);
  $data['searchresults'] = $postdata;

  $data['project'] = $this->projects_model->get_projects();
  $data['clientlist'] = $this->client_model->get_clients();

  $this->load->view('main_header', $header);
  $this->load->view('reports/client_info', $data);
  $this->load->view('main_footer');
}

public function search_dates()
{
  $header['menuitem'] = '9';
  $header['usergroup'] = '';
  $header['pagetitle'] = 'Projects';

  $postdata = $this->input->post();
  $datestart_date = date("Y-m-d H:i:s", strtotime($postdata['startdate']));
  $lastdate_date = date("Y-m-d 23:59:59", strtotime($postdata['lastdate']));

  $startdate = strtotime($datestart_date);
  $lastdate = strtotime($lastdate_date);

  $data['clientData'] = $this->reports_model->search_client_dates($postdata, $startdate, $lastdate);

  $data['searchresults'] = $postdata;

  $data['project'] = $this->projects_model->get_projects();
  $data['clientlist'] = $this->client_model->get_clients();

  $this->load->view('main_header', $header);
  $this->load->view('reports/client_list', $data);
  $this->load->view('main_footer');
}

  // Client Information
public function client_information($clientID)
{
    $data['clientData'] = $this->client_model->get_client_info($clientID);
    $data['client_contacts'] = $this->client_model->get_client_contact($clientID);
    $data['client_notes'] = $this->client_model->get_client_notes($clientID);
    $data['client_domain'] = $this->client_model->get_client_domains($clientID);/**/
    $data['client_projects'] = $this->client_model->get_client_projects($clientID);


    $title = $this->client_model->get_client_info($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
// $first_column_width = 85;
// $second_column_width = 85;
    $column_space = 25; 
    $pdf->SetY(0);
    $current_y_position = $pdf->getY();
    $line_height = 20;

// Files to view
    $reportheader = $this->load->view('reports/client_name', $data, true);
    $client_information = $this->load->view('reports/client_information', $data, true);
    $pdf->setFontSubsetting(true);   
    $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
    $pdf->AddPage('P', 'A4');
    $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
    $pdf->writeHTMLCell(0, 0, '', '', $client_information, 0, 1, 0, true, '', true);


    $pdf->Output("client_information.pdf",'I');

}
  // Download client Information
public function client_information_dl($clientID)
{
    $data['clientData'] = $this->client_model->get_client_info($clientID);
    $data['client_contacts'] = $this->client_model->get_client_contact($clientID);
    $data['client_notes'] = $this->client_model->get_client_notes($clientID);
    $data['client_domain'] = $this->client_model->get_client_domains($clientID);/**/
    $data['client_projects'] = $this->client_model->get_client_projects($clientID);


    $title = $this->client_model->get_client_info($clientID);
    $titlename = $title[0]['client_name']; 
    $this->load->library("Pdf");

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);$pdf->SetTitle($titlename);     
     // remove default header/footer
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED); 
    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
    // set default header data
    $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);    
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM); 
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);  
    $pdf->SetAutoPageBreak(true, PDF_MARGIN_FOOTER+3);
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }   
// $first_column_width = 85;
// $second_column_width = 85;
    $column_space = 25; 
    $pdf->SetY(0);
    $current_y_position = $pdf->getY();
    $line_height = 20;

// Files to view
    $reportheader = $this->load->view('reports/client_name', $data, true);
    $client_information = $this->load->view('reports/client_information', $data, true);
    $pdf->setFontSubsetting(true);   
    $pdf->SetFont('dejavusans', '', 8, '', true);   
    // // set header data
        // // Portrait
    $pdf->AddPage('P', 'A4');
    $pdf->setHeaderData('', 0, '', $reportheader, 0, 0);
    $pdf->writeHTMLCell(0, 0, '', '', $client_information, 0, 1, 0, true, '', true);


    $pdf->Output("client_information.pdf",'D');

}
}