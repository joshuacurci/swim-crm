<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projectnotes extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('projects_model');
          $this->load->model('projectnotes_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($noteID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Notes';

          $data['clientnote'] = $this->projectnotes_model->get_client_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('projectnotes/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($noteID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Notes';

          $data['staffID'] = $_SESSION['userID'];
          $data['clientnote'] = $this->projectnotes_model->get_client_note($noteID);

          $this->load->view('main_header', $header);
          $this->load->view('projectnotes/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Notes';

          $data['projectID'] = $projectID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('projectnotes/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projectnotes_model->update_note_info($postdata);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newnote()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projectnotes_model->new_clientnote_info($postdata);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newnotewip()
  {
    $postdata = $this->input->post();
    $projectID = $this->input->post('projectID');

    $this->projectnotes_model->new_clientnote_wip($postdata);
         
    redirect('/projects/wip/#linked-'.$projectID);
  }

  public function delete($noteID,$projectID)
  {
    $this->projectnotes_model->delete_clientnote_info($noteID);
         
    redirect('/projects/view/'.$projectID);
  }

}
