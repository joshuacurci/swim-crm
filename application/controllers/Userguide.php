<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userguide extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		      //$this->load->model('home_model');

      if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function index()
	{
          $header['menuitem'] = '0';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'User guide';

		      $this->load->view('main_header', $header);
          $this->load->view('userguide/index');
          $this->load->view('main_footer');
	}

  public function projectguide1() {$header['menuitem'] = '0'; $header['usergroup'] = ''; $header['pagetitle'] = 'Adding a new job bag/project'; $this->load->view('main_header', $header); $this->load->view('userguide/project/guide1'); $this->load->view('main_footer'); }
  public function projectguide2() {$header['menuitem'] = '0'; $header['usergroup'] = ''; $header['pagetitle'] = 'Changing the status of the job bag'; $this->load->view('main_header', $header); $this->load->view('userguide/project/guide2'); $this->load->view('main_footer'); }
  public function domainguide1() {$header['menuitem'] = '0'; $header['usergroup'] = ''; $header['pagetitle'] = 'Type of domain data'; $this->load->view('main_header', $header); $this->load->view('userguide/domain/guide1'); $this->load->view('main_footer'); }

}
