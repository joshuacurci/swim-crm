<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          $this->load->model('login_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
		  
		  //if ( ! $this->session->userdata('loginuser'))
		   //     { 
        	//	    redirect('login/index');
		   //     }
     }
	
	public function index()
	{
		$this->load->view('login/index');
	}

	public function login()
     {
          //get the posted values
          $username = $this->input->post("txt_username");
          $password = $this->input->post("txt_password");

          //set validations
          $this->form_validation->set_rules("txt_username", "Username", "trim|required");
          $this->form_validation->set_rules("txt_password", "Password", "trim|required");

          if ($this->form_validation->run() == FALSE)
          {

               //validation fails
				$this->load->view('login/index');
          }
          else
          {
               //validation succeeds
               if ($this->input->post('btn_login') == "Login")
               {
				   //check if the master is logging in
					if ($username == 'master@swim.com.au' && $password == 'master') {
						//set the session variables
                         $sessiondata['username'] = $username;
						 $sessiondata['loginuser'] = TRUE;
						 $sessiondata['userID'] = 0;
						 $sessiondata['usertype'] = 'A';
						 $sessiondata['usersname'] = 'The Creator';
                               $sessiondata['useremail'] = 'master@swim.com.au';
                         //$this->login_model->setlastlogin($username, $password);
                         $this->session->set_userdata($sessiondata);
                         redirect('home/index');
					} else {
				   
                    //check if username and password is correct
                    $usr_result = $this->login_model->get_user($username, $password);
                    if ($usr_result['rownumber'] > 0) //active user record is present
                    {
                         //set the session variables
                         $sessiondata['username'] = $username;
						 $sessiondata['loginuser'] = TRUE;
						 
						 foreach ($usr_result['userdetails'] as $userdata) {
							 $sessiondata['userID'] = $userdata['userID'];
							 $sessiondata['usertype'] = $userdata['type_letter'];
							 $sessiondata['usersname'] = $userdata['name'];
                                    $sessiondata['useremail'] =  $userdata['email'];
						 }
					$this->login_model->setlastlogin($username, $password);	 
                         $this->session->set_userdata($sessiondata);
                         redirect('home/index');
                    }
                    else
                    {
                         $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password! - 1</div>');
                         redirect('login/index');
                    }
					}
               }
               else
               {
               		$this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Invalid username and password! - 2</div>');
                    redirect('login/index');
               }
          }
     }
	 
	 public function logout() {
		session_destroy();
		 redirect('login/index');
	 }
}
