<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientcontact extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('client_model');
          $this->load->model('clientcontact_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
  public function view($contactID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Contact';

          $data['clientcontact'] = $this->clientcontact_model->get_client_contact($contactID);

          $this->load->view('main_header', $header);
          $this->load->view('clientcontact/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($contactID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Contact';

          $data['clientcontact'] = $this->clientcontact_model->get_client_contact($contactID);

          $this->load->view('main_header', $header);
          $this->load->view('clientcontact/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($clientID)
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Contact';

          $data['clientID'] = $clientID;

          $this->load->view('main_header', $header);
          $this->load->view('clientcontact/add', $data);
          $this->load->view('main_footer');
  }

  public function adddash()
  {
          $header['menuitem'] = '2';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Client Contact';

          $data['client'] = $this->client_model->get_clients();

          $this->load->view('main_header', $header);
          $this->load->view('clientcontact/adddash', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('clientid');

    $this->clientcontact_model->update_contact_info($postdata);
         
    redirect('/client/view/'.$clientid);
  }

  public function newcontact()
  {
    $postdata = $this->input->post();
    $clientid = $this->input->post('clientid');

    $this->clientcontact_model->new_contact_info($postdata);

    if ($postdata['action'] == "Submit and Add another"){
      redirect('/clientcontact/add/'.$clientid);
    } else {
      redirect('/client/view/'.$clientid);
    }
         
    
  }

  public function delete($contactID,$clientid)
  {
    $this->clientcontact_model->delete_clientcontact_info($contactID);
         
    redirect('/client/view/'.$clientid);
  }

}
