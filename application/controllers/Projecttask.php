<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projecttask extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('projecttask_model');
          $this->load->model('projects_model');
          $this->load->model('staff_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function complete($projectID)
	{
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Tasks';

          $data['project'] = $this->projects_model->get_project_info($projectID);
          $data['project_tasks'] = $this->projecttask_model->get_project_tasks($projectID);

		      $this->load->view('main_header', $header);
          $this->load->view('projecttask/index', $data);
          $this->load->view('main_footer');
	}

  public function view($projecttaskID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Tasks';

          $data['project_tasks'] = $this->projecttask_model->get_project_tasks_info($projecttaskID);
          //$data['project_notes'] = $this->projects_model->get_project_notes($projectID);
          //$data['project_domain'] = $this->projects_model->get_project_notes($projectID);
          //$data['project_projects'] = $this->projects_model->get_project_notes($projectID);

          $this->load->view('main_header', $header);
          $this->load->view('projecttask/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($projecttaskID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Tasks';

          $data['project_tasks'] = $this->projecttask_model->get_project_tasks_info($projecttaskID);
		  $data['stafflist'] = $this->staff_model->get_staff();

          $this->load->view('main_header', $header);
          $this->load->view('projecttask/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($projectID)
  {
          $header['menuitem'] = '4';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Project Tasks';

          $data['stafflist'] = $this->staff_model->get_staff();
          $data['projectID'] = $projectID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('projecttask/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
	
    $this->projecttask_model->update_projecttask_info($postdata);
    $projectID = $postdata['projectID'];
         
    redirect('/projects/view/'.$projectID);
  }

  public function markcomplete($projectID,$projecttaskID)
  {
  
    $this->projecttask_model->mark_projecttask_complete($projecttaskID);
         
    redirect('/projects/view/'.$projectID);
  }

  public function newtask()
  {
    $postdata = $this->input->post();

    $this->projecttask_model->new_projecttask_info($postdata);
    $projectID = $postdata['projectID'];
	   
    if ($postdata['action'] == "Submit and Add another"){
      redirect('/projecttask/add/'.$projectID);
    } else {
      redirect('/projects/view/'.$projectID);
    }
  }

  public function delete($projecttaskID,$projectID)
  {
    $this->projecttask_model->delete_projecttask_info($projecttaskID);
         
    redirect('/projects/view/'.$projectID);
  }

}
