<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class domainsattribute extends CI_Controller {

	public function __construct()
     {
          parent::__construct();
        	
          $this->load->library('session');
          $this->load->helper('form');
          $this->load->helper('url');
          $this->load->helper('html');
          $this->load->database();
          $this->load->library('form_validation');
          //load the login model
          //$this->load->model('login_model');
		  //$this->load->model('news_model');
		  //$this->load->model('admin_model');
          $this->load->model('domainsattribute_model');
          $this->load->model('domains_model');
          $this->load->model('staff_model');

          if($this->config->item('maintenance_mode') == TRUE) {
              $this->load->view('under_construction');
              $content = $this->load->view('under_construction', '', TRUE); 
              echo $content;
              die();
          }
		  
		  if ( ! $this->session->userdata('loginuser'))
		        { 
        		    redirect('login/index');
		        }
     }
	
	public function complete($domainID)
	{
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Attribute';

          $data['domains'] = $this->domains_model->get_domains_info($domainID);
          $data['domainsattribute'] = $this->domainsattribute_model->get_domains_attribute_info($domainID);

		      $this->load->view('main_header', $header);
          $this->load->view('domainsattribute/index', $data);
          $this->load->view('main_footer');
	}

  public function view($domainattID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Attribute';

          $data['domainsattribute'] = $this->domainsattribute_model->get_domains_attribute_info($domainattID);
          //$data['domains_notes'] = $this->domainss_model->get_domains_notes($domainID);
          //$data['domains_domain'] = $this->domainss_model->get_domains_notes($domainID);
          //$data['domains_domainss'] = $this->domainss_model->get_domains_notes($domainID);

          $this->load->view('main_header', $header);
          $this->load->view('domainsattribute/view', $data);
          $this->load->view('main_footer');
  }

  public function edit($domainattID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Attribute';

          $data['domainsattribute'] = $this->domainsattribute_model->get_domains_attribute_info($domainattID);
		  $data['stafflist'] = $this->staff_model->get_staff();

          $this->load->view('main_header', $header);
          $this->load->view('domainsattribute/edit', $data);
          $this->load->view('main_footer');
  }

  public function add($domainID)
  {
          $header['menuitem'] = '6';
          $header['usergroup'] = '';
          $header['pagetitle'] = 'Domain Attribute';

          $data['stafflist'] = $this->staff_model->get_staff();
          $data['domainID'] = $domainID;
          $data['staffID'] = $_SESSION['userID'];

          $this->load->view('main_header', $header);
          $this->load->view('domainsattribute/add', $data);
          $this->load->view('main_footer');
  }

  public function save()
  {
    $postdata = $this->input->post();
	
    $this->domainsattribute_model->update_domainsattribute_info($postdata);
    $domainID = $postdata['domainID'];
         
    redirect('/domains/view/'.$domainID);
  }

  public function newdomainsattribute()
  {
    $postdata = $this->input->post();

    $this->domainsattribute_model->new_domainsattribute_info($postdata);
    $domainID = $postdata['domainID'];
	
    redirect('/domains/view/'.$domainID);
  }

  public function delete($domainattID,$domainID)
  {
    $this->domainsattribute_model->delete_domainsattribute_info($domainattID);
         
    redirect('/domains/view/'.$domainID);
  }


}
