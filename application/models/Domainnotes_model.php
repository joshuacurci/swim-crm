<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class domainnotes_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_domain_notes($noteID) {
          $sql = "SELECT * FROM tbl_domains_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domainnotes_info($domainID) {
          $sql = "SELECT * FROM tbl_domains_notes WHERE domainID = '".$domainID."' ORDER BY note_date DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_domainnotes_info ($domaindata) {
          $sql = "UPDATE tbl_domains_notes SET 
          domainID = '".$domaindata['domainID']."',
          staffID = '".$domaindata['staffID']."',
          note_title = '".$domaindata['note_title']."',
          note_content = '".str_replace("'", "&#039;", $domaindata['note_content'])."',
          note_type = '".$domaindata['note_type']."',
          note_date = '".$domaindata['note_date']."'
          WHERE noteID = ".$domaindata['noteID']."";
          $query = $this->db->query($sql);
     }

     function new_domainnotes_info ($domaindata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_domains_notes (domainID,staffID,note_title,note_content,note_type,note_date) 
          VALUES 
          ('".$domaindata['domainID']."','".$domaindata['staffID']."','".$domaindata['note_title']."','".str_replace("'", "&#039;", $domaindata['note_content'])."','".$domaindata['note_type']."','".$currentdate."')";
          $query = $this->db->query($sql);
     }

     function delete_domainnotes_info($noteID) {
          $sql = "DELETE FROM tbl_domains_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
     }
}?>