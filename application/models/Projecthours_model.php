<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class projecthours_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_hour_info($hourID) {
          $sql = "SELECT * FROM tbl_projects_hours WHERE hourID = '".$hourID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_hours_info($projectID) {
          $sql = "SELECT * FROM tbl_projects_hours WHERE projectID = '".$projectID."' ORDER BY note_date DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_project_hours_info ($clientdata) {
          $sql = "UPDATE tbl_projects_hours SET 
          projectID = '".$clientdata['projectID']."',
          staffID = '".$clientdata['staffID']."',
          hour_title = '".str_replace("'", "&#039;", $clientdata['hour_title'])."',
          hour_content = '".str_replace("'", "&#039;", $clientdata['hour_content'])."',
          hour_amount = '".$clientdata['hour_amount']."',
          hour_datedone = '".strtotime($clientdata['hour_datedone'])."'
          WHERE hourID = ".$clientdata['hourID']."";
          $query = $this->db->query($sql);
     }

     function new_project_hours_info ($clientdata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_projects_hours (projectID,staffID,hour_title,hour_content,hour_amount,hour_datedone,hour_dateuploaded) 
          VALUES 
          ('".$clientdata['projectID']."','".$clientdata['staffID']."','".str_replace("'", "&#039;", $clientdata['hour_title'])."','".str_replace("'", "&#039;", $clientdata['hour_content'])."','".$clientdata['hour_amount']."','".strtotime($clientdata['hour_datedone'])."','".$currentdate."')";
          $query = $this->db->query($sql);
     }

     function delete_project_hours_info($hourID) {
          $sql = "DELETE FROM tbl_projects_hours WHERE hourID = '".$hourID."'";
          $query = $this->db->query($sql);
     }
}?>