<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class client_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_clients() {
          $sql = "SELECT * FROM tbl_client WHERE client_archive = 'N' OR client_archive IS NULL ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_clients($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE client_name LIKE '%".$searchData['client_name']."%' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_contacts_clients($searchData) {
          $sql = "SELECT * FROM tbl_client_contact WHERE contact_name LIKE '%".$searchData['contact_name']."%' OR contact_lastname LIKE '%".$searchData['contact_name']."%' ORDER BY contact_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_clients($filterletter) {
          $sql = "SELECT * FROM tbl_client WHERE (client_archive = 'N' OR client_archive IS NULL) AND client_name LIKE '".$filterletter."%' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_archived_clients() {
          $sql = "SELECT * FROM tbl_client WHERE client_archive = 'Y' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function filter_letter_archived_clients($filterletter) {
          $sql = "SELECT * FROM tbl_client WHERE client_archive = 'Y' AND client_name LIKE '".$filterletter."%' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_info($clientID) {
          $sql = "SELECT * FROM tbl_client WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_work() {
          $sql = "SELECT * FROM tbl_client_work";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_contact($clientID) {
          $sql = "SELECT * FROM tbl_client_contact WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_notes($clientID) {
          $sql = "SELECT * FROM tbl_client_notes WHERE clientID = '".$clientID."' ORDER BY note_date DESC LIMIT 5";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_domains($clientID) {
          $sql = "SELECT * FROM tbl_domains WHERE clientID = '".$clientID."' ORDER BY domain_name DESC LIMIT 5";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_recurring($clientID) {
          $sql = "SELECT * FROM tbl_recuring WHERE clientID = '".$clientID."' ORDER BY rec_nextrenew DESC LIMIT 5";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_proposals($clientID) {
          $sql = "SELECT * FROM tbl_salesproposals WHERE sales_clientID = '".$clientID."' ORDER BY salesID DESC LIMIT 5";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_projects($clientID) {
          $sql = "SELECT * FROM tbl_projects WHERE clientID = '".$clientID."' ORDER BY job_bag DESC LIMIT 5";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_client_info ($clientdata) {
          $clientwork = "";
          foreach ($clientdata['work_type'] as $worktype) {
               $clientwork .= $worktype.";";
          }

          $sql = "UPDATE tbl_client SET 
          client_name = '".$clientdata['client_name']."',
          client_add_num = '".$clientdata['client_add_num']."',
          client_add_street = '".$clientdata['client_add_street']."',
          client_add_city = '".$clientdata['client_add_city']."',
          client_add_state = '".$clientdata['client_add_state']."',
          client_add_country = '".$clientdata['client_add_country']."',
          client_add_postcode = '".$clientdata['client_add_postcode']."',
          client_phone = '".$clientdata['client_phone']."',
          client_fax = '".$clientdata['client_fax']."',
          client_acn = '".$clientdata['client_acn']."',
          client_abn = '".$clientdata['client_abn']."',
          client_traiding_terms = '".$clientdata['client_traiding_terms']."',
          client_type = '".$clientdata['client_type']."',
          client_archive = '".$clientdata['client_archive']."',
          client_resoled = '".$clientdata['client_resoled']."',
          client_light = '".$clientdata['client_light']."',
          client_work = '".$clientwork."'
          WHERE clientID = ".$clientdata['clientid']."";
          $query = $this->db->query($sql);
     }

     function new_client_info ($clientdata) {
          $clientwork = "";
          foreach ($clientdata['work_type'] as $worktype) {
               $clientwork .= $worktype.";";
          }

          $sql = "INSERT INTO tbl_client (client_name,client_add_num,client_add_street,client_add_city,client_add_state,client_add_country,client_add_postcode,client_phone,client_fax,client_acn,client_abn,client_traiding_terms,client_type,client_archive,client_resoled,client_work) 
          VALUES 
          ('".$clientdata['client_name']."','".$clientdata['client_add_num']."','".$clientdata['client_add_street']."','".$clientdata['client_add_city']."','".$clientdata['client_add_state']."','".$clientdata['client_add_country']."','".$clientdata['client_add_postcode']."','".$clientdata['client_phone']."','".$clientdata['client_fax']."','".$clientdata['client_acn']."','".$clientdata['client_abn']."','".$clientdata['client_traiding_terms']."','".$clientdata['client_type']."','N','".$clientdata['client_resoled']."','".$clientwork."')";
          $query = $this->db->query($sql);

          $sql = "SELECT * FROM tbl_client WHERE client_name = '".$clientdata['client_name']."'  AND client_abn = '".$clientdata['client_abn']."'"; 
          $query = $this->db->query($sql);

     }

     function delete_client_info($clientID) {
          $sql = "DELETE FROM tbl_client WHERE clientID = '".$clientID."'";
          $query = $this->db->query($sql);
     }
}?>