<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class recuring_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_recuring() {
          $sql = "SELECT * FROM tbl_recuring ORDER BY rec_nextrenew ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_recuring_items() {
          $sql = "SELECT * FROM tbl_recuring_item WHERE recitem_completed = 'N' ORDER BY recitem_datedue ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_recuring_ind_items($recID) {
          $sql = "SELECT * FROM tbl_recuring_item WHERE recID = '".$recID."' ORDER BY recitemID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_recuring_client($clientID) {
          $sql = "SELECT * FROM tbl_recuring WHERE clientID = '".$clientID."' ORDER BY rec_nextrenew ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_recuring_info($recID) {
          $sql = "SELECT * FROM tbl_recuring WHERE recID = '".$recID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domains_renewal($domainID) {
          $sql = "SELECT * FROM tbl_recuring WHERE domainID = '".$domainID."' ORDER BY rec_nextrenew ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     

     function new_recuring_info ($recuringdata) {

          if ($recuringdata['rec_nextrenew'] == ""){
               if($recuringdata['rec_frequency'] == 'M'){ $timestamp = strtotime('+30 days', strtotime($recuringdata['rec_lastdate'])); }
               elseif($recuringdata['rec_frequency'] == 'Q'){ $timestamp = strtotime('+90 days', strtotime($recuringdata['rec_lastdate'])); }
               elseif($recuringdata['rec_frequency'] == 'A'){ $timestamp = strtotime('+365 days', strtotime($recuringdata['rec_lastdate'])); }
               elseif($recuringdata['rec_frequency'] == 'T'){ $timestamp = strtotime('+730 days', strtotime($recuringdata['rec_lastdate'])); }
          } else {
               $timestamp = strtotime($recuringdata['rec_nextrenew']);
          }
          

          $sql = "INSERT INTO tbl_recuring (
               clientID,
               domainID,
               rec_name,
               rec_startdate,
               rec_lastdate,
               rec_rate,
               rec_frequency,
               rec_notify,
               rec_details,
               rec_nextrenew) 
          VALUES 
               ('".$recuringdata['clientID']."',
               '".$recuringdata['domainID']."',
               '".str_replace("'", "&#039;", $recuringdata['rec_name'])."',
               '".strtotime($recuringdata['rec_startdate'])."',
               '".strtotime($recuringdata['rec_lastdate'])."',
               '".$recuringdata['rec_rate']."',
               '".$recuringdata['rec_frequency']."',
               '".$recuringdata['rec_notify']."',
               '".str_replace("'", "&#039;", $recuringdata['rec_details'])."',
               '".$timestamp."')";

          $query = $this->db->query($sql);
     }

     function update_recuring_info ($recuringdata) {
          if ($recuringdata['rec_nextrenew'] == ""){
               if($recuringdata['rec_frequency'] == 'M'){ $timestamp = strtotime('+30 days', strtotime($recuringdata['rec_lastdate'])); }
               elseif($recuringdata['rec_frequency'] == 'Q'){ $timestamp = strtotime('+90 days', strtotime($recuringdata['rec_lastdate'])); }
               elseif($recuringdata['rec_frequency'] == 'A'){ $timestamp = strtotime('+365 days', strtotime($recuringdata['rec_lastdate'])); }
               elseif($recuringdata['rec_frequency'] == 'T'){ $timestamp = strtotime('+730 days', strtotime($recuringdata['rec_lastdate'])); }
          } else {
               $timestamp = strtotime($recuringdata['rec_nextrenew']);
          }

          $sql = "UPDATE tbl_recuring SET 
          clientID = '".$recuringdata['clientID']."',
          domainID = '".$recuringdata['domainID']."',
          rec_name = '".str_replace("'", "&#039;", $recuringdata['rec_name'])."',
          rec_startdate = '".strtotime($recuringdata['rec_startdate'])."',
          rec_lastdate = '".strtotime($recuringdata['rec_lastdate'])."',
          rec_rate = '".$recuringdata['rec_rate']."',
          rec_frequency = '".$recuringdata['rec_frequency']."',
          rec_notify = '".$recuringdata['rec_notify']."',
          rec_details = '".str_replace("'", "&#039;", $recuringdata['rec_details'])."',
          rec_nextrenew = '".$timestamp."'
          WHERE recID = ".$recuringdata['recID']."";
          $query = $this->db->query($sql);
     }

     function delete_recuring_info($recID) {
          $sql = "DELETE FROM tbl_recuring WHERE recID = '".$recID."'";
          $query = $this->db->query($sql);
     }

     

}?>