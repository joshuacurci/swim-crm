<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class projectnotes_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_note($noteID) {
          $sql = "SELECT * FROM tbl_projects_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_clientnote_info($projectID) {
          $sql = "SELECT * FROM tbl_projects_notes WHERE projectID = '".$projectID."' ORDER BY note_date DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_note_info ($clientdata) {
          $sql = "UPDATE tbl_projects_notes SET 
          projectID = '".$clientdata['projectID']."',
          staffID = '".$clientdata['staffID']."',
          note_title = '".str_replace("'", "&#039;", $clientdata['note_title'])."',
          note_content = '".str_replace("'", "&#039;", $clientdata['note_content'])."',
          note_type = '".$clientdata['note_type']."',
          note_date = '".$clientdata['note_date']."'
          WHERE noteID = ".$clientdata['noteID']."";
          $query = $this->db->query($sql);
     }

     function new_clientnote_info ($clientdata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_projects_notes (projectID,staffID,note_title,note_content,note_type,note_date) 
          VALUES 
          ('".$clientdata['projectID']."','".$clientdata['staffID']."','".str_replace("'", "&#039;", $clientdata['note_title'])."','".str_replace("'", "&#039;", $clientdata['note_content'])."','".$clientdata['note_type']."','".$currentdate."')";
          $query = $this->db->query($sql);
     }

     function new_clientnote_wip ($clientdata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_projects_notes (projectID,staffID,note_title,note_content,note_type,note_date) 
          VALUES 
          ('".$clientdata['projectID']."','".$clientdata['staffID']."','".str_replace("'", "&#039;", $clientdata['note_title'])."','".str_replace("'", "&#039;", $clientdata['note_content'])."','".$clientdata['note_type']."','".$currentdate."')";
          $query = $this->db->query($sql);
     }

     function delete_clientnote_info($noteID) {
          $sql = "DELETE FROM tbl_projects_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
     }
}?>