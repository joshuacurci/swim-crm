<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clientnotes_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_note($noteID) {
          $sql = "SELECT * FROM tbl_client_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_clientnote_info($clientID) {
          $sql = "SELECT * FROM tbl_client_notes WHERE clientID = '".$clientID."' ORDER BY note_date DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_note_info ($clientdata) {
          $sql = "UPDATE tbl_client_notes SET 
          clientID = '".$clientdata['clientID']."',
          staffID = '".$clientdata['staffID']."',
          note_title = '".str_replace("'", "&#039;", $clientdata['note_title'])."',
          note_content = '".str_replace("'", "&#039;", $clientdata['note_content'])."',
          note_type = '".$clientdata['note_type']."',
          note_date = '".$clientdata['note_date']."'
          WHERE noteID = ".$clientdata['noteID']."";
          $query = $this->db->query($sql);
     }

     function new_clientnote_info ($clientdata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_client_notes (clientID,staffID,note_title,note_content,note_type,note_date) 
          VALUES 
          ('".$clientdata['clientID']."','".$clientdata['staffID']."','".str_replace("'", "&#039;", $clientdata['note_title'])."','".str_replace("'", "&#039;", $clientdata['note_content'])."','".$clientdata['note_type']."','".$currentdate."')";
          $query = $this->db->query($sql);
     }

     function delete_clientnote_info($noteID) {
          $sql = "DELETE FROM tbl_client_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
     }
}?>