<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class reports_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_projects() {
          $sql = "SELECT * FROM tbl_projects WHERE project_reserved = 'N' ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client($searchData) {
          $sql = "SELECT * FROM tbl_client WHERE clientID LIKE '%".$searchData['clientID']."%'";
          // $sql = "Select * 
          //         FROM tbl_client
          //         JOIN tbl_client_contact ON tbl_client.clientID=tbl_client_contact.clientID
          //         JOIN tbl_client_notes ON tbl_client_contact.clientID=tbl_client_notes.clientID
          //         WHERE tbl_client.clientID LIKE '%".$searchData['clientID']."%'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_client_dates($searchData, $startdate, $lastdate) {
          $sql = "Select * 
                  FROM tbl_client
                  LEFT JOIN tbl_projects ON tbl_client.clientID=tbl_projects.clientID
                  LEFT JOIN tbl_client_notes ON tbl_client.clientID=tbl_client_notes.clientID
                  WHERE tbl_projects.project_createdate BETWEEN '".$startdate."' AND '".$lastdate."' ORDER BY client_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
}