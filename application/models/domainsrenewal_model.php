<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class domainsrenewal_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_domains() {
          $sql = "SELECT * FROM tbl_domains ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domain_info($domainID) {
          $sql = "SELECT * FROM tbl_domains WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domains_renewal($domainID) {
          $sql = "SELECT * FROM tbl_domains_renewals WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
	 
	 function get_domains_renewal_info($domainrenewID) {
          $sql = "SELECT * FROM tbl_domains_renewals WHERE domainrenewID = '".$domainrenewID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_domains_renewal($userID) {
          $sql = "SELECT * FROM tbl_domains_renewals WHERE assuserID = '".$userID."' AND task_status = 'A' ORDER BY task_duedate ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domains_notes($domainID) {
          $sql = "SELECT * FROM tbl_domains_notes WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domain_asigned_staff($domainID) {
          $sql = "SELECT * FROM tbl_domains_assign WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_domainsrenewal_info ($domaindata) {
          $sql = "UPDATE tbl_domains_renewals SET 
          domainID = '".$domaindata['domainID']."',
          renew_name = '".str_replace("'", "&#039;", $domaindata['renew_name'])."',
          renew_startdate = '".strtotime($domaindata['renew_startdate'])."',
          renew_lastdate = '".strtotime($domaindata['renew_lastdate'])."',
          renew_rate = '".$domaindata['renew_rate']."',
          renew_frequency = '".$domaindata['renew_frequency']."'
          WHERE domainrenewID = ".$domaindata['domainrenewID']."";
          $query = $this->db->query($sql);
     }

     function new_domainsrenewal_info ($domaindata) {
          $timestamp = time();
          $sql = "INSERT INTO tbl_domains_renewals (domainID,renew_name,renew_startdate,renew_lastdate,renew_rate,renew_frequency) 
          VALUES 
          ('".$domaindata['domainID']."','".str_replace("'", "&#039;", $domaindata['renew_name'])."','".strtotime($domaindata['renew_startdate'])."','".strtotime($domaindata['renew_lastdate'])."','".$domaindata['renew_rate']."','".$domaindata['renew_frequency']."')";
          $query = $this->db->query($sql);
     }

     function new_domain_staffassigned ($domaindata,$domainID) {
          foreach ($domaindata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_domains_assign (domainID,userID) 
               VALUES 
               ('".$domainID[0]['domainID']."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function update_domain_staffassigned ($domaindata,$domainID) {
          $sql = "DELETE FROM tbl_domains_assign WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);

          foreach ($domaindata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_domains_assign (domainID,userID) 
               VALUES 
               ('".$domainID."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function delete_domainsrenewal_info($domainrenewID) {
          $sql = "DELETE FROM tbl_domains_renewals WHERE domainrenewID = '".$domainrenewID."'";
          $query = $this->db->query($sql);
     }
}?>