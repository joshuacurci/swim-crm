<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_projects($userID) {
          $sqlassign = "SELECT projectID FROM tbl_projects_assign WHERE userID = '".$userID."' ORDER BY projectID DESC";
          $queryassign = $this->db->query($sqlassign);
          $assignedprojects = $queryassign->result_array();

          $finalnumbers = "";
          $numItems = count($assignedprojects);
          $i = 0;

          foreach ($assignedprojects as $projectassdata) {
              if(++$i === $numItems) {
                  $finalnumbers .= $projectassdata['projectID']; 
               } else {
                   $finalnumbers .= $projectassdata['projectID'].','; 
               }

          }

          if ($finalnumbers == ""){} else{
               $sql = "SELECT * FROM tbl_projects WHERE projectID IN (".$finalnumbers.") AND project_status = 'Job in progress' ORDER BY job_bag DESC LIMIT 6";
               $query = $this->db->query($sql);
               return $query->result_array();
          }
     }

     function get_projects_tasks($userID) {
          $sql = "SELECT * FROM tbl_projects_tasks WHERE assuserID = '".$userID."' AND task_status = 'A' ORDER BY task_duedate LIMIT 6";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_support_tickets($userID) {
          $sql = "SELECT * FROM tbl_tickets WHERE assignID = '".$userID."' AND ticket_status = 'Open' LIMIT 6";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_salesproposals($userID) {
          $sql = "SELECT * FROM tbl_salesproposals ORDER BY sales_followup LIMIT 6";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

}?>