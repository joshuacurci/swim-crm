<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class domains_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_domains() {
          $sql = "SELECT * FROM tbl_domains ORDER BY domain_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_domains($searchData) {
          $sql = "SELECT * FROM tbl_domains WHERE domain_name LIKE '%".$searchData['domain_name']."%'";
          if ($searchData['clientID'] != '') { $sql .= "AND clientID = '".$searchData['clientID']."' ";}
          if ($searchData['domain_status'] != '') { $sql .= "AND domain_status = '".$searchData['domain_status']."' ";}
          $sql .= "ORDER BY domain_name ";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_domains($clientID) {
          $sql = "SELECT * FROM tbl_domains WHERE clientID = '".$clientID."' ORDER BY domain_name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domains_info($domainID) {
          $sql = "SELECT * FROM tbl_domains WHERE domainID = ".$domainID."";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_domain_info ($domaindata) {
          $sql = "UPDATE tbl_domains SET 
          domain_name = '".$domaindata['domain_name']."',
          clientID = '".$domaindata['clientID']."',
          domain_registrar = '".$domaindata['domain_registrar']."',
          domain_registrant = '".$domaindata['domain_registrant']."',
          domain_status = '".$domaindata['domain_status']."',
          domain_webhost = '".$domaindata['domain_webhost']."',
          domain_webhost_rate = '".$domaindata['domain_webhost_rate']."',
          domain_webhost_rate_frequency = '".$domaindata['domain_webhost_rate_frequency']."',
          domain_managed = '".$domaindata['domain_managed']."',
          domain_renewaldate = '".strtotime($domaindata['domain_renewaldate'])."'
          WHERE domainID = ".$domaindata['domainID']."";
          $query = $this->db->query($sql);
     }
     function get_domains_renewal($domainID) {
          $sql = "SELECT * FROM tbl_domains_renewals WHERE domainID = '".$domainID."' ORDER BY renew_lastdate DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     
     function get_domains_attribute($domainID) {
          $sql = "SELECT * FROM tbl_domains_attributes WHERE domainID = '".$domainID."' ORDER BY att_name DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domain_notes($domainID) {
          $sql = "SELECT * FROM tbl_domains_notes WHERE domainID = '".$domainID."' ORDER BY note_title DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function new_domain_info ($domaindata) {
          $sql = "INSERT INTO tbl_domains (domain_name,clientID,domain_registrar,domain_registrant,domain_status,domain_webhost,domain_managed,domain_renewaldate,domain_webhost_rate,domain_webhost_rate_frequency) 
          VALUES 
          ('".$domaindata['domain_name']."','".$domaindata['clientID']."','".$domaindata['domain_registrar']."','".$domaindata['domain_registrant']."','".$domaindata['domain_status']."','".$domaindata['domain_webhost']."','".$domaindata['domain_managed']."','".strtotime($domaindata['domain_renewaldate'])."','".$domaindata['domain_webhost_rate']."','".$domaindata['domain_webhost_rate_frequency']."')";
          $query = $this->db->query($sql);
     }

     function delete_domain_info($domainID) {
          $sql = "DELETE FROM tbl_domains WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
     }

}?>