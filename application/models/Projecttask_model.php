<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class projecttask_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_projects() {
          $sql = "SELECT * FROM tbl_projects ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_info($projectID) {
          $sql = "SELECT * FROM tbl_projects WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_tasks($projectID) {
          $sql = "SELECT * FROM tbl_projects_tasks WHERE task_status = 'C' AND projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
	 
	 function get_project_tasks_info($projecttaskID) {
          $sql = "SELECT * FROM tbl_projects_tasks WHERE projecttaskID = '".$projecttaskID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_project_tasks($userID) {
          $sql = "SELECT * FROM tbl_projects_tasks WHERE assuserID = '".$userID."' AND task_status = 'A' ORDER BY task_duedate ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function mark_projecttask_complete($projecttaskID) {
          $sql = "UPDATE tbl_projects_tasks SET task_status = 'C' WHERE projecttaskID = '".$projecttaskID."'";
          $query = $this->db->query($sql);
          //return $query->result_array();
     }

     function get_project_notes($projectID) {
          $sql = "SELECT * FROM tbl_projects_notes WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_asigned_staff($projectID) {
          $sql = "SELECT * FROM tbl_projects_assign WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_projecttask_info ($projectdata) {
          $sql = "UPDATE tbl_projects_tasks SET 
          assuserID = '".$projectdata['assuserID']."',
          task_name = '".str_replace("'", "&#039;", $projectdata['task_name'])."',
          task_content = '".str_replace("'", "&#039;", $projectdata['task_content'])."',
          task_priority = '".$projectdata['task_priority']."',
          task_duedate = '".strtotime($projectdata['task_duedate'])."',
          task_status = '".$projectdata['task_status']."'
          WHERE projecttaskID = ".$projectdata['projecttaskID']."";
          $query = $this->db->query($sql);
     }

     function new_projecttask_info ($projectdata) {
          $timestamp = time();
          $sql = "INSERT INTO tbl_projects_tasks (projectID,assuserID,task_name,task_content,task_priority,task_duedate,task_createdbyID,task_createdate,task_status) 
          VALUES 
          ('".$projectdata['projectID']."','".$projectdata['assuserID']."','".str_replace("'", "&#039;", $projectdata['task_name'])."','".str_replace("'", "&#039;", $projectdata['task_content'])."','".$projectdata['task_priority']."','".strtotime($projectdata['task_duedate'])."','".$projectdata['staffID']."','".$timestamp."','A')";
          $query = $this->db->query($sql);
     }

     function new_project_staffassigned ($projectdata,$projectID) {
          foreach ($projectdata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_projects_assign (projectID,userID) 
               VALUES 
               ('".$projectID[0]['projectID']."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function update_project_staffassigned ($projectdata,$projectID) {
          $sql = "DELETE FROM tbl_projects_assign WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);

          foreach ($projectdata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_projects_assign (projectID,userID) 
               VALUES 
               ('".$projectID."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function delete_projecttask_info($projecttaskID) {
          $sql = "DELETE FROM tbl_projects_tasks WHERE projecttaskID = '".$projecttaskID."'";
          $query = $this->db->query($sql);
     }
}?>