<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class staff_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_staff() {
          $sql = "SELECT * FROM tbl_users WHERE usr_status = 'A' ORDER BY name";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_staff_details($userID) {
          $sql = "SELECT * FROM tbl_users WHERE userID = '".$userID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_usertypes() {
          $sql = "SELECT * FROM tbl_usertypes";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_staff_info ($projectdata) {
          $sql = "UPDATE tbl_users SET 
          type_letter = '".$projectdata['type_letter']."',
          username = '".$projectdata['email']."',";

          if ($projectdata['password']) {$sql .= "password = '".md5($projectdata['password'])."',";}

          $sql .= "name = '".$projectdata['name']."',
          email = '".$projectdata['email']."',
          mobile = '".$projectdata['mobile']."'
          WHERE userID = ".$projectdata['userID']."";
          $query = $this->db->query($sql);
     }

     function new_staff_info ($projectdata) {
          $timestamp = time();
          $sql = "INSERT INTO tbl_users (type_letter,username,password,name,email,usr_status,mobile) 
          VALUES 
          ('".$projectdata['type_letter']."','".$projectdata['email']."','".md5($projectdata['password'])."','".$projectdata['name']."','".$projectdata['email']."','A','".$projectdata['mobile']."')";
          $query = $this->db->query($sql);
     }

}?>