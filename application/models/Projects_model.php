<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class projects_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_projects() {
          $sql = "SELECT * FROM tbl_projects WHERE project_reserved = 'N' ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_wip_projects() {
          $sql = "SELECT * FROM tbl_projects WHERE project_status = 'Job in progress' AND project_reserved = 'N' ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_projects($searchData) {
          $sql = "SELECT * FROM tbl_projects WHERE clientID LIKE '%".$searchData['clientID']."%' AND job_bag LIKE '%".$searchData['job_bag']."%' AND project_name LIKE '%".$searchData['project_name']."%' AND project_status LIKE '%".$searchData['project_status']."%' ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_job_id() {
          $sql = "SELECT * FROM tbl_global WHERE valueID = '1'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_client_projects($clientID) {
          $sql = "SELECT * FROM tbl_projects WHERE clientID = '".$clientID."' ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_projects($userID) {
          $sqlassign = "SELECT projectID FROM tbl_projects_assign WHERE userID = '".$userID."' ORDER BY projectID DESC";
          $queryassign = $this->db->query($sqlassign);
          $assignedprojects = $queryassign->result_array();

          $finalnumbers = "";
          $numItems = count($assignedprojects);
          $i = 0;

          foreach ($assignedprojects as $projectassdata) {
              if(++$i === $numItems) {
                  $finalnumbers .= $projectassdata['projectID']; 
               } else {
                   $finalnumbers .= $projectassdata['projectID'].','; 
               }

          }

          if ($finalnumbers == ""){} else{
               $sql = "SELECT * FROM tbl_projects WHERE projectID IN (".$finalnumbers.") AND project_status = 'Job in progress' ORDER BY job_bag DESC";
               $query = $this->db->query($sql);
               return $query->result_array();
          }
     }

     function get_project_info($projectID) {
          $sql = "SELECT * FROM tbl_projects WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_tasks($projectID) {
          $sql = "SELECT * FROM tbl_projects_tasks WHERE projectID = '".$projectID."' AND task_status = 'A' ORDER BY task_createdate DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_notes($projectID) {
          $sql = "SELECT * FROM tbl_projects_notes WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_hours($projectID) {
          $sql = "SELECT * FROM tbl_projects_hours WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_expences($projectID) {
          $sql = "SELECT * FROM tbl_projects_expences WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_contacts($projectID) {
          $sql = "SELECT * FROM tbl_projects_contacts WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_asigned_staff($projectID) {
          $sql = "SELECT * FROM tbl_projects_assign WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_project_info ($projectdata) {
          $sql = "UPDATE tbl_projects SET 
          clientID = '".$projectdata['clientID']."',
          project_name = '".str_replace("'", "&#039;", $projectdata['project_name'])."',
          project_type = '".$projectdata['project_type']."',
          project_status = '".$projectdata['project_status']."',
          project_budget = '".$projectdata['project_budget']."',
          project_priority = '".$projectdata['project_priority']."',
          project_duedate = '".strtotime($projectdata['project_duedate'])."',
          project_workbag = '".$projectdata['project_workbag']."',
          project_followupdate = '".strtotime($projectdata['project_followupdate'])."'
          WHERE projectID = ".$projectdata['projectID']."";
          $query = $this->db->query($sql);
     }

     function new_project_info ($projectdata) {
          $sql3 = "SELECT * FROM tbl_projects WHERE job_bag = '".$projectdata['job_bag']."'";
          $query3 = $this->db->query($sql3);
          $array = $query3->result_array();
          $row_cnt = count($array);

          if ($row_cnt == 0) {
               $timestamp = time();
               $sql = "INSERT INTO tbl_projects (clientID,salesuserID,job_bag,project_name,project_type,project_status,project_budget,project_priority,project_duedate,project_followupdate,project_createdbyID,project_createdate,project_workbag,project_progress) 
               VALUES 
               ('".$projectdata['clientID']."','".$projectdata['salesuserID']."','".$projectdata['job_bag']."','".str_replace("'", "&#039;", $projectdata['project_name'])."','".$projectdata['project_type']."','".$projectdata['project_status']."','".$projectdata['project_budget']."','".$projectdata['project_priority']."','".strtotime($projectdata['project_duedate'])."','".strtotime($projectdata['project_followupdate'])."','".$projectdata['staffID']."','".$timestamp."','".$projectdata['project_workbag']."','Ongoing')";
               $query = $this->db->query($sql);

               if ($projectdata['recuring'] == "Y") {
                    if ($projectdata['rec_nextrenew'] == ""){
                         if($projectdata['rec_frequency'] == 'M'){ $timestamp = strtotime('+30 days', strtotime($projectdata['rec_lastdate'])); }
                         elseif($projectdata['rec_frequency'] == 'Q'){ $timestamp = strtotime('+90 days', strtotime($projectdata['rec_lastdate'])); }
                         elseif($projectdata['rec_frequency'] == 'A'){ $timestamp = strtotime('+365 days', strtotime($projectdata['rec_lastdate'])); }
                         elseif($projectdata['rec_frequency'] == 'T'){ $timestamp = strtotime('+730 days', strtotime($projectdata['rec_lastdate'])); }
                    } else {
                         $timestamp = strtotime($projectdata['rec_nextrenew']);
                    }
          

          $sql2 = "INSERT INTO tbl_recuring (
               clientID,
               domainID,
               rec_name,
               rec_startdate,
               rec_lastdate,
               rec_rate,
               rec_frequency,
               rec_notify,
               rec_details,
               rec_nextrenew) 
          VALUES 
               ('".$projectdata['clientID']."',
               '".$projectdata['domainID']."',
               '".$projectdata['project_name']."',
               '".strtotime($projectdata['rec_startdate'])."',
               '".strtotime($projectdata['rec_lastdate'])."',
               '".$projectdata['rec_rate']."',
               '".$projectdata['rec_frequency']."',
               '".$projectdata['rec_notify']."',
               '".$projectdata['rec_details']."',
               '".$timestamp."')";

          $query2 = $this->db->query($sql2);
          }

          if ($projectdata['project_type'] == 'Security and Maintenance') {
               $sql = "SELECT projectID FROM tbl_projects WHERE project_name = '".$projectdata['project_name']."' AND clientID = '".$projectdata['clientID']."' AND project_type = '".$projectdata['project_type']."'  AND salesuserID = '".$projectdata['salesuserID']."'";
               $query = $this->db->query($sql);
               $projectID = $query->result_array();

               $sql2 = "INSERT INTO tbl_projects_security (projectID) VALUES ('".$projectID[0]['projectID']."')";
               $query2 = $this->db->query($sql2);
          }

               if ($projectdata['job_bag'] == "") {
                    $sql = "SELECT projectID FROM tbl_projects WHERE project_name = '".$projectdata['project_name']."' AND clientID = '".$projectdata['clientID']."' AND project_type = '".$projectdata['project_type']."'  AND salesuserID = '".$projectdata['salesuserID']."'";
                    $query = $this->db->query($sql);
                    return $query->result_array();
               } else {

                    $newjobbag = $projectdata['job_bag'];

                    $sql2 = "UPDATE tbl_global SET global_value = '".$projectdata['job_bag']."' WHERE valueID = '1';";
                    $query2 = $this->db->query($sql2);

                    $sql = "SELECT projectID FROM tbl_projects WHERE job_bag = '".$projectdata['job_bag']."'";
                    $query = $this->db->query($sql);
                    return $query->result_array();

                    //return $row_cnt." - ".$projectdata['job_bag'];
                    //return $array;
               }
          } else {
               return "JBE";
          }

          
     }

     function new_project_staffassigned ($projectdata,$projectID) {
          foreach ($projectdata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_projects_assign (projectID,userID) 
               VALUES 
               ('".$projectID[0]['projectID']."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function update_project_staffassigned ($projectdata,$projectID) {
          $sql = "DELETE FROM tbl_projects_assign WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);

          foreach ($projectdata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_projects_assign (projectID,userID) 
               VALUES 
               ('".$projectID."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function match_sales_project ($projectdata,$projectID) {
          $sql = "INSERT INTO tbl_salesproposals_conected (projectID,salesID) 
          VALUES 
          ('".$projectID[0]['projectID']."','".$projectdata['propConected']."')";
          $query = $this->db->query($sql);
     }

     function get_salesproposals_project_conected($projectID) {
          $sql = "SELECT * FROM tbl_salesproposals_conected WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function delete_project_info($projectID) {
          $sql = "DELETE FROM tbl_projects WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
     }

     function quickupdate_project_status($projectID, $status) {
          $sql = "UPDATE tbl_projects SET project_status = '".str_replace("%20"," ",$status)."' WHERE projectID = '".$projectID."'";
          $query = $this->db->query($sql);
     }

     function quickupdate_project_list($projectID, $userID, $month) {
          if ($month == 'M') {
               if ($userID == 'NA') {
                    $sql = "UPDATE tbl_projects_security SET security_march = 'N', march_by = '0' WHERE projectID = '".$projectID."'";
               } else {
                    $sql = "UPDATE tbl_projects_security SET security_march = 'Y', march_by = '".$userID."' WHERE projectID = '".$projectID."'";
               }
          } elseif ($month == 'J') {
               if ($userID == 'NA') {
                    $sql = "UPDATE tbl_projects_security SET security_june = 'N', june_by = '0' WHERE projectID = '".$projectID."'";
               } else {
                    $sql = "UPDATE tbl_projects_security SET security_june = 'Y', june_by = '".$userID."' WHERE projectID = '".$projectID."'";
               }
          } elseif ($month == 'S') {
               if ($userID == 'NA') {
                    $sql = "UPDATE tbl_projects_security SET security_sep = 'N', sep_by = '0' WHERE projectID = '".$projectID."'";
               } else {
                    $sql = "UPDATE tbl_projects_security SET security_sep = 'Y', sep_by = '".$userID."' WHERE projectID = '".$projectID."'";
               }
          } elseif ($month == 'D') {
               if ($userID == 'NA') {
                    $sql = "UPDATE tbl_projects_security SET security_dec = 'N', dec_by = '0' WHERE projectID = '".$projectID."'";
               } else {
                    $sql = "UPDATE tbl_projects_security SET security_dec = 'Y', dec_by = '".$userID."' WHERE projectID = '".$projectID."'";
               }
          }
          
          $query = $this->db->query($sql);
          return $sql;
     }
}?>