<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class clientcontact_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_client_contact($contactID) {
          $sql = "SELECT * FROM tbl_client_contact WHERE contactID = '".$contactID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_contact_info ($clientdata) {
          $sql = "UPDATE tbl_client_contact SET 
          contact_name = '".$clientdata['contact_name']."',
          contact_lastname = '".$clientdata['contact_lastname']."',
          contact_jobtitle = '".$clientdata['contact_jobtitle']."',
          contact_add_num = '".$clientdata['contact_add_num']."',
          contact_add_street = '".$clientdata['contact_add_street']."',
          contact_add_city = '".$clientdata['contact_add_city']."',
          contact_add_state = '".$clientdata['contact_add_state']."',
          contact_add_country = '".$clientdata['contact_add_country']."',
          contact_add_postcode = '".$clientdata['contact_add_postcode']."',
          contact_phone = '".$clientdata['contact_phone']."',
          contact_fax = '".$clientdata['contact_fax']."',
          contact_mobile = '".$clientdata['contact_mobile']."',
          contact_email = '".$clientdata['contact_email']."',
          contact_hotdrink = '".$clientdata['contact_hotdrink']."'
          WHERE contactID = ".$clientdata['contactid']."";
          $query = $this->db->query($sql);
     }

     function new_contact_info ($clientdata) {
          $sql = "INSERT INTO tbl_client_contact (clientID,contact_name,contact_lastname,contact_jobtitle,contact_add_num,contact_add_street,contact_add_city,contact_add_state,contact_add_country,contact_add_postcode,contact_phone,contact_fax,contact_mobile,contact_email,contact_hotdrink) 
          VALUES 
          ('".$clientdata['clientid']."','".$clientdata['contact_name']."','".$clientdata['contact_lastname']."','".$clientdata['contact_jobtitle']."','".$clientdata['contact_add_num']."','".$clientdata['contact_add_street']."','".$clientdata['contact_add_city']."','".$clientdata['contact_add_state']."','".$clientdata['contact_add_country']."','".$clientdata['contact_add_postcode']."','".$clientdata['contact_phone']."','".$clientdata['contact_fax']."','".$clientdata['contact_mobile']."','".$clientdata['contact_email']."','".$clientdata['contact_hotdrink']."')";
          $query = $this->db->query($sql);
     }

     function delete_clientcontact_info($contactID) {
          $sql = "DELETE FROM tbl_client_contact WHERE contactID = '".$contactID."'";
          $query = $this->db->query($sql);
     }
}?>