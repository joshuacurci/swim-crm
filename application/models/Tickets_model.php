<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class tickets_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_tickets() {
          $sql = "SELECT * FROM tbl_tickets ORDER BY ticketID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_tickets($userID) {
          $sql = "SELECT ticketID FROM tbl_tickets_assign WHERE userID = '".$userID."' ORDER BY ticketID DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_ticket_info($ticketID) {
          $sql = "SELECT * FROM tbl_tickets WHERE ticketID = '".$ticketID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_ticket_domains($domainID) {
          $sql = "SELECT * FROM tbl_domains WHERE domainID = '".$ticketdata['ticket_domain']."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function search_tickets($searchData) {
          $sql = "SELECT * FROM tbl_tickets WHERE assignID LIKE '%".$searchData['assignID']."%' AND ticket_title LIKE '%".$searchData['ticket_title']."%' AND ticket_status LIKE '%".$searchData['ticket_status']."%' ORDER BY ticket_title DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_ticket_tasks($ticketID) {
          $sql = "SELECT * FROM tbl_tickets_tasks WHERE ticketID = '".$ticketID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_ticket_notes($ticketID) {
          $sql = "SELECT * FROM tbl_tickets_notes WHERE ticketID = '".$ticketID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_ticket_asigned_staff($ticketID) {
          $sql = "SELECT * FROM tbl_tickets_assign WHERE ticketID = '".$ticketID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_support_tickets($userID) {
          $sql = "SELECT * FROM tbl_tickets WHERE assignID = '".$userID."' AND ticket_status = 'Open'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_ticket_info ($ticketdata) {
          $sql = "UPDATE tbl_tickets SET
          assignID = '".$ticketdata['assignID']."',
          ticket_title = '".str_replace("'", "&#039;", $ticketdata['ticket_title'])."',
          ticket_urgency = '".$ticketdata['ticket_urgency']."',
          ticket_type = '".$ticketdata['ticket_type']."', 
          ticket_status = '".$ticketdata['ticket_status']."',
          ticket_info = '".str_replace("'", "&#039;", $ticketdata['ticket_info'])."', 
          ticket_domain = '".$ticketdata['ticket_domain']."'          
          WHERE ticketID = ".$ticketdata['ticketID']."";
          $query = $this->db->query($sql);
     }

     function new_ticket_info ($ticketdata) {
          $timestamp = time();
          $sql = "INSERT INTO tbl_tickets (
          ticket_title,
          ticket_urgency,
          ticket_type,
          ticket_status,
          ticket_createdbyID,          
          ticket_domain,
          ticket_info,
          ticket_createdate,
          assignID) VALUES 
          ('".str_replace("'", "&#039;", $ticketdata['ticket_title'])."',
          '".$ticketdata['ticket_urgency']."',
          '".$ticketdata['ticket_type']."',
          '".$ticketdata['ticket_status']."',
          '".$ticketdata['staffID']."',
          '".$ticketdata['ticket_domain']."',
          '".str_replace("'", "&#039;", $ticketdata['ticket_info'])."',
          '".$timestamp."',
          '".$ticketdata['assignID']."')";
          $query = $this->db->query($sql);

     }

     function update_ticket_staffassigned ($ticketdata,$ticketID) {
          $sql = "UPDATE tbl_tickets_assign SET ticketID = '".$ticketdata['ticketID']."', assignID = '".$ticketdata['assignID']."' WHERE ticketID = ".$ticketdata['ticketID']."";
          $query = $this->db->query($sql);

     }

     function delete_ticket_info($ticketID) {
          $sql = "DELETE FROM tbl_tickets WHERE ticketID = '".$ticketID."'";
          $query = $this->db->query($sql);
     }
}?>