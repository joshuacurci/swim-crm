<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class projectexpences_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_expence_info($expenceID) {
          $sql = "SELECT * FROM tbl_projects_expences WHERE expenceID = '".$expenceID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_project_expence_info($projectID, $expenceID) {
          $sql = "SELECT * FROM tbl_projects_expences WHERE projectID = '".$projectID."' ORDER BY expence_dateinvoicedue DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_expence_info ($clientdata) {
          $sql = "UPDATE tbl_projects_expences SET 
          projectID = '".$clientdata['projectID']."',
          clientID = '".$clientdata['userID']."',
          expence_name = '".str_replace("'", "&#039;", $clientdata['expence_name'])."',
          expence_value = '".$clientdata['expence_value']."',
          expence_markup = '".$clientdata['expence_markup']."',
          expence_clientcharged = '".$clientdata['expence_clientcharged']."',
          expence_dateinvoicedue = '".strtotime($clientdata['expence_dateinvoicedue'])."',
          expence_supppaid = '".strtotime($clientdata['expence_supppaid'])."',
          expence_condition = '".$clientdata['expence_condition']."',
          expence_jobbag = '".$clientdata['expence_jobbag']."',
          expence_notes = '".str_replace("'", "&#039;", $clientdata['expence_notes'])."'
          WHERE expenceID = ".$clientdata['expenceID']."";
          $query = $this->db->query($sql);
     }

     function new_project_expence_info ($clientdata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_projects_expences (projectID,clientID,userID,expence_name,expence_value,expence_markup,expence_clientcharged,expence_dateinvoicedue,expence_supppaid,expence_condition,expence_jobbag,expence_notes) 
          VALUES 
          ('".$clientdata['projectID']."',
          '".$clientdata['clientID']."',
          '".$clientdata['userID']."',
          '".str_replace("'", "&#039;", $clientdata['expence_name'])."',
          '".$clientdata['expence_value']."',
          '".$clientdata['expence_markup']."',
          '".$clientdata['expence_clientcharged']."',
          '".strtotime($clientdata['expence_dateinvoicedue'])."',
          '".strtotime($clientdata['expence_supppaid'])."',
          '".$clientdata['expence_condition']."',
          '".$clientdata['expence_jobbag']."',
          '".str_replace("'", "&#039;", $clientdata['expence_notes'])."')";
          $query = $this->db->query($sql);
     }

     function delete_expence_info($expenceID) {
          $sql = "DELETE FROM tbl_projects_expences WHERE expenceID = '".$expenceID."'";
          $query = $this->db->query($sql);
     }

     function send_notification($clientdata) {
          $to = 'joe@swim.com.au';

          $subject = 'New expense Request';

               //User Data
          $sqluser = "SELECT * FROM tbl_users WHERE userID = '".$clientdata['userID']."'";
          $queryuser = $this->db->query($sqluser);
          $userdata = $queryuser->result_array();

          $headers = "From: " . $userdata[0]['email'] . "\r\n";
          $headers .= "Reply-To: ". $userdata[0]['email'] . "\r\n";
          $headers .= "MIME-Version: 1.0\r\n";
          $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

               //Project Data
          $sqlproject = "SELECT * FROM tbl_projects WHERE projectID = '".$clientdata['projectID']."'";
          $queryproject = $this->db->query($sqlproject);
          $projectdata = $queryproject->result_array();

               //Client Data
          $sqlclient = "SELECT * FROM tbl_client WHERE clientID = '".$clientdata['clientID']."'";
          $queryclient = $this->db->query($sqlclient);
          $clientdata = $queryclient->result_array();

          $message = '<html><body>';
          $message .= '<h1>New expense Request!</h1>';
          $message .= '<table>';
          $message .= '<tr><td>Job Bag Number</td><td>#'.$projectdata[0]['job_bag'].'</td></tr>';
          $message .= '<tr><td>Job Name</td><td>'.$projectdata[0]['project_name'].'</td></tr>';
          $message .= '<tr><td>Client</td><td>'.$clientdata[0]['client_name'].'</td></tr>';
          $message .= '<tr><td></td><td></td></tr>';
          $message .= '<tr><td>Nature of expense</td><td>'.$projectdata[0]['job_bag'].'</td></tr>';
          $message .= '<tr><td>Nature of expense</td><td>'.$projectdata[0]['job_bag'].'</td></tr>';
          $message .= '</table>';
          $message .= '</body></html>';

          mail($to, $subject, $message, $header);
     }
}?>