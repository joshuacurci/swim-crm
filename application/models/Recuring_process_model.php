<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class recuring_process_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

	function process_renewals() {
          $sql = "SELECT * FROM tbl_recuring_item WHERE recitem_completed = 'N'";
          $query = $this->db->query($sql);
          $results = $query->result_array();

          $returnValues = "";

          foreach($results as $row) {
			// Get job bag number
			$getJB = "SELECT * FROM tbl_global WHERE valueID = '1'";
          	$getJB_query = $this->db->query($getJB);
          	$currentJB = $getJB_query->result_array();

          	// Get recurring details
			$getREC = "SELECT * FROM tbl_recuring WHERE recID = '".$row['recID']."'";
          	$getREC_query = $this->db->query($getREC);
          	$currentREC = $getREC_query->result_array();

          	// New JB number
            if (($currentJB[0]['global_value'] > 7500 && $currentJB[0]['global_value'] < 8000)){ $newJB = '8000';} else {$newJB = $currentJB[0]['global_value']+1;}

			// Create job bag
			$timestamp = time();
            $createSQL = "INSERT INTO tbl_projects (
            	clientID,
            	salesuserID,
            	job_bag,
            	project_name,
            	project_type,
            	project_status,
            	project_budget,
            	project_priority,
            	project_duedate,
            	project_followupdate,
            	project_createdbyID,
            	project_createdate,
            	project_workbag) 
            VALUES 
            ('".$currentREC[0]['clientID']."',
            	'9',
            	'".$newJB."',
            	'".$currentREC[0]['rec_name']."',
            	'Renewal',
            	'Job in progress',
            	'".$currentREC[0]['rec_rate']."',
            	'Medium',
            	'".$currentREC[0]['rec_nextrenew']."',
            	'".$currentREC[0]['rec_nextrenew']."',
            	'9',
            	'".$timestamp."',
            	NULL)";
            $projectQuery = $this->db->query($createSQL);/**/

      // Get projectID
      $getprojectID = "SELECT * FROM tbl_projects WHERE job_bag = '".$newJB."'";
      $getprojectID_query = $this->db->query($getprojectID);
      $currentprojectID = $getprojectID_query->result_array();

			// Update job bag number
			$sql2 = "UPDATE tbl_global SET global_value = '".$newJB."' WHERE valueID = '1';";
			$query2 = $this->db->query($sql2);

			// Change the rec item next date
			if($currentREC[0]['rec_frequency'] == 'M'){ $nextDate = strtotime('+30 days', $currentREC[0]['rec_nextrenew']); }
			elseif($currentREC[0]['rec_frequency'] == 'Q'){ $nextDate = strtotime('+90 days', $currentREC[0]['rec_nextrenew']); }
			elseif($currentREC[0]['rec_frequency'] == 'A'){ $nextDate = strtotime('+365 days', $currentREC[0]['rec_nextrenew']); }
			elseif($currentREC[0]['rec_frequency'] == 'T'){ $nextDate = strtotime('+730 days', $currentREC[0]['rec_nextrenew']); }

			$upRec = "UPDATE tbl_recuring SET rec_nextrenew = '".$nextDate."', rec_lastdate = '".time()."' WHERE recID = '".$row['recID']."';";
			$this->db->query($upRec);

			// Update the item status
			$upItem = "UPDATE tbl_recuring_item SET recitem_completed = 'Y', projectID = '".$currentprojectID[0]['projectID']."', recitem_dateactioned = ".time()." WHERE recitemID = '".$row['recitemID']."';";
			$this->db->query($upItem);

			$returnValues[] = array(
				"assignedJB" => $newJB,
				"Name" => $currentREC[0]['rec_name'],
				"clientID" => $currentREC[0]['clientID']
			);
          }

          return $returnValues;
     }

}?>