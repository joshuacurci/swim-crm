<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class salesproposals_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_salesproposals() {
          $sql = "SELECT * FROM tbl_salesproposals WHERE sales_deleted = 'N' ORDER BY sales_createddate DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_salesproposals_client($clientID) {
          $sql = "SELECT * FROM tbl_salesproposals WHERE sales_clientID = '".$clientID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_salesproposals_conected($salesID) {
          $sql = "SELECT * FROM tbl_salesproposals_conected WHERE salesID = '".$salesID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_salesproposals_info($salesID) {
          $sql = "SELECT * FROM tbl_salesproposals WHERE salesID = '".$salesID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_salesproposals_notes($salesID) {
          $sql = "SELECT * FROM tbl_salesproposals_notes WHERE salesID = '".$salesID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_salesproposals ($postedData) {
        if ($postedData['existingcustomer'] == 'N') {
          $sql = "UPDATE tbl_salesproposals SET 
          sales_title = '".str_replace("'", "&#039;", $postedData['sales_title'])."',
          sales_budget = '".$postedData['sales_budget']."',
          sales_goingahead = '".$postedData['sales_goingahead']."',
          sales_new = '".$postedData['existingcustomer']."',
          sales_aheadwith = '".$postedData['sales_aheadwith']."',
          sales_startdate = '".strtotime($postedData['sales_startdate'])."',
          sales_followup = '".strtotime($postedData['sales_followup'])."',
          sales_details = '".str_replace("'", "&#039;", $postedData['sales_details'])."',
          sales_duedate = '".strtotime($postedData['sales_duedate'])."'
          WHERE salesID = ".$postedData['salesID']."";
        } else {
          $sql = "UPDATE tbl_salesproposals SET 
          sales_title = '".str_replace("'", "&#039;", $postedData['sales_title'])."',
          sales_companyname = '".$postedData['sales_companyname']."',
          sales_name = '".$postedData['sales_name']."',
          sales_lastname = '".$postedData['sales_lastname']."',
          sales_add_1 = '".$postedData['sales_add_1']."',
          sales_add_2 = '".$postedData['sales_add_2']."',
          sales_add_city = '".$postedData['sales_add_city']."',
          sales_add_state = '".$postedData['sales_add_state']."',
          sales_add_country = '".$postedData['sales_add_country']."',
          sales_add_postcode = '".$postedData['sales_add_postcode']."',
          sales_phone = '".$postedData['sales_phone']."',
          sales_fax = '".$postedData['sales_fax']."',
          sales_mobile = '".$postedData['sales_mobile']."',
          sales_email = '".$postedData['sales_email']."',
          sales_budget = '".$postedData['sales_budget']."',
          sales_new = '".$postedData['existingcustomer']."',
          sales_goingahead = '".$postedData['sales_goingahead']."',
          sales_aheadwith = '".$postedData['sales_aheadwith']."',
          sales_startdate = '".strtotime($postedData['sales_startdate'])."',
          sales_followup = '".strtotime($postedData['sales_followup'])."',
          sales_details = '".str_replace("'", "&#039;", $postedData['sales_details'])."',
          sales_duedate = '".strtotime($postedData['sales_duedate'])."'
          WHERE salesID = ".$postedData['salesID']."";
        }


          $query = $this->db->query($sql);
     }

     function new_salesproposals ($postedData) {
          $timestamp = time();

          if ($postedData['existingcustomer'] == 'N') {
            $sql = "INSERT INTO tbl_salesproposals 
                 (sales_title,
                  sales_clientID,
                  sales_contactID,
                  sales_budget,
                  sales_goingahead,
                  sales_aheadwith,
                  sales_startdate,
                  sales_duedate,
                  sales_followup,
                  sales_createddate,
                  sales_createdby,
                  sales_details,
                  sales_new,
                  sales_status) 
            VALUES 
            ('".str_replace("'", "&#039;", $postedData['sales_title'])."',
              '".$postedData['clientID']."',
              '".$postedData['contactID']."',
             '".$postedData['sales_budget']."',
             '".$postedData['sales_goingahead']."',
             '".$postedData['sales_aheadwith']."',
             '".strtotime($postedData['sales_startdate'])."',
             '".strtotime($postedData['sales_duedate'])."',
             '".strtotime($postedData['sales_followup'])."',
             '".$timestamp."',
             '".$postedData['sales_createdby']."',
             '".str_replace("'", "&#039;", $postedData['sales_details'])."',
             'N',
             'A')";
        } else {
          $sql = "INSERT INTO tbl_salesproposals 
               (sales_title,
                sales_companyname,
                sales_name,
                sales_lastname,
                sales_add_1,
                sales_add_2,
                sales_add_city,
                sales_add_state,
                sales_add_country,
                sales_add_postcode,
                sales_phone,
                sales_fax,
                sales_mobile,
                sales_email,
                sales_budget,
                sales_goingahead,
                sales_aheadwith,
                sales_startdate,
                sales_duedate,
                sales_followup,
                sales_createddate,
                sales_createdby,
                sales_details,
                sales_new,
                sales_status) 
          VALUES 
          ('".str_replace("'", "&#039;", $postedData['sales_title'])."',
           '".$postedData['sales_companyname']."',
           '".$postedData['sales_name']."',
           '".$postedData['sales_lastname']."',
           '".$postedData['sales_add_1']."',
           '".$postedData['sales_add_2']."',
           '".$postedData['sales_add_city']."',
           '".$postedData['sales_add_state']."',
           '".$postedData['sales_add_country']."',
           '".$postedData['sales_add_postcode']."',
           '".$postedData['sales_phone']."',
           '".$postedData['sales_fax']."',
           '".$postedData['sales_mobile']."',
           '".$postedData['sales_email']."',
           '".$postedData['sales_budget']."',
           '".$postedData['sales_goingahead']."',
           '".$postedData['sales_aheadwith']."',
           '".strtotime($postedData['sales_startdate'])."',
           '".strtotime($postedData['sales_duedate'])."',
           '".strtotime($postedData['sales_followup'])."',
           '".$timestamp."',
           '".$postedData['sales_createdby']."',
           '".str_replace("'", "&#039;", $postedData['sales_details'])."',
           'Y',
           'A')";
        }
          $query = $this->db->query($sql);
     }

     function delete_salesproposals($projectID) {
      $sql = "UPDATE tbl_salesproposals SET sales_deleted = 'Y' WHERE salesID = '".$projectID."'";
      $query = $this->db->query($sql);
     }

     
}?>