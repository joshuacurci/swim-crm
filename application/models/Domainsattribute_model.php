<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class domainsattribute_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     //get the username & password from tbl_usrs
     function get_domains() {
          $sql = "SELECT * FROM tbl_domains ORDER BY job_bag DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domain_info($domainID) {
          $sql = "SELECT * FROM tbl_domains WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_domains_attribute($domainID) {
          $sql = "SELECT * FROM tbl_domains_attributes WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
      
      function get_domains_attribute_info($domainattID) {
          $sql = "SELECT * FROM tbl_domains_attributes WHERE domainattID = '".$domainattID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_user_domains_attribute($userID) {
          $sql = "SELECT * FROM tbl_domains_attributes WHERE assuserID = '".$userID."' AND task_status = 'A' ORDER BY task_duedate ASC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }
     
     function get_domain_asigned_staff($domainID) {
          $sql = "SELECT * FROM tbl_domains_assign WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_domainsattribute_info ($domaindata) {
          $sql = "UPDATE tbl_domains_attributes SET 
          domainID = '".$domaindata['domainID']."',
          att_name = '".str_replace("'", "&#039;", $domaindata['att_name'])."',
          att_version = '".$domaindata['att_version']."',
          att_type = '".$domaindata['att_type']."',
          att_details = '".str_replace("'", "&#039;", $domaindata['att_details'])."'
          WHERE domainattID = ".$domaindata['domainattID']."";
          $query = $this->db->query($sql);
     }

     function new_domainsattribute_info ($domaindata) {
          $timestamp = time();
          $sql = "INSERT INTO tbl_domains_attributes (domainID,att_name,att_version,att_type,att_details) 
          VALUES 
          ('".$domaindata['domainID']."','".str_replace("'", "&#039;", $domaindata['att_name'])."','".$domaindata['att_version']."','".$domaindata['att_type']."','".str_replace("'", "&#039;", $domaindata['att_details'])."')";
          $query = $this->db->query($sql);
     }

     function new_domain_staffassigned ($domaindata,$domainID) {
          foreach ($domaindata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_domains_assign (domainID,userID) 
               VALUES 
               ('".$domainID[0]['domainID']."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function update_domain_staffassigned ($domaindata,$domainID) {
          $sql = "DELETE FROM tbl_domains_assign WHERE domainID = '".$domainID."'";
          $query = $this->db->query($sql);

          foreach ($domaindata['assigned_staff'] as $newuserid) {
               $sql = "INSERT INTO tbl_domains_assign (domainID,userID) 
               VALUES 
               ('".$domainID."','".$newuserid."')";
               $query = $this->db->query($sql);
          }
     }

     function delete_domainsattribute_info($domainattID) {
          $sql = "DELETE FROM tbl_domains_attributes WHERE domainattID = '".$domainattID."'";
          $query = $this->db->query($sql);
     }
}?>