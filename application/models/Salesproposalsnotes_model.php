<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class salesproposalsnotes_model extends CI_Model
{
     function __construct()
     {
          // Call the Model constructor
          parent::__construct();
     }

     function get_salesnotes_note($noteID) {
          $sql = "SELECT * FROM tbl_salesproposals_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function get_salesnotes_info($clientID) {
          $sql = "SELECT * FROM tbl_salesproposals_notes WHERE salesID = '".$clientID."' ORDER BY note_date DESC";
          $query = $this->db->query($sql);
          return $query->result_array();
     }

     function update_salesnotes_info ($clientdata) {
          $sql = "UPDATE tbl_salesproposals_notes SET 
          staffID = '".$clientdata['staffID']."',
          salesID = '".$clientdata['salesID']."',
          note_title = '".str_replace("'", "&#039;", $clientdata['note_title'])."',
          note_content = '".str_replace("'", "&#039;", $clientdata['note_content'])."',
          note_type = '".$clientdata['note_type']."',
          note_date = '".$clientdata['note_date']."'
          WHERE noteID = ".$clientdata['noteID']."";
          $query = $this->db->query($sql);
     }

     function new_salesnotes_info ($clientdata) {
          $currentdate = time();
          $sql = "INSERT INTO tbl_salesproposals_notes (staffID,salesID,note_title,note_content,note_type,note_date) 
          VALUES 
          ('".$clientdata['staffID']."','".$clientdata['salesID']."','".str_replace("'", "&#039;", $clientdata['note_title'])."','".str_replace("'", "&#039;", $clientdata['note_content'])."','".$clientdata['note_type']."','".$currentdate."')";
          $query = $this->db->query($sql);
     }

     function delete_salesnotes_info($noteID) {
          $sql = "DELETE FROM tbl_salesproposals_notes WHERE noteID = '".$noteID."'";
          $query = $this->db->query($sql);
     }
}?>

