###################
Change Log
###################

**************************
Version 0.7.3
**************************
- Updated recorses page
- Added the ability to hide archived staff profiles
- Fixed up spacing in client section
- Added aditional job statuses and fixed bugs with date fields

Bugs
--------------------------
BR064 - Need new item added to list - Support
BR065 - Layout of client page needs touching up (COMPLETED)
BR066 - Under server, you should have seperate coptions for the different Web Prophets servers (COMPLETED)
BR069 - Getting 404 error when clicking cancel button on project edit page (FIXED)

**************************
Version 0.7.2
**************************
- Recurring items recure on a CRON
- Welcome form imports into CRM
- Add a new WIP page with quick note
- Transfer proposals to a job bag

Bugs
--------------------------
BR056 - Added extra domain status
BR058 - Block out job bag numbers
BR061 - Added links to the dashboard
BR062 - Error when adding client note (FIXED)

**************************
Version 0.7.1
**************************
- Add recuring block to client section
- Add sale in progress block to the client section
- Prospect name is now in the index
- Fixed single quote issue for all options

Bugs
--------------------------
BR043 - Auto populate client in sales proposal
BR049 - Link sales proposal to client
BR050 - Add recuring option for every 2 years (FIXED)
BR051 - Changed Add new proposal to Add new prospect
BR052 - Changed Prospect contact name to 2 feilds (FIXED)
BR053 - Add 'Add new proposal' to dashboard
BR054 - Sales and Prop notes have comma error (FIXED)
BR055 - Support tickets arn't saving (FIXED)
BR057 - Added in 'traffic light' system for client view for projects

**************************
Version 0.7.0
**************************
- Resources Page has been added
- Half of the recurring items are complete

Bugs
--------------------------
BR047 - Relabel Sales & proposals to Sales in progress
BR046 - Budget input error
BR045 - Add extra work bag option
BR037 - View all projects button
BR042 - Add a contact button to dashboard
BR044 - Duplicate of BR045
BR048 - Click to mail for a client contact

**************************
Version 0.6.0
**************************
- Staff Section
- Add a complete button to project tasks
- Add 'Submit and add another' to the client contact section

Bugs
--------------------------
BR039 - Add job title for client contact
BR038 - Have the ability to set "resold by" to no one
BR040 - Set defult priority for tasks to medium
BR041 - Add the ability to set a project as a 'Work Bag'

**************************
Version 0.5.0
**************************
- Sales and Proposals section

Bugs
--------------------------
BR034 - Spelling mistake for expence -> Fixed
BR028 - Remove client type and replace with reseller option
BR033 - Added project contacts

**************************
Version 0.4.0
**************************
- Include client filter for each letter
- Client contact search
- Added in Domain Attributes
- Added in Domain Renewals
- Added in Domain Notes
- Included the functionality to add a task/hours then add another instead of redirecting back to the project page
- Added a 'resold by' option for clients
- Added in the option to assign project contacts based on the projects assigned client
- Included a client archive option


**************************
Version 0.3.2
**************************
- Add hours from task
- Add hosting cost and details

Bugs
--------------------------
BR031 - Make VIC defult state -> Fixed
BR032 - Change button text for client go back
BR035 - Domain renewal dates not saving -> Fixed

**************************
Version 0.3.1
**************************
- Added in domain search
- Added in support ticket search
- Removed Webhosts and Resellers from menu

Bugs
--------------------------
BR018 - Responsive styling fixed up -> Fixed
BR022 - Option to set if a domain is manages by SWiM or External
BR023 - Client values coming up in webshost dropdown -> Fixed
BR025 - Add a client contact button to the dashboard -> Fixed
BR026 - View project but took me to add client contact -> Fixed
BR027 - Add an 'Add client contact' button to home page -> Fixed
BR029 - Remove the budget formating -> Completed
BR030 - Add aditional options to the project type and status -> Completed

**************************
Version 0.3.0
**************************
- Cleaned up URL structure
- Auto populated Job Bag number
- Improved Date picker
- Support Tickets section now active
- Domain information imported

Bugs
--------------------------
BR014 - Add a cancel button to every form page -> Fixed
BR015 - Add a today button and fix styling for date picker -> Fixed
BR016 - Issue with adding multiple contacts -> Fixed
BR017 - Fix auto close on date picker -> Fixed
BR019 - Clicked on add domain in client section and went to add contact -> Fixed
BR020 - Add other to project type -> Fixed
BR021 - Add video to project type -> Fixed

**************************
Version 0.2.0
**************************
- Domains section now available. Data to be added in soon. Atributes soon to be complete

Bugs
--------------------------
BR012 - Client contact to have first and last name seperate -> Fixed
BR011 - Set medium urgency as the defult -> Fixed
BR013 - Adding job bag letters to the end -> Fixed with user input

**************************
Version 0.1.2
**************************
- Added footer to Dashboard

Bugs
--------------------------
BR002 - Default list of projects -> Fixed
BR009 - Client Name and Project name are hotlinks -> Fixed
BR010 - New project and New client button -> Fixed


**************************
Version 0.1.1
**************************

Bugs
--------------------------
BR001 - First letter of the task title to be a capital -> Fixed
BR003 - Auto populate client when coming from client view -> Fixed
BR004 - Quick add project on the client page -> Fixed
BR005 - Open bug link in a new tab -> Fixed
BR006 - Defult Salesman is logged in user -> Fixed
BR007 - Missing Client -> NA
BR008 - Pre exsisting function

**************************
Version 0.1
**************************
The inital upload
Clients and Project sections are fully functioning

