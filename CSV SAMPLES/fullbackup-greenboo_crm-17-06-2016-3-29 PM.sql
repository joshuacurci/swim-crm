# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 122.129.218.34 (MySQL 5.5.47-cll)
# Database: greenboo_crm
# Generation Time: 2016-06-17 05:29:37 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `site_access`, `lastlogin`)
VALUES
	(3,'A','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',NULL,'June 17, 2016, 11:14 am'),
	(4,'A','joe@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joe Manariti','joe@swim.com.au','A',NULL,'June 17, 2016, 9:36 am'),
	(5,'A','mike@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Michael Brooks','mike@swim.com.au','A',NULL,NULL),
	(6,'A','jacob@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Jacob Leech','jacob@swim.com.au','A',NULL,'June 15, 2016, 2:12 pm'),
	(7,'A','rachelle@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Rachelle Salvadora','rachelle@swim.com.au','A',NULL,'June 15, 2016, 2:12 pm'),
	(8,'A','genevieve@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Genevieve Laidlaw','genevieve@swim.com.au','A',NULL,'June 14, 2016, 3:45 pm'),
	(9,'A','accounts@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Jules Pullen','accounts@swim.com.au','A',NULL,'June 17, 2016, 12:09 pm'),
	(10,'A','layla@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Layla Stanton','layla@swim.com.au','A',NULL,NULL);

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Admin','B'),
	(3,'Accounts','C'),
	(4,'Studio','S'),
	(5,'Account Manager','M'),
	(6,'Private','P'),
	(7,'Contractor','Z');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
