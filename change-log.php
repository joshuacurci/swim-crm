<h1>Change Log</h1>
<br/>

<!---------------------- Version 0.7.3 ---------------------->
<h2 id="version-0-7-2">Version 0.7.3</h2>
<p>- Updated recorses page</p>
<p>- Added the ability to hide archived staff profiles</p>
<p>- Fixed up spacing in client section</p>
<p>- Added aditional job statuses and fixed bugs with date fields</p>
<p><strong>Bugs</strong><br/>
BR064 - Need new item added to list - Support<br/>
BR065 - Layout of client page needs touching up (COMPLETED)<br/>
BR066 - Under server, you should have seperate coptions for the different Web Prophets servers (COMPLETED)<br/>
BR069 - Getting 404 error when clicking cancel button on project edit page (FIXED)<br/>
<hr/>

<!---------------------- Version 0.7.2 ---------------------->
<h2 id="version-0-7-2">Version 0.7.2</h2>
<p>- Recurring items recure on a CRON</p>
<p>- Welcome form imports into CRM</p>
<p>- Add a new WIP page with quick note</p>
<p>- Transfer proposals to a job bag</p>
<p><strong>Bugs</strong><br/>
BR056 - Added extra domain status<br/>
BR058 - Block out job bag numbers<br/>
BR061 - Added links to the dashboard<br/>
BR062 - Error when adding client note (FIXED)<br/>
<hr/>

<!---------------------- Version 0.7.1 ---------------------->
<h2 id="version-0-7-1">Version 0.7.1</h2>
<p>- Add recuring block to client section</p>
<p>- Add sale in progress block to the client section</p>
<p>- Prospect name is now in the index</p>
<p>- Fixed single quote issue for all options</p>
<p><strong>Bugs</strong><br/>
BR043 - Auto populate client in sales proposal<br/>
BR049 - Link sales proposal to client<br/>
BR050 - Add recuring option for every 2 years (FIXED)<br/>
BR051 - Changed Add new proposal to Add new prospect<br/>
BR052 - Changed Prospect contact name to 2 feilds (FIXED)<br/>
BR053 - Add 'Add new proposal' to dashboard<br/>
BR054 - Sales and Prop notes have comma error (FIXED)<br/>
BR055 - Support tickets arn't saving (FIXED)<br/>
BR057 - Added in 'traffic light' system for client view for projects</p>
<hr/>

<!---------------------- Version 0.7.0 ---------------------->
<h2 id="version-0-7-0">Version 0.7.0</h2>
<p>- Resources Page has been added</p>
<p>- Half of the recurring items are complete</p>
<p><strong>Bugs</strong><br/>
BR047 - Relabel Sales & proposals to Sales in progress<br/>
BR046 - Budget input error<br/>
BR045 - Add extra work bag option<br/>
BR037 - View all projects button<br/>
BR042 - Add a contact button to dashboard<br/>
BR044 - Duplicate of BR045<br/>
BR048 - Click to mail for a client contact</p>
<hr/>

<!---------------------- Version 0.6.0 ---------------------->
<h2 id="version-0-3-1">Version 0.6.0</h2>
<p>- Staff Section</p>
<p>- Add a complete button to project tasks</p>
<p>- Add 'Submit and add another' to the client contact section</p>
<p><strong>Bugs</strong><br/>
BR039 - Add job title for client contact<br/>
BR038 - Have the ability to set "resold by" to no one<br/>
BR040 - Set defult priority for tasks to medium<br/>
BR041 - Add the ability to set a project as a 'Work Bag'</p>
<hr/>

<!---------------------- Version 0.5.0 ---------------------->
<h2 id="version-0-3-1">Version 0.5.0</h2>
<p>- Sales and Proposals section</p>
<p><strong>Bugs</strong><br/>
BR034 - Spelling mistake for expence -> Fixed<br/>
BR028 - Remove client type and replace with reseller option<br/>
BR033 - Added project contacts</p>
<hr/>

<!---------------------- Version 0.4.0 ---------------------->
<h2 id="version-0-3-1">Version 0.4.0</h2>
<p>- Include client filter for each letter</p>
<p>- Client contact search</p>
<p>- Added in Domain Attributes</p>
<p>- Added in Domain Renewals</p>
<p>- Added in Domain Notes</p>
<p>- Included the functionality to add a task/hours then add another instead of redirecting back to the project page</p>
<p>- Added a 'resold by' option for clients</p>
<p>- Added in the option to assign project contacts based on the projects assigned client</p>
<p>- Included a client archive option</p>
<hr/>

<!---------------------- Version 0.3.2 ---------------------->
<h2 id="version-0-3-1">Version 0.3.2</h2>
<p>- Add hours from task</p>
<p>- Add hosting cost and details</p>
<p><strong>Bugs</strong><br/>
BR031 - Make VIC defult state -> Fixed<br/>
BR032 - Change button text for client go back<br/>
BR035 - Domain renewal dates not saving -> Fixed</p>
<hr/>

<!---------------------- Version 0.3.1 ---------------------->
<h2 id="version-0-3-1">Version 0.3.1</h2>
<p>- Added in domain search</p>
<p>- Added in support ticket search</p>
<p>- Removed Webhosts and Resellers from menu</p>
<p><strong>Bugs</strong><br/>
BR018 - Responsive styling fixed up -> Fixed<br/>
BR022 - Option to set if a domain is manages by SWiM or External<br/>
BR023 - Client values coming up in webshost dropdown -> Fixed<br/>
BR025 - Add a client contact button to the dashboard -> Fixed<br/>
BR026 - View project but took me to add client contact -> Fixed<br/>
BR027 - Add an 'Add client contact' button to home page -> Fixed<br/>
BR029 - Remove the budget formating -> Completed<br/>
BR030 - Add aditional options to the project type and status -> Completed</p>
<hr/>

<!---------------------- Version 0.3.0 ---------------------->
<h2 id="version-0-3-0">Version 0.3.0</h2>
<p>- Domains section now available. Data to be added in soon. Atributes soon to be complete</p>
<p>- Cleaned up URL structure</p>
<p>- Auto populated Job Bag number</p>
<p>- Improved Date picker</p>
<p>- Support Tickets section now active</p>
<p>- Domain information imported</p>
<p><strong>Bugs</strong><br/>
BR014 - Add a cancel button to every form page -> Fixed<br/>
BR015 - Add a today button and fix styling for date picker -> Fixed<br/>
BR016 - Issue with adding multiple contacts -> Fixed<br/>
BR017 - Fix auto close on date picker -> Fixed<br/>
BR019 - Clicked on add domain in client section and went to add contact -> Fixed<br/>
BR020 - Add other to project type -> Fixed<br/>
BR021 - Add video to project type -> Fixed</p>
<hr/>

<!---------------------- Version 0.3.0 ---------------------->
<h2 id="version-0-2-0">Version 0.2.0</h2>
<p>- Domains section now available. Data to be added in soon. Atributes soon to be complete</p>
<p><strong>Bugs</strong><br/>
BR012 - Client contact to have first and last name seperate -> Fixed<br/>
BR011 - Set medium urgency as the defult -> Fixed<br/>
BR013 - Adding job bag letters to the end -> Fixed with user input</p>
<hr/>

<!---------------------- Version 0.3.0 ---------------------->
<h2 id="version-0-1-2">Version 0.1.2</h2>
<p>- Added footer to Dashboard</p>
<p><strong>Bugs</strong><br/>
BR002 - Default list of projects -> Fixed<br/>
BR009 - Client Name and Project name are hotlinks -> Fixed<br/>
BR010 - New project and New client button -> Fixed</p>
<hr/>

<!---------------------- Version 0.3.0 ---------------------->
<h2 id="version-0-1-1">Version 0.1.1</h2>
<p><strong>Bugs</strong><br/>
BR001 - First letter of the task title to be a capital -> Fixed<br/>
BR003 - Auto populate client when coming from client view -> Fixed<br/>
BR004 - Quick add project on the client page -> Fixed<br/>
BR005 - Open bug link in a new tab -> Fixed<br/>
BR006 - Defult Salesman is logged in user -> Fixed<br/>
BR007 - Missing Client -> NA<br/>
BR008 - Pre exsisting function</p>
<hr/>
<h2 id="version-0-1">Version 0.1</h2>
<p>The inital upload Clients and Project sections are fully functioning</p>