# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: localhost (MySQL 5.5.42)
# Database: customcrm
# Generation Time: 2016-05-16 07:08:11 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `client_add_num` varchar(255) DEFAULT NULL,
  `client_add_street` varchar(255) DEFAULT NULL,
  `client_add_city` varchar(255) DEFAULT NULL,
  `client_add_state` varchar(255) DEFAULT NULL,
  `client_add_country` varchar(255) DEFAULT NULL,
  `client_add_postcode` varchar(5) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `client_fax` varchar(255) DEFAULT NULL,
  `client_acn` varchar(255) DEFAULT NULL,
  `client_abn` varchar(255) DEFAULT NULL,
  `client_traiding_terms` varchar(255) DEFAULT NULL,
  `client_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `client_name`, `client_add_num`, `client_add_street`, `client_add_city`, `client_add_state`, `client_add_country`, `client_add_postcode`, `client_phone`, `client_fax`, `client_acn`, `client_abn`, `client_traiding_terms`, `client_type`)
VALUES
	(2,'Sample Client','2','Street 2','City 2','NSW','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(3,'Client 3','3','Street 3','City 3','State 3','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(4,'Client 4','4','Street 4','City 4','State 4','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(5,'Client 5','5','Street 5','City 5','State 5','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(6,'Client 6','6','Street 6','City 6','State 6','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(7,'Client 7','7','Street 7','City 7','State 7','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(8,'Client 8','8','Street 8','City 8','State 8','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(9,'Client 9','9','Street 9','City 9','State 9','Australia','3001','98765432','98765432','987346215','987346215','',''),
	(11,'SWiM Communications','134','Langford Street','North Melbourne','VIC','Australia','3051','93268000','93268888','987951651984','98764316597','30 Day account','Fun');

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_contact`;

CREATE TABLE `tbl_client_contact` (
  `contactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_add_num` varchar(255) DEFAULT NULL,
  `contact_add_street` varchar(255) DEFAULT NULL,
  `contact_add_city` varchar(255) DEFAULT NULL,
  `contact_add_state` varchar(255) DEFAULT NULL,
  `contact_add_country` varchar(255) DEFAULT NULL,
  `contact_add_postcode` varchar(5) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `contact_mobile` varchar(255) DEFAULT NULL,
  `contact_fax` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_hotdrink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_contact` WRITE;
/*!40000 ALTER TABLE `tbl_client_contact` DISABLE KEYS */;

INSERT INTO `tbl_client_contact` (`contactID`, `clientID`, `contact_name`, `contact_add_num`, `contact_add_street`, `contact_add_city`, `contact_add_state`, `contact_add_country`, `contact_add_postcode`, `contact_phone`, `contact_mobile`, `contact_fax`, `contact_email`, `contact_hotdrink`)
VALUES
	(1,1,'Client 1','1','Street 1','City 1','State 1','Australia','3001','98765432','98765432','987346215',NULL,NULL),
	(2,11,'Joshua Curci','134','Langford Street','North Melbourne','VIC','Australia','3051','9326 8000','0402264959','9326 8000','josh@swim.com.au','Hot Chocolate'),
	(3,3,'Client 3','3','Street 3','City 3','State 3','Australia','3001','98765432','98765432','987346215',NULL,NULL),
	(4,1,'Client 4','4','Street 4','City 4','State 4','Australia','3001','98765432','98765432','987346215',NULL,NULL),
	(7,1,'Client 7','7','Street 7','City 7','State 7','Australia','3001','98765432','98765432','987346215',NULL,NULL),
	(9,3,'Client 9','9','Street 9','City 9','State 9','Australia','3001','98765432','98765432','987346215',NULL,NULL),
	(10,11,'Client 10','10','Street 10','City 10','State 10','Australia','3001','98765432','98765432','987346215','sample@test.com.au',NULL),
	(11,3,'Sample Contact','123','Tester Street','Sample Town','VIC','Australia','3059','9357 6485','0451 649 348','9762 8456','test@swim.com.au','Low fat soy caramel late with extra milk'),
	(12,2,'New Contact','135','Test Street','Sample Town','NSW','Australia','3057','9875 1648','0458 315 982','9734 8137','sample@swim.com.au','black coffie');

/*!40000 ALTER TABLE `tbl_client_contact` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_notes`;

CREATE TABLE `tbl_client_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `site_access`)
VALUES
	(3,'A','override@swim.com.au','9283a03246ef2dacdc21a9b137817ec1',NULL,'override@swim.com.au','A',NULL);

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Admin','B'),
	(3,'Accounts','C'),
	(4,'Studio','S'),
	(5,'Account Manager','M'),
	(6,'Private','P'),
	(7,'Contractor','Z');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
