# ************************************************************
# Sequel Pro SQL dump
# Version 4135
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.5.42)
# Database: crmswimc_customcrm
# Generation Time: 2016-08-10 02:51:09 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_client
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client`;

CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `client_resoled` int(11) DEFAULT NULL,
  `client_add_num` varchar(255) DEFAULT NULL,
  `client_add_street` varchar(255) DEFAULT NULL,
  `client_add_city` varchar(255) DEFAULT NULL,
  `client_add_state` varchar(255) DEFAULT NULL,
  `client_add_country` varchar(255) DEFAULT NULL,
  `client_add_postcode` varchar(5) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `client_fax` varchar(255) DEFAULT NULL,
  `client_acn` varchar(255) DEFAULT NULL,
  `client_abn` varchar(255) DEFAULT NULL,
  `client_traiding_terms` varchar(255) DEFAULT NULL,
  `client_type` varchar(255) DEFAULT NULL,
  `client_archive` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client` WRITE;
/*!40000 ALTER TABLE `tbl_client` DISABLE KEYS */;

INSERT INTO `tbl_client` (`clientID`, `client_name`, `client_resoled`, `client_add_num`, `client_add_street`, `client_add_city`, `client_add_state`, `client_add_country`, `client_add_postcode`, `client_phone`, `client_fax`, `client_acn`, `client_abn`, `client_traiding_terms`, `client_type`, `client_archive`)
VALUES
	(1,'1300 WebPro',13,'PO Box 277','','Toowoomba','QLD','Australia','4350','1300 932 776','','','','','','N'),
	(2,'1957 Design Studio',NULL,'17 Gordon Road','','Mt Waverley','Vic','Australia','3149','','','','','','','Y'),
	(3,'20/20 Design & Print',NULL,'','','','','Australia','','','','','','','','Y'),
	(4,'A J Dever Pty Ltd',NULL,'205 William Street','','Melbourne','Vic','Australia','3000','','','','','','','Y'),
	(5,'A.Grech & Son Billiards Pty Ltd',NULL,'39-49 Greens Rd','','Dandenong','Vic','Australia','3175','','','','','','',NULL),
	(6,'About Water Solutions',NULL,'PO Box 808','','Bacchus Marsh','Vic','Australia','3340','+61 3 53677710','','','','','',NULL),
	(7,'Aconex',NULL,'Level 13','Suite 13.01','North Sydney','NSW','Australia','2060','02 9922 9221','','','','','',NULL),
	(8,'Acquaint Pty Ltd T/As WebSetGo',NULL,'Level 1','195 Little Collins Street','Melbourne','Vic','Australia','3000','0419 991 300','','','','','',NULL),
	(9,'ACT 3',NULL,'Suite G02','175 Sturt Street','Southbank','Vic','Australia','3006','+61 3 95316221','','','','','',NULL),
	(10,'Activetics',NULL,'PO Box 6127','','Melbourne','VIC','Australia','3004','9852 1677','','','','','',NULL),
	(11,'ACTSA',NULL,'PO Box 1091','','MITCHAM','VIC','Australia','3132','','','','','','',NULL),
	(12,'Acute Concepts',NULL,'38-40 Kent Street','','Ascot Vale','VIC','Australia','3032','','','','','','',NULL),
	(13,'Adrenalin Adventure',NULL,'','','','','Australia','','418355621','','','','','',NULL),
	(14,'Advanced Cat Enclosures',NULL,'P.O. Box 2024','','Hazeldene','VIC','Australia','3658','438139045','','','','','',NULL),
	(15,'Advanced Fireworks Australia Pty Ltd',NULL,'PO Box 2024','','Hazeldene','VIC','Australia','3658','0438 139 045','','','','','',NULL),
	(16,'Advanced Vet Care',NULL,'26 Robertson Street','','Kensington','Vic','Australia','3031','03 9092 0408','','','','','',NULL),
	(17,'Aeramix',NULL,'1 Stoney Way','','Derrimut','VIC','Australia','3030','1300 667 077','','','','','',NULL),
	(18,'AgFab Engineering',NULL,'14 Wallace Avenue','','Point Cook','Vic','Australia','3030','03 93695577','','','','','',NULL),
	(19,'AGFAB ENGINEERING PTY LTD',NULL,'P O BOX 6566','','Point Cook','Vic','Australia','3030','93695577','','','','','',NULL),
	(20,'AGM Electrical Pty Ltd',NULL,'3/52-54 Parramatta Road','','Croydon','NSW','Australia','2132','02 9745 1302','','','','','',NULL),
	(21,'AJ Dever Pty Ltd (do not use)',NULL,'205 William Street','','Melbourne','Vic','Australia','3000','','','','','','',NULL),
	(22,'Alex Anderson',NULL,'12 Millar Street','','Prahran','VIC','Australia','3181','0413 750 077','','','','','',NULL),
	(23,'All Suburbs Upholstery',NULL,'44-46 Turner Street','','Pascoe Vale','VIC','Australia','3044','','','','','','',NULL),
	(24,'Alpha One Homes',NULL,'PO Box 219','','Heidelberg','Vic','Australia','3084','','','','','','',NULL),
	(25,'Anbeef',NULL,'14 John Rowell Lane','','Mornington','Vic','Australia','3931','','','','','','',NULL),
	(26,'Anderson Consulting - Migration & Education P/Ltd',NULL,'12 Miller Street','','Prahran','VIC','Australia','3181','','','','','','',NULL),
	(27,'Andrea Gorrie',NULL,'Kings Arcade','974-978 High Street','Armadale','VIC','Australia','3143','','','','','','',NULL),
	(28,'Andrew Christie',NULL,'35 Moorhouse Street','','Camberwell','Vic','Australia','3124','','','','','','',NULL),
	(29,'Anthony Jaye',NULL,'203/18 Bent Street','','Kensington','VIC','Australia','3031','03 9329 9686','','','','','',NULL),
	(30,'Apex Trusses',NULL,'Factory 10','81-83 Canterbury Road','Kilsyth','VIC','Australia','3137','9728 2144','','','','','',NULL),
	(31,'Apparel/Additions',NULL,'Unit 2/9 - 11 Murdock St','','South Clayton','Vic','Australia','3169','','','','','','',NULL),
	(32,'Apple Bits',NULL,'66 Athurton Road','','Oakleigh','Vic','Australia','','','','','','','',NULL),
	(33,'Argyle',NULL,'5 / 24 Palings Crt','','Nerang','Qld','Australia','4211','07 55960744','','','','','',NULL),
	(34,'Arlec',NULL,'Attention: David Smith','41 Joseph Street','Blackburn North','VIC','Australia','3130','9982 5000','','','','','',NULL),
	(35,'Arlee',NULL,'','','','','Australia','','','','','','','',NULL),
	(36,'Armadale LPO',NULL,'1006 High Street','','Armadale','Vic','Australia','3143','9509-1035','','','79903578092','','',NULL),
	(37,'Art Strategies Australia',NULL,'1/27 South Ave','','BENTLEIGH','VIC','Australia','3204','9017 0554','9596 8417','','','','',NULL),
	(38,'Ash Marton',NULL,'1/454 Nepean Highway','','Frankston','NSW','Australia','3199','0437 754 372','','','','','',NULL),
	(39,'Asher Judah',NULL,'Unit 8','6 Brentwood Street','Bentleigh','VIC','Australia','3204','','','','','','',NULL),
	(40,'Ashurst Australia',NULL,'Att : Christine Kahwaji','225 George Street','Sydney','NSW','Australia','2000','02 9258 5925','','','','','',NULL),
	(41,'Asia Pacific Security Group',NULL,'Ground Floor,','134 Langford Street','North Melbourne','Vic','Australia','3051','03 9600 9090','','','','','',NULL),
	(42,'Assignments Australia P/L',NULL,'Level 4, 332 Albert Street','','East Melbourne','VIC','Australia','3002','','','','','','',NULL),
	(43,'Assisi Centre',NULL,'230 Rosanna Road','','ROSANNA','VIC','Australia','3084','9455 1199','','','','','',NULL),
	(44,'Aston Social',NULL,'530 Little Collins St','','MELBOURNE','VIC','Australia','3000','0400 836 877','','','','','',NULL),
	(45,'Attards',NULL,'234-238 Hammond Rd','','Dandenong','Vic','Australia','3175','97919992','97919995','','','','',NULL),
	(46,'Aust Health',NULL,'','','','','Australia','','','','','','','',NULL),
	(47,'Austco Environmental Pty Ltd',NULL,'1 Stoney Way','','Derrimut','Vic','Australia','3030','1300 667 077','03 8353 2652','','','','',NULL),
	(48,'Australia National Beef',NULL,'14 John Rowell Lane','','Mornington','VIC','Australia','3931','','','','','','',NULL),
	(49,'Australia-Wide Assessing',NULL,'PO Box 681','','Richmond','Vic','Australia','3121','9428-9027','9428-6246','','','','',NULL),
	(50,'Australian Association of Relationship Counsellors',NULL,'PO Box 50','','KENDALL','NSW','Australia','2439','0412 591 335','','','69773240834','','',NULL),
	(51,'Australian Blueberry Growers Association',NULL,'Range Road','','Corindi','NSW','Australia','2456','','','','','','',NULL),
	(52,'Australian Garlic Producers',NULL,'PO Box 5084','','Mildura','Vic','Australia','3502','0417 312 760','','','','','',NULL),
	(53,'Australian Garlic Producers',NULL,'PO Box 8347','','Armadale','Vic','Australia','3143','+61 3 95001499','','','21090062070','','',NULL),
	(54,'Australian Gift Hampers',NULL,'21 Gregg St','','DIAMOND CREEK','VIC','Australia','3089','9438 2017','','','29507009794','','',NULL),
	(55,'Australian Grand Prix Corporation',NULL,'220 Albert Road','','South Melbourne','Vic','Australia','3205','92587189','','','','','',NULL),
	(56,'Australian Institute of Building Surveyors',NULL,'Ground Floor','71 Ridge Street','Gordon','NSW','Australia','2072','','','','','','',NULL),
	(57,'Australian Life Insurance Distribution Pty Ltd',NULL,'GPO Box 4737','','Sydney','NSW','Australia','2001','0411 584 694','','','','','',NULL),
	(58,'Australian Luggage Co',NULL,'30-32 Moore St','','Airport West','Vic','Australia','','93345500','','','','','',NULL),
	(59,'Australian Safety Rail',NULL,'17 Blackshaws Rd','','Newport','Vic','Australia','3015','0437 545 430','+61 3 9397 6770','','96150276947','','',NULL),
	(60,'Australian Somatic Psychotherapy Association',NULL,'PO Box 283','','St Leonards','NSW','Australia','2065','','','','54262453260','','',NULL),
	(61,'Australian Support I.T.',NULL,'8A 339 Williamstown Road','','Port Melbourne','Vic','Australia','3207','9682 9300','','','','','',NULL),
	(62,'Australian Taxation Office',NULL,'Small Business Consultation','Att : Famida Esmail','','','Australia','','','','','','','',NULL),
	(63,'Auto Partners',NULL,'PO Box 500','','NORTH MELBOURNE','VIC','Australia','3051','','','','','','',NULL),
	(64,'Auto Power House Pty Ltd',NULL,'PO Box 5135','','ROBINA TOWN CENTRE','QLD','Australia','4230','07 55808995','','','','','',NULL),
	(65,'AUZ Country Carriers',NULL,'12 Kimberly Rd','','Dandenong','Vic','Australia','','4185112162','','','','','',NULL),
	(66,'Avion Communications',NULL,'119 Ferrars Street','','South Melbourne','Vic','Australia','3205','03 9939 3322','','','','','',NULL),
	(67,'Avoca Capital',NULL,'Bennelong House','Level 1','Melbourne','VIC','Australia','3000','03 9078 5850','','','','','',NULL),
	(68,'AVS Networks',NULL,'Suite 62','330 Wattle Street','Ultimo','NSW','Australia','2007','02 8022 8326','','','','','',NULL),
	(69,'AWIA',NULL,'PO Box 771','','Inglewood','WA','Australia','6932','','','','','','',NULL),
	(70,'B2B Marketing Services',NULL,'PO Box 634','','Richmond','Vic','Australia','3121','0417 560 475','','','','','',NULL),
	(71,'Backpacking for Babyboomers',NULL,'421/253 Bridge Road','','RICHMOND','VIC','Australia','3121','0414 995 683','','','','','',NULL),
	(72,'Banyandah Naturally  Pty Ltd',NULL,'Banyandah','PO Box 2','Howlong','NSW','Australia','2643','02 6026 8800','','','57008453165','','',NULL),
	(73,'Bargains For Babies',NULL,'16 Closter Ave','','Ashwood','VIC','Australia','3147','0402 355 584','','','','','',NULL),
	(74,'Bark Busters',NULL,'','','','','Australia','','','','','','','',NULL),
	(75,'Barmah Pty Ltd T/As Nextra Chermside',NULL,'PO Box 2274','','CHERMSIDE CENTRE','QLD','Australia','4032','07 3861 5666','','','','','',NULL),
	(76,'Barrington Centre',NULL,'PO Box 2262','','Melbourne','VIC','Australia','3001','','','','','','',NULL),
	(77,'BASF Coatings',NULL,'231-233 Newton Rd','','Wethemill Park','Vic','Australia','2164','9746-3385','','','','','',NULL),
	(78,'Bay City Sea Planes',NULL,'280 Latrobe Terrace','','NEWTOWN','VIC','Australia','3220','0438 840 205','','','','','',NULL),
	(79,'Beauty in Touch',NULL,'','','','','Australia','','','','','','','',NULL),
	(80,'Beemac Consulting Pty Ltd',NULL,'19 Melosa Ave','','Brighton','Vic','Australia','3187','','','','','','',NULL),
	(81,'Beer Line Cleaner',NULL,'12B Kylie Place','','Cheltenham','Vic','Australia','3193','','','','','','',NULL),
	(82,'Beller TBM Commercial',NULL,'C/- Marc Hayden','Level 4, 613 St Kilda Road','Melbourne','VIC','Australia','3004','03 8532 2211','03 85322202','','86743520261','','',NULL),
	(83,'Bendex Stainless Steel',NULL,'1-3 Malua Street','','Reservoir','Vic','Australia','3073','9469 5822','9469 5833','','','','',NULL),
	(84,'Beyond Skin Beauty',NULL,'10 Apex Court','','Thomastown','Vic','Australia','3074','03 9463 8999','lisa.m@bsbpacific.com','','11849955274','','',NULL),
	(85,'BGM Homeloans',NULL,'PO Box 8317','','Carrum Downs','Vic','Australia','3201','03 87877087','','','','','',NULL),
	(86,'Bilfinger Berger Project Investments',NULL,'Level 16 600 Bourke Street','','Melbourne','VIC','Australia','3000','','','','','','',NULL),
	(87,'Biz Card',NULL,'','','','','Australia','','','','','','','',NULL),
	(88,'Boon Electrical',NULL,'PO Box 871','','BRAESIDE','VIC','Australia','3195','0409 795 309','','','','','',NULL),
	(89,'Brandson Enterprises Pty Ltd',NULL,'PO Box 1394','','Darling','Vic','Australia','3145','411111426','','','','','',NULL),
	(90,'Bravado Beverages',NULL,'PO Box 424','','Ivanhoe','Vic','Australia','3079','','','','','','',NULL),
	(91,'Brien',NULL,'92 Elizabeth Street','','Kooyong','VIC','Australia','3144','','','','','','',NULL),
	(92,'Brooks',NULL,'5/74 Croydon Road','','Surrey Hills','Vic','Australia','3129','','','','','','',NULL),
	(93,'Buckets and Spades',NULL,'19A Carpenter Street','','Brighton','Vic','Australia','3186','03 9592 1266','zlata@bucketsandspade','','','','',NULL),
	(94,'Building Check Pty Ltd',NULL,'PO Box 462','','Canterbury','VIC','Australia','3126','9859 2226','','','','','',NULL),
	(95,'Bulla Dairy Foods',NULL,'15 Swann Drive','','Derrimut','Vic','Australia','3030','9931-7888','','','11845336184','','',NULL),
	(96,'Buoyancy Services Inc',NULL,'293 Punt Road','','Richmond','Vic','Australia','3121','+61 3 94293322','','','47765926969','','',NULL),
	(97,'Bupa',NULL,'','','','','Australia','','','','','','','',NULL),
	(98,'Business Now Asia Pacific',NULL,'316 Mary Street','','Richmond','VIC','Australia','3121','','','','','','',NULL),
	(99,'buyfromusa',NULL,'62 Bulwara Road','','Pyrmont','NSW','Australia','2009','','','','','','',NULL),
	(100,'CANCELLED SALES',NULL,'','','','','Australia','','','','','','','',NULL),
	(101,'Captains Club',NULL,'','','','','Australia','','','','','','','',NULL),
	(102,'Career Source World',NULL,'121 Queen Pde','','Clifton Hill','Vic','Australia','','401690346','','','','','',NULL),
	(103,'Carlo Piccolo',NULL,'25 Pamay Road','','Mt Waverly','Vic','Australia','3149','','','','','','',NULL),
	(104,'Castellan Financial Consulting',NULL,'Ground Floor','2 Stawell St','RICHMOND','VIC','Australia','3121','9429 4007','','','','','',NULL),
	(105,'Catalyst Consulting',NULL,'1 Patrick St','','BOX HILL NORTH','VIC','Australia','3129','9329 9000','','','','','',NULL),
	(106,'Catherine Hyde',NULL,'70 City Road','','Southbank','Vic','Australia','3006','','','','','','',NULL),
	(107,'CC Apparel',NULL,'4 Corr Street','','Moorabbin','VIC','Australia','3189','1300388677','','','','','',NULL),
	(108,'CEED Pty Ltd',NULL,'Suite G02','175 Sturt Street','Southbank','Vic','Australia','3006','Email Invoices','','','','','',NULL),
	(109,'Challenge Paintball',NULL,'Unit 4/24 Elster Ave','','Garenvale','Vic','Australia','3185','95721430','','','','','',NULL),
	(110,'Chapman Marketing',NULL,'4 Aralee Place','','Brighton East','VIC','Australia','3187','','','','','','',NULL),
	(111,'Charles Schaefer',NULL,'23 Feez Street','','Yeronga','Qld','Australia','4104','+61 7 33927071','','','','','',NULL),
	(112,'Chris Love Design Pty Ltd',NULL,'PO Box 207','','ELSTERNWICK','VIC','Australia','3185','0424 750 538','','','','','',NULL),
	(113,'Chris Witton',NULL,'21 Campbell Road','','Kyagle','Vic','Australia','2474','','','','','','',NULL),
	(114,'Christom',NULL,'7 Parkview Tce','','Chirnside Park','Vic','Australia','','','','','','','',NULL),
	(115,'Christopher Costuna',NULL,'4 Coolabah Ave','','Glen Waverley','','Australia','','','','','','','',NULL),
	(116,'Citrofresh',NULL,'2/55 West Fyans Street','','Newtown','Vic','Australia','3320','52227867','','','','','',NULL),
	(117,'City West Water',NULL,'1 McNab Avenue','','Footscray','Vic','Australia','3011','9313 8248','','','','','',NULL),
	(118,'Ckaos ink',NULL,'18b Equitable Place','','Melbourne','Vic','Australia','3000','9670 2599','','','','','',NULL),
	(119,'Clever Chics',NULL,'PO Box 125','','Pascoe Vale South','Vic','Australia','3044','','','','','','',NULL),
	(120,'Client Service Support',NULL,'Suite 110','425 Docklands Drive','Docklands','Vic','Australia','3008','','','','','','',NULL),
	(121,'Clifton Production',NULL,'84-88 Chifley Drive','','Preston','Vic','Australia','','','','','','','',NULL),
	(122,'Clock Work Constructions',NULL,'6/908 Glenferrie Rd','','Kew','Vic','Australia','3101','','','','','','',NULL),
	(123,'Club de Capitanes',NULL,'38 Milton St','','West Melbourne','Vic','Australia','3003','93283055','93267325','','','','',NULL),
	(124,'Club Warehouse',NULL,'Unit 122','45 Gilby Road','Mount Waverley','Vic','Australia','3149','03 9550 9600','','','','','',NULL),
	(125,'Cockram',NULL,'PO Box 274','','Hawthorn','VIC','Australia','3122','','','','','','',NULL),
	(126,'Colrain',NULL,'137 Swann Drive','','Derrimut','VIC','Australia','3030','03 93687000','','','76110786215','','',NULL),
	(127,'Committed Financial Group',NULL,'42 Parmelia Drive','','Taylors Lakes','VIC','Australia','3038','0412 886 272','','','','','',NULL),
	(128,'Consider it Done (No longer requires hosting)',NULL,'PO Box  8485','','Armadale','VIC','Australia','3143','0407 402 499','','','','','',NULL),
	(129,'Construction Material Processors Association',NULL,'PO Box 396','','Kilmore','Vic','Australia','3764','1300267222','','','','','',NULL),
	(130,'Continance Care',NULL,'PO Box 1201','','Darling','3145','Australia','','1300784233','','','','','',NULL),
	(131,'Contract Check',NULL,'PO Box 638','','Hawthorn','Vic','Australia','3122','03 9811 9980','','','','','',NULL),
	(132,'Copier Clinic',NULL,'47 University Street','','Carlton','Vic','Australia','3053','9347 7000','9347 0500','','','','',NULL),
	(133,'Core Consulting',NULL,'Level 8, 140 Bourke St','','MELBOURNE','VIC','Australia','3000','0418 578 456','','','','','',NULL),
	(134,'Corporate Information Systems',NULL,'5/935 Station St','','Box Hill','Vic','Australia','3128','98994433','','','','','',NULL),
	(135,'Correct Panels',NULL,'183-199 Macaulay Rd','','North Melbourne','Vic','Australia','3051','9329 9000','','','','','',NULL),
	(136,'Cowgirl Marketing',NULL,'203 (b) Whitehorse Road','','Balwyn','VIC','Australia','3103','0419 374 707','','','','','',NULL),
	(137,'Cracker Systems',NULL,'Rear 304 High Street','','Kew','Vic','Australia','3101','9853 4444','','','','','',NULL),
	(138,'Creative Unit',NULL,'Suite 4, Level 4','332 Albert Street','East Melbourne','Vic','Australia','3002','+61 3 98209898','','','','','',NULL),
	(139,'Crocodile Tuff',NULL,'','','','','Australia','','','','','','','',NULL),
	(140,'CTG Technology',NULL,'116 Bank Street','','South Melbourne','Vic','Australia','3205','9038 8600','','','','','',NULL),
	(141,'Cubic Consulting',NULL,'Level 7','333 Collins Street','Melbourne','Vic','Australia','3000','+61 3 9935 8304','','','','','',NULL),
	(142,'Cupcake Queens',NULL,'','','','','Australia','','','','','','','',NULL),
	(143,'Custom Kitchens & Shopfittings',NULL,'Att: Andrew Dip','Factory 32','Dandenong','Vic','Australia','3175','97085500','9708 -5511','','','','',NULL),
	(144,'Cycle City West',NULL,'19 Lockheed Street','','Strathmore Heights','Vic','Australia','3041','0417 103 806','','','','','',NULL),
	(145,'D. Keith Millar',NULL,'31 Arden Street','','North Melbourne','Vic','Australia','3051','+61 3 93283055','','','','','','Y'),
	(146,'D.Millar & Associates(Leadership Management Aust)',NULL,'38 Milton Street','','West Melbourne','VIC','Australia','3003','','','','','','',NULL),
	(147,'Dagmar Art Gallery Pty Ltd',NULL,'2 Kent Street','','Ascot Vale','Vic','Australia','3032','0416 202 647','','','','','',NULL),
	(148,'Daily Review',NULL,'Ground Floor (Valegro Office)','22 William Street','Melbourne','Vic','Australia','3000','0402 403 182','','','','','',NULL),
	(149,'Daytone',NULL,'78 Maidstone Street','','Altona','Vic','Australia','3018','408173957','','','','','',NULL),
	(150,'De-Junking',NULL,'PO Box 460','','Hawthorn','VIC','Australia','3122','','','','','','',NULL),
	(151,'Debenham International',NULL,'P.O Box 5230','','Geelong North','VIC','Australia','3215','0408 193 433','','','','','',NULL),
	(152,'Delta Dogz',NULL,'30 Merton Street','','Box Hill','Vic','Australia','3128','0425 777 029','','','','','',NULL),
	(153,'Department of Health & Human Services',NULL,'ABN 74 410 330 756','PO Box 4057','Melbourne','Vic','Australia','3001','','','','','','',NULL),
	(154,'Derek Rawson Consulting',NULL,'18 Kingsley Grove','','Mount Waverley','Vic','Australia','3149','','','','','','',NULL),
	(155,'Design Otaku Pty Ltd',NULL,'Level 11, 488 Bourke St','','MELBOURNE','VIC','Australia','3000','9670 2801','','','','','',NULL),
	(156,'Designerform Pty Ltd',NULL,'34 Northgate Drive','','Thomastown','Vic','Australia','3074','94641914','','','','','',NULL),
	(157,'Desmier Designs',NULL,'P O Box 417','','Canterbury','VIC','Australia','3126','0408 990 900','','','','','',NULL),
	(158,'Dex',NULL,'Unit 5-35 Tarton Drive','','Cheltenham','Vic','Australia','3192','95559422','','','','','',NULL),
	(159,'dG Policy Solutions',NULL,'5 Moama Place','','ROWVILLE','VIC','Australia','3178','0431 170 669','','','','','',NULL),
	(160,'Diamond Agricultural Products',NULL,'PO Box 8347','','Armadale','VIC','Australia','3143','0417 312 760','','','','','',NULL),
	(161,'Diesel CSD',NULL,'8A Palm Avenue','','North Manly','NSW','Australia','2100','414555715','','','97136107778','','',NULL),
	(162,'DiMarca',NULL,'101/175B Stephen Street','','Yarraville','Vic','Australia','3013','0414 733 496','','','','','',NULL),
	(163,'Dovewood',NULL,'232 Lennox Street','','Richmond','Vic','Australia','','9428-9011','9428-6246','','','','',NULL),
	(164,'DPOA',NULL,'PO Box 831','','North Melbourne','Vic','Australia','3051','93299806','','','','','',NULL),
	(165,'Dryen Australia',NULL,'PO Box 1584','','Moorabbin','Vic','Australia','3189','8558 2222','','','','','',NULL),
	(166,'Dynamic Windows',NULL,'PO Box 1136','','Bairnsdale','Vic','Australia','3875','458511693','','','','','',NULL),
	(167,'E-Ton Australia Pty Ltd',NULL,'280 Wolsely Place','','Thomastown','Vic','Australia','','','','','','','',NULL),
	(168,'Eastlayne Shoes',NULL,'53 Whitneys Road','','SOMERVILLE','VIC','Australia','3912','0409 255 613','','','','','',NULL),
	(169,'Eastrise Constructions Pty Ltd',NULL,'PO Box 8424','','Armadale','VIC','Australia','3143','9827 9922','','','','','',NULL),
	(170,'Easy Bookings',NULL,'Suite 4','Level 3','MELBOURNE','VIC','Australia','3004','1300 303 146','','','','','',NULL),
	(171,'ECM Group',NULL,'760 Queensberry Street','','North Melbourne','Vic','Australia','3051','','','','','','',NULL),
	(172,'Electric',NULL,'32 Parlington Street','','Canterbury','Vic','Australia','3126','','','','','','',NULL),
	(173,'Ellimiah',NULL,'35A Mary Street','','Richmond','Vic','Australia','3121','0438 414 763','brittjames3@gmail.com','','','','',NULL),
	(174,'Emily Elliot',NULL,'','','','','Australia','','','','','','','',NULL),
	(175,'Engineers Aust',NULL,'21 Bedford Street','','North Melbourne','','Australia','','','','','','','',NULL),
	(176,'Enjoy Inspire!',NULL,'37 Grandison Street','','Moonee Ponds','VIC','Australia','3139','449167776','','','','','',NULL),
	(177,'Epatch',NULL,'PO Box 1022','','Hartwell','Vic','Australia','3124','','','','','','',NULL),
	(178,'Eric Hanby',NULL,'','','','','Australia','','','','','','','',NULL),
	(179,'Essentially',NULL,'Level 1','214 Graham Street','Port Melbourne','Vic','Australia','3207','+61 3 96456070','+61 3 96819913','','66098178191','','',NULL),
	(180,'Ethiopian Consulate',NULL,'3 Sirius Place','','Red Hill','ACT','Australia','2603','+61 3 94173419','','','','','',NULL),
	(181,'Ethnic Channels Group Limited',NULL,'120 Amber Street','Markham','Ontario','L3R','Australia','3A3','','','','','','',NULL),
	(182,'Etrack',NULL,'36 Vista St','','MOSMAN','NSW','Australia','2008','02 9003 1540','','','','','',NULL),
	(183,'Eureka Living Surveys',NULL,'Building Management','7 Riverside Quay','SOUTHBANK','VIC','Australia','3006','','','','','','',NULL),
	(184,'Eureka Tower PS437659C',NULL,'Building Management','7 Riverside Quay','Southbank','Vic','Australia','3006','','','','','','',NULL),
	(185,'Eureka Tower Pty Ltd',NULL,'C/ Building Manager','7 Riverside Key','Southbank','Vic','Australia','3006','9685 0188','','','','','',NULL),
	(186,'Euro Gold Jewellers',NULL,'56 Puckle Street','','Moonee Ponds','Vic','Australia','3039','03 9370 8218','','','','','',NULL),
	(187,'Excel Group',NULL,'Factory 10','55 McClure Street','Thornbury','VIC','Australia','3071','9440 9717','','','','','',NULL),
	(188,'Ezy-As LED (No longer client)',NULL,'Att : Sean Mulligan','PO Box 27','Braeside','Vic','Australia','3195','','','','','','',NULL),
	(189,'Fabian Ramirez',NULL,'20 Hounslow Green','','Caroline Springs','VIC','Australia','3023','','','','','','',NULL),
	(190,'Family Business Australia',NULL,'Suite 3','Level 5','South Melbourne','Vic','Australia','3205','03 9867 5322','03 9867 5344','','','','',NULL),
	(191,'Fawkner Body Repairs',NULL,'1259 Sydney Road','','FAWKNER','VIC','Australia','3060','9359 9417','','','','','',NULL),
	(192,'Feel Healing',NULL,'5 Correys Ave','','Concord','Vic','Australia','2137','428442400','','','72415853149','','',NULL),
	(193,'Ferdinand Zito & Associates',NULL,'Suite 3','251 Lower Heidelberg Road','Ivanhoe East','VIC','Australia','3079','9497 1800','','','','','',NULL),
	(194,'Fiat Car Club',NULL,'GPO Box 4','','Melbourne','Vic','Australia','3001','','','','','','',NULL),
	(195,'Finascor',NULL,'PO BOX 18223','Collins St East','Melbourne','Vic','Australia','8003','98900341','','','','','',NULL),
	(196,'Fine Paper',NULL,'32 Plummer Rd','','Laverton North','Vic','Australia','','93146007','','','','','',NULL),
	(197,'First Call Connect Pty Ltd',NULL,'Unit 26','34-36 Abel Street','Penrith','NSW','Australia','2750','1300 994 096','','','','','',NULL),
	(198,'First Care',NULL,'38 Milton Street','','West Melbourne','Vic','Australia','3003','','','','','','',NULL),
	(199,'Fix It Paint',NULL,'3 McKinnon Road','','McKinnon','Vic','Australia','3204','0418 996 726','','','','','',NULL),
	(200,'Fleet Trades',NULL,'67 Capital Link Drive','','CAMPBELLFIELD','VIC','Australia','3061','9357 9642','','','','','',NULL),
	(201,'Foamex',NULL,'31-33 Gatwick Road','','Bayswater North','Vic','Australia','3153','','97295050','','','','',NULL),
	(202,'Footy Cards',NULL,'PO Box 45','','Seddon','Vic','Australia','3011','+61 3 93980111','','','38567172580','','',NULL),
	(203,'Francesca Siska',NULL,'438 King Street','','West Melbourne','Vic','Australia','3003','','','','','','',NULL),
	(204,'Freeway Car Audio',NULL,'1299 Malvern Road','','Malvern','Vic','Australia','3144','','','','','','',NULL),
	(205,'Fresh Exchange',NULL,'27 Holbeche Rd','','Arndell Park','NSW','Australia','2148','','','','','','',NULL),
	(206,'Fruit Talk',NULL,'136 Shearson Crescent','','Mentone','Vic','Australia','3194','92586108(Nancy)','','','','','',NULL),
	(207,'G P Productions',NULL,'240 Old Sydney Road','','Mickleham','VIC','Australia','3064','438538496','','','','','',NULL),
	(208,'G T Recycling',NULL,'PO Box 125','','Geelong','Vic','Australia','3220','','','','','','',NULL),
	(209,'Ganache Chocolate',NULL,'250 Toorak Road','','SOUTH YARRA','VIC','Australia','3141','9804 8216','','','','','',NULL),
	(210,'Garden World',NULL,'810 Springvale Rd','','Braeside','Vic','Australia','3195','','','','','','',NULL),
	(211,'Gardenworld',NULL,'810 Springvale Rd','','Braeside','Vic','Australia','3195','','','','','','',NULL),
	(212,'Gashcorp Pty Ltd',NULL,'Trading as The Sydney Rubbish Truck','204 Harris Street','Pyrmont','NSW','Australia','2009','0433 785 295','','','','','',NULL),
	(213,'Genjusho',NULL,'PO Box 315','','Vermont','Vic','Australia','3133','','','','18077983450','','',NULL),
	(214,'Georgia McKay',NULL,'PO Box 638','','Hawthorn','Vic','Australia','3122','419894977','','','','','',NULL),
	(215,'Get Glossy',NULL,'147 High Street ','','PRESTON','VIC','Australia','3072','','','','','','',NULL),
	(216,'GIO',NULL,'','','','','Australia','','','','','','','',NULL),
	(217,'Glass House Strategies',NULL,'Unit 1, 32 Iris Rd','','Glen Iris','VIC','Australia','3146','','','','','','',NULL),
	(218,'Go Catch',NULL,'','','','','Australia','','','','','','','',NULL),
	(219,'Go Kart Sport',NULL,'244-248 Chesterville Road','','Moorabbin','Vic','Australia','3189','95556118','','','','','',NULL),
	(220,'Godek International',NULL,'PO Box 178','','MORNINGTON','VIC','Australia','3931','0427 605 591','','','','','',NULL),
	(221,'Gold Age',NULL,'15 Cornell Street','','Camberwell','VIC','Australia','3124','9836 9507','','','','','',NULL),
	(222,'Grafica',NULL,'15-17 Dudley Street','','West Melbourne','3003','Australia','','1300363894','93299854','','66113518737','','',NULL),
	(223,'Grant Carne',NULL,'41 Erskine Road','','Macleod','VIC','Australia','3085','','','','','','',NULL),
	(224,'Graphic Effects',NULL,'PO Box 3552','','NUNAWADING','VIC','Australia','3131','03 9894 4950','','','','','',NULL),
	(225,'Green Event',NULL,'PO Box 5135','','Garden City','Vic','Australia','3207','9646 2300','','','','','',NULL),
	(226,'Green Rock Vintage Pty Ltd',NULL,'2B Glen Orme Avenue','','ORMOND','VIC','Australia','3204','0402 449 908','','','','','',NULL),
	(227,'Green Technology Services',NULL,'Level 1','10 Greenhill Road','Wayville','SA','Australia','5034','08 73245229','','','81148858397','','',NULL),
	(228,'Grek Enterprises',NULL,'34 Nimblefoot Way','','Lilydale','Vic','Australia','3140','','','','','','',NULL),
	(229,'Grix Eye Wear',NULL,'28 Holyrood Drive','','Vermont','VIC','Australia','3133','','','','','','',NULL),
	(230,'Group of 100',NULL,'Level 20','28 Freshwater Place','Southbank','Vic','Australia','3006','9288 5174','','','','','',NULL),
	(231,'Grovedale Kitchens',NULL,'142- 146 Marshalltown Rd','','Grovedale','Vic','Australia','3216','52442606','','','','','',NULL),
	(232,'Hadlow Charters',NULL,'36 Roehampton Cres','','Mt. Eliza','Vic','Australia','3930','','','','','','',NULL),
	(233,'Hanley',NULL,'48 Bulwara Road','','PYRMONT','NSW','Australia','2009','','','','','','',NULL),
	(234,'HEAC Electrical Contractors',NULL,'PO Box 615','','BENTLEIGH EAST','VIC','Australia','3165','0414 548 979','','','','','',NULL),
	(235,'Hiatt Group',NULL,'1-7 Weston Street','','BRUNSWICK','VIC','Australia','3056','9388 0076','','','','','',NULL),
	(236,'Hillcrest Farm',NULL,'Dennis Brien','92 Elizabeth Street','Kooyong','Vic','Australia','3144','','','','','','',NULL),
	(237,'Holdsworth',NULL,'28 Holyrood Drive','','Vermont','VIC','Australia','3133','','','','','','',NULL),
	(238,'Holman Industries',NULL,'94 Northgate Drive','','Thomastown','Vic','Australia','3044','9357-9991','','','','','',NULL),
	(239,'Holmes Colleges',NULL,'Att: Alex Anderson','185 Spring St','Melbourne','Vic','Australia','3000','9662 2055','','','','','',NULL),
	(240,'Home Touch Systems',NULL,'15/219 Balaclava Road','','North Caulfield','Vic','Australia','3161','+61 3 95283700','','','92104234175','','',NULL),
	(241,'Horizon Golf',NULL,'Unit 2, 64 Brooks Street','','East Bentleigh','VIC','Australia','3165','0414 776 457','','','','','',NULL),
	(242,'HPR Resources (no longer client)',NULL,'Suite 9','296 Bay Road','Cheltenham','VIC','Australia','3192','9583 1377','','','','','',NULL),
	(243,'HR Inside',NULL,'32 Bourne Rd','','Glen Iris','Vic','Australia','3146','9885 1990','','','','','',NULL),
	(244,'HVA Group',NULL,'130-132 Freight Drive','','Somerton','VIC','Australia','3062','9305 2299','','','','','',NULL),
	(245,'HWT',NULL,'Attention:Ms Teresa Tantaro','Level 6-40 City Rd','Southbank','Vic','Australia','','','','','','','',NULL),
	(246,'IEMS Pty Ltd',NULL,'Unit 2 /405 Toorak Rd','','South Yarra','Vic','Australia','3148','','','','','','',NULL),
	(247,'Images By Rocco',NULL,'6 Louisa Place','','Templestowe','Vic','Australia','','','','','','','',NULL),
	(248,'Impressions Furniture',NULL,'1 Fleet Street','','Somerton','VIC','Australia','3062','','','','','','',NULL),
	(249,'Inkerman Panels',NULL,'Attention Diane Davie','PO box 167','St Kilda','Vic','Australia','3182','+61 3 95371111','','','','','',NULL),
	(250,'Inkerman Panels',NULL,'','','','','Australia','','','','','','','',NULL),
	(251,'Inner City Dweller',NULL,'PO Box 15','','ABBOTSFORD','VIC','Australia','3067','','','','','','',NULL),
	(252,'Innovia',NULL,'19 Potter Street','','Craigieburn','Vic','Australia','','412098651','','','','','',NULL),
	(253,'Inprintations',NULL,'1/22 Tarnard Drive','','Braeside','Vic','Australia','3195','','','','','','',NULL),
	(254,'Inscon Pty Ltd',NULL,'Level 13, 135 King Street','','SYDNEY','NSW','Australia','2000','0412 193 552','','','','','',NULL),
	(255,'Insulcap',NULL,'Unit 1','111-113 William Angliss Drive','Laverton North','Vic','Australia','3029','93295528','','','','','',NULL),
	(256,'Isaacs Cole',NULL,'Attention:  Rebecca Collins','Suite 314, 838 Collins Street','DOCKLANDS','VIC','Australia','3008','9081 0470','','','','','',NULL),
	(257,'ISIP Consulting',NULL,'128 Kooyong Rd','','Armadale','Vic','Australia','3143','','','','','','','Y'),
	(258,'Italian Job Auto',NULL,'9 Wells Rd','','Oakleigh','Vic','Australia','','','','','','','','Y'),
	(259,'iTunes',NULL,'','','','','Australia','','','','','','','','Y'),
	(260,'Ivory',NULL,'GPO Box 3155','','Melbourne','Vic','Australia','3001','','','','','','',NULL),
	(261,'IZARO Haute Couture',NULL,'553 Burwood Rd','','Hawthorn','Vic','Australia','3122','','','','','','',NULL),
	(262,'J & A Cabinets',NULL,'6 Maida Ave','','Sunshine Nth','Vic','Australia','3020','+61 3 93646677','','','90766975120','','',NULL),
	(263,'Janis Elsts',NULL,'','','','','Australia','','','','','','','',NULL),
	(264,'Jeff Barnard',NULL,'1119 Hoddle Street','','East Melbourne','Vic','Australia','3002','','','','','','',NULL),
	(265,'Jensen',NULL,'62 Bulwara Road','','Pyrmont','NSW','Australia','2009','','','','','','',NULL),
	(266,'Jenso Pty Ltd',NULL,'234 Boundary Road','','Braeside Road','VIC','Australia','3195','','','','','','',NULL),
	(267,'Jeremy Hancock',NULL,'52 Retreat Road','','Newtown','Vic','Australia','3220','','','','','','',NULL),
	(268,'Joanna Verykios',NULL,'','','','','Australia','','','','','','','',NULL),
	(269,'John Dever Pty Ltd',NULL,'Owen Dixon Chambers','205 William Street','Melbourne','Vic','Australia','3000','','','','','','',NULL),
	(270,'John Irving',NULL,'86 York Street','','Richmond','Vic','Australia','3121','','','','','','',NULL),
	(271,'John Myers',NULL,'','','','','Australia','','','','','','','',NULL),
	(272,'Jones',NULL,'','','','','Australia','','','','','','','',NULL),
	(273,'Josh Wagn Photography',NULL,'48 View Street','','Alphington','Vic','Australia','','','','','','','',NULL),
	(274,'JT Consulting Engineers',NULL,'460 Neerim Road','','Murrumbeena','Vic','Australia','163','03 95683590','','','','','',NULL),
	(275,'Julia Barnett Business Development',NULL,'31 Warrick St','','Ascot Vale','Vic','Australia','3032','0400 958 572','','','48048957245','','',NULL),
	(276,'Just Fitness',NULL,'9 Macquarie Drive','','Thomastown','VIC','Australia','3074','+61 3 94658722','','','','','',NULL),
	(277,'Justin Serong',NULL,'16 William Street','','PORT FAIRY','VIC','Australia','3284','','','','','','',NULL),
	(278,'K & K Plastics',NULL,'522 Clayton Rd','','Clayton','Vic','Australia','3168','95525000','','','','','',NULL),
	(279,'Kaidon Park',NULL,'PO BOX 354','','Romsey','Vic','Australia','3434','438538496','','','','','',NULL),
	(280,'Katchow Kaching',NULL,'Attention Tim Carroll','32 Parlington St','Canterbury','Vic','Australia','3126','','','','','','',NULL),
	(281,'Kathryn Jones',NULL,'15/14 The Avenue','','Prahran','Vic','Australia','3181','','','','','','',NULL),
	(282,'Keep It Together',NULL,'Attention: Kerri Muller','PO Box 3687','SUCCESS','WA','Australia','6164','0417 995 747','','','','','',NULL),
	(283,'Keith Miller',NULL,'','','','','Australia','','','','','','','',NULL),
	(284,'Kensington Association',NULL,'PO Box 1208','','Kensington','VIC','Australia','3031','0424 468 486','','','','','',NULL),
	(285,'KeyNovation',NULL,'167-169 Cremorne Street','','Cremorne','Vic','Australia','3121','1300 121 129','','','','','',NULL),
	(286,'Kiel Plastics',NULL,'PO Box 64','','Mirboo North','Vic','Australia','3871','56682203','','','','','',NULL),
	(287,'King\'s Arcade',NULL,'PO Box 8241','','Armadale','Vic','Australia','3143','','','','','','',NULL),
	(288,'Kingfisher Recruitment',NULL,'Attention:Cathy Stanley','Level 27 - 360 Collins Street','Melbourne','Vic','Australia','3000','8610 9630','','','','','',NULL),
	(289,'Kirra Australia',NULL,'1648 Centre Rd','','Springvale','Vic','Australia','3171','','','','','','',NULL),
	(290,'KPMG',NULL,'147 Collins Street','','Melbourne','Vic','Australia','3000','03 9838 4472','','','66986945907','','',NULL),
	(291,'Kreatific Media',NULL,'57 Derby Street','','Rooty Hill','NSW','Australia','2766','','','','','','',NULL),
	(292,'Kwik Log',NULL,'','','','','Australia','','','','','','','',NULL),
	(293,'Labertouche Organic Produce',NULL,'100 Alcorn Rd','','Labertouche','3816','Australia','','56287637','','','','','',NULL),
	(294,'Lachard',NULL,'PO Box 848','','Glen Waverly','VIC','Australia','3150','0423 806 655','','','','','',NULL),
	(295,'Larkin & Drought Homes',NULL,'66 Canowindra Way','','Leopold','Vic','Australia','3224','','','','','','',NULL),
	(296,'Laura Manariti',NULL,'Upstairs 119 Dundas Place','','Albert Park','Vic','Australia','3206','0402 148 068','','','','','',NULL),
	(297,'Leadership Management Australia',NULL,'38 Milton Street','','West Melbourne','Vic','Australia','3003','93314344','93314355','','','','',NULL),
	(298,'Leadlight Station',NULL,'76 Smith Street','','Kensington','VIC','Australia','3031','0422 154 222','','','','','',NULL),
	(299,'Leaf Busters',NULL,'1 Olinda Basin Rd','','Olinda','Vic','Australia','3788','9292-1033','','','','','',NULL),
	(300,'Leasemasters',NULL,'P.O Box831','','North Melbourne','Vic','Australia','3051','','','','','','',NULL),
	(301,'Left & Right',NULL,'14 Cooke Street','','Sandringham','VIC','Australia','3191','','','','','','',NULL),
	(302,'Les Zigomanis',NULL,'27 Bendigo Cresent','','Thomastown','Vic','Australia','3074','','','','','','',NULL),
	(303,'Lexicon Gulf',NULL,'21 Spring Street','','Fitzroy','Vic','Australia','3065','','','','','','',NULL),
	(304,'Libby Conheady',NULL,'3/ 483 Kooyong Road','','Elsterwick','Voc','Australia','3185','95969041','','','','','',NULL),
	(305,'Lifestyle Furniture',NULL,'9 Moncrief Road','','Nunawadding','VIC','Australia','3131','9894 2446','','','','','',NULL),
	(306,'Liger Design (no longer client)',NULL,'159A Bay Road','','SANDRINGHAM','VIC','Australia','3191','0416 850 757','','','','','',NULL),
	(307,'Lime Catering',NULL,'PO Box 745','','Kew','VIC','Australia','3101','0419 800 190','','','','','',NULL),
	(308,'Little Star Gift Vouchers',NULL,'Unit 104','181 St Kilda Road','St Kilda','Vic','Australia','3182','9537 1737','','','','','',NULL),
	(309,'Little Star Gifts',NULL,'5-Jan','242 Hawthorn Road','Caulfield','Vic','Australia','3162','1300 884 649','','','','','',NULL),
	(310,'Living Doll',NULL,'','','','','Australia','','','','','','','',NULL),
	(311,'Local Builders Group',NULL,'PO Box 2406','','TAYLORS LAKES','VIC','Australia','3038','8390 7456','','','','','',NULL),
	(312,'Local Markets Online Pty Ltd',NULL,'Unit 7','91 Fitzroy Street','St Kilda','Vic','Australia','3182','0418 342 800','','','','','',NULL),
	(313,'LOD Workwear',NULL,'3/200 Boundary Road','','Braeside','Vic','Australia','3195','9588 2898','','','','','',NULL),
	(314,'Lomi Lomi (ref transformations)',NULL,'','','','','Australia','','','','','','','',NULL),
	(315,'Lusty EMS',NULL,'PO BOX 362','','Altona North','Vic','Australia','3025','','','','','','',NULL),
	(316,'M & A Partners',NULL,'Level 4,','164 Flinders Lane','MELBOURNE','Vic','Australia','3000','03 9078 5850','03 8678 1143','','','','',NULL),
	(317,'M2 Real Estate',NULL,'4 Howitt Street','','South Yarra','VIC','Australia','3141','+61 3 9827 9930','','','','','',NULL),
	(318,'Macedon Grand Tour',NULL,'','','','','Australia','','','','','','','',NULL),
	(319,'Mamadoo.com',NULL,'3 Tasman St','','DEE WHY','NSW','Australia','2099','415447812','','','','','',NULL),
	(320,'Mamma Massage',NULL,'','','','','Australia','','','','','','','',NULL),
	(321,'Mannagum Outdoors',NULL,'','','','','Australia','','','','','','','',NULL),
	(322,'Mannagum Outdoors',NULL,'Refer to Debenham Int\'l','','','','Australia','','','','','','','',NULL),
	(323,'Marcela Sampaio',NULL,'8/61 Davis Ave','','South Yarra','vic','Australia','3141','','','','','','',NULL),
	(324,'Maria Brett Counselling & Psychotherapy',NULL,'9/36 Livingstone Road','','ELTHAM','VIC','Australia','3096','','','','','','',NULL),
	(325,'Marjorie Milner College',NULL,'401 Canterbury Road','','Surrey Hills','Vic','Australia','3127','03 9880 7257','','','','','',NULL),
	(326,'Mark',NULL,'','','','','Australia','','','','','','','',NULL),
	(327,'Market Sports',NULL,'Unit 2','300 Macaulay Road','North Melbourne','Vic','Australia','3051','8679 4530','','','','','',NULL),
	(328,'Marvel Homes',NULL,'PO Box 656','','Balwyn','Vic','Australia','3104','','','','','','',NULL),
	(329,'Master Builders Association Victoria',NULL,'332 Albert Street','','East Melbourne','VIC','Australia','3002','9411 4555','','','','','',NULL),
	(330,'Maternity & Baby Boutique',NULL,'8A Irving Ave','','Murrumbeena','Vic','Australia','3163','','','','','','',NULL),
	(331,'Mavi Homes',NULL,'Suite 10','222 Keilor Road','North Essendon','Vic','Australia','3041','9379 5000','','','','','',NULL),
	(332,'MaxiTRANS Australia Pty Ltd',NULL,'Ash Vague','346 Boundary Road','DERRIMUT','VIC','Australia','3030','03 83681183','','','','','',NULL),
	(333,'MaxiTRANS Parts',NULL,'PO Box 768','','Sunshine','VIC','Australia','3020','+61 3 93687000','','','','','',NULL),
	(334,'MCA Media',NULL,'Level1-195 Little Collins St','','Melbourne','Vic','Australia','3000','','','','','','',NULL),
	(335,'McCormicks',NULL,'121 Rayhur Street','Building 14 Level 1','Clayton','Vic','Australia','3168','','','','','','',NULL),
	(336,'MDM Copy Centre',NULL,'47 University Street','','Carlton','Vic','Australia','3053','9347 7000','9347 0500','','','','',NULL),
	(337,'Media Sales Consulting Pty Ltd',NULL,'44 Lambeth Street','','Kensington','Vic','Australia','3031','0402 403 182','','','','','',NULL),
	(338,'Melbourne Business Network Incorporated',NULL,'Suite 202C, Level 2','282 Collins St','MELBOURNE','VIC','Australia','3000','9639 6950','','','','','',NULL),
	(339,'Melbourne Glass & Aluminium Services',NULL,'PO Box 2338','','North Brighton','Vic','Australia','3186','+61 3 85859595','+61 3 85859555','','','','',NULL),
	(340,'Melbourne Panel Repairs',NULL,'369 - 375 City Road','','SOUTH MELBOURNE','VIC','Australia','3025','','','','47058052234','','',NULL),
	(341,'Melbourne School of Fashion',NULL,'3/185 Spring Street','','Melbourne','Vic','Australia','3000','9945 9511','','','','','',NULL),
	(342,'Melissa Dark and Associates (no longer client)',NULL,'2/125 Glenhuntly Rd','','Elwood','Vic','Australia','3184','418684918','','','13346550159','','',NULL),
	(343,'Melissa Johnson',NULL,'11 Fawkner Street','','St Kilda','Vic','Australia','3182','','','','','','',NULL),
	(344,'Melton Youth Service',NULL,'PO Box 21','','Melton','Vic','Australia','3337','','','','','','',NULL),
	(345,'Mentor Press',NULL,'2/32 Taunton Drive','','Cheltenham','Vic','Australia','3192','','','','','','',NULL),
	(346,'Mera Chemicals Pty Ltd',NULL,'34 Law Court','','West Sunshine','Vic','Australia','3020','93114244','93113755','','','','',NULL),
	(347,'Merinda Garrett (no longer client)',NULL,'PO Box 15','','Abbotsford','VIC','Australia','3067','0417 947 989','','','','','',NULL),
	(348,'Metabo',NULL,'10 Dalmore Drive','','Scoresby','Vic','Australia','3179','97650138','','','','','',NULL),
	(349,'Michael Angelo Massage Therapist',NULL,'13 Meagher Rd','','Ferntree Gully','Vic 3156','Australia','','','','','','','',NULL),
	(350,'Michael Loh',NULL,'14 Willis Street','','North Balwyn','VIC','Australia','3104','','','','','','',NULL),
	(351,'Midcity TV',NULL,'419 Brunswick Street','','Fitzroy','Vic','Australia','3065','','','','','','',NULL),
	(352,'Miles for Smiles',NULL,'peter@milesforsmiles.org.au','','','','Australia','','','','','','','',NULL),
	(353,'Millar Inc',NULL,'','','','','Australia','','0412 513 882','','','','','',NULL),
	(354,'Millie & More',NULL,'Suite 2-64 Sutton St','','North Melbourne','Vic','Australia','3051','9428-8072','','','','','',NULL),
	(355,'Mills Elastomers',NULL,'44-80 Sinclair Road','','Dandenong','VIC','Australia','3175','','','','','','',NULL),
	(356,'Mindful By Design',NULL,'PO Box 4206','','Balwyn East','VC','Australia','3103','','','','','','',NULL),
	(357,'MJM Pty Ltd',NULL,'45 Lark Rd','','Exford','3338','Australia','','409898803','','','','','',NULL),
	(358,'Mordialloc Myotherapy',NULL,'73 Warren Road','','Mordialloc','Vic','Australia','3195','414387969','','','','','',NULL),
	(359,'Mornington Sea Glass',NULL,'PO Box 2175','','MORNINGTON','VIC','Australia','3931','0402 421 911','','','','','',NULL),
	(360,'Morrows Freightlines',NULL,'PO Box 4313','','Dandenong South','Vic','Australia','3164','97672600','97013881','','35005897165','','',NULL),
	(361,'Mosaic Marketing',NULL,'35 Donald Street','','Highett','Vic','Australia','3190','418996726','','','','','',NULL),
	(362,'Motorised Metal Craft',NULL,'PO Box 814','','Boronia','Vic','Australia','3155','','','','','','',NULL),
	(363,'Moxie',NULL,'c/o Millie & More','Suite 2-64 Sutton St','North Melbourne','3051','Australia','','','','','','','',NULL),
	(364,'Moxxi',NULL,'21 Frank Street','','Balwyn North','Vic','Australia','3104','','','','','','',NULL),
	(365,'Mr Cornelius',NULL,'14-18 Cremorne St','','RICHMOND','VIC','Australia','3121','','','','','','',NULL),
	(366,'Mr Piccolo',NULL,'203/18 Bent Street','','Kensington','NSW','Australia','3031','03 9329 9686','','','','','','Y'),
	(367,'Mumma Massage',NULL,'4 Flowerdale Road','','Glen Iris','VIC','Australia','3146','425785488','','','','','',NULL),
	(368,'Murrumbeena Park Bowls Club',NULL,'PO Box 53','','CARNEGIE','VIC','Australia','3163','','','','','','',NULL),
	(369,'MWL Limited',NULL,'','','','','Australia','','','','','','','',NULL),
	(370,'Naiko PC',NULL,'174A Somerville Road','','WEST FOOTSCRAY','VIC','Australia','3012','9315 4050','','','','','',NULL),
	(371,'NAOL',NULL,'Unit 31-32','3 Box Road','TAREN POINT','NSW','Australia','2229','02 9540 6111','','','','','',NULL),
	(372,'Naomi Crisante',NULL,'6 Burnside Ave','','Canterbury','Vic','Australia','3126','','','','','','',NULL),
	(373,'NDEVR',NULL,'Suite 6 Level 3','499 St Kilda Rd','Melbourne','Vic','Australia','3004','','','','','','',NULL),
	(374,'Nectar Design',NULL,'35 Somerville Rd','','Yarraville','Vic','Australia','3013','9332 7337','','','','','',NULL),
	(375,'Neon Effect',NULL,'PO Box 52','','Cambridge','','Australia','3450','0064 508 4 63667','0064 7 8279415','','','','',NULL),
	(376,'Neutral Instinct (no longer client)',NULL,'33 Browning St','','SEDDON','VIC','Australia','3011','0418 547 361','','','','','',NULL),
	(377,'New Dimension Drafting',NULL,'Level 1 273 Mt Alexander Rd','','Ascot Vale','3032','Australia','','','','','','','',NULL),
	(378,'Nicholas Jones',NULL,'','','','','Australia','','','','','','','',NULL),
	(379,'Northern Master Builders',NULL,'37 Fitzroy St','','KILMORE','VIC','Australia','3764','','','','','','',NULL),
	(380,'Number Hunter',NULL,'PO Box 5168','','Hughesdale','VIC','Australia','3166','','','','','','',NULL),
	(381,'Nutrition & Fitness Inspirations',NULL,'10 Broxbourne Brae','','Mornington','VIC','Australia','3931','','','','','','',NULL),
	(382,'Oakford Australia P/L',NULL,'','','','','Australia','','','','','','','',NULL),
	(383,'Oaktower',NULL,'266 Barkly Street','','North Fitzroy','VIC','Australia','3068','','','','','','',NULL),
	(384,'Olive Contruction',NULL,'1415 Malvern Road','','Malvern','VIC','Australia','3144','','','','','','',NULL),
	(385,'one life',NULL,'670 Canterbury Road','','Surrey Hills','Vic','Australia','3120','','','','','','',NULL),
	(386,'One Life Solutions (Aust) Pty Ltd',NULL,'C/o Darrer Muir Fleiter','670 Canterbury Road','Surrey Hills','Vic','Australia','3120','','','','','','',NULL),
	(387,'Onyx Finance',NULL,'Suite 1','1463 Malvern Rd','Malvern','Vic','Australia','3144','','','','','','',NULL),
	(388,'Opera in the Market',NULL,'St Vincent\'s Foundation','PO Box 2900','FITZROY','VIC','Australia','3065','03 9231 3287','','','','','',NULL),
	(389,'Options Consulting Group',NULL,'Level 3, 20-22 Albert Road','','South Melbourne','VIC','Australia','3205','+61 3 9693 9300','','','','','',NULL),
	(390,'Orbigate Pty Ltd',NULL,'496 Lutwyche Road','','LUTWYCHE','QLD','Australia','4030','07 3857 1685','','','','','',NULL),
	(391,'OTS Developments',NULL,'C/- Carmelo Papalia','41 Fulton Street','St Kilda East','Vic','Australia','3183','0418 716 655','','','','','',NULL),
	(392,'PA Foley',NULL,'','','','','Australia','','','','','','','',NULL),
	(393,'Pacific Coast Bananas',NULL,'PO Box 142','','Innisfail','Qld','Australia','4860','Andrew Scully','','','','','',NULL),
	(394,'Par Taps',NULL,'Att: Perry Macridis','23-27 Bellevve Cresent','Preston','Vic','Australia','3072','','9484-0949','','','','',NULL),
	(395,'Parkhill Freeman',NULL,'PO Box 3024','','WHEELERS HILL','Vic','Australia','3150','','','','','','',NULL),
	(396,'Party Perfect',NULL,'C/O Helen Pilley','94 Asling Street','Brighton','Vic','Australia','3186','0423 242 080','','','','','',NULL),
	(397,'Party Supplies Emporium',NULL,'4/1A Byth Street','','STAFFORD','QLD','Australia','4053','07 3352 6066','','','','','',NULL),
	(398,'Pat\'s Music',NULL,'940-944 Centre Road','','Oakleigh South','Vic','Australia','3167','9563 8711','','','','','',NULL),
	(399,'Pea In A Pod',NULL,'Unit 1','222 Johnstone Street','Collingwood','Vic','Australia','3066','03 9415 6800','','','','','',NULL),
	(400,'Peach Air Conditioning',NULL,'4/227 Wells Rd','','Chelsea Heights','Vic','Australia','3196','9773-1241','','','','','',NULL),
	(401,'Pedalista',NULL,'8 Hurley Street','','Balnaring','VIC','Australia','3926','','','','','','',NULL),
	(402,'Pelican Education Services',NULL,'6 Heron Street','','WOODEND','VIC','Australia','3442','0418 563 692','','','','','',NULL),
	(403,'Pepper Corn Cottage Escapes',NULL,'Mr & Mrs C & P Smith','The Pines','Maldon','Vic','Australia','3463','','','','','','',NULL),
	(404,'PERSA (Personal Emergency Response Services Assoc)',NULL,'PO Box 1100','','Lane Cove','NSW','Australia','1595','','','','','','',NULL),
	(405,'Peter Jones Special Events',NULL,'172-174 Chetwynd Street','','North Melbourne','Vic','Australia','3051','9320 5700','','','','','',NULL),
	(406,'Peter Katsambanis',NULL,'4 Bagot Place','','Hillarys','WA','Australia','6025','','','','','','',NULL),
	(407,'Photomagnetics',NULL,'','','','','Australia','','','','','','','',NULL),
	(408,'Physio Health',NULL,'224 Keilor Road','','ESSENDON','VIC','Australia','3040','9379 3716','','','','','',NULL),
	(409,'Pia Sabbadina',NULL,'','','','','Australia','','','','','','','',NULL),
	(410,'Planet Paintball',NULL,'4-24 Elster Ave','','Gardenvale','Vic','Australia','3185','','','','','','',NULL),
	(411,'Poly 4 x 4',NULL,'256 Huntingdale Road','','Oakleigh','VIC','Australia','3166','9562 7779','barryd@poly4x4.com.au','','','','',NULL),
	(412,'Port Melbourne Football Club',NULL,'PO Box 247','','PORT MELBOURNE','VIC','Australia','3207','0407 964 620','','','','','',NULL),
	(413,'Premier Doors',NULL,'69 Killara Road','','Campbellfield','VIC','Australia','3061','+61 3 93578477','+61 3 93578938','','68007392930','','',NULL),
	(414,'Premier Extrusion',NULL,'Rear Factory','26-28 Glenbarry Road','Campbellfield','VIC','Australia','3060','+61 3 93578372','','','','','',NULL),
	(415,'Printism',NULL,'1/27 South Ave','','BENTLEIGH','VIC','Australia','3204','0425 757 733','','','','','',NULL),
	(416,'Prinzi Collection',NULL,'389 Rathdowne Street','','CARLTON','VIC','Australia','3053','9347 2188','','','','','',NULL),
	(417,'Proeye TV',NULL,'1/617 Spencer Street','','West Melbourne','Vic','Australia','3003','0410 441 888','','','','','',NULL),
	(418,'Project Ceilings',NULL,'Factory 2, 3 Sir Laurence Drive','','Seaford','Vic','Australia','3198','','','','','','',NULL),
	(419,'Psychotherapy &Counselling Federation of Australia',NULL,'290 Park Street','','Fitzroy North','VIC','Australia','3068','9486 3077','','','','','',NULL),
	(420,'Qua Promotions Pty Ltd',NULL,'9 Ladner Ct','','Chadstone','Vic','Australia','3148','411111426','','','','','',NULL),
	(421,'Real Time',NULL,'3/3 Struan Street','','Toorak','Vic','Australia','3142','95963382','','','','','',NULL),
	(422,'Red Gem',NULL,'63 Nar Nar Goon Road','','Nar Nar Goon','VIC','Australia','3812','+61 3 5942 5205','','','','','',NULL),
	(423,'Redrock Gallery',NULL,'Shop 5 & 6 Level 3','Southgate Ave','Southbank','Vic','Australia','3006','98261244','','','','','',NULL),
	(424,'Rees Electrics Pty Ltd',NULL,'12 The Boulevard','','McCrae','Vic','Australia','3938','0409 455 655','','','','','',NULL),
	(425,'Reflective Fabrications of Australia',NULL,'34 Murdock Street','','South Clayton','Vic','Australia','3169','','','','','','',NULL),
	(426,'Regency Tourism',NULL,'R J FROGGATT','7 Centennial Place','Campbells Bay','Auckland','Australia','630','4021750944','','','','','',NULL),
	(427,'Relux Slabs',NULL,'Suite 1 - 275 Wattletree Road','','Malvern','Vic','Australia','3145','95099533','9509 7866','','','','',NULL),
	(428,'Rennew Homes',NULL,'32 Rutherglen Cres','','Gowanbrae','Vic','Australia','3043','83361031','','','','','',NULL),
	(429,'Renold Australia',NULL,'508-520 Wellington Rd','','Mulgrave','Vic','Australia','3170','03 92623323','','','27004270179','','',NULL),
	(430,'Return Med',NULL,'PO Box 2856','','CHELTENHAM','VIC','Australia','3192','1300 650 835','','','','','',NULL),
	(431,'Review Me',NULL,'G25/202 Wells Rd','','Wheelers Hill','Vic','Australia','3150','','','','','','',NULL),
	(432,'Revive2Survive.com.au',NULL,'Suite 18, 1 East Ridge Drive','','CHIRNSIDE PARK','VIC','Australia','3116','1300 000 112','','','','','',NULL),
	(433,'Reynolds(do not use)',NULL,'508','','','','Australia','','','','','','','',NULL),
	(434,'Riley Plumbing',NULL,'PO Box 220','','SORRENTO','VIC','Australia','3943','0416 107 649','','','','','',NULL),
	(435,'Ringwood Jewellers',NULL,'Shop 2 Centro Ringwood','Cnr New St & Maroondah Hwy','Ringwood','Vic','Australia','3134','9879 6884','candice@yourjeweller.','','','','',NULL),
	(436,'Risktech',NULL,'Attention: Kristine Gatt','PO Box 6127','Melbourne','Vic','Australia','3004','0433 777 663','','','18095583556','','',NULL),
	(437,'River Of Life Conference',NULL,'12 John Street','','Kew','Vic','Australia','3101','+61 3 98167111','','','24699016115','','',NULL),
	(438,'Robinson',NULL,'14 Florence Street','','Prahran','VIC','Australia','3181','','','','','','',NULL),
	(439,'Rochester Castle Hotel',NULL,'202 Johnston Street','','Fitzroy','Vic','Australia','3065','','','','','','',NULL),
	(440,'Rustic & Country Decor',NULL,'48 Nobility Street','','MOOLAP','VIC','Australia','3221','03 5244 5319','','','95495494505','','',NULL),
	(441,'Ryan-Ryte Enterprises Pty Ltd',NULL,'4 Frankston Garden Drive','','Carrum Downs','Vic','Australia','3201','97825515','','','','','',NULL),
	(442,'Sacred Heart Mission',NULL,'87 Grey Street','','St Kilda','VIC','Australia','3182','','','','','','',NULL),
	(443,'Sam\'s Fresh',NULL,'Melbourne Market Box 198','542 Footscray Road','West Melbourne','Vic','Australia','3003','95553482','','','66117063297','','',NULL),
	(444,'Samson Educational Consultants',NULL,'Level 2','343 King Street','Melbourne','Vic','Australia','3000','','','','','','',NULL),
	(445,'Scarpa Imports',NULL,'Unit 2','41 Rose St','Richmond','Vic','Australia','3121','','','','','','',NULL),
	(446,'Secure Retail Solutions Pty Ltd',NULL,'PO Box 713','','St Ives','NSW','Australia','2075','02 9449 8296','','','','','',NULL),
	(447,'Sefton & Associates',NULL,'PO Box 1715','','TAMWORTH','NSW','Australia','2340','02 6766 5222','','','','','',NULL),
	(448,'Setia St Kilda (Melbourne) Pty Ltd',NULL,'132 Franklin Street','','MELBOURNE','VIC','Australia','3000','0430 344 968','','','','','',NULL),
	(449,'SGW',NULL,'PO Box 415','','Soth Yarra','Vic','Australia','3141','','','','','','',NULL),
	(450,'Shepherd\'s View',NULL,'95 Yarrabee Rd','','Markwood','Vic','Australia','3678','+61 3 57522232','','','','','',NULL),
	(451,'Shugg Nominees (Vic) Pty Ltd',NULL,'30 Maple St','','BLACKBURN','VIC','Australia','3130','0403 715 207','','','','','',NULL),
	(452,'Simply Set-Up Bookkeeping',NULL,'12B, 80 Keilor Road','','Essendon North','Vic','Australia','3041','0402 345 227','','','','','',NULL),
	(453,'Sira Group',NULL,'530 Little Collins Street','','Melbourne','Vic','Australia','3000','9909 7018','','','','','',NULL),
	(454,'Site Management',NULL,'PO Box 5222','','Heidelberg West','VIC','Australia','3081','','','','','','',NULL),
	(455,'Small Change Design',NULL,'PO Box 569','','Northcote','Vic','Australia','3070','428343004','','','55074931061','','',NULL),
	(456,'Smart Home Products',NULL,'PO Box 4032','','Dandenong Sth','Vic','Australia','3164','8788 5900','','','','','',NULL),
	(457,'Smart HP',NULL,'96-108 Greens Road','','Dandenong South','VIC','Australia','3175','','','','','','',NULL),
	(458,'Smash Pilot',NULL,'4 Tiara Drive','','South Morang','Vic','Australia','3752','407514005','','','','','',NULL),
	(459,'SMSF (Invoice Shugg Nominees)',NULL,'','','','','Australia','','','','','','','',NULL),
	(460,'Snap Pearls',NULL,'PO Box 99','','Rosanna','VIC','Australia','3084','0408 459 320','','','99527828772','','',NULL),
	(461,'Solah Cafe',NULL,'250 Victoria Parade','','EAST MELBOURNE','VIC','Australia','3002','03 9417 1170','','','','','',NULL),
	(462,'Solid Concrete Services',NULL,'PO Box 1362','','LALOR','VIC','Australia','3075','0437 006 398','','','93175606153','','',NULL),
	(463,'Sooper Design',NULL,'PO Box 387','','ALBERT PARK','VIC','Australia','3206','0417 359 465','','','','','',NULL),
	(464,'Sprout Tasmania',NULL,'PO Box 2073','','Howrah','TAS','Australia','7018','0488 991 254','','','','','',NULL),
	(465,'Spyre Digital',NULL,'PO Box 3370','','WETHERILL PARK','NSW','Australia','2164','02 8734 0330','','','','','',NULL),
	(466,'St Jude Medical Australia',NULL,'Unit C2, 63-85 Turner Street','','Port Melbourne','Vic','Australia','3207','','','','','','',NULL),
	(467,'St Vincent\'s Hospital',NULL,'Mission Department','41 Victoria Parade','Fitzroy','Vic','Australia','3065','0410 599 912','','','22052110755','','',NULL),
	(468,'St Vincent\'s Institute of Medical Research',NULL,'41 Victoria Parade','','FITZROY','VIC','Australia','3065','9288 2484','','','','','',NULL),
	(469,'St Vincents Hospital Foundation',NULL,'Attention:  Penny Wedesweller','Level 1 Healey Wing','Fitzroy','VIC','Australia','3065','','','','','','',NULL),
	(470,'Stallion Print Group',NULL,'15 Goulburn Street','','Cheltenham','VIC','Australia','3192','9585 1333','9585 1666','','','','',NULL),
	(471,'Stax of Bargains',NULL,'PO Box 4067','','WEST FOOTSCRAY','VIC','Australia','3012','0409 048 007','','','30321598293','','',NULL),
	(472,'Stefanie Carne Property Consulting',NULL,'33 Atkins St','','KEW','VIC','Australia','3101','402157659','','','','','',NULL),
	(473,'Stick on Signs',NULL,'93-95 Hawthorn Road','','Caulfield North','VIC','Australia','3161','9519 7444','','','50786174596','','',NULL),
	(474,'Stickittome Australia (no longer client)',NULL,'129 Herald Street','','Cheltenham','Vic','Australia','3192','+61 3 95325933','','','61098168006','','',NULL),
	(475,'Strategy Direct',NULL,'2/316 Governor Road','','Braeside','VIC','Australia','3195','+61 3 95872138','','','','','',NULL),
	(476,'Studio Pomodoro',NULL,'38 Hardwick Street','','Coburg','VIC','Australia','3058','402153712','','','31130182764','','',NULL),
	(477,'Stylic',NULL,'22 Charnfield Ct','','Thomastown','Vic','Australia','3074','','','','','','',NULL),
	(478,'Sun Installations Pty Ltd',NULL,'7 Berkeley Avenue','','HEIDELBERG','VIC','Australia','3084','0403 233 956','','','','','',NULL),
	(479,'Sunnybrook',NULL,'553 North Rd','','Ormond','Vic','Australia','3204','95786400','','','','','',NULL),
	(480,'Supreme Packaging',NULL,'522 Clayton Rd','','Clayton','Vic','Australia','3168','','','','','','',NULL),
	(481,'Surefact',NULL,'Suite 22','Level 1','Melbourne','Vic','Australia','3004','8379 3603','','','','','',NULL),
	(482,'Susan Alberti Medical Research Foundation',NULL,'Suite 1, Level 1','202 Jells Road','Wheelers Hill','VIC','Australia','3150','+61 3 9560 1595','+61 3 95616638','','90991709074','','',NULL),
	(483,'SWiM',NULL,'','','','','Australia','','','','','','','',NULL),
	(484,'Swish Print',NULL,'PO Box 8185','','Carrum Downs','Vic','Australia','3201','','','','','','',NULL),
	(485,'Sydney Rubbish Guy',NULL,'Gashcorp Pty Ltd?æ','Trading as The?æSydney?æRubbish Truck','Pyrmont','NSW','Australia','2009','','','','','','',NULL),
	(486,'Tax Affair',NULL,'203 Commercial Road','','South Yarra','VIC','Australia','3141','','','','','','',NULL),
	(487,'Teachers Marketplace',NULL,'The Trustee for Teachers Marketplace Trust','17 Prelate Court','Wynn Vale','SA','Australia','5125','','','','','','',NULL),
	(488,'Technical Protection Systems',NULL,'2/21 Malvern St','','BAYSWATER','VIC','Australia','3153','03 9720 1930','03 9720 1929','','','','',NULL),
	(489,'Tenerant Pty Ltd',NULL,'Suites 11 & 12','2 Central Avenue','MOORABBIN','VIC','Australia','3189','+61 3 9585 6100','','','','','',NULL),
	(490,'Termite Force',NULL,'16 Monique Drive','','Langwarrin','Vic','Australia','3910','','','','','','',NULL),
	(491,'Tertiary Textbooks',NULL,'59A Armadale St','','Armadale','Vic','Australia','3143','92886422','','','','','',NULL),
	(492,'The Abyssinian',NULL,'8 Stanton Court','','Glen Waverley','Vic','Australia','3150','93768754','','','','','',NULL),
	(493,'The Cry',NULL,'','','','','Australia','','','','','','','',NULL),
	(494,'The Curtin',NULL,'29 Lygon St','','Carlton','Vic','Australia','3053','','','','','','',NULL),
	(495,'The Energy Store',NULL,'Flat 4, 373 Park Street','','South Melbourne','Vic','Australia','3205','409665446','','','','','',NULL),
	(496,'The Frontier Group',NULL,'44 Kings Park Road','','West Perth','WA','Australia','6005','','','','','','','Y'),
	(497,'The Jade Group Pty Ltd',NULL,'105 Glenaroua Rd','','Broadford','Vic','Australia','3658','+ 61 3 57844114','','','','','',NULL),
	(498,'The Lomi Connection',NULL,'PO Box 223','','Brighton','Vic','Australia','3186','','','','','','',NULL),
	(499,'The Mindful Executive',NULL,'PO Box 10403','','Brisbane Adelaide Street','Qld','Australia','4000','439461567','','','','','',NULL),
	(500,'The Rum Project',NULL,'PO Box 2856','','Cheltenham','VIC','Australia','3192','','','','','','',NULL),
	(501,'The Sydney Rubbish Bloke',NULL,'48 Bulwara Road','','Pyrmont','NSW','Australia','2009','','','','','','',NULL),
	(502,'The Walking Company',NULL,'','','','','Australia','','','','','','','','Y'),
	(503,'Think Llama Pty Ltd',NULL,'Suite 1','14 Yarraford Ave','Alphington','Vic','Australia','3078','','','','','','',NULL),
	(504,'Tindarra Resort',NULL,'PO Box 160','','Moama','NSW','Australia','2731','','','','','','','Y'),
	(505,'Toorak Accident Repairs',NULL,'718 Malvern Road','','Prahran','VIC','Australia','3181','','','','44708772352','','',NULL),
	(506,'Toscanos',NULL,'217 High Street','','Kew','Vic','Australia','3101','98537762','','','','','',NULL),
	(507,'Totecs (Q-Imaging)',NULL,'28 Abbotsford Street','','West Melbourne','Vic','Australia','3003','03 9645 2900','','','','','',NULL),
	(508,'Touchdown Tours (no longer client)',NULL,'488 High St','','NORTHCOTE','VIC','Australia','3070','9482 5215','','','','','',NULL),
	(509,'Townsend Cobain & Partners',NULL,'Level 4','448 St Kilda Road','MELBOURNE','VIC','Australia','3004','0487 485 487','','','','','',NULL),
	(510,'Tranceformations',NULL,'PO Box 242','','Sandringham','VIC','Australia','3191','','','','','','',NULL),
	(511,'Travel With A Twist',NULL,'Suite 12','210 Toorak Road','SOUTH YARRA','VIC','Australia','3141','+61 3 98264001','','','77001925891','','',NULL),
	(512,'Trust For Nature',NULL,'Level 5','379 Collins Street','Melbourne','VIC','Australia','3000','','','','','','','Y'),
	(513,'Turbo Marketing',NULL,'Unit 18','20 Commercial Road','Melbourne','VIC','Australia','3004','03 9867 2915','','','','','',NULL),
	(514,'Valley Windows',NULL,'PO Box 3130','','GMC','Vic','Australia','3841','88329000','','','','','',NULL),
	(515,'Varvara Ioannou',NULL,'14 Stradmore Avenue','','Templestowe','VIC','Australia','3106','','','','','','','Y'),
	(516,'Vic Restorations Building Services',NULL,'29 Hunter Street','','Castlemaine','Vic','Australia','3450','438725159','','','','','',NULL),
	(517,'Viking Plastics Engineering',NULL,'143 Woodlands Drive','','Braeside','Vic','Australia','3195','95872297','','','','','',NULL),
	(518,'Virtue Homes',NULL,'PO Box 1589','','Traralgon','Vic','Australia','3844','','','','','','',NULL),
	(519,'Visa Perfect',NULL,'30 North Street','','Marrickville','NSW','Australia','2204','','','','','','',NULL),
	(520,'VPNG',NULL,'PO Box 593','','EAST MELBOURNE','VIC','Australia','8002','1300 721 169','','','','','',NULL),
	(521,'VVAA Vic Branch',NULL,'c/o The Secretary','ANZAC House','Melbourne','Vic','Australia','3001','+61 3 96555588','','','19068073450','','',NULL),
	(522,'Web Design By The Bay',NULL,'12,1140 Nepean Highway','','Mornington','Vic','Australia','3931','','','','','','',NULL),
	(523,'Web Prophets',NULL,'PO Box 2007','','ST KILDA','VIC','Australia','3182','9534 1800','','','','','',NULL),
	(524,'Whipp Industries',NULL,'2/21 Malvern St','','BAYSWATER','VIC','Australia','3153','9720 1930','','','','','',NULL),
	(525,'Whole 9 Yards Group',NULL,'','','','','Australia','','0409 108 453','','','','','',NULL),
	(526,'WhyteSands',NULL,'Mr Stuart Johnson','Level 1, 6 Riverside Quay','Southbank','VIC','Australia','3006','0407 250 461','','','','','',NULL),
	(527,'Wild Diva',NULL,'9 Ladner Crt','','Chadstone','Vic','Australia','3148','','','','','','',NULL),
	(528,'Wilson & Bradley Pty Ltd',NULL,'94 Bell Street','','Preston','Vic','Australia','3072','9495 8900','','','','','',NULL),
	(529,'Winn Legal',NULL,'Suite 4 A Ground Floor','116 Hardware Street','Melbourne','Vic','Australia','3000','','','','','','',NULL),
	(530,'WISE Employment',NULL,'552 Victoria Street','','North Melbourne','VIC','Australia','3051','','','','','','',NULL),
	(531,'Workers Compensation Solutions',NULL,'Att: Kristine Gatt','PO Box 6127','Melbourne','Vic','Australia','3004','0433 777 663','','','22085585744','','',NULL),
	(532,'WorkHealth',NULL,'WorkSafe Victoria','GPO Box 4306','Melbourne','VIC','Australia','3001','','','','','','',NULL),
	(533,'Youth Now',NULL,'PO Box 901','','Sunshine','Vic','Australia','3020','0439 636 682','','','','','',NULL),
	(534,'Zito Lawyers',NULL,'','','','','Australia','','','','','','',NULL,NULL),
	(535,'B3000',NULL,'Suite 126, Level 1, The Block Arcade 100 Elizabeth Street','','','VIC','Australia','3000','0429 008 814','','','','','',NULL),
	(536,'Hobsons Bay City Coucil',NULL,'115 Civic Parade','','Altona','VIC','Australia','3018','99321220','','','','14 days','',NULL),
	(537,'Arnold Dallas Mcpherson Lawyers',NULL,'337 Hargreaves Street','','Bendigo','VIC','Australia','3550','5445 9229','','','80 297 162 244','14 Days','',NULL),
	(538,'Australian Web Industry Association',NULL,'PO Box 771','','Inglewood','WA','Australia','6932','','','','','','',NULL);

/*!40000 ALTER TABLE `tbl_client` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_contact
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_contact`;

CREATE TABLE `tbl_client_contact` (
  `contactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_lastname` varchar(255) DEFAULT NULL,
  `contact_add_num` varchar(255) DEFAULT NULL,
  `contact_add_street` varchar(255) DEFAULT NULL,
  `contact_add_city` varchar(255) DEFAULT NULL,
  `contact_add_state` varchar(255) DEFAULT NULL,
  `contact_add_country` varchar(255) DEFAULT NULL,
  `contact_add_postcode` varchar(5) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `contact_mobile` varchar(255) DEFAULT NULL,
  `contact_fax` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_hotdrink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contactID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_contact` WRITE;
/*!40000 ALTER TABLE `tbl_client_contact` DISABLE KEYS */;

INSERT INTO `tbl_client_contact` (`contactID`, `clientID`, `contact_name`, `contact_lastname`, `contact_add_num`, `contact_add_street`, `contact_add_city`, `contact_add_state`, `contact_add_country`, `contact_add_postcode`, `contact_phone`, `contact_mobile`, `contact_fax`, `contact_email`, `contact_hotdrink`)
VALUES
	(14,1,'James Deck',NULL,'','','','NSW','Australia','','','','','',''),
	(20,7,'Ljubica Radoicic',NULL,'','','','','','','','','','Lradoicic@aconex.com',''),
	(21,8,'Anden Rosenberg',NULL,'','','','','','','','','','arosenberg@websetgo.com.au',''),
	(22,9,'',NULL,'','','','','','','','','','accounts@act3planning.com',''),
	(23,10,'Wayne Bishop',NULL,'','','','','','','','','','wbishop@activetics.com.au',''),
	(26,13,'Peter Robinson',NULL,'','','','','','','','','','',''),
	(28,15,'Sue Schrieber',NULL,'','','','','','','','','','sue@advancedfireworksaustralia.com.au',''),
	(29,16,'Sam Snelling',NULL,'','','','','','','','','','sam.snelling@advancedvetcare.com.au',''),
	(30,17,'',NULL,'','','','','','','','','','christineg@aeramix.com.au',''),
	(31,18,'Penny',NULL,'','','','','','','','','','office@agfab.com.au',''),
	(33,20,'Michael Lu',NULL,'','','','','','','','','','michael@agmelectrical.com.au',''),
	(41,28,'Andrew Christie',NULL,'','','','','','','','','','andrew@christie.cc',''),
	(42,29,'Kate Lumsden',NULL,'','','','','','','','','','kate@anthonyjaye.com',''),
	(43,30,'',NULL,'','','','','','','','','','sales@apextruss.com.au',''),
	(44,31,'Chris Brennan',NULL,'','','','','','','','','','',''),
	(45,32,'Derek Rawson',NULL,'','','','','','','','','','',''),
	(47,34,'',NULL,'','','','','','','','','','accountspayable@arlec.com.au',''),
	(49,36,'Lloyd',NULL,'','','','','','','','','','',''),
	(50,37,'Roger',NULL,'','','','','','','','','','info@artstrategies.com.au',''),
	(51,38,'Ash Marton',NULL,'','','','','','','','','','accounts@ashmarton.com.au',''),
	(52,39,'Asher Judah',NULL,'','','','','','','','','','ajudah@ipa.org.au',''),
	(53,40,'Christine Kahwaji',NULL,'','','','','','','','','','christine.kahwaji@ashurst.com',''),
	(54,41,'Peter McMillan',NULL,'','','','','','','','','','admin@apsecurity.com.au',''),
	(56,43,'Philip Kam Accts Pay',NULL,'','','','','','','','','','pkam@assisicentre.com.au',''),
	(57,44,'Dan Martin',NULL,'','','','','','','','','','daniel.martin@astonsocial.com.au',''),
	(58,45,'Pam',NULL,'','','','','','','','','','',''),
	(60,47,'Robert',NULL,'','','','','','','','','','robert@austco-e.com.au',''),
	(61,48,'',NULL,'','','','','','','','','','wd@anbeef.com.au',''),
	(63,50,'Guy Vicars',NULL,'','','','','','','','','','info@guysdomain.com.au',''),
	(64,51,'Sarah Phoonie',NULL,'','','','','','','','','','sarah.phoonie@costaexchange.com.au',''),
	(65,52,'See Diamond Agric Prod',NULL,'','','','','','','','','','jhynes@australiangarlic.com.au',''),
	(66,53,'',NULL,'','','','','','','','','','garlic@bigpond.net.au',''),
	(67,54,'Beverley Smith',NULL,'','','','','','','','','','smith.beverley.s@edumail.vic.gov.au',''),
	(68,55,'Att: Georgia Farmers',NULL,'','','','','','','','','','',''),
	(70,57,'Jardin Truong',NULL,'','','','','','','','','','jardin@thelounge.com.au',''),
	(72,59,'Michael Blewitt',NULL,'','','','','','','','','','info@safetyrail.net.au',''),
	(73,60,'Merisa Ranieri (Treasurer',NULL,'','','','','','','','','','merisa@ranieri.com.au',''),
	(74,61,'Oliver Lindsaar',NULL,'','','','','','','','','','oliver.lindsaar@asit.com.au',''),
	(75,62,'',NULL,'','','','','','','','','','smallbusinessconsultation@ato.gov.au',''),
	(76,63,'Paul McHenry',NULL,'','','','','','','','','','',''),
	(77,64,'Neil Sutton',NULL,'','','','','','','','','','',''),
	(79,66,'',NULL,'','','','','','','','','','natalie@avioncommunications.com.au',''),
	(80,67,'Mark Hardgrave',NULL,'','','','','','','','','','mhardgrave@mapartners.com.au',''),
	(81,68,'',NULL,'','','','','','','','','','avsn@ozemail.com.au',''),
	(83,70,'Stuart McMurtrie',NULL,'','','','','','','','','','stuart.mcmurtrie@gmail.com',''),
	(84,71,'Gerry Robinson',NULL,'','','','','','','','','','mrgfrobinson@hotmail.com',''),
	(88,75,'',NULL,'','','','','','','','','','chris@mybigevent.com.au',''),
	(89,76,'Kelly',NULL,'','','','','','','','','','accounts@barringtoncentre.com.au',''),
	(91,78,'Greg Bolton',NULL,'','','','','','','','','','info@baycityseaplanes.com.au',''),
	(95,82,'Marc Hayden',NULL,'','','','','','','','','','',''),
	(96,83,'Joe Mancuso',NULL,'','','','','','','','','','info@bendex.com.au',''),
	(97,84,'',NULL,'','','','','','','','','','vanessa@bsbpacific.com',''),
	(100,87,'Gerry Robinson',NULL,'','','','','','','','','','',''),
	(101,88,'Mark Boon',NULL,'','','','','','','','','','mark@boonelec.com.au',''),
	(102,89,'Peter Kalaban',NULL,'','','','','','','','','','',''),
	(106,93,'Zlata',NULL,'','','','','','','','','','sales@bucketsandspades.com.au',''),
	(107,94,'',NULL,'','','','','','','','','','rob@buildingcheck.com.au',''),
	(109,96,'Tony Dane',NULL,'','','','','','','','','','info@buoyancy.org.au',''),
	(114,101,'Keith',NULL,'','','','','','','','','','',''),
	(117,104,'Bruce Bramwall',NULL,'','','','','','','','','','admin@castellanfinancial.com.au',''),
	(118,105,'Paul McKenry',NULL,'','','','','','','','','','',''),
	(120,107,'Steven Veenstra',NULL,'','','','','','','','','','steven@corporate-apparel.com.au',''),
	(121,108,'Katherine Smith',NULL,'','','','','','','','','','accounts@act3planning.com',''),
	(123,110,'Lisa Chapman',NULL,'','','','','','','','','','',''),
	(125,112,'',NULL,'','','','','','','','','','cldesign@netspace.net.au',''),
	(130,117,'Clare Lombardi',NULL,'','','','','','','','','','clombardi@citywestwater.com.au',''),
	(137,124,'Kim Littlejohn',NULL,'','','','','','','','','','accounts@clubwarehouse.com.au',''),
	(140,127,'Robert Todoroski',NULL,'','','','','','','','','','robert@cfg4u.com.au',''),
	(141,128,'',NULL,'','','','','','','','','','kerryn@consideritdone.net.au',''),
	(142,129,'Jacinta Gall',NULL,'','','','','','','','','','enquiries@cmpavic.asn.au',''),
	(145,132,'Sue',NULL,'','','','','','','','','','printme@mdmcopycentre.com.au',''),
	(146,133,'',NULL,'','','','','','','','','','chris.dwyer@coreconsulting.com.au',''),
	(148,135,'Paul McKenry',NULL,'','','','','','','','','','support@correctpanels.com.au',''),
	(149,136,'Elissa Porritt',NULL,'','','','','','','','','','elissa@cowgirlmarketing.com.au',''),
	(150,137,'Leonie Tonner',NULL,'','','','','','','','','','leoniet@crackersystems.com',''),
	(151,138,'',NULL,'','','','','','','','','','david@creativeunit.com.au',''),
	(153,140,'Chris Giouris',NULL,'','','','','','','','','','chris@ctgtechnology.com.au',''),
	(154,141,'',NULL,'','','','','','','','','','info@cubiccolsulting.com.au',''),
	(157,144,'Rodney Dedman',NULL,'','','','','','','','','','rod@cyclecitywest.org.au',''),
	(158,145,'Keith Millar',NULL,'','','','','','','','','','keith@millarinc.com.au',''),
	(160,147,'Dagmar Cyrulla',NULL,'','','','','','','','','','',''),
	(161,148,'Michael Pollard',NULL,'','','','','','','','','','mpollard@dailyreview.com.au',''),
	(162,149,'Greg Findlay',NULL,'','','','','','','','','','greg@daytone.com.au',''),
	(163,150,'Rachelle Better-Johnston',NULL,'','','','','','','','','','dejunk1@bigpond.net.au',''),
	(164,151,'Peter Debenham',NULL,'','','','','','','','','','peter@debenhamint.com.au',''),
	(165,152,'',NULL,'','','','','','','','','','info@deltadogz.com.au',''),
	(166,153,'Kate Groves',NULL,'','','','','','','','','','',''),
	(168,155,'Chris Burke',NULL,'','','','','','','','','','chris@otakudigital.com.au',''),
	(169,156,'Robert Bactieri',NULL,'','','','','','','','','','',''),
	(170,157,'Janette Desmier',NULL,'','','','','','','','','','',''),
	(172,159,'Simone de Groot',NULL,'','','','','','','','','','',''),
	(173,160,'Nick',NULL,'','','','','','','','','','',''),
	(174,161,'Andrew Douglas',NULL,'','','','','','','','','','andrew@diesel.csd.com.au',''),
	(175,162,'Dean Millson',NULL,'175 Stevenson Street','','Yarraville','VIC','Australia','','','0414733496','','accounts@dimarca.com.au',''),
	(178,165,'Brian Churchill',NULL,'','','','','','','','','','jburns@dryen.com.au',''),
	(179,166,'Belinder Riddler',NULL,'','','','','','','','','','accounts@dynamicwindows.com.au',''),
	(181,168,'Kym Bell-Goodwin',NULL,'','','','','','','','','','',''),
	(182,169,'Michael Garfield',NULL,'','','','','','','','','','michael@eastrise.com.au',''),
	(183,170,'David Carruthers',NULL,'','','','','','','','','','davidc@easybookings.com.au',''),
	(186,173,'Britt James',NULL,'','','','','','','','','','',''),
	(192,179,'Adam Gurwitch',NULL,'','','','','','','','','','adam.gurwitch@essentiallygroup.com',''),
	(193,180,'',NULL,'','','','','','','','','','info@ethiopianembassy.net',''),
	(194,181,'Hari Srinivas',NULL,'','','','','','','','','','hari@ethnicchannels.com',''),
	(195,182,'John Warner',NULL,'','','','','','','','','','accounts@etrack.com.au',''),
	(198,185,'Antoinette Hall',NULL,'','','','','','','','','','a.hall@eurekatower.com.au',''),
	(200,187,'Sam Pratico',NULL,'','','','','','','','','','sam@siteman.com.au',''),
	(201,188,'Sean Mulligan',NULL,'','','','','','','','','','matt@strategydirect.com.au',''),
	(203,190,'',NULL,'','','','','','','','','','finance@fambiz.org.au',''),
	(204,191,'Paul Sprunt','','','','','QLD','Australia','','','0414 599 417','','paul@fbrpl.com.au',''),
	(205,192,'BELINDA FAULKNER',NULL,'','','','','','','','','','',''),
	(206,193,'',NULL,'','','','','','','','','','fzlegal@netspace.net.au',''),
	(207,194,'',NULL,'','','','','','','','','','stuartg@dodo.com.au',''),
	(210,197,'Julia Cantello',NULL,'','','','','','','','','','jcantello@vcint.com.au',''),
	(212,199,'Alan Pavey',NULL,'','','','','','','','','','alan@mosaic-mc.com.au',''),
	(213,200,'Adam Linguroski',NULL,'','','','','','','','','','adam@fleettrades.com.au',''),
	(215,202,'Dave Scop',NULL,'','','','','','','','','','dave@footycards.com.au',''),
	(220,207,'Glen Pearce',NULL,'','','','','','','','','','',''),
	(221,208,'',NULL,'','','','','','','','','','Gtrecyling@bigpond.com',''),
	(222,209,'Sian Mackenzie',NULL,'','','','','','','','','','sales@ganache.com.au',''),
	(223,210,'James Wall',NULL,'','','','','','','','','','maree@gardenworldnursery.com.au',''),
	(225,212,'Justin Hanley',NULL,'','','','','','','','','','gashcorp@gmail.com',''),
	(227,214,'Georgia',NULL,'','','','','','','','','','',''),
	(230,217,'',NULL,'','','','','','','','','','kim@glasshousestrategies.com',''),
	(233,220,'Stan Godek',NULL,'','','','','','','','','','info@godek.com.au',''),
	(234,221,'',NULL,'','','','','','','','','','d.tepper@goldage.com.au',''),
	(237,224,'Iain Gartley',NULL,'','','','','','','','','','',''),
	(238,225,'Amy Leech',NULL,'','','','','','','','','','amy@greenevent.com.au',''),
	(239,226,'Cameron Black',NULL,'','','','','','','','','','admin@greenrockvintage.com',''),
	(240,227,'Andrew Todd',NULL,'','','','','','','','','','andrew.todd@gtsgroup.com.au',''),
	(243,230,'Margot Vincent',NULL,'','','','','','','','','','g100@group100.com.au',''),
	(247,234,'',NULL,'','','','','','','','','','michael.heac@bigpond.com',''),
	(248,235,'Ally Hiatt',NULL,'','','','','','','','','','gracie@hiatt.net.au',''),
	(251,238,'Ray Toscano',NULL,'','','','','','','','','','',''),
	(252,239,'Alex Anderson',NULL,'','','','','','','','','','aanderson@holmes.edu.au',''),
	(253,240,'Michael',NULL,'','','','','','','','','','janine@hometouchsystems.com.au',''),
	(254,241,'Todd Nicholson',NULL,'','','','','','','','','','todd.nicholson0@gmail.com',''),
	(255,242,'Troy',NULL,'','','','','','','','','','cj@healthycommunication.com.au',''),
	(256,243,'Meg Price',NULL,'','','','','','','','','','megprice@hrinside.com.au',''),
	(257,244,'Phil Ramsten',NULL,'','','','','','','','','','info@hvagroup.com.au',''),
	(262,249,'Dianne',NULL,'','','','','','','','','','admin@inkermanpanels.com.au',''),
	(267,254,'Wayne Bramley',NULL,'','','','','','','','','','wayne@inscon.com.au',''),
	(268,255,'Adam',NULL,'','','','','','','','','','adam.wilson@wilpak.com.au',''),
	(269,256,'Rebecca Collins',NULL,'','','','','','','','','','JaclynT@isaacscole.com.au',''),
	(279,266,'Send to Viking',NULL,'','','','','','','','','','kathy@Vikingplastics.com.au',''),
	(285,272,'',NULL,'','','','','','','','','','darvos51@gmail.com',''),
	(287,274,'Dani Nowicki',NULL,'','','','','','','','','','reception@jtce.com.au',''),
	(289,276,'',NULL,'','','','','','','','','','rose@justfitness.com.au',''),
	(290,277,'412293099',NULL,'','','','','','','','','','jockserong@yahoo.com.au',''),
	(295,282,'Kerri Muller',NULL,'','','','','','','','','','admin@keepittogether.com.au',''),
	(297,284,'Rilke Muir',NULL,'','','','','','','','','','',''),
	(298,285,'Matt Walkerden',NULL,'','','','','','','','','','mat@keynovation.com.au',''),
	(300,287,'',NULL,'','','','','','','','','','philip@kingsarcade.com.au',''),
	(301,288,'Matthew Barnett',NULL,'','','','','','','','','','maureen@kingfisherrecruitment.com.au',''),
	(303,290,'Anthony Battye',NULL,'','','','','','','','','','jlboyce@kpmg.com.au',''),
	(307,294,'',NULL,'','','','','','','','','','richard@officemails.net',''),
	(309,296,'Laura Manariti',NULL,'','','','','','','','','','laura@manariti.id.au',''),
	(311,298,'David Whitcher',NULL,'','','','','','','','','','info@leadlightstation.com.au',''),
	(312,299,'Mark Simpson',NULL,'','','','','','','','','','',''),
	(315,302,'',NULL,'','','','','','','','','','les@zigomanis.com',''),
	(318,305,'Mark Mortelliti',NULL,'','','','','','','','','','mark@lifestylefurniture.com.au',''),
	(319,306,'Laure Pennell',NULL,'','','','','','','','','','laure@ligerdesign.com.au',''),
	(320,307,'Andrew Oxland',NULL,'','','','','','','','','','accounts@limecatering.com.au',''),
	(321,308,'Sam Emms',NULL,'','','','','','','','','','sam@giftpax.com.au',''),
	(322,309,'Sam Emms',NULL,'','','','','','','','','','sam@giftpax.com.au',''),
	(324,311,'',NULL,'','','','','','','','','','david@crosspointhomes.com.au',''),
	(325,312,'Folker Schaumann',NULL,'','','','','','','','','','fschaumann@brightred.com.au',''),
	(326,313,'Liam O\'Donnell',NULL,'','','','','','','','','','Shea@lodpromotions.com',''),
	(329,316,'Antony Lynch',NULL,'','','','','','','','','','alynch@mapartners.com.au',''),
	(330,317,'',NULL,'','','','','','','','','','info@m2re.com.au',''),
	(332,319,'Natalie Dinsdale',NULL,'','','','','','','','','','',''),
	(337,324,'',NULL,'','','','','','','','','','therapy@mariabrett.com.au',''),
	(338,325,'James Milner',NULL,'','','','','','','','','','info@marjoriemilner.edu.au',''),
	(340,327,'Melvyn Port',NULL,'','','','','','','','','','melvyn.port@marketsports.com.au',''),
	(342,329,'Michelle',NULL,'','','','','','','','','','mbassist@mbav.com.au',''),
	(344,331,'Christina Raftopoulos',NULL,'','','','','','','','','','christina@mavihomes.com.au',''),
	(345,332,'Stuart McMurtie or Ash',NULL,'','','','','','','','','','ap@maxitrans.com.au',''),
	(346,333,'Karina Seylim',NULL,'','','','','','','','','','karina.seylim@maxiparts.com.au',''),
	(347,334,'',NULL,'','','','','','','','','','ben.bartlett@mediacommunications.com.au',''),
	(348,335,'Mr Mark Whitelaw',NULL,'','','','','','','','','','',''),
	(349,336,'Sue',NULL,'','','','','','','','','','printme@mdmcopycentre.com.au',''),
	(350,337,'Michael Pollard',NULL,'','','','','','','','','','mpollard@gmail.com',''),
	(351,338,'John Stock',NULL,'','','','','','','','','','john.stock@melbournebusinessnetwork.org.au',''),
	(353,340,'Paul McHenry',NULL,'','','','','','','','','','enq@melbournepanels.com.au',''),
	(354,341,'Marie Brezzi',NULL,'','','','','','','','','','',''),
	(355,342,'Melissa',NULL,'','','','','','','','','','mdark@melissadark.com.au',''),
	(357,344,'Mr Jones',NULL,'','','','','','','','','','',''),
	(359,346,'John Stavrakis',NULL,'','','','','','','','','','admin@merachemicals.com.au',''),
	(360,347,'',NULL,'','','','','','','','','','merinda@merindagarrett.com',''),
	(361,348,'Fiona O\'Callaghan',NULL,'','','','','','','','','','',''),
	(365,352,'',NULL,'','','','','','','','','','peter@milesforsmiles.org.au',''),
	(366,353,'Keith Millar',NULL,'','','','','','','','','','keith@millarinc.com.au',''),
	(368,355,'',NULL,'','','','','','','','','','mrigby@millsrubber.com',''),
	(370,357,'Jenni Marshall',NULL,'','','','','','','','','','',''),
	(371,358,'Andrew',NULL,'','','','','','','','','','agluyas@bluebottle.com',''),
	(373,360,'David Morrow',NULL,'','','','','','','','','','',''),
	(374,361,'Alan Pavey',NULL,'','','','','','','','','','alan@mosaic-mc.com.au',''),
	(379,366,'Anthony Jaye',NULL,'','','','','','','','','','aj@anthonyjaye.com',''),
	(380,367,'April Thewlis',NULL,'','','','','','','','','','',''),
	(383,370,'Michael Frendo',NULL,'','','','','','','','','','michael@naikopc.com.au',''),
	(384,371,'Allan Mann',NULL,'','','','','','','','','','accounts@naol.com.au',''),
	(387,374,'Enzo Conti',NULL,'','','','','','','','','','enzo@nectardesign.com.au',''),
	(388,375,'Marion Bennett',NULL,'','','','','','','','','','martin@neoneffect.co.nz',''),
	(389,376,'Amanda Tiernan',NULL,'','','','','','','','','','info@neutralinstinct.com.au',''),
	(391,378,'',NULL,'','','','','','','','','','nicholas.p.jones@hotmail.com',''),
	(392,379,'',NULL,'','','','','','','','','','peter@nmbuilders.com.au',''),
	(394,381,'Kerstin Lindsay',NULL,'','','','','','','','','','kerstinlindsay@hotmail.com',''),
	(397,384,'Boris Malenic',NULL,'','','','','','','','','','boris@oliveconstruction.com.au',''),
	(401,388,'Jill McKenna',NULL,'','','','','','','','','','jill.mckenna@svha.org.au',''),
	(402,389,'John Gilbert/Barbara Gord',NULL,'','','','','','','','','','BGordon@optionsgroup.com.au',''),
	(404,391,'Carl Papalia',NULL,'','','','','','','','','','carl@otsdevelopments.com.au',''),
	(408,395,'',NULL,'','','','','','','','','','david@parkhillfreeman.com.au',''),
	(409,396,'Helen',NULL,'','','','','','','','','','',''),
	(410,397,'Les Hussey-Smith',NULL,'','','','','','','','','','les@packagingdirect.com.au',''),
	(411,398,'Adam Goodwin',NULL,'','','','','','','','','','accounts@patsmusic.com.au',''),
	(412,399,'Nicky Birkill',NULL,'','','','','','','','','','nicky@peainapod.com.au',''),
	(415,402,'Glenda Fisher',NULL,'','','','','','','','','','glenda@pelicaneducation.com.au',''),
	(417,404,'Julie Cantello',NULL,'','','','','','','','','','jcantello@vcint.com.au',''),
	(418,405,'Rachael Pruden',NULL,'','','','','','','','','','reception@pjse.com.au',''),
	(419,406,'',NULL,'','','','','','','','','','katsambanis@gmail.com',''),
	(421,408,'Stuart Blyth',NULL,'','','','','','','','','','stuart@physiohealth.com.au',''),
	(424,411,'Jim / Jo Cooke',NULL,'','','','','','','','','','accounts@poly4x4.com.au',''),
	(425,412,'Barry Kidd',NULL,'','','','','','','','','','pmfc@bigpond.net.au',''),
	(426,413,'Marcus Tutty',NULL,'','','','','','','','','','marcus@premierdoors.com.au',''),
	(427,414,'Rubi Johnson',NULL,'','','','','','','','','','rubi@premierextrusion.com.au',''),
	(428,415,'Roger Saddington',NULL,'','','','','','','','','','info@printism.com.au',''),
	(429,416,'Pino Prinzi',NULL,'','','','','','','','','','pino@prinzicollections.com.au',''),
	(430,417,'Richard Spanicek',NULL,'','','','','','','','','','info@proeye.com.au',''),
	(432,419,'Luisa Moreno',NULL,'','','','','','','','','','luisa.moreno@pacfa.org.au',''),
	(435,422,'Rob Cerchiaro',NULL,'','','','','','','','','','robertc@redgem.com.au',''),
	(437,424,'Ian Rees',NULL,'','','','','','','','','','irees@bigpond.net.au',''),
	(439,426,'Richard Froggatt',NULL,'','','','','','','','','','rfroggatt@regencytourism.com',''),
	(442,429,'',NULL,'','','','','','','','','','subo.joseph@renold.com.au',''),
	(445,432,'Dominique Laragy',NULL,'','','','','','','','','','admin@revive2survive.com.au',''),
	(447,434,'John Riley',NULL,'','','','','','','','','','rileyplumbing@bigpond.com',''),
	(448,435,'Peter Beever',NULL,'','','','','','','','','','peter@yourjeweller.com.au',''),
	(449,436,'Kristine Gatt',NULL,'','','','','','','','','','accounts@wcdcomp.com.au',''),
	(450,437,'Peter Kentley',NULL,'','','','','','','','','','',''),
	(458,445,'',NULL,'','','','','','','','','','admin@scarpaimports.com',''),
	(459,446,'Lawrence Bove',NULL,'','','','','','','','','','l.bove@secureretailsolutions.com.au',''),
	(460,447,'',NULL,'','','','','','','','','','alison.treloar@seftonpr.com.au',''),
	(461,448,'Katrina Leong',NULL,'','','','','','','','','','katrina.leong@spsetia.com',''),
	(464,451,'Phillip Shugg',NULL,'','','','','','','','','','shuggnick@gmail.com',''),
	(465,452,'Sharen',NULL,'','','','','','','','','','sharenmcd66@gmail.com',''),
	(467,454,'',NULL,'','','','','','','','','','nick@sitemanagement.com.au',''),
	(468,455,'Sally Wills',NULL,'','','','','','','','','','sally@smallchangedesign.com.au',''),
	(471,458,'',NULL,'','','','','','','','','','smashpilot@gmail.com',''),
	(473,460,'Kym',NULL,'','','','','','','','','','kym@snappearls.com.au',''),
	(474,461,'Jay',NULL,'','','','','','','','','','',''),
	(475,462,'Giuseppe Andrea Mamone',NULL,'','','','','','','','','','',''),
	(476,463,'Anne Sherlock',NULL,'','','','','','','','','','anne@sooperdesign.com.au',''),
	(477,464,'Erika Avellaneda',NULL,'','','','','','','','','','erika@sprout.org.au',''),
	(478,465,'Matt Marchetta',NULL,'','','','','','','','','','matt@spyre.com.au',''),
	(480,467,'Jill McKenna',NULL,'','','','','','','','','','jill.mckenna@svha.org.au',''),
	(481,468,'Annie Johnston',NULL,'','','','','','','','','','ajohnston@svi.edu.au',''),
	(482,469,'',NULL,'','','','','','','','','','SVHM.Mission@svha.org.au',''),
	(483,470,'Darren Buck',NULL,'','','','','','','','','','spdesign@bigpond.net.au',''),
	(485,472,'',NULL,'','','','','','','','','','stefanie@stefaniecarne.com.au',''),
	(486,473,'',NULL,'','','','','','','','','','abe@stickonsigns.com.au',''),
	(487,474,'Scott Niven',NULL,'','','','','','','','','','',''),
	(488,475,'Matty / Lisa',NULL,'','','','','','','','','','info@strategydirect.com.au',''),
	(489,476,'',NULL,'','','','','','','','','','kj@studiopomodoro.com.au',''),
	(491,478,'Sally Leslie',NULL,'','','','','','','','','','suninsta@bigpond.net.au',''),
	(492,479,'Geoff',NULL,'','','','','','','','','','',''),
	(494,481,'Paul York',NULL,'','','','','','','','','','pyork@surefact.com.au',''),
	(495,482,'Sue Le Fevre',NULL,'','','','','','','','','','Sue.LeFevre@susanalbertifoundation.org.au',''),
	(496,483,'',NULL,'','','','','','','','','','joe@swim.com.au',''),
	(498,485,'',NULL,'','','','','','','','','','thesydneyrubbishbloke@gmail.com',''),
	(501,488,'',NULL,'','','','','','','','','','steve@technicalprotection.com.au',''),
	(502,489,'',NULL,'','','','','','','','','','pat.walsh@whatsonsale.com.au',''),
	(504,491,'',NULL,'','','','','','','','','','kzorzi@kpmg,com,au',''),
	(505,492,'Ali Robertson',NULL,'','','','','','','','','','ali@ali.id.au',''),
	(508,495,'Libby Conheads',NULL,'','','','','','','','','','',''),
	(512,499,'Elizabeth Hughes',NULL,'','','','','','','','','','elizabeth@themindfulexecutive.com.au',''),
	(517,504,'',NULL,'','','','','','','','','','jonc@tindarra.com.au',''),
	(518,505,'',NULL,'','','','','','','','','','admin@toorakaccidentrepairs.com.au',''),
	(520,507,'Glenn Drew',NULL,'','','','','','','','','','accounts@totecs.com',''),
	(521,508,'Jaqui',NULL,'','','','','','','','','','lizetta@touchdowntours.com.au',''),
	(522,509,'',NULL,'','','','','','','','','','julie.carlos@tcpartners.com.au',''),
	(523,510,'Tracey',NULL,'','','','','','','','','','tracey@aloharainbows.earth',''),
	(524,511,'Romina',NULL,'','','','','','','','','','info@travelwithatwist.com.au',''),
	(527,514,'Deanna Greg',NULL,'','','','','','','','','','gail@valleywindows.com.au',''),
	(528,515,'',NULL,'','','','','','','','','','info@fftn.org.au',''),
	(530,517,'MALCOLM LING',NULL,'','','','','','','','','','kathy@Vikingplastics.com.au',''),
	(533,520,'',NULL,'','','','','','','','','','enquiries@vpng.org.au',''),
	(534,521,'Len Barlow',NULL,'','','','','','','','','','it@vvaavic.org.au',''),
	(536,523,'Carlos Quiraga',NULL,'','','','','','','','','','carlos@webprophets.net.au',''),
	(537,524,'',NULL,'','','','','','','','','','steve@technicalprotection.com.au',''),
	(538,525,'Ben Sciberras',NULL,'','','','','','','','','','ben@wholenineyardsgroup.com.au',''),
	(539,526,'Stuart Johnson',NULL,'','','','','','','','','','sjlj@bigpond.net.au',''),
	(541,528,'Ashleigh O\'Brien',NULL,'','','','','','','','','','kylieg@wilbrad.com.au',''),
	(544,531,'Accounts',NULL,'','','','','','','','','','accounts@wcdcomp.com.au',''),
	(546,533,'',NULL,'','','','','','','','','','info@youthnow.org.au',''),
	(547,23,'Tony',NULL,'','','','NSW','Australia','','','','','tony@allsuburbsupholstery.com.au',''),
	(548,482,'Elda Basso',NULL,'','','','NSW','Australia','','','','','Elda.Basso@susanalbertifoundation.org.au',''),
	(549,482,'Susan Alberti',NULL,'','','','NSW','Australia','','','','','','Tea'),
	(550,536,'Emma Ciolli',NULL,'','','','VIC','Australia','','99321220','','','eciolli@hobsonsbay.vic.gov.au',''),
	(551,436,'Ruth Heywood',NULL,'','','','NSW','Australia','','02 8745 2057','0433 368 887','','RHeywood@risktech.com.au',''),
	(552,537,'Sandy Hall - Project Contact',NULL,'','','','NSW','Australia','','0417 376 865','0417 376 865','','hallfame@netconnect.com.au','Iced Water - no hot drinks'),
	(553,191,'Melanie','','','','','NSW','Australia','','9359 9417','','','',''),
	(554,436,'Mike','McKelliget ','','','','QLD','Australia','','','0416 109 332','','mmckelliget@risktech.com.au',''),
	(555,329,'Leanne','Edwards','','','','NSW','Australia','','03 9411 4515','','','ledwards@mbav.com.au',''),
	(556,124,'Rob','Duncan','','','','NSW','Australia','','','','','rob@clubwarehouse.com.au',''),
	(557,0,'Joshua','Curci','','','','NSW','Australia','','','','','',''),
	(558,1,'Joshua','Curci','','','','NSW','Australia','','','','','','');

/*!40000 ALTER TABLE `tbl_client_contact` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_client_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_client_notes`;

CREATE TABLE `tbl_client_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_client_notes` WRITE;
/*!40000 ALTER TABLE `tbl_client_notes` DISABLE KEYS */;

INSERT INTO `tbl_client_notes` (`noteID`, `clientID`, `staffID`, `note_title`, `note_content`, `note_type`, `note_date`)
VALUES
	(1,162,7,'Waiting on Rhiannon','<p>Waiting for further information from Rhiannon&nbsp;</p>\r\n<p>Need to know what hosting server its going.</p>','','1466405589'),
	(2,NULL,NULL,NULL,NULL,NULL,NULL);

/*!40000 ALTER TABLE `tbl_client_notes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_domains
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_domains`;

CREATE TABLE `tbl_domains` (
  `domainID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `domain_name` varchar(255) DEFAULT NULL,
  `domain_registrar` varchar(255) DEFAULT NULL,
  `domain_registrant` varchar(255) DEFAULT NULL,
  `domain_status` varchar(255) DEFAULT NULL,
  `domain_webhost` varchar(255) DEFAULT NULL,
  `domain_webhost_rate` varchar(255) DEFAULT NULL,
  `domain_webhost_rate_frequency` varchar(255) DEFAULT NULL,
  `domain_renewaldate` varchar(255) DEFAULT NULL,
  `domain_managed` char(1) DEFAULT '',
  PRIMARY KEY (`domainID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_domains` WRITE;
/*!40000 ALTER TABLE `tbl_domains` DISABLE KEYS */;

INSERT INTO `tbl_domains` (`domainID`, `clientID`, `domain_name`, `domain_registrar`, `domain_registrant`, `domain_status`, `domain_webhost`, `domain_webhost_rate`, `domain_webhost_rate_frequency`, `domain_renewaldate`, `domain_managed`)
VALUES
	(548,10,'swim.com.au','Instra','SWiM Communications','Active','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(549,1,'domain.com.au','Melbourne IT','SWiM Communications','Active','Spintel','','F','1469714400','Y'),
	(550,20,'sample.com.au','Instra','SWiM Communications','Lapsed','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(551,34,'google.com.au','Melbourne IT','SWiM Communications','Lapsed','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(552,80,'facebook.com','Instra','SWiM Communications','Active','Spintel',NULL,NULL,'1469887200','N'),
	(553,3,'test.com','Melbourne IT','Swim Communications','Active','Spintel/Comcen',NULL,NULL,'1468332000','N'),
	(554,7,'melbourne.net','Instra','SWiM Communications','Active','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(555,54,'youtube.tv','Melbourne IT','SWiM Communications','Expired','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(556,82,'domains.melbourne','Instra','SWiM Communications','Active','Spintel',NULL,NULL,'1469887200','N'),
	(557,34,'host.network','Melbourne IT','SWiM Communications','Active','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(558,62,'random.crap','Instra','SWiM Communications','Active','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(559,84,'sample.test.au','Melbourne IT','SWiM Communications','Expired','Spintel/Comcen',NULL,NULL,'1469887200','0'),
	(560,1,'joshua.com','Tester','Swim','Active','Spintel','150','M','1469023200','Y');

/*!40000 ALTER TABLE `tbl_domains` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_domains_attributes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_domains_attributes`;

CREATE TABLE `tbl_domains_attributes` (
  `domainattID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainID` int(11) DEFAULT NULL,
  `att_name` varchar(255) DEFAULT NULL,
  `att_version` varchar(255) DEFAULT NULL,
  `att_type` varchar(255) DEFAULT NULL,
  `att_details` text,
  PRIMARY KEY (`domainattID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_domains_attributes` WRITE;
/*!40000 ALTER TABLE `tbl_domains_attributes` DISABLE KEYS */;

INSERT INTO `tbl_domains_attributes` (`domainattID`, `domainID`, `att_name`, `att_version`, `att_type`, `att_details`)
VALUES
	(1,549,'Wordpress','4.3.2','Website Software change','<p>This is a custom install with a few added features</p>');

/*!40000 ALTER TABLE `tbl_domains_attributes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_domains_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_domains_notes`;

CREATE TABLE `tbl_domains_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_domains_notes` WRITE;
/*!40000 ALTER TABLE `tbl_domains_notes` DISABLE KEYS */;

INSERT INTO `tbl_domains_notes` (`noteID`, `domainID`, `staffID`, `note_title`, `note_content`, `note_type`, `note_date`)
VALUES
	(1,549,3,'This is a cheap client','<p>Don&#039;t invest to much time in this client or this domain</p>','General Message','1470268433');

/*!40000 ALTER TABLE `tbl_domains_notes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_domains_renewals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_domains_renewals`;

CREATE TABLE `tbl_domains_renewals` (
  `domainrenewID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainID` int(11) DEFAULT NULL,
  `renew_name` varchar(255) DEFAULT NULL,
  `renew_startdate` varchar(255) DEFAULT NULL,
  `renew_lastdate` varchar(255) DEFAULT NULL,
  `renew_rate` varchar(255) DEFAULT NULL,
  `renew_frequency` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domainrenewID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_domains_renewals` WRITE;
/*!40000 ALTER TABLE `tbl_domains_renewals` DISABLE KEYS */;

INSERT INTO `tbl_domains_renewals` (`domainrenewID`, `domainID`, `renew_name`, `renew_startdate`, `renew_lastdate`, `renew_rate`, `renew_frequency`)
VALUES
	(1,549,'SSL Certificate','1470060000','1501596000','300','Annually');

/*!40000 ALTER TABLE `tbl_domains_renewals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_global
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_global`;

CREATE TABLE `tbl_global` (
  `valueID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value_desc` varchar(255) DEFAULT NULL,
  `global_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`valueID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_global` WRITE;
/*!40000 ALTER TABLE `tbl_global` DISABLE KEYS */;

INSERT INTO `tbl_global` (`valueID`, `value_desc`, `global_value`)
VALUES
	(1,'Job Bag Number','7184');

/*!40000 ALTER TABLE `tbl_global` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects`;

CREATE TABLE `tbl_projects` (
  `projectID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `salesuserID` int(11) DEFAULT NULL,
  `job_bag` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_type` varchar(255) DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  `project_budget` varchar(255) NOT NULL DEFAULT '',
  `project_priority` varchar(255) DEFAULT NULL,
  `project_duedate` varchar(255) DEFAULT NULL,
  `project_followupdate` varchar(255) DEFAULT NULL,
  `project_createdbyID` varchar(255) DEFAULT NULL,
  `project_createdate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`projectID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects` WRITE;
/*!40000 ALTER TABLE `tbl_projects` DISABLE KEYS */;

INSERT INTO `tbl_projects` (`projectID`, `clientID`, `salesuserID`, `job_bag`, `project_name`, `project_type`, `project_status`, `project_budget`, `project_priority`, `project_duedate`, `project_followupdate`, `project_createdbyID`, `project_createdate`)
VALUES
	(26,483,3,'6763','SWiM CRM','Custom Build','Job in progress','999999999','High','1469887200','1466085600','3','1465524046'),
	(27,1,4,'7130','Nurse Database Compilation','Database','Job in progress','600.00','Medium','1466085600','','4','1465534928'),
	(28,455,4,'7128','Small Home Big Life Blog','Website Build','Job in progress','500.00','Medium','','','4','1465535422'),
	(29,455,4,'7127','Niche Project Management Website','Website Build','Completed','300.00','Medium','','','4','1465535480'),
	(30,455,4,'7126','Domain Registration','Domain Registration','Completed','200.00','Medium','1465308000','','4','1465535547'),
	(31,417,3,'6937','Form Redevelopment','Proeye Form Final Agreement & application to live site','Invoiced','900','High','','','9','1465869528'),
	(32,129,4,'7129A','SSL Certificate Renewal','SSL Certificate Renewal - 1 year','Invoiced','350','High','','','9','1465869606'),
	(33,287,3,'7131','Email Support','Email support/email client set up','Invoiced','450','Medium','1467036000','','9','1465875686'),
	(34,398,4,'7064','Prelim EDM','EDM Design','Invoiced','600','High','1467036000','','9','1465876121'),
	(35,38,3,'7132','Menu fix for mobile','Bug Fixes','Completed','???','High','','','3','1465947931'),
	(36,157,7,'7080','Quarterly Website Content Updates','Contact updates','Job in progress','','Medium','1472652000','','7','1465959853'),
	(37,375,3,'6616','Neon Effect ecom upgrade','Ecommerce Website upgrade','Job in progress','','Medium','1466085600','1465999200','7','1465960294'),
	(38,325,6,'7133','Website Restore & Cleanup','Support','Completed','150.00','Low','','','6','1465964199'),
	(39,162,3,'7135H','Hosting - stevealley.com.au','Hosting','Completed','','Medium','','','3','1466126140'),
	(40,162,3,'7134H','Hosting - dimarca.com.au','Hosting','Completed','','Medium','','','3','1466126202'),
	(41,333,9,'7006','eCommerce Upgrades','eCommerce Upgrades','Invoiced','1500','Medium','','','9','1466129093'),
	(42,338,9,'7114','Security & Maintenance Schedule','Security & Maintenance - June to December 2016','Invoiced','700','High','','','9','1466129476'),
	(43,445,9,'7034','Website Updates & Training','Research & planning, training in cs cart, multi image upload, troubleshooting connection issues & liason with 3rd parties','Invoiced','1125.00','High','','','9','1466129566'),
	(44,408,4,'7136','Table Covers - Cancelled (refer Job #7145)','Website Build','Completed','','Medium','','','4','1466141678'),
	(45,162,4,'7137','Cancer Specialist Website & Blog','Website Build','Job in progress','$3,600 + GST','Medium','','','4','1466149591'),
	(46,483,4,'7138','Layla Business Cards','Design','Job in progress','','High','','','4','1466150747'),
	(47,482,4,'7139','Women In Australian Rules Football Save the Date EDM','Website Build','Invoiced','','High','','','4','1466152615'),
	(48,482,4,'7140','Women In Australian Rules Football Website Slider','Design','Invoiced','','High','','','4','1466153486'),
	(49,482,4,'7141','SAMRF Website - major update','Website Build','Job in progress','','Medium','','','4','1466153829'),
	(50,57,3,'7142','New bug requests','Website Alt','Job in progress','','Medium','','','3','1466399823'),
	(51,536,4,'7143','Website Hosting: www.artinpublicplaces.com.au','Hosting','Job in progress','$30 + GST/month','Medium','','','4','1466404859'),
	(52,536,4,'7144','Website Hosting: www.sonsofwilliamstown.com.au','Website Build','Job in progress','$30 + GST/month','Medium','','','4','1466405033'),
	(53,411,4,'7002','Poly 4x4 Security and maintenance','Website Build','Prospect','','Medium','1466344800','1472652000','7','1466405712'),
	(54,436,3,'7146','RTI Impairments Upgrade','Website Alt','Invoiced','2250','High','1467208800','','3','1466550784'),
	(55,459,4,'7101','SMSF Benchmarks Landing Page','Website Build','Invoiced','','Medium','','','8','1466661683'),
	(56,451,4,'7108','T90 Amendment','Website Build','Invoiced','150.00','Medium','','','9','1466721717'),
	(57,1,7,'6847','Security Update','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466728647'),
	(58,190,7,'6838','FBA - Security Update','Security and Maintenance','Job in progress','','Low','1466344800','1464703200','7','1466728749'),
	(59,129,9,'6842','CMPA - Security Updates','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466729050'),
	(60,200,7,'6845','Fleet Trades - Security Updates','Security and Maintenance','Completed','','Low','1466344800','1464703200','7','1466729173'),
	(61,531,7,'6844','WCD - Security','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466729375'),
	(62,332,7,'6837','Maxitrans - Security Update','Security and Maintenance','Job in progress','','Low','1464703200','1474293600','7','1466729456'),
	(63,482,7,'6840','SAMRF - Security Update','Security and Maintenance','Completed','','Low','1466344800','1472652000','7','1466729584'),
	(64,517,7,'6853','Viking Plastics - Security update','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466729975'),
	(65,183,7,'6839','Eureka Living - Security Update','Security and Maintenance','Job in progress','','Low','1466344800','','7','1466730082'),
	(66,459,7,'6841','SMSF - Securiy update','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466730173'),
	(67,230,7,'6843','G100 - Security Update','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466730422'),
	(68,389,7,'6794','Options Consulting Group  - Security and Maintenance','Security and Maintenance','Job in progress','','Low','1466344800','1474293600','7','1466730589'),
	(69,389,7,'6846','Options Consulting Group  - Security and Maintenance','Security and Maintenance','Job in progress','','Low','1466344800','1472652000','7','1466731569'),
	(70,408,7,'7103','Physio Health - Security update','Security and Maintenance','Job in progress','','Low','1466344800','1474293600','7','1466733233'),
	(71,537,4,'7147','Website Hosting','Hosting','Invoiced','','High','','','4','1466744253'),
	(72,537,4,'7148','Security Updates','Security and Maintenance','Invoiced','$100/month','Medium','','','4','1466744414'),
	(73,537,4,'7149','Security Updates - Work Bag','Security and Maintenance','Job in progress','$100/month','Medium','','1473861600','4','1466744491'),
	(74,537,4,'7150','Online Works - Deposit','Website Build','Invoiced','15,855.00','Medium','','','4','1466744822'),
	(75,537,4,'7151','Online Works - second instalment','Website Build','Invoiced','','High','','','4','1466744926'),
	(76,537,4,'7152','Online Works - third instalment','Website Build','Invoiced','','High','','','4','1466744972'),
	(77,537,4,'7153','Website Build & Training - Work Bag','Website Build','Job in progress','','Medium','','','4','1466745231'),
	(78,537,4,'7154','Social Media Work Bag: Linkedin + Youtube','Social Media Works','Job in progress','','Medium','','','4','1466745389'),
	(79,537,4,'7155','Video Filming + Editing - work bag','Social Media Works','Job in progress','1350.00','Medium','','','4','1466745553'),
	(80,290,9,'7083','Barangaroo 23/5 Editing','Social Media Works','Invoiced','4950.00','Medium','','','9','1466746071'),
	(81,525,7,'6898','Whole 9 Yards - Security and Maintenance','Security and Maintenance','Job in progress','','Low','1464703200','1472652000','7','1466748151'),
	(82,84,4,'7156','CS-Cart License extension','Website Alt','Invoiced','','Medium','','','4','1467011894'),
	(83,191,4,'7157','Malware Removal','Security and Maintenance','Invoiced','$450','Medium','','','4','1467084084'),
	(84,408,10,'7158','Bike bag corporate embroidery','Design','Job in progress','1000','Medium','1469368800','1468159200','4','1467094697'),
	(86,411,6,'7000','Website Redesign and Development','Website Build','Job in progress','','Medium','','','6','1467095841'),
	(87,84,6,'7095','Adore Tanning Website Redesign','Website Build','Job in progress','7970','High','','','6','1467095966'),
	(88,16,6,'6799','Social Media Works','Social Media Works','Job in progress','','Medium','','','6','1467096012'),
	(89,16,6,'6798','Website Redesign and Development','Website Build','Job in progress','14990','Medium','','','6','1467096055'),
	(90,405,6,'6977','Website Redesign and Development','Website Build','Job in progress','13600','Medium','','','6','1467096634'),
	(91,405,6,'6977','Instagram Integration','Social Media Works','Completed','600','Medium','','','6','1467096679'),
	(92,405,6,'6979','YouTube Account Customisation','Social Media Works','Job in progress','480','Medium','','','6','1467096725'),
	(93,197,6,'7009','Website Image Search','Website Alt','Job in progress','','Medium','','','6','1467096771'),
	(94,517,6,'6920','Hoka Website Redesign','Website Build','Job in progress','','Medium','','','6','1467096823'),
	(95,398,6,'6896','EDM Design & Build','EDM Design','Job in progress','','Medium','','','6','1467096877'),
	(96,165,4,'7159','Domain Transfer of Ownership: www.designerschoice.com.au','Domain Registration','Invoiced','','High','','','4','1467257986'),
	(97,538,9,'6364','New Website','Website Build','Invoiced','400','Medium','','','9','1467259327'),
	(98,329,9,'6180','MBAV Blog Updates - Work to date','EDM Update','Invoiced','975','Medium','','','9','1467259368'),
	(99,375,9,'7012','BNZ Works','Website Alt','Prospect','525','Medium','','','9','1467259427'),
	(100,422,9,'6820','Malware Cleanup','Security and Maintenance','Prospect','300','Medium','','','9','1467259465'),
	(101,451,9,'6991','Changes as advised 23/3/2016','Security and Maintenance','Prospect','120','Medium','','','9','1467259506'),
	(102,408,10,'7160','PhysioHealth July works','Social Media Works','Job in progress','2000','Medium','1469887200','1469887200','10','1467261350'),
	(103,408,10,'7161','PhysioHealth Aug works','Social Media Works','Job in progress','2000','Medium','1469973600','1472565600','10','1467261461'),
	(104,408,10,'7162','PhysioHealth Sept works','Social Media Works','Job in progress','2000','Medium','1472652000','1475157600','10','1467261676'),
	(105,408,10,'7163','PhysioHealth Oct works','Social Media Works','Job in progress','2000','Medium','1475244000','1477832400','10','1467261740'),
	(106,408,10,'7164','PhysioHealth Nov works','Social Media Works','Job in progress','2000','Medium','1477918800','1480424400','10','1467261787'),
	(107,408,10,'7165','PhysioHealth Dec works','Social Media Works','Job in progress','2000','Medium','1480510800','1483102800','10','1467261854'),
	(108,408,9,'7109','June Marketing & Comms Support','Social Media Works','Invoiced','2000','Medium','','','9','1467264015'),
	(109,408,9,'7103A','Website core & plugin upgrades','Website Build','Invoiced','450.00','Medium','','','9','1467264124'),
	(110,333,9,'6658M','June Marketing & Comms Support','Social Media Works','Invoiced','1250.00','Medium','','','9','1467264885'),
	(111,435,9,'6637','June 2016 EDM & Social Support','Social Media Works','Invoiced','500.00','Medium','','','9','1467264956'),
	(112,408,10,'7166','eCom Store commencement fee','Website Build','Invoiced','3262.50','Medium','1483102800','1469887200','10','1467265089'),
	(113,408,10,'7167','eCom Store second installment','Website Build','Invoiced','3262.50','Medium','1483102800','1469887200','10','1467265202'),
	(114,408,10,'7168','eCom Store third installment','Website Build','Invoiced','3262.50','Medium','1483102800','1469887200','10','1467265302'),
	(115,408,10,'7169','eCom Store fourth installment','Website Build','Invoiced','3262.50','Medium','1483102800','1469887200','10','1467265373'),
	(116,408,9,'7145','2 x Physio Health Branded Table Covers','Website Build','Invoiced','','Medium','','','9','1467265610'),
	(117,124,9,'7040','June 2016 Digital Communications','Social Media Works','Invoiced','','Medium','','','9','1467265676'),
	(118,408,10,'7170','eCom Store work bag','Website Build','Job in progress','2000','Medium','1483102800','1469887200','10','1467265695'),
	(119,419,9,'6989','Navigation Update','Security and Maintenance','Invoiced','','Medium','','','9','1467265711'),
	(120,408,10,'7171','eCom Store development and line environment July - Dec 2016','Hosting','Invoiced','300','Medium','1483102800','','10','1467265886'),
	(121,408,10,'7172','eCom Store SSL certificate','SSL','Invoiced','300','Medium','1498744800','','10','1467265970'),
	(122,482,8,'7173','Women In Football Breakfast Invite EDM','EDM Design','Job in progress','','Medium','1467727200','','8','1467336134'),
	(123,162,6,'7177','SOW - Website Hosting','Hosting','Job in progress','','Low','','','6','1467337251'),
	(124,162,6,'7176','Airtopia - Website Hosting','Hosting','Job in progress','','Low','','','6','1467337274'),
	(125,162,6,'7175','PSH Aus - Website Hosting','Hosting','Job in progress','','Low','','','6','1467337294'),
	(126,162,6,'7174','Trent - Website Hosting','Hosting','Job in progress','','Low','','','6','1467337311'),
	(127,1,3,'7125','Tester','Website Build','Prospect','3000','Medium','','','3','1467867355'),
	(128,1,3,'7125','Tester','Website Build','Prospect','3000','Medium','','','3','1467867384'),
	(129,1,3,'7125','tester','Website Build','Prospect','3649816','Medium','','','3','1467868029'),
	(130,1,3,'7125','tester','Website Build','Prospect','9876','Medium','1467813600','1467813600','3','1467869928'),
	(131,1,3,'7125','sample','Website Build','Prospect','987654','Medium','1467813600','1467813600','3','1467870135'),
	(132,1,3,'7125','tester','Website Build','Prospect','9753','Medium','','','3','1467870268'),
	(133,1,3,'7125','tester','Website Build','Prospect','9753','Medium','','','3','1467870350'),
	(134,1,3,'7125','tester','Website Build','Prospect','9753','Medium','','','3','1467870442'),
	(135,1,3,'7125','tester','Website Build','Prospect','9753','Medium','','','3','1467870475'),
	(136,1,3,'7125','tester','Website Build','Prospect','9753','Medium','','','3','1467870512'),
	(137,1,3,'7125','tester','Website Build','Prospect','9753','Medium','','','3','1467870540'),
	(138,1,3,'7125','tester2','Website Build','Prospect','','Medium','','','3','1467870652'),
	(139,1,3,'7125','tester','Website Build','Prospect','97632','Medium','','','3','1467870707'),
	(140,1,3,'7129','tester','Website Build','Prospect','9876','Medium','1469541600','1467813600','','1467871494'),
	(141,1,3,'7178','tester','Website Build','Prospect','9876','Medium','1469541600','1467813600','3','1467871594'),
	(142,1,3,'7179','Sample','Website Build','Prospect','36498','Medium','1467813600','1467813600','3','1467871915'),
	(143,1,3,'7180','tester','Website Build','Prospect','97852','Medium','1467813600','1467813600','3','1467872012'),
	(144,1,3,'7184','tester','Website Build','Prospect','30000','Medium','1467813600','1467813600','3','1467872203');

/*!40000 ALTER TABLE `tbl_projects` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_assign
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_assign`;

CREATE TABLE `tbl_projects_assign` (
  `assignID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`assignID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects_assign` WRITE;
/*!40000 ALTER TABLE `tbl_projects_assign` DISABLE KEYS */;

INSERT INTO `tbl_projects_assign` (`assignID`, `projectID`, `userID`)
VALUES
	(42,26,8),
	(43,26,6),
	(44,26,4),
	(45,26,3),
	(46,26,9),
	(47,26,5),
	(48,26,7),
	(49,27,3),
	(53,31,3),
	(54,31,9),
	(55,32,4),
	(56,32,9),
	(57,33,3),
	(58,33,9),
	(59,33,7),
	(60,34,4),
	(61,34,9),
	(62,30,3),
	(63,35,4),
	(64,35,3),
	(65,36,7),
	(66,37,7),
	(69,38,4),
	(70,38,7),
	(72,39,4),
	(73,39,3),
	(74,39,9),
	(75,40,4),
	(76,40,3),
	(77,40,9),
	(78,41,4),
	(79,41,9),
	(80,42,4),
	(81,42,9),
	(82,43,4),
	(83,43,9),
	(85,45,6),
	(86,46,8),
	(90,50,4),
	(91,50,3),
	(92,51,7),
	(93,52,7),
	(104,28,7),
	(108,29,4),
	(110,44,9),
	(111,56,3),
	(112,56,9),
	(113,55,4),
	(114,53,7),
	(115,57,7),
	(116,58,7),
	(117,59,7),
	(118,60,7),
	(119,61,7),
	(120,62,7),
	(121,63,7),
	(123,64,7),
	(124,65,7),
	(125,66,7),
	(126,67,7),
	(127,68,7),
	(128,69,7),
	(129,70,7),
	(130,47,8),
	(131,47,6),
	(135,73,7),
	(136,72,9),
	(137,71,3),
	(138,78,8),
	(140,79,3),
	(141,80,9),
	(142,81,7),
	(145,84,10),
	(146,77,6),
	(147,49,6),
	(148,49,4),
	(149,86,6),
	(157,89,6),
	(158,89,4),
	(159,89,5),
	(161,88,6),
	(162,88,4),
	(163,87,6),
	(164,87,4),
	(166,90,6),
	(167,90,4),
	(168,90,4),
	(169,92,6),
	(170,92,4),
	(171,93,6),
	(172,93,4),
	(173,94,6),
	(174,94,4),
	(175,95,8),
	(176,95,6),
	(177,95,4),
	(180,54,4),
	(181,54,3),
	(182,83,3),
	(183,83,7),
	(184,82,4),
	(185,48,8),
	(186,97,4),
	(187,97,9),
	(188,98,4),
	(189,98,9),
	(190,99,3),
	(191,99,9),
	(192,100,3),
	(193,100,9),
	(194,101,3),
	(195,101,9),
	(196,102,10),
	(197,103,10),
	(198,104,10),
	(202,106,10),
	(203,107,10),
	(204,105,10),
	(205,108,9),
	(206,109,9),
	(207,110,9),
	(208,111,9),
	(214,116,9),
	(215,118,10),
	(218,115,10),
	(219,112,10),
	(220,113,10),
	(221,114,10),
	(222,120,10),
	(223,121,10),
	(224,122,8),
	(225,123,4),
	(226,124,4),
	(227,125,4),
	(228,126,4),
	(229,127,4),
	(230,127,3),
	(231,127,6),
	(232,127,4),
	(233,127,4),
	(234,127,3),
	(235,127,4),
	(236,127,3),
	(237,0,6),
	(238,0,4),
	(239,0,6),
	(240,0,4),
	(241,0,6),
	(242,0,4),
	(243,127,6),
	(244,127,4),
	(245,0,6),
	(246,0,4),
	(247,1,6),
	(248,1,4),
	(249,0,6),
	(250,0,4),
	(251,7,6),
	(252,7,4),
	(253,7,8),
	(254,7,4),
	(255,140,6),
	(256,140,4),
	(257,141,6),
	(258,141,4),
	(259,142,6),
	(260,142,4),
	(261,143,6),
	(262,143,4),
	(263,144,8),
	(264,144,6),
	(265,144,4);

/*!40000 ALTER TABLE `tbl_projects_assign` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_contacts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_contacts`;

CREATE TABLE `tbl_projects_contacts` (
  `projectcontactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `contactID` int(11) DEFAULT NULL,
  PRIMARY KEY (`projectcontactID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects_contacts` WRITE;
/*!40000 ALTER TABLE `tbl_projects_contacts` DISABLE KEYS */;

INSERT INTO `tbl_projects_contacts` (`projectcontactID`, `projectID`, `contactID`)
VALUES
	(1,144,14),
	(2,144,558);

/*!40000 ALTER TABLE `tbl_projects_contacts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_expences
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_expences`;

CREATE TABLE `tbl_projects_expences` (
  `expenceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `expence_name` varchar(255) DEFAULT NULL,
  `expence_value` varchar(255) DEFAULT NULL,
  `expence_markup` varchar(255) DEFAULT NULL,
  `expence_clientcharged` varchar(255) DEFAULT NULL,
  `expence_dateinvoicedue` varchar(255) DEFAULT NULL,
  `expence_supppaid` varchar(255) DEFAULT NULL,
  `expence_condition` varchar(255) DEFAULT NULL,
  `expence_jobbag` varchar(255) DEFAULT NULL,
  `expence_notes` text,
  `expence_auth` varchar(2) DEFAULT NULL,
  `expence_authby` int(11) DEFAULT NULL,
  PRIMARY KEY (`expenceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects_expences` WRITE;
/*!40000 ALTER TABLE `tbl_projects_expences` DISABLE KEYS */;

INSERT INTO `tbl_projects_expences` (`expenceID`, `projectID`, `clientID`, `userID`, `expence_name`, `expence_value`, `expence_markup`, `expence_clientcharged`, `expence_dateinvoicedue`, `expence_supppaid`, `expence_condition`, `expence_jobbag`, `expence_notes`, `expence_auth`, `expence_authby`)
VALUES
	(6,64,517,7,'Test1','Test1','Test1','N','1470578400','1470578400','Y','Y','<p>Test</p>',NULL,NULL),
	(7,144,1,7,'Test Expense','Test incurred','test','Y','1470751200','1470751200','N','N','<p>Test</p>',NULL,NULL),
	(8,144,1,7,'Te 123','TEst 123','test 123','Y','1470751200','1470751200','N','N','<p>Test</p>',NULL,NULL);

/*!40000 ALTER TABLE `tbl_projects_expences` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_hours
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_hours`;

CREATE TABLE `tbl_projects_hours` (
  `hourID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `hour_title` varchar(255) DEFAULT NULL,
  `hour_content` text,
  `hour_amount` varchar(255) DEFAULT NULL,
  `hour_datedone` varchar(255) DEFAULT NULL,
  `hour_dateuploaded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hourID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects_hours` WRITE;
/*!40000 ALTER TABLE `tbl_projects_hours` DISABLE KEYS */;

INSERT INTO `tbl_projects_hours` (`hourID`, `projectID`, `staffID`, `hour_title`, `hour_content`, `hour_amount`, `hour_datedone`, `hour_dateuploaded`)
VALUES
	(1,30,3,'Domain Regestration','<p>Registration of 2 domains</p>','0.25','1465826400','1465890152'),
	(2,35,3,'Menu Fix','<p>Changes to stylesheet to make menu apear correctly on mobile</p>','0.5','1465912800','1465948109'),
	(3,46,8,'Create cards and send for approval','','0.25','1466344800','1466477239'),
	(4,46,8,'Minor edits','<p>Update contact info and create v1 and v2.</p>','0.25','1466344800','1466477299'),
	(5,47,8,'EDM Mock-up Designs','','6','1466344800','1466636853'),
	(6,47,8,'EDM Mock-up Design finalise','','3','1466431200','1466637020'),
	(7,47,8,'Shutterstock Purchase ','<p>Purchased 2 images from Shutterstock. $27 for 2 images. Updated designs with purchased images.</p>','','1466517600','1466637179'),
	(8,47,8,'Client Approval | Campaign Setup | Minor Edits','<p>Several emails and phone calls were had with Elda to confirm. Also minor edits to campaign copy. Campaign set up and schedule.</p>','1','1466517600','1466637797'),
	(9,47,6,'EDM Build','','0.5','1466517600','1466638042'),
	(10,54,3,'New user group for impairments','<p>Code set to include a new usergroup for the impairments as well as have page restrictions</p>','0.5','1466604000','1466645364'),
	(11,54,3,'Input area to save group of users to be notified of new impairments','','1.5','1466604000','1466645773'),
	(12,54,3,'Changes to emailsystem to include above','','2','1466604000','1466646637'),
	(13,55,8,'Landing Page Mock-up','<p>Mock-up includes page design and content collation and creation.</p>','5','1465308000','1466661776'),
	(14,55,8,'Landing Page Mock-up Edits','<p>Minor edits after feedback from client. Finalise design and approval.</p>','1','1465394400','1466661894'),
	(15,55,6,'Landing Page Build and Responsive Test','','3','1465826400','1466667143'),
	(16,54,3,'CRON job setup and programing','<p>Set up of the CRON job to run every hour to test if there are any outstanding items not yet ticked off</p>','4','1466949600','1466996596');

/*!40000 ALTER TABLE `tbl_projects_hours` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_notes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_notes`;

CREATE TABLE `tbl_projects_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects_notes` WRITE;
/*!40000 ALTER TABLE `tbl_projects_notes` DISABLE KEYS */;

INSERT INTO `tbl_projects_notes` (`noteID`, `projectID`, `staffID`, `note_title`, `note_content`, `note_type`, `note_date`)
VALUES
	(1,32,9,'Recurring Item','<p>Annual Recurring item</p>','','1465870358'),
	(2,30,3,'For future reference','<p>For future reference, these are 2 seperate recurring items and will require 2 seperate job bags</p>','Info','1465890007'),
	(3,52,7,'Waiting on Rhiannon','<p>Waiting for further information from Rhiannon&nbsp;</p>\r\n<p>Need to know what hosting server its going.</p>','','1466405545'),
	(4,28,6,'Handover to Rachelle','<p>I briefed Rachelle on the 23rd of June who&nbsp;will do the initial build and style.</p>','','1466667222');

/*!40000 ALTER TABLE `tbl_projects_notes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_tasks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_tasks`;

CREATE TABLE `tbl_projects_tasks` (
  `projecttaskID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `assuserID` int(11) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `task_content` text,
  `task_priority` varchar(255) DEFAULT NULL,
  `task_duedate` varchar(255) DEFAULT NULL,
  `task_createdbyID` varchar(255) DEFAULT NULL,
  `task_createdate` varchar(255) DEFAULT NULL,
  `task_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`projecttaskID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_projects_tasks` WRITE;
/*!40000 ALTER TABLE `tbl_projects_tasks` DISABLE KEYS */;

INSERT INTO `tbl_projects_tasks` (`projecttaskID`, `projectID`, `assuserID`, `task_name`, `task_content`, `task_priority`, `task_duedate`, `task_createdbyID`, `task_createdate`, `task_status`)
VALUES
	(1,30,3,'register 2 x domains','<p>Please register 2 x domains to small change designs: ABN 55 074 931 061</p>\r\n<p>Www.nicheprojectmanagement.com.au<br />Www.smallhomebiglife.com.au</p>\r\n<p>Use this to show Gen how we register domains</p>\r\n<p>&nbsp;</p>','High','','4','1465535706','C'),
	(2,30,3,'Review list and submit a quote','<p>Review list of items submitted in email and write up a quote.</p>\r\n<p>Items are in the notes section</p>','Medium','1466690400','3','1466400003','A'),
	(3,30,3,'Sample task','<p>This is a test to make sure this works</p>','High','','3','1469769399','A'),
	(4,30,3,'Tester','<p>This is a test</p>','High','1470232800','3','1470270153','A');

/*!40000 ALTER TABLE `tbl_projects_tasks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_projects_tasks_reply
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_projects_tasks_reply`;

CREATE TABLE `tbl_projects_tasks_reply` (
  `replyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conectorID` int(11) DEFAULT NULL,
  `reply_user` varchar(255) DEFAULT NULL,
  `reply_content` text,
  `reply_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`replyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_salesproposals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_salesproposals`;

CREATE TABLE `tbl_salesproposals` (
  `salesID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sales_title` varchar(255) DEFAULT NULL,
  `sales_companyname` varchar(255) DEFAULT NULL,
  `sales_name` varchar(255) DEFAULT NULL,
  `sales_add_1` varchar(255) DEFAULT NULL,
  `sales_add_2` varchar(255) DEFAULT NULL,
  `sales_add_city` varchar(255) DEFAULT NULL,
  `sales_add_state` varchar(255) DEFAULT NULL,
  `sales_add_country` varchar(255) DEFAULT NULL,
  `sales_add_postcode` varchar(5) DEFAULT NULL,
  `sales_phone` varchar(255) DEFAULT NULL,
  `sales_mobile` varchar(255) DEFAULT NULL,
  `sales_fax` varchar(255) DEFAULT NULL,
  `sales_email` varchar(255) DEFAULT NULL,
  `sales_budget` varchar(255) DEFAULT NULL,
  `sales_goingahead` varchar(255) DEFAULT NULL,
  `sales_aheadwith` varchar(255) DEFAULT NULL,
  `sales_startdate` varchar(255) DEFAULT NULL,
  `sales_duedate` varchar(255) DEFAULT NULL,
  `sales_followup` varchar(255) DEFAULT NULL,
  `sales_createddate` varchar(255) DEFAULT NULL,
  `sales_createdby` varchar(255) DEFAULT NULL,
  `sales_details` text,
  `sales_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`salesID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_salesproposals` WRITE;
/*!40000 ALTER TABLE `tbl_salesproposals` DISABLE KEYS */;

INSERT INTO `tbl_salesproposals` (`salesID`, `sales_title`, `sales_companyname`, `sales_name`, `sales_add_1`, `sales_add_2`, `sales_add_city`, `sales_add_state`, `sales_add_country`, `sales_add_postcode`, `sales_phone`, `sales_mobile`, `sales_fax`, `sales_email`, `sales_budget`, `sales_goingahead`, `sales_aheadwith`, `sales_startdate`, `sales_duedate`, `sales_followup`, `sales_createddate`, `sales_createdby`, `sales_details`, `sales_status`)
VALUES
	(1,'Sample','Company 2','John Smith','123 Fake Street',NULL,'Sample Town','VIC','Australia','3000','93268431','4064987315','93248137','sample@tester.com.au','3684227','33','67','1465826400','1465826400','1465826400','1465826400','3','This is a bunch to text just to see how this will apear',NULL),
	(7,'Title 2','Company name here','Jane Smith','123 Fake Street',NULL,'Sample Town','VIC','Australia','3000','93268431','4064987315','93248137','sample@tester.com.au','3684227','33','67','1465826400','1465826400','1465826400','1465826400','3','This is a bunch to text just to see how this will apear',NULL),
	(8,'Title 3','Dummy content','Tony Stark','123 Fake Street',NULL,'Sample Town','VIC','Australia','3000','93268431','4064987315','93248137','sample@tester.com.au','3684227','33','67','1465826400','1465826400','1465826400','1465826400','3','This is a bunch to text just to see how this will apear',NULL),
	(9,'Title 4','Sample','Frank Tank','123 Fake Street',NULL,'Sample Town','VIC','Australia','3000','93268431','4064987315','93248137','sample@tester.com.au','3684227','33','67','1465826400','1465826400','1465826400','1465826400','3','This is a bunch to text just to see how this will apear',NULL);

/*!40000 ALTER TABLE `tbl_salesproposals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_tickets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_tickets`;

CREATE TABLE `tbl_tickets` (
  `ticketID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_title` varchar(255) DEFAULT NULL,
  `ticket_domain` varchar(255) NOT NULL,
  `ticket_type` varchar(255) DEFAULT NULL,
  `ticket_status` varchar(255) DEFAULT NULL,
  `ticket_urgency` varchar(255) DEFAULT NULL,
  `assignID` int(255) NOT NULL,
  `ticket_info` text,
  `ticket_createdbyID` varchar(255) DEFAULT NULL,
  `ticket_createdate` varchar(255) DEFAULT NULL,
  `ticket_clientID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ticketID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_tickets` WRITE;
/*!40000 ALTER TABLE `tbl_tickets` DISABLE KEYS */;

INSERT INTO `tbl_tickets` (`ticketID`, `ticket_title`, `ticket_domain`, `ticket_type`, `ticket_status`, `ticket_urgency`, `assignID`, `ticket_info`, `ticket_createdbyID`, `ticket_createdate`, `ticket_clientID`)
VALUES
	(1,'Tester','554','Email Support','Open','Medium',3,'<p>This is a test</p>','3','1467874646',1),
	(2,'Testing','','Submitted from website','Open','Medium',3,'<strong>Details</strong><br/>Name: Joshua Curci<br/>Email: josh@swim.com.au<br/>Phone Number: 0402264959<br/><br/>Date Noticed: 2016-01-01<br/>Operating System: Mac<br/>Web URL: http://swim.com.au<br/>2nd Web URL: http://swim.net.au<br/>Details:<br/>In the previous chapter we created an empty table named \"MyGuests\" with five columns: \"id\", \"firstname\", \"lastname\", \"email\" and \"reg_date\". Now, let us fill the table with data<br/>','3','1468550551',NULL);

/*!40000 ALTER TABLE `tbl_tickets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_tickets_reply
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_tickets_reply`;

CREATE TABLE `tbl_tickets_reply` (
  `replyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conectorID` int(11) DEFAULT NULL,
  `reply_user` varchar(255) DEFAULT NULL,
  `reply_content` text,
  `reply_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`replyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_tickets_reply` WRITE;
/*!40000 ALTER TABLE `tbl_tickets_reply` DISABLE KEYS */;

INSERT INTO `tbl_tickets_reply` (`replyID`, `conectorID`, `reply_user`, `reply_content`, `reply_date`)
VALUES
	(1,1,'Joshua Curci','<p>Making sure this works</p>','1467875574');

/*!40000 ALTER TABLE `tbl_tickets_reply` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_users`;

CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_users` WRITE;
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;

INSERT INTO `tbl_users` (`userID`, `type_letter`, `username`, `password`, `name`, `email`, `usr_status`, `site_access`, `lastlogin`)
VALUES
	(3,'A','josh@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joshua Curci','josh@swim.com.au','A',NULL,'August 4, 2016, 3:41 pm'),
	(4,'A','joe@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Joe Manariti','joe@swim.com.au','A',NULL,'June 30, 2016, 5:50 pm'),
	(5,'A','mike@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Michael Brooks','mike@swim.com.au','A',NULL,NULL),
	(6,'A','jacob@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Jacob Leech','jacob@swim.com.au','A',NULL,'July 1, 2016, 11:39 am'),
	(7,'A','rachelle@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Rachelle Salvadora','rachelle@swim.com.au','A',NULL,'August 10, 2016, 11:33 am'),
	(8,'A','genevieve@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Genevieve Laidlaw','genevieve@swim.com.au','A',NULL,'July 1, 2016, 11:20 am'),
	(9,'A','accounts@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Jules Pullen','accounts@swim.com.au','A',NULL,'June 30, 2016, 3:45 pm'),
	(10,'A','layla@swim.com.au','a01a08880fb84a11f444af8541ba41c5','Layla Stanton','layla@swim.com.au','A',NULL,'June 30, 2016, 3:36 pm');

/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_usertypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_usertypes`;

CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_usertypes` WRITE;
/*!40000 ALTER TABLE `tbl_usertypes` DISABLE KEYS */;

INSERT INTO `tbl_usertypes` (`typeID`, `type_name`, `type_letter`)
VALUES
	(1,'Super Admin','A'),
	(2,'Admin','B'),
	(3,'Accounts','C'),
	(4,'Studio','S'),
	(5,'Account Manager','M'),
	(6,'Private','P'),
	(7,'Contractor','Z');

/*!40000 ALTER TABLE `tbl_usertypes` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
