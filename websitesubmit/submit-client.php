<?php
	// CRM Testing
	$servername = "localhost";
	$username = "crmswimc_swim";
	$password = "1uz6f2dSS70i";
	$dbname = "crmswimc_customcrm";/**/
	// Live Greenbook
	/*$servername = "localhost";
	$username = "greenboo_swim";
	$password = "HfmUWoO38wN1";
	$dbname = "greenboo_crm";/**/

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

$ticket_createdate = time();
$emailbuild = "";
foreach ($_POST as $key => $value) {
	$emailbuild .= $key." = ".$value."<br/>";
}

$sql = "INSERT INTO tbl_client (
			client_name,
			client_add_num,
			client_add_street,
			client_add_city,
			client_add_state,
			client_add_country,
			client_add_postcode,
			client_phone,
			client_abn,
			client_archive
		) VALUES (
			'".$_POST['company']."',
			'".$_POST['address-line1']."',
			'".$_POST['address-line2']."',
			'".$_POST['suburb']."',
			'".$_POST['state']."',
			'Australia',
			'".$_POST['postcode']."',
			'".$_POST['telephone-project-contact']."',
			'".$_POST['abn']."',
			'N'
		)";
$query1 = $conn->query($sql);
$newclientID = $conn->insert_id;

$sql2 = "INSERT INTO tbl_client_contact (
			clientID,
			contact_name,
			contact_lastname,
			contact_add_num,
			contact_add_street,
			contact_add_city,
			contact_add_state,
			contact_add_country,
			contact_add_postcode,
			contact_phone,
			contact_mobile,
			contact_email,
			contact_hotdrink,
			contact_jobtitle
		) VALUES (
			'".$newclientID."',
			'".$_POST['project-first-contact']."',
			'".$_POST['project-last-contact']."',
			'".$_POST['address-line1']."',
			'".$_POST['address-line2']."',
			'".$_POST['suburb']."',
			'".$_POST['state']."',
			'Australia',
			'".$_POST['postcode']."',
			'".$_POST['telephone-project-contact']."',
			'".$_POST['mobile-project-contact']."',
			'".$_POST['email-project-contact']."',
			'".$_POST['coffee']." - ".$_POST['somethingyoudprefer']."',
			'Project Contact'
		)";
$query2 = $conn->query($sql2);

$sql3 = "INSERT INTO tbl_client_contact (
			clientID,
			contact_name,
			contact_lastname,
			contact_add_num,
			contact_add_street,
			contact_add_city,
			contact_add_state,
			contact_add_country,
			contact_add_postcode,
			contact_phone,
			contact_mobile,
			contact_email,
			contact_jobtitle
		) VALUES (
			'".$newclientID."',
			'".$_POST['accounts-first-contact']."',
			'".$_POST['accounts-last-contact']."',
			'".$_POST['address-line1']."',
			'".$_POST['address-line2']."',
			'".$_POST['suburb']."',
			'".$_POST['state']."',
			'Australia',
			'".$_POST['postcode']."',
			'".$_POST['telephone-accounts-contact']."',
			'".$_POST['mobile-accounts-contact']."',
			'".$_POST['email-accounts-contact']."',
			'Accounts Contact'
		)";
$query3 = $conn->query($sql3);

$sql4 = "INSERT INTO tbl_client_notes (
			clientID,
			staffID,
			note_title,
			note_content,
			note_type,
			note_date
		) VALUES (
			'".$newclientID."',
			'3',
			'Submitted Contact Form',
			'".$emailbuild."',
			'Submitted Contact Form',
			'".$ticket_createdate."'
		)";
$query4 = $conn->query($sql4);


if ($query1 === TRUE && $query2 === TRUE && $query3 === TRUE && $query4 === TRUE) {
    $fp = fopen('file.txt', 'w');
	fwrite($fp, 'Client Added - '.date());
	fclose($fp);
} else {
	$error = "Error: " . $sql . "<br>" . $conn->error;
	$fp = fopen('file.txt', 'w');
	fwrite($fp, $error.' - '.date());
	fclose($fp);
}

$conn->close();

?>