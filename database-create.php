-- Create syntax for TABLE 'tbl_client'
CREATE TABLE `tbl_client` (
  `clientID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `client_name` varchar(255) DEFAULT NULL,
  `client_add_num` varchar(255) DEFAULT NULL,
  `client_add_street` varchar(255) DEFAULT NULL,
  `client_add_city` varchar(255) DEFAULT NULL,
  `client_add_state` varchar(255) DEFAULT NULL,
  `client_add_country` varchar(255) DEFAULT NULL,
  `client_add_postcode` varchar(5) DEFAULT NULL,
  `client_phone` varchar(255) DEFAULT NULL,
  `client_fax` varchar(255) DEFAULT NULL,
  `client_acn` varchar(255) DEFAULT NULL,
  `client_abn` varchar(255) DEFAULT NULL,
  `client_traiding_terms` varchar(255) DEFAULT NULL,
  `client_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`clientID`)
) ENGINE=InnoDB AUTO_INCREMENT=535 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_client_contact'
CREATE TABLE `tbl_client_contact` (
  `contactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `contact_add_num` varchar(255) DEFAULT NULL,
  `contact_add_street` varchar(255) DEFAULT NULL,
  `contact_add_city` varchar(255) DEFAULT NULL,
  `contact_add_state` varchar(255) DEFAULT NULL,
  `contact_add_country` varchar(255) DEFAULT NULL,
  `contact_add_postcode` varchar(5) DEFAULT NULL,
  `contact_phone` varchar(255) DEFAULT NULL,
  `contact_mobile` varchar(255) DEFAULT NULL,
  `contact_fax` varchar(255) DEFAULT NULL,
  `contact_email` varchar(255) DEFAULT NULL,
  `contact_hotdrink` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contactID`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_client_notes'
CREATE TABLE `tbl_client_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects'
CREATE TABLE `tbl_projects` (
  `projectID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `salesuserID` int(11) DEFAULT NULL,
  `job_bag` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `project_type` varchar(255) DEFAULT NULL,
  `project_status` varchar(255) DEFAULT NULL,
  `project_budget` varchar(255) DEFAULT NULL,
  `project_priority` varchar(255) DEFAULT NULL,
  `project_duedate` varchar(255) DEFAULT NULL,
  `project_followupdate` varchar(255) DEFAULT NULL,
  `project_createdbyID` varchar(255) DEFAULT NULL,
  `project_createdate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`projectID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects_assign'
CREATE TABLE `tbl_projects_assign` (
  `assignID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`assignID`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects_expences'
CREATE TABLE `tbl_projects_expences` (
  `expenceID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  `expence_name` varchar(255) DEFAULT NULL,
  `expence_value` varchar(255) DEFAULT NULL,
  `expence_markup` varchar(255) DEFAULT NULL,
  `expence_clientcharged` varchar(255) DEFAULT NULL,
  `expence_dateinvoicedue` varchar(255) DEFAULT NULL,
  `expence_supppaid` varchar(255) DEFAULT NULL,
  `expence_condition` varchar(255) DEFAULT NULL,
  `expence_jobbag` varchar(255) DEFAULT NULL,
  `expence_notes` varchar(255) DEFAULT NULL,
  `expence_auth` varchar(2) DEFAULT NULL,
  `expence_authby` int(11) DEFAULT NULL,
  PRIMARY KEY (`expenceID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects_hours'
CREATE TABLE `tbl_projects_hours` (
  `hourID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `hour_title` varchar(255) DEFAULT NULL,
  `hour_content` text,
  `hour_amount` varchar(255) DEFAULT NULL,
  `hour_datedone` varchar(255) DEFAULT NULL,
  `hour_dateuploaded` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`hourID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects_notes'
CREATE TABLE `tbl_projects_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects_tasks'
CREATE TABLE `tbl_projects_tasks` (
  `projecttaskID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `assuserID` int(11) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `task_content` text,
  `task_priority` varchar(255) DEFAULT NULL,
  `task_duedate` varchar(255) DEFAULT NULL,
  `task_createdbyID` varchar(255) DEFAULT NULL,
  `task_createdate` varchar(255) DEFAULT NULL,
  `task_status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`projecttaskID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_projects_tasks_reply'
CREATE TABLE `tbl_projects_tasks_reply` (
  `replyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conectorID` int(11) DEFAULT NULL,
  `reply_user` varchar(255) DEFAULT NULL,
  `reply_content` text,
  `reply_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`replyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_tickets'
CREATE TABLE `tbl_tickets` (
  `ticketID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `salesuserID` int(11) DEFAULT NULL,
  `job_bag` varchar(255) DEFAULT NULL,
  `ticket_name` varchar(255) DEFAULT NULL,
  `ticket_type` varchar(255) DEFAULT NULL,
  `ticket_status` varchar(255) DEFAULT NULL,
  `project_budget` varchar(255) DEFAULT NULL,
  `ticket_urgency` varchar(255) DEFAULT NULL,
  `project_duedate` varchar(255) DEFAULT NULL,
  `project_followupdate` varchar(255) DEFAULT NULL,
  `project_createdbyID` varchar(255) DEFAULT NULL,
  `ticket_createdate` varchar(255) DEFAULT NULL,
  `assignID` int(255) NOT NULL,
  `ticket_domain` char(255) NOT NULL,
  PRIMARY KEY (`ticketID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_tickets_assign'
CREATE TABLE `tbl_tickets_assign` (
  `assignID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticketID` int(11) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`assignID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_users'
CREATE TABLE `tbl_users` (
  `userID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_letter` varchar(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `usr_status` varchar(255) DEFAULT NULL,
  `site_access` varchar(255) DEFAULT NULL,
  `lastlogin` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_usertypes'
CREATE TABLE `tbl_usertypes` (
  `typeID` int(20) unsigned NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) DEFAULT NULL,
  `type_letter` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`typeID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_domains'
CREATE TABLE `tbl_domains` (
  `domainID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `clientID` int(11) DEFAULT NULL,
  `domain_name` varchar(255) DEFAULT NULL,
  `domain_registrar` varchar(255) DEFAULT NULL,
  `domain_registrant` varchar(255) DEFAULT NULL,
  `domain_status` varchar(255) DEFAULT NULL,
  `domain_webhost` int(11) DEFAULT NULL,
  `domain_renewaldate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domainID`)
) ENGINE=InnoDB AUTO_INCREMENT=548 DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_domains_attributes'
CREATE TABLE `tbl_domains_attributes` (
  `domainattID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainID` int(11) DEFAULT NULL,
  `att_name` varchar(255) DEFAULT NULL,
  `att_version` varchar(255) DEFAULT NULL,
  `att_type` varchar(255) DEFAULT NULL,
  `att_details` TEXT DEFAULT NULL,
  PRIMARY KEY (`domainattID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_domains_renewals'
CREATE TABLE `tbl_domains_renewals` (
  `domainrenewID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainID` int(11) DEFAULT NULL,
  `renew_name` varchar(255) DEFAULT NULL,
  `renew_startdate` varchar(255) DEFAULT NULL,
  `renew_lastdate` varchar(255) DEFAULT NULL,
  `renew_rate` varchar(255) DEFAULT NULL,
  `renew_frequency` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`domainrenewID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_tickets` (
  `ticketID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ticket_title` varchar(255) DEFAULT NULL,
  `ticket_domain` varchar(255) NOT NULL,
  `ticket_type` varchar(255) DEFAULT NULL,
  `ticket_status` varchar(255) DEFAULT NULL,
  `ticket_urgency` varchar(255) DEFAULT NULL,
  `assignID` int(255) NOT NULL,
  `ticket_info` TEXT DEFAULT NULL,
  `ticket_createdbyID` varchar(255) DEFAULT NULL,
  `ticket_createdate` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ticketID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_tickets_reply` (
  `replyID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `conectorID` int(11) DEFAULT NULL,
  `reply_user` varchar(255) DEFAULT NULL,
  `reply_content` text,
  `reply_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`replyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_global` (
  `valueID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value_desc` varchar(255) DEFAULT NULL,
  `global_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`valueID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE tbl_domains ADD domain_webhost_rate varchar(255);
ALTER TABLE tbl_domains ADD domain_webhost_rate_frequency varchar(255);
ALTER TABLE tbl_client ADD client_archive varchar(5);
ALTER TABLE tbl_client ADD client_resoled int(11);

CREATE TABLE `tbl_projects_contacts` (
  `projectcontactID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `projectID` int(11) DEFAULT NULL,
  `contactID` int(11) DEFAULT NULL,
  PRIMARY KEY (`projectcontactID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `tbl_salesproposals` (
  `salesID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sales_title` varchar(255) DEFAULT NULL,
  `sales_companyname` varchar(255) DEFAULT NULL,
  `sales_name` varchar(255) DEFAULT NULL,
  `sales_add_1` varchar(255) DEFAULT NULL,
  `sales_add_2` varchar(255) DEFAULT NULL,
  `sales_add_city` varchar(255) DEFAULT NULL,
  `sales_add_state` varchar(255) DEFAULT NULL,
  `sales_add_country` varchar(255) DEFAULT NULL,
  `sales_add_postcode` varchar(5) DEFAULT NULL,
  `sales_phone` varchar(255) DEFAULT NULL,
  `sales_mobile` varchar(255) DEFAULT NULL,
  `sales_fax` varchar(255) DEFAULT NULL,
  `sales_email` varchar(255) DEFAULT NULL,
  `sales_budget` varchar(255) DEFAULT NULL,
  `sales_goingahead` varchar(255) DEFAULT NULL,
  `sales_aheadwith` varchar(255) DEFAULT NULL,
  `sales_startdate` varchar(255) DEFAULT NULL,
  `sales_duedate` varchar(255) DEFAULT NULL,
  `sales_createddate` varchar(255) DEFAULT NULL,
  `sales_createdby` varchar(255) DEFAULT NULL,
  `sales_details` TEXT DEFAULT NULL,
  PRIMARY KEY (`salesID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE tbl_salesproposals ADD sales_status varchar(255);

-- Create syntax for TABLE 'tbl_salesproposals_notes'
CREATE TABLE `tbl_salesproposals_notes` (
  `noteID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `salesID` int(11) DEFAULT NULL,
  `staffID` int(11) DEFAULT NULL,
  `note_title` varchar(255) DEFAULT NULL,
  `note_content` text,
  `note_type` varchar(255) DEFAULT NULL,
  `note_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`noteID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE tbl_client_contact ADD contact_jobtitle varchar(255);
ALTER TABLE tbl_users ADD mobile varchar(255);

CREATE TABLE `tbl_client_work` (
  `workID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `work_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`workID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE tbl_projects ADD project_workbag varchar(255);
ALTER TABLE tbl_client ADD client_work varchar(255);

-- Create syntax for TABLE 'tbl_recuring'
CREATE TABLE `tbl_recuring` (
  `recID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `domainID` int(11) DEFAULT NULL,
  `clientID` int(11) DEFAULT NULL,
  `rec_name` varchar(255) DEFAULT NULL,
  `rec_startdate` varchar(255) DEFAULT NULL,
  `rec_lastdate` varchar(255) DEFAULT NULL,
  `rec_rate` varchar(255) DEFAULT NULL,
  `rec_frequency` varchar(255) DEFAULT NULL,
  `rec_notify` varchar(255) DEFAULT NULL,
  `rec_details` TEXT DEFAULT NULL,  
  PRIMARY KEY (`recID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Create syntax for TABLE 'tbl_recuring_item'
CREATE TABLE `tbl_recuring_item` (
  `recitemID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `recID` int(11) DEFAULT NULL,
  `recitem_datecreated` varchar(255) DEFAULT NULL,
  `recitem_dateactioned` varchar(255) DEFAULT NULL,
  `recitem_datedue` varchar(255) DEFAULT NULL,
  `projectID` int(11) DEFAULT NULL,
  `recitem_completed` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`recitemID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE tbl_recuring ADD rec_nextrenew varchar(255);

ALTER TABLE tbl_salesproposals ADD sales_clientID varchar(255);
ALTER TABLE tbl_salesproposals ADD sales_contactID varchar(255);
ALTER TABLE tbl_salesproposals ADD sales_new varchar(255);
ALTER TABLE tbl_salesproposals ADD sales_lastname varchar(255);

-- 4/10/2016 Changes

CREATE TABLE `tbl_salesproposals_conected` (
  `conID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `salesID` int(11) DEFAULT NULL,
  `projectID` int(11) DEFAULT NULL,
  PRIMARY KEY (`conID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE tbl_projects ADD project_reserved varchar(2) DEFAULT 'N';
