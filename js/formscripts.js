$(document).ready(function() {
    $("input[name$='recuring']").click(function() {
        var test = $(this).val();
        if (test == 'Y') {
          $("#recuringinfo").show();
        } else {
          $("#recuringinfo").hide();
        };        
    });
});

$(document).ready(function() {
    $("input[name$='existingcustomer']").click(function() {
        var test = $(this).val();
        if (test == 'Y') {
          $("#newcustomer").show();
          $("#customerexists").hide();
        } else {
          $("#newcustomer").hide();
          $("#customerexists").show();
        };        
    });
});

function getXMLHTTP() { //fuction to return the xml http object
	var xmlhttp=false;	
	try{
		xmlhttp=new XMLHttpRequest();
	}
	catch(e)	{		
		try{			
			xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e){
				try{
				xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
				}
				catch(e1){
					xmlhttp=false;
				}
			}
		}
		 	
		return xmlhttp;
	}
	
	
	
	function getCity(strURL) {		
		
		var req = getXMLHTTP();
		
		if (req) {
			
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {						
						document.getElementById('clientcontact').innerHTML=req.responseText;						
					} else {
						alert("There was a problem while using XMLHTTP:\n" + req.statusText);
					}
				}				
			}			
			req.open("GET", strURL, true);
			req.send(null);
		}
				
	}